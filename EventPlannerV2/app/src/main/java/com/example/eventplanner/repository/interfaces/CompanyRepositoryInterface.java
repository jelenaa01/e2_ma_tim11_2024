package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.Task;

import java.util.List;

public interface CompanyRepositoryInterface {
    void addCompany(Company company);
    Task<Company> getCompanyByOwnerEmail(String ownerEmail);
    Task<List<User>> getCompanyEmployees(String ownerId);
    void addEmployeeToCompany(String ownerEmail, String employeeEmail);
    Task<Company> getCompanyByName(String companyName);
    Task<User> getVendorByCompanyName(String companyName);
    Task<Void> updateCompany(Company company);
}
