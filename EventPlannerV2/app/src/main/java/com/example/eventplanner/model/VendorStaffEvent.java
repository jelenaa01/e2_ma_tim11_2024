package com.example.eventplanner.model;

import java.io.Serializable;
import java.util.Date;

public class VendorStaffEvent implements Serializable{
    public enum Type {
        RESERVED,
        OCCUPIED
    }
    private String id;
    private String eventName;
    private Date date;
    private String startTime;
    private String endTime;
    private Type eventType;
    private String vendorStaffEmail;

    public VendorStaffEvent() {}

    public VendorStaffEvent(String id, String eventName, Date date, String startTime, String endTime, Type eventType, String vendorStaffEmail) {
        this.id = id;
        this.eventName = eventName;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.eventType = eventType;
        this.vendorStaffEmail =vendorStaffEmail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Type getEventType() {
        return eventType;
    }

    public void setEventType(Type eventType) {
        this.eventType = eventType;
    }

    public String getVendorStaffEmail() {
        return vendorStaffEmail;
    }

    public void setVendorStaffEmail(String vendorStaffEmail) {
        this.vendorStaffEmail = vendorStaffEmail;
    }

    @Override
    public String toString() {
        return "VendorStaffEvent{" +
                "id='" + id + '\'' +
                ", eventName='" + eventName + '\'' +
                ", date=" + date +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", eventType=" + eventType +
                ", vendorStaffEmail='" + vendorStaffEmail + '\'' +
                '}';
    }
}
