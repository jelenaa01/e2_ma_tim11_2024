package com.example.eventplanner.fragments.reports;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.SearchView;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.reservation.VendorReservationAdapter;
import com.example.eventplanner.fragments.reservation.VendorReservationFragment;
import com.example.eventplanner.model.CompanyRatingReport;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Reservation;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.CompanyRatingRepositoryInterface;
import com.example.eventplanner.repository.interfaces.NotificationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.PackageRepositoryInterface;
import com.example.eventplanner.repository.interfaces.RatingReportRepositoryInterface;
import com.example.eventplanner.repository.interfaces.ReservationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.ServiceRepositoryInterface;
import com.example.eventplanner.repository.interfaces.StaffEventRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RatingReportOverviewFragment extends Fragment {
    private static final String TAG = "RatingReportOverviewFragment";
    private List<CompanyRatingReport> reportList;
    private RatingReportAdapter adapter;
    private DrawerLayout drawerLayout;
    private Map<String, User> emailToUserMap;
    private RatingReportRepositoryInterface ratingReportRepository;
    private UserRepositoryInterface userRepository;
    private CompanyRatingRepositoryInterface companyRatingRepository;
    private NotificationRepositoryInterface notificationRepository;

    public RatingReportOverviewFragment() {}

    public static RatingReportOverviewFragment newInstance() {
        RatingReportOverviewFragment fragment = new RatingReportOverviewFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        ratingReportRepository = eventPlannerApp.getDIContainer().resolve(RatingReportRepositoryInterface.class);
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);
        companyRatingRepository = eventPlannerApp.getDIContainer().resolve(CompanyRatingRepositoryInterface.class);
        notificationRepository = eventPlannerApp.getDIContainer().resolve(NotificationRepositoryInterface.class);

    }

    @SuppressLint("MissingInflatedId")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rating_report_overview, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.report_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        reportList = new ArrayList<>();
        emailToUserMap = new HashMap<>();
        adapter = new RatingReportAdapter(reportList, emailToUserMap, ratingReportRepository, companyRatingRepository, userRepository, notificationRepository);
        recyclerView.setAdapter(adapter);

        loadReports();
        populateEmailToUserMap();

        drawerLayout = view.findViewById(R.id.report_drawer_layout);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        drawerLayout = view.findViewById(R.id.report_drawer_layout);
    }

    private void loadReports() {
        ratingReportRepository.getAll().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<CompanyRatingReport> reports= task.getResult();
                reportList.clear();
                reportList.addAll(reports);
                adapter.notifyDataSetChanged();
            } else {
                Log.e(TAG, "Error getting reports: ", task.getException());
            }
        });
    }

    private void populateEmailToUserMap() {
        userRepository.getAllUsers().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<User> users = task.getResult();
                for (User user : users) {
                    String lowercaseEmail = user.getEmail().toLowerCase();
                    emailToUserMap.put(lowercaseEmail, user);
                }
                adapter.notifyDataSetChanged();
            } else {
                Log.e(TAG, "Error fetching users: ", task.getException());
            }
        });
    }

}