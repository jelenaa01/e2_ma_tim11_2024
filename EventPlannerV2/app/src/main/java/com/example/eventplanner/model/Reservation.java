package com.example.eventplanner.model;

import java.io.Serializable;
import java.util.Date;

public class Reservation implements Serializable {
    public enum ReservationStatus {
        NEW,
        CANCELLED_BY_PUP,
        CANCELLED_BY_ORGANIZER,
        CANCELLED_BY_ADMIN,
        ACCEPTED,
        REALIZED
    }
    private String id;
    private String serviceId;
    private String packageId;
    private String vendorStaffEmail;
    private String organizerEmail;
    private ReservationStatus reservationStatus;
    private Date date;
    private String startTime;
    private String endTime;
    private String staffEventId;

    public Reservation() {}

    public Reservation(String id, String serviceId, String packageId, String vendorStaffEmail, String organizerEmail, ReservationStatus reservationStatus,
                       Date date, String startTime, String endTime) {
        this.id = id;
        this.serviceId = serviceId;
        this.packageId = packageId;
        this.vendorStaffEmail = vendorStaffEmail;
        this.organizerEmail = organizerEmail;
        this.reservationStatus = reservationStatus;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getVendorStaffEmail() {
        return vendorStaffEmail;
    }

    public void setVendorStaffEmail(String vendorStaffEmail) {
        this.vendorStaffEmail = vendorStaffEmail;
    }

    public String getOrganizerEmail() {
        return organizerEmail;
    }

    public void setOrganizerEmail(String organizerEmail) {
        this.organizerEmail = organizerEmail;
    }

    public ReservationStatus getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(ReservationStatus reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getStaffEventId() {
        return staffEventId;
    }

    public void setStaffEventId(String staffEventId) {
        this.staffEventId = staffEventId;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id='" + id + '\'' +
                ", serviceId='" + serviceId + '\'' +
                ", vendorStaffEmail='" + vendorStaffEmail + '\'' +
                ", organizerEmail='" + organizerEmail + '\'' +
                ", reservationStatus=" + reservationStatus +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", date=" + date +
                ", packageId='" + packageId + '\'' +
                '}';
    }
}
