package com.example.eventplanner.fragments.packagesOverview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.repository.interfaces.PackageRepositoryInterface;

import java.util.ArrayList;
import java.util.List;

public class PackagesRecyclerViewAdapter extends RecyclerView.Adapter<PackagesRecyclerViewAdapter.ViewHolder> {

    private final List<Package> packages;
    private final PackageRepositoryInterface packageRepository;
    private OnItemClickListener listener;

    public PackagesRecyclerViewAdapter(List<Package> packages, PackageRepositoryInterface packageRepository) {
        this.packages = new ArrayList<>(packages);
        this.packageRepository = packageRepository;
    }

    @NonNull
    @Override
    public PackagesRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.package_item, parent, false);
        return new PackagesRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PackagesRecyclerViewAdapter.ViewHolder holder, int position) {
        Package myPackage = packages.get(position);
        holder.tvName.setText(myPackage.getName());
        holder.tvDescription.setText(myPackage.getCategory());
        holder.bind(myPackage, listener);
    }

    @Override
    public int getItemCount() {
        return packages.size();
    }

    public void setOnItemClickListener(PackagesRecyclerViewAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(Package myPackage);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvDescription;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.packageNameTextView1);
            tvDescription = itemView.findViewById(R.id.packageDescriptionTextView1);
        }

        public void bind(final Package myPackage, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(myPackage);
                }
            });
        }
    }
}
