package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.VendorStaffEvent;
import com.google.android.gms.tasks.Task;

import java.util.List;

public interface StaffEventRepositoryInterface {
    void addStaffEvent(VendorStaffEvent vendorStaffEvent);
    Task<List<VendorStaffEvent>> getEventsByStaff(String email);
    void removeStaffEvent(String id);
}
