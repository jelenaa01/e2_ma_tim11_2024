package com.example.eventplanner.model;

import java.io.Serializable;
import java.util.Date;

public class CompanyRating implements Serializable {
    private String id;
    private int rating;
    private String comment;
    private String companyId;
    private String organizerEmail;
    private Date date;

    public CompanyRating() {}

    public CompanyRating(String id, int rating, String comment, String companyId, String organizerEmail, Date date) {
        this.id = id;
        this.rating = rating;
        this.comment = comment;
        this.companyId = companyId;
        this.organizerEmail = organizerEmail;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getOrganizerEmail() {
        return organizerEmail;
    }

    public void setOrganizerEmail(String organizerEmail) {
        this.organizerEmail = organizerEmail;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "CompanyRating{" +
                "id='" + id + '\'' +
                ", rating=" + rating +
                ", comment='" + comment + '\'' +
                ", companyId='" + companyId + '\'' +
                ", organizerEmail='" + organizerEmail + '\'' +
                ", date=" + date +
                '}';
    }
}
