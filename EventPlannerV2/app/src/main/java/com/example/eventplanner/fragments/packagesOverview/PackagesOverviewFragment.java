package com.example.eventplanner.fragments.packagesOverview;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.servicesOverview.ServiceOverviewFragment;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.PackageRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PackagesOverviewFragment extends Fragment {
    private static final String TAG = "PackagesOverviewFragment";
    private List<Package> packages;
    private PackagesRecyclerViewAdapter adapter;
    private PackageRepositoryInterface packageRepository;
    private User loggedInUser;
    private UserRepositoryInterface userRepository;
    private FirebaseAuth mAuth;

    public PackagesOverviewFragment() {
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        packageRepository = eventPlannerApp.getDIContainer().resolve(PackageRepositoryInterface.class);
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);

        getLoggedInUser();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_organiser_packages, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.packageRecyclerView1);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        packageRepository.getAllVisiblePackages()
                .addOnSuccessListener(ps -> {
                    this.packages = ps;
                    // Sorting the products after fetching them
                    Collections.sort(packages, new Comparator<Package>() {
                        @Override
                        public int compare(Package o1, Package o2) {
                            String name1 = o1.getName();
                            String name2 = o2.getName();
                            if (name1 == null && name2 == null) {
                                return 0;
                            } else if (name1 == null) {
                                return -1;
                            } else if (name2 == null) {
                                return 1;
                            } else {
                                return name1.compareToIgnoreCase(name2);
                            }
                        }
                    });

                    adapter = new PackagesRecyclerViewAdapter(packages, packageRepository);
                    recyclerView.setAdapter(adapter);

                    if (adapter != null) {
                        adapter.setOnItemClickListener(product -> {
                            navigateToPage(product);
                        });
                    }

                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Failed to fetch services", e);
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                });

        return view;
    }

    private void navigateToPage(Package myPackage){
        if (myPackage == null) {
            Log.e(TAG, "Service is null, cannot navigate");
            return;
        }

        PackageOverviewFragment fragment = PackageOverviewFragment.newInstance(myPackage);

        FragmentTransaction transaction = getParentFragmentManager().beginTransaction();

        if (loggedInUser != null) {
            switch (loggedInUser.getRole()) {
                case VENDOR:
                    transaction.replace(R.id.vendor_fragment_container, fragment);
                    break;
                case ORGANISER:
                    transaction.replace(R.id.organizer_fragment_container, fragment);
                    break;
                case VENDOR_STAFF:
                    transaction.replace(R.id.vendor_staff_fragment_container, fragment);
                    break;
                case ADMIN:
                    transaction.replace(R.id.admin_fragment_container, fragment);
                    break;
                default:
                    Log.e(TAG, "Unknown user role.");
                    return;
            }

            transaction.addToBackStack(null);
            transaction.commit();
        } else {
            Log.e(TAG, "Logged in user is null.");
        }
    }

    private void getLoggedInUser() {
        if (mAuth.getCurrentUser() != null) {
            userRepository.getUserByEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<User>() {
                @Override
                public void onComplete(@NonNull Task<User> task) {
                    if (task.isSuccessful()) {
                        loggedInUser = task.getResult();
                    } else {
                        Log.e("CompanyOverviewFragment", "Failed to get logged in vendor: " + task.getException());
                    }
                }
            });
        }
    }
}
