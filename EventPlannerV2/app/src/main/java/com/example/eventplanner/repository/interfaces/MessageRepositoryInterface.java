package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.Message;
import com.google.android.gms.tasks.Task;

import java.util.List;

public interface MessageRepositoryInterface {
    void add(Message message);
    Task<List<Message>> getBySenderAndRecipient(String firstEmail, String secondEmail);
}
