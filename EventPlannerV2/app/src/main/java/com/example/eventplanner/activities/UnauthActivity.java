package com.example.eventplanner.activities;

import static android.content.ContentValues.TAG;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.authentication.LoginFragment;
import com.example.eventplanner.fragments.authentication.OrganizerRegistrationFragment;
import com.example.eventplanner.fragments.authentication.VendorRegistrationFragment;
import com.example.eventplanner.database.DBHandler;
import com.example.eventplanner.fragments.serviceProduct.ServiceFragment;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class UnauthActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Toolbar toolbar;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;

    private UserRepositoryInterface userRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getApplication();
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);

        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            DocumentReference userRef = db.collection("users").document(user.getUid());
            userRef.get().addOnSuccessListener(documentSnapshot -> {
                if (documentSnapshot.exists()) {
                    User userData = documentSnapshot.toObject(User.class);
                    handleNextPage(userData);
                } else {
                    Log.d(TAG, "No such document");
                }
            }).addOnFailureListener(e -> {
                Log.e(TAG, "Error getting user document", e);
            });
        }

        setContentView(R.layout.activity_unauth);

        toolbar = findViewById(R.id.unauth_toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.unauth_drawer_layout);
        navigationView = findViewById(R.id.unauth_nav_view);  // Fix here

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        );
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        // Set default fragment
        replaceFragment(new LoginFragment(), getString(R.string.login), R.id.unauth_nav_item4);  // Fix here

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                // Handle menu item selected
                drawerLayout.closeDrawers();

                int itemId = menuItem.getItemId();
                if (itemId == R.id.unauth_nav_item1) {
                    replaceFragment(new ServiceFragment(), getString(R.string.view_services), R.id.unauth_nav_item1);
                } else if (itemId == R.id.unauth_nav_item2) {
                    replaceFragment(new OrganizerRegistrationFragment(), getString(R.string.register_as_organisator), R.id.unauth_nav_item2);
                } else if (itemId == R.id.unauth_nav_item3) {
                    replaceFragment(new VendorRegistrationFragment(), getString(R.string.register_as_vendor), R.id.unauth_nav_item3);
                } else if (itemId == R.id.unauth_nav_item4) {
                    replaceFragment(new LoginFragment(), getString(R.string.login), R.id.unauth_nav_item4);
                }
                // Add more items as needed

                return true;
            }
        });
    }

    public void handleNextPage(User user) {
        if (user == null) {
            Log.e(TAG, "User data is null, cannot proceed");
            return;
        }

        Log.d(TAG, "Handling next page for user: " + user.getEmail());

        Intent intent;
        if (user.getRole() == User.UserRole.ORGANISER) {
            Log.d(TAG, "Navigating to OrganizerActivity");
            intent = new Intent(this, OrganizerActivity.class);
        }
        else if (user.getRole() == User.UserRole.VENDOR) {
            Log.d(TAG, "Navigating to VendorActivity");
            intent = new Intent(this, VendorActivity.class);
        }
        else if (user.getRole() == User.UserRole.VENDOR_STAFF) {
            Log.d(TAG, "Navigating to VendorStaffActivity");
            intent = new Intent(this, VendorStaffActivity.class);
        }
        else if (user.getRole() == User.UserRole.ADMIN) {
            Log.d(TAG, "Navigating to AdminActivity");
            intent = new Intent(this, AdminActivity.class);
        }
        else {
            Log.e(TAG, "Unknown user role: " + user.getRole());
            return;
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
    private void replaceFragment(Fragment fragment, String title, int menuItemId) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.unauth_fragment_container, fragment)
                .commit();

        setTitle(title);
    }


}
