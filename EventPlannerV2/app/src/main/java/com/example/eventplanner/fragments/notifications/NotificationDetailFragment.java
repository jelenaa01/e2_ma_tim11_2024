package com.example.eventplanner.fragments.notifications;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.messages.ChatFragment;
import com.example.eventplanner.fragments.productsOverview.ProductOverviewFragment;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.NotificationRepository;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class NotificationDetailFragment extends Fragment {

    private static final String ARG_NOTIFICATION = "notification";

    private Notification notification;
    private NotificationRepository notificationRepository;
    private User loggedInUser;
    private UserRepositoryInterface userRepository;
    private FirebaseAuth mAuth;

    public NotificationDetailFragment() {
        mAuth = FirebaseAuth.getInstance();
    }

    public static NotificationDetailFragment newInstance(Notification notification) {
        NotificationDetailFragment fragment = new NotificationDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_NOTIFICATION, notification);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            notification = getArguments().getParcelable(ARG_NOTIFICATION);
        }
        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        notificationRepository = new NotificationRepository(FirebaseFirestore.getInstance());
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);

        getLoggedInUser();
    }

    private void getLoggedInUser() {
        if (mAuth.getCurrentUser() != null) {
            userRepository.getUserByEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<User>() {
                @Override
                public void onComplete(@NonNull Task<User> task) {
                    if (task.isSuccessful()) {
                        loggedInUser = task.getResult();
                    } else {
                        Log.e("CompanyOverviewFragment", "Failed to get logged in vendor: " + task.getException());
                    }
                }
            });
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notification_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView titleTextView = view.findViewById(R.id.notification_title);
        TextView descriptionTextView = view.findViewById(R.id.notification_description);
        TextView timestampTextView = view.findViewById(R.id.notification_timestamp);
        Button newMessageButton = view.findViewById(R.id.new_message_button);
        newMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] parts = notification.getText().split(":");
                navigateToPage(parts[0].trim());
            }
        });

        // Set notification details
        if (notification != null) {
            titleTextView.setText(notification.getTitle());
            descriptionTextView.setText(notification.getText());

            // Format and set the timestamp
            String formattedTimestamp = formatTimestamp(notification.getTimeStamp());
            timestampTextView.setText("Sent " + formattedTimestamp);

            // Mark notification as read if it's shown
            markNotificationAsRead(notification.getId());

            if (Objects.equals(notification.getTitle(), "New Message")) {
                newMessageButton.setVisibility(View.VISIBLE);
            } else {
                newMessageButton.setVisibility(View.GONE);
            }
        }
    }

    private void markNotificationAsRead(String notificationId) {
        // Call the repository method to mark the notification as read
        notificationRepository.setNotificationAsRead(notificationId)
                .addOnSuccessListener(aVoid -> {
                    // Notification marked as read successfully
                    // You can add any further handling here if needed
                })
                .addOnFailureListener(e -> {
                    // Error marking notification as read
                    // Handle the error here
                });
    }

    private String formatTimestamp(Timestamp timestamp) {
        Date date = timestamp.toDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss", Locale.getDefault());
        return sdf.format(date);
    }

    private void navigateToPage(String email){

        ChatFragment fragment = ChatFragment.newInstance(email);

        FragmentTransaction transaction = getParentFragmentManager().beginTransaction();

        if (loggedInUser != null) {
            switch (loggedInUser.getRole()) {
                case VENDOR:
                    transaction.replace(R.id.vendor_fragment_container, fragment);
                    break;
                case ORGANISER:
                    transaction.replace(R.id.organizer_fragment_container, fragment);
                    break;
                case VENDOR_STAFF:
                    transaction.replace(R.id.vendor_staff_fragment_container, fragment);
                    break;
                case ADMIN:
                    transaction.replace(R.id.admin_fragment_container, fragment);
                    break;
                default:
                    Log.e("TAG", "Unknown user role.");
                    return;
            }

            transaction.addToBackStack(null);
            transaction.commit();
        } else {
            Log.e("TAG", "Logged in user is null.");
        }
    }
}
