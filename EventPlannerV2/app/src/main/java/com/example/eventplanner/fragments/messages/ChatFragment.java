package com.example.eventplanner.fragments.messages;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.productsOverview.VendorOverviewFragment;
import com.example.eventplanner.fragments.reports.ReportUserFragment;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.MessageRepositoryInterface;
import com.example.eventplanner.repository.interfaces.NotificationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.example.eventplanner.model.Message;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ChatFragment extends Fragment implements MessagesRecyclerViewAdapter.OnEmailClickListener {
    private static final String ARG_SELECTED_RECIPIENT = "selected-recipient";
    private FirebaseAuth mAuth;
    private String recipientEmail;
    private MessageRepositoryInterface messageRepository;
    private List<Message> messages;
    private EditText etSendMessage;
    private Button btnSendMessage;
    private RecyclerView recyclerViewMessages;
    private MessagesRecyclerViewAdapter adapter;
    private NotificationRepositoryInterface notificationRepository;
    private UserRepositoryInterface userRepository;
    private User loggedInUser;

    public ChatFragment() {
        mAuth = FirebaseAuth.getInstance();
    }
    public static ChatFragment newInstance(String recipientEmail) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_SELECTED_RECIPIENT, recipientEmail);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            recipientEmail = (String) getArguments().getSerializable(ARG_SELECTED_RECIPIENT);
        }

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        messageRepository = eventPlannerApp.getDIContainer().resolve(MessageRepositoryInterface.class);
        notificationRepository = eventPlannerApp.getDIContainer().resolve(NotificationRepositoryInterface.class);
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);

        getLoggedInUser();
    }

    private void getLoggedInUser() {
        if (mAuth.getCurrentUser() != null) {
            userRepository.getUserByEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<User>() {
                @Override
                public void onComplete(@NonNull Task<User> task) {
                    if (task.isSuccessful()) {
                        loggedInUser = task.getResult();
                    } else {
                        Log.e("TAG", "Failed to get logged in vendor: " + task.getException());
                    }
                }
            });
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        if (recipientEmail == null) {
            Log.e("ChatFragment", "Selected chat is null");
            return view;
        }

        etSendMessage = view.findViewById(R.id.edit_text_message);

        getMessages(view);

        return view;
    }

    private void sendMessage() {
        Message message = new Message();
        message.setSender(mAuth.getCurrentUser().getEmail());
        message.setRecipient(recipientEmail);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            message.setTimestamp(String.valueOf(LocalDateTime.now()));
        }
        message.setContent(String.valueOf(etSendMessage.getText()));
        etSendMessage.setText("");
        message.setRead(false);

        messages.add(message);
        adapter.notifyDataSetChanged();
        messageRepository.add(message);

        Notification notification = new Notification("1", "New Message", message.getSender() + ": " + message.getContent(), message.getRecipient(), Notification.NotificationStatus.CREATED, Timestamp.now() );
        notificationRepository.createNotification(notification);
    }

    private void getMessages(View view) {
        messages = new ArrayList<>();
        String currentUserEmail = mAuth.getCurrentUser().getEmail();

        if (currentUserEmail == null || recipientEmail == null) {
            Log.e("ChatFragment", "Emails are not properly initialized");
            return;
        }

        messageRepository.getBySenderAndRecipient(currentUserEmail, recipientEmail)
                .addOnCompleteListener(new OnCompleteListener<List<Message>>() {
                    @Override
                    public void onComplete(@NonNull Task<List<Message>> task) {
                        if (task.isSuccessful()) {
                            messages = task.getResult();
                            recyclerViewMessages = view.findViewById(R.id.recycler_view_chat);
                            recyclerViewMessages.setLayoutManager(new LinearLayoutManager(getContext()));
                            adapter = new MessagesRecyclerViewAdapter(messages);
                            adapter.setOnEmailClickListener(ChatFragment.this);
                            recyclerViewMessages.setAdapter(adapter);

                            btnSendMessage = view.findViewById(R.id.button_send);
                            btnSendMessage.setOnClickListener(v -> {
                                sendMessage();
                            });
                            for (Message message : messages) {
                                Log.d("ChatFragment", "Message: " + message.getContent());
                            }
                        } else {
                            Log.e("ChatFragment", "Error getting messages", task.getException());
                        }
                    }
                });
    }

    @Override
    public void onItemClick(String email) {
        userRepository.getUserByEmail(email)
                        .addOnCompleteListener(new OnCompleteListener<User>() {
                            @Override
                            public void onComplete(@NonNull Task<User> task) {
                                navigateToUserPage(task.getResult());
                            }
                        });
    }

    private void navigateToUserPage(User user) {
        VendorOverviewFragment fragment = VendorOverviewFragment.newInstance(user);
        FragmentTransaction transaction = getParentFragmentManager().beginTransaction();

        if (loggedInUser != null) {
            switch (loggedInUser.getRole()) {
                case VENDOR:
                    transaction.replace(R.id.vendor_fragment_container, fragment);
                    break;
                case ORGANISER:
                    transaction.replace(R.id.organizer_fragment_container, fragment);
                    break;
                case VENDOR_STAFF:
                    transaction.replace(R.id.vendor_staff_fragment_container, fragment);
                    break;
                case ADMIN:
                    transaction.replace(R.id.admin_fragment_container, fragment);
                    break;
                default:
                    Log.e("navigateToCompanyOverview", "Unknown user role.");
                    return;
            }
        }

        transaction.addToBackStack(null);
        transaction.commit();
    }
}
