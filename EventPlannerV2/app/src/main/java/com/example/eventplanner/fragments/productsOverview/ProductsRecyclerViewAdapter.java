package com.example.eventplanner.fragments.productsOverview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.repository.interfaces.ProductRepositoryInterface;

import java.util.ArrayList;
import java.util.List;

public class ProductsRecyclerViewAdapter extends RecyclerView.Adapter<ProductsRecyclerViewAdapter.ViewHolder>{

    private final List<Product> products;
    private final ProductRepositoryInterface productRepository;
    private OnItemClickListener listener;

    public ProductsRecyclerViewAdapter(List<Product> products, ProductRepositoryInterface productRepository) {
        this.products = new ArrayList<>(products);
        this.productRepository = productRepository;
    }

    @NonNull
    @Override
    public ProductsRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item, parent, false);
        return new ProductsRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsRecyclerViewAdapter.ViewHolder holder, int position) {
        Product product = products.get(position);
        holder.tvProductName.setText(product.getName());
        holder.tvProductDescription.setText(product.getDescription());
        holder.tvProductPrice.setText(String.valueOf(product.getPrice()));
        holder.bind(product, listener);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(Product product);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName;
        public TextView tvProductDescription;
        public TextView tvProductPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            tvProductName = itemView.findViewById(R.id.nameTextView1);
            tvProductDescription = itemView.findViewById(R.id.descriptionTextView1);
            tvProductPrice = itemView.findViewById(R.id.priceTextView1);
        }

        public void bind(final Product product, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(product);
                }
            });
        }
    }
}
