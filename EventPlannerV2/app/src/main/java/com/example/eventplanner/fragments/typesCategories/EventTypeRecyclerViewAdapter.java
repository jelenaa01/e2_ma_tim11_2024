package com.example.eventplanner.fragments.typesCategories;

import android.app.AlertDialog;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.database.DBHandler;
import com.example.eventplanner.model.EventType;

import java.util.ArrayList;
import java.util.List;

public class EventTypeRecyclerViewAdapter extends RecyclerView.Adapter<EventTypeRecyclerViewAdapter.ViewHolder> {

    private List<EventType> eventTypes;
    private OnItemClickListener listener;
    private Context context;
    private DBHandler dbHandler;

    public EventTypeRecyclerViewAdapter(List<EventType> eventTypes, Context context) {
        this.context = context;
        this.dbHandler = DBHandler.getInstance(context);
        this.eventTypes = eventTypes;
    }


    private List<EventType> getAllEventTypesFromDatabase() {
        List<EventType> eventTypes = new ArrayList<>();
        Cursor cursor = dbHandler.getAllEventTypes(); // Assuming you have a method getAllEventTypes() in DBHandler

        if (cursor != null && cursor.moveToFirst()) {
            do {
                int idIndex = cursor.getColumnIndex(DBHandler.EVENT_TYPE_ID_COL);
                int nameIndex = cursor.getColumnIndex(DBHandler.EVENT_TYPE_NAME_COL);
                int descriptionIndex = cursor.getColumnIndex(DBHandler.EVENT_TYPE_DESCRIPTION_COL);
                int isActiveIndex = cursor.getColumnIndex(DBHandler.EVENT_TYPE_IS_ACTIVE_COL);

                int id = idIndex != -1 ? cursor.getInt(idIndex) : 0;
                String name = cursor.getString(nameIndex);
                String description = cursor.getString(descriptionIndex);
                boolean isActive = cursor.getInt(isActiveIndex) == 1;

                EventType eventType = new EventType(id, name, description, isActive);
                eventTypes.add(eventType);
            } while (cursor.moveToNext());

            cursor.close();
        }

        return eventTypes;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_type_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        EventType eventType = eventTypes.get(position);
        holder.tvName.setText(eventType.getName());
        holder.tvDescription.setText(eventType.getDescription());

        holder.btnEdit.setOnClickListener(v -> {
            showEditDialog(eventType);
        });

        if (eventType.isActive()) {
            holder.tvStatus.setText("ACTIVE");

            holder.btnReturn.setVisibility(View.GONE); // Hide the return button for active items
            holder.btnDelete.setVisibility(View.VISIBLE); // Show the delete button for active items
            holder.btnDelete.setOnClickListener(v -> {
                // Set the isActive status of the EventType to false in the database
                dbHandler.updateEventTypeStatus(eventType.getId(), false);
                // Refresh the RecyclerView
                eventTypes = getAllEventTypesFromDatabase();
                notifyDataSetChanged();
            });
        } else {
            holder.tvStatus.setText("INACTIVE");

            holder.btnDelete.setVisibility(View.GONE); // Hide the delete button for inactive items
            holder.btnReturn.setVisibility(View.VISIBLE); // Show the return button for inactive items
            holder.btnReturn.setOnClickListener(v -> {
                // Set the isActive status of the EventType to true in the database
                dbHandler.updateEventTypeStatus(eventType.getId(), true);
                // Refresh the RecyclerView
                eventTypes = getAllEventTypesFromDatabase();
                notifyDataSetChanged();
            });
        }

        holder.itemView.setOnClickListener(v -> {
            if (listener != null) {
                listener.onItemClick(eventType);
            }
        });
    }

    @Override
    public int getItemCount() {
        return eventTypes.size();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(EventType eventType);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView tvName;
        public final TextView tvDescription;
        public final TextView tvStatus;
        public final ImageButton btnEdit;
        public final ImageButton btnReturn; // New button
        public final ImageButton btnDelete;

        public ViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvEventTypeName);
            tvDescription = view.findViewById(R.id.tvEventTypeDescription);
            tvStatus = view.findViewById(R.id.tvEventTypeStatus);
            btnEdit = view.findViewById(R.id.btnEventTypeEdit);
            btnDelete = view.findViewById(R.id.btnEventTypeDelete);
            btnReturn = view.findViewById(R.id.btnEventTypeReturn);
        }
    }

    private void showEditDialog(EventType eventType) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.fragment_edit_event_type, null);
        EditText editTextDescription = view.findViewById(R.id.editTextDescription);
        editTextDescription.setText(eventType.getDescription());

        builder.setView(view);

        AlertDialog dialog = builder.create();

        Button buttonCancel = view.findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(v -> dialog.dismiss());

        Button buttonSave = view.findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(v -> {
            String newDescription = editTextDescription.getText().toString();
            eventType.setDescription(newDescription);
            dbHandler.updateEventTypeDescription(eventType.getId(), newDescription);
            eventTypes = getAllEventTypesFromDatabase();
            notifyDataSetChanged();
            dialog.dismiss();
        });

        dialog.show();
    }
}
