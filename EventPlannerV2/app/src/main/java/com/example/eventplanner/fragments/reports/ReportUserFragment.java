package com.example.eventplanner.fragments.reports;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.UserReport;
import com.example.eventplanner.repository.interfaces.NotificationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class ReportUserFragment extends Fragment {
    private User loggedInVendor;
    private String reportedEmail; // change to User when 20 is finished

    private EditText description;
    private Button button;

    private UserRepositoryInterface userRepository;
    private NotificationRepositoryInterface notificationRepository;
    private FirebaseAuth mAuth;

    private static final String TAG = "ReportUserFragment";

    public ReportUserFragment() {
        mAuth = FirebaseAuth.getInstance();
    }

    public ReportUserFragment(String reportedEmail) {
        this.reportedEmail = reportedEmail;
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);
        notificationRepository = eventPlannerApp.getDIContainer().resolve(NotificationRepositoryInterface.class);

        getLoggedInVendor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report_user, container, false);

        initViews(view);

        button.setOnClickListener(v -> validateAndSaveReport());

        return view;
    }

    private void getLoggedInVendor() {
        if (mAuth.getCurrentUser() != null) {
            userRepository.getUserByEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<User>() {
                @Override
                public void onComplete(@NonNull Task<User> task) {
                    if (task.isSuccessful()) {
                        loggedInVendor = task.getResult();
                    } else {
                        Log.e(TAG, "Failed to get logged in vendor: " + task.getException());
                    }
                }
            });
        }
    }

    private void validateAndSaveReport() {
        if (loggedInVendor == null) {
            Toast.makeText(getActivity(), "No one is logged in.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (userRepository == null) {
            Toast.makeText(getActivity(), "UserRepository is null.", Toast.LENGTH_SHORT).show();
            return;
        }

        String _description = description.getText().toString().trim();

        if (_description.isEmpty()) {
            Toast.makeText(getActivity(), "Please fill out the description field.", Toast.LENGTH_SHORT).show();
            return;
        }

        LocalDate today;
        String dateAsString = "";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            today = LocalDate.now();
            dateAsString = today.toString();
        }

        UserReport userReport = new UserReport("id22", loggedInVendor.getEmail(), reportedEmail, _description, dateAsString, "REPORTED");

        userRepository.addUserReport(userReport);
        Toast.makeText(getActivity(), "Report Added.", Toast.LENGTH_SHORT).show();

        Notification notification = new Notification("1", "New Report", userReport.getUserEmail() + " has reported " + userReport.getVendorEmail() + " because of the following reason: " + userReport.getDescription(), "admin@gmail.com", Notification.NotificationStatus.CREATED, Timestamp.now() );
        notificationRepository.createNotification(notification);
    }

    private void initViews(View view) {
        description = view.findViewById(R.id.description);
        button = view.findViewById(R.id.button);
    }
}
