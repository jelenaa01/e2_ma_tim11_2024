package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.Service;
import com.google.android.gms.tasks.Task;

import java.util.List;

public interface ServiceRepositoryInterface {
    void addService(Service service);
    Task<List<Service>> getAll();
    Task<Service> getById(String id);
    Task<List<Service>> getAllServices();
    Task<List<Service>> getAllVisibleServices();
    Task<Service> getByName(String itemName);

    Task<Void> updateService(Service service);
}
