package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.EventType;

import java.util.List;

public interface EventTypeRepositoryInterface {
    void addEventType(String eventName, String eventDescription, boolean isActive);

    List<EventType> getAllEventTypes();

    void updateEventTypeStatus(int id, boolean isActive);

    void updateEventTypeDescription(int id, String description);
}
