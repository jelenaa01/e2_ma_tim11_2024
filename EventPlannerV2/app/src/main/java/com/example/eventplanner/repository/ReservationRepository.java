package com.example.eventplanner.repository;

import android.util.Log;

import com.example.eventplanner.model.Reservation;
import com.example.eventplanner.repository.interfaces.ReservationRepositoryInterface;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReservationRepository implements ReservationRepositoryInterface {
    private static final String TAG = "ReservationRepository";
    private FirebaseFirestore db;
    public ReservationRepository(FirebaseFirestore firestore) {
        this.db = firestore;
    }

    @Override
    public void createReservation(Reservation reservation) {
        if (reservation == null) {
            Log.e(TAG, "Reservation is null");
            return;
        }
        db.collection("reservations")
                .add(reservation)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Reservation successfully created");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error creating reservation", e);
                });
    }

    @Override
    public Task<List<Reservation>> getAll() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("reservations")
                .get()
                .continueWith(task -> {
                    List<Reservation> reservations = new ArrayList<>();
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                Reservation reservation = documentSnapshot.toObject(Reservation.class);
                                reservations.add(reservation);
                            }
                        }
                    }
                    return reservations;
                });
    }

    @Override
    public Task<List<Reservation>> getReservationByOragnizer(String email) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("reservations")
                .whereEqualTo("organizerEmail", email)
                .get()
                .continueWith(task -> {
                    List<Reservation> reservations = new ArrayList<>();
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                Reservation reservation = documentSnapshot.toObject(Reservation.class);
                                reservations.add(reservation);
                            }
                        }
                    }
                    return reservations;
                });
    }

    @Override
    public Task<List<Reservation>> getReservationByStaff(String email) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("reservations")
                .whereEqualTo("vendorStaffEmail", email)
                .get()
                .continueWith(task -> {
                    List<Reservation> reservations = new ArrayList<>();
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                Reservation reservation = documentSnapshot.toObject(Reservation.class);
                                reservations.add(reservation);
                            }
                        }
                    }
                    return reservations;
                });
    }

    @Override
    public void changeReservationStatus(String reservationId, Reservation.ReservationStatus newStatus) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("reservations")
                .whereEqualTo("id", reservationId)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        DocumentSnapshot documentSnapshot = queryDocumentSnapshots.getDocuments().get(0);
                        String docId = documentSnapshot.getId();
                        DocumentReference docRef = db.collection("reservations").document(docId);

                        docRef.update("reservationStatus", newStatus.name())
                                .addOnSuccessListener(aVoid -> {
                                    Log.d(TAG, "Reservation status updated");
                                })
                                .addOnFailureListener(e -> {
                                    Log.e(TAG, "Error during reservation status update", e);
                                });
                    } else {
                        Log.d(TAG, "No reservation found for id: " + reservationId);
                    }
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error finding reservation", e);
                });
    }

    @Override
    public void updateReservationStaffEventId(String reservationId, String staffEventId) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("reservations")
                .whereEqualTo("id", reservationId)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        DocumentSnapshot documentSnapshot = queryDocumentSnapshots.getDocuments().get(0);
                        String docId = documentSnapshot.getId();
                        DocumentReference docRef = db.collection("reservations").document(docId);

                        docRef.update("staffEventId", staffEventId)
                                .addOnSuccessListener(aVoid -> {
                                    Log.d(TAG, "Reservation staff event id updated");
                                })
                                .addOnFailureListener(e -> {
                                    Log.e(TAG, "Error during reservation staff event id update", e);
                                });
                    } else {
                        Log.d(TAG, "No reservation found for id: " + reservationId);
                    }
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error finding reservation", e);
                });
    }

    public Task<Boolean> doesReservationExistWithEmailAndDatePassed(String email) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Calendar currentDate = Calendar.getInstance();
        currentDate.setTime(new Date());

        return db.collection("reservations")
                .whereEqualTo("organizerEmail", email)
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                Reservation reservation = documentSnapshot.toObject(Reservation.class);
                                Date reservationDate = reservation.getDate();
                                if (reservationDate != null && reservationDate.before(currentDate.getTime())) {
                                    return true;
                                }
                            }
                        }
                    }
                    return false;
                });
    }

    @Override
    public void cancelReservationsByOrganizerEmail(String organizerEmail) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("reservations")
                .whereEqualTo("organizerEmail", organizerEmail)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                        String reservationId = documentSnapshot.getId();
                        DocumentReference docRef = db.collection("reservations").document(reservationId);

                        // Update the reservation status to CANCELLED_BY_ADMIN
                        docRef.update("reservationStatus", Reservation.ReservationStatus.CANCELLED_BY_ADMIN.name())
                                .addOnSuccessListener(aVoid -> {
                                    Log.d(TAG, "Reservation with ID " + reservationId + " cancelled by admin");
                                })
                                .addOnFailureListener(e -> {
                                    Log.e(TAG, "Error cancelling reservation with ID " + reservationId, e);
                                });
                    }
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error finding reservations by organizer email", e);
                });
    }

}
