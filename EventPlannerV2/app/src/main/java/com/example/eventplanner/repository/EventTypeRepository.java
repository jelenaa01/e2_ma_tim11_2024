package com.example.eventplanner.repository;

import android.database.Cursor;

import com.example.eventplanner.database.DBHandler;
import com.example.eventplanner.model.EventType;
import com.example.eventplanner.repository.interfaces.EventTypeRepositoryInterface;

import java.util.ArrayList;
import java.util.List;

public class EventTypeRepository implements EventTypeRepositoryInterface {

    private final DBHandler dbHandler;

    public EventTypeRepository(DBHandler dbHandler) {
        this.dbHandler = dbHandler;
    }

    @Override
    public void addEventType(String eventName, String eventDescription, boolean isActive) {
        dbHandler.addEventType(eventName, eventDescription, isActive);
    }

    @Override
    public List<EventType> getAllEventTypes() {
        Cursor cursor = dbHandler.getAllEventTypes();
        return getEventTypesFromCursor(cursor);
    }

    private List<EventType> getEventTypesFromCursor(Cursor cursor) {
        List<EventType> eventTypes = new ArrayList<>();
        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(DBHandler.EVENT_TYPE_ID_COL);
            int nameIndex = cursor.getColumnIndex(DBHandler.EVENT_TYPE_NAME_COL);
            int descriptionIndex = cursor.getColumnIndex(DBHandler.EVENT_TYPE_DESCRIPTION_COL);
            int isActiveIndex = cursor.getColumnIndex(DBHandler.EVENT_TYPE_IS_ACTIVE_COL);

            do {
                int id = cursor.getInt(idIndex);
                String name = cursor.getString(nameIndex);
                String description = cursor.getString(descriptionIndex);
                boolean isActive = cursor.getInt(isActiveIndex) == 1;

                EventType eventType = new EventType(id, name, description, isActive);
                eventTypes.add(eventType);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return eventTypes;
    }

    @Override
    public void updateEventTypeStatus(int id, boolean isActive) {
        dbHandler.updateEventTypeStatus(id, isActive);
    }

    @Override
    public void updateEventTypeDescription(int id, String description) {
        dbHandler.updateEventTypeDescription(id, description);
    }
}
