package com.example.eventplanner.fragments.companyRating;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageButton;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.reports.ReportCompanyRatingFragment;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.CompanyRating;
import com.example.eventplanner.model.Reservation;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.CompanyRatingRepositoryInterface;
import com.example.eventplanner.repository.interfaces.CompanyRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class VendorRatingOverviewFragment extends Fragment implements CompanyRatingAdapter.OnReportButtonClickListener  {
    private static final String TAG = "VendorRatingOverviewFragment";
    private List<CompanyRating> companyRatingList;
    private List<CompanyRating> originalCompanyRatingList;
    private CompanyRatingAdapter adapter;
    private DrawerLayout drawerLayout;
    private CalendarView calendarView;
    private Button applyButton, clearDateButton;
    private long selectedDateInMillis = -1;
    private Map<String, User> emailToUserMap;
    private UserRepositoryInterface userRepository;
    private CompanyRepositoryInterface companyRepository;
    private CompanyRatingRepositoryInterface companyRatingRepository;
    private FirebaseAuth mAuth;

    public VendorRatingOverviewFragment() {}

    public static VendorRatingOverviewFragment newInstance() {
        VendorRatingOverviewFragment fragment = new VendorRatingOverviewFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        companyRepository = eventPlannerApp.getDIContainer().resolve(CompanyRepositoryInterface.class);
        companyRatingRepository = eventPlannerApp.getDIContainer().resolve(CompanyRatingRepositoryInterface.class);
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);

        mAuth = FirebaseAuth.getInstance();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_vendor_rating_overview, container, false);
       RecyclerView recyclerView = view.findViewById(R.id.rating_recyclerView);
       recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

       companyRatingList = new ArrayList<>();
       originalCompanyRatingList = new ArrayList<>();
       emailToUserMap = new HashMap<>();
       adapter = new CompanyRatingAdapter(companyRatingList, emailToUserMap);
       adapter.setOnReportButtonClickListener(this);
       recyclerView.setAdapter(adapter);

       loadCompanyRatings();
       populateEmailToUserMap();

       drawerLayout = view.findViewById(R.id.rating_drawer_layout);

       calendarView = view.findViewById(R.id.calendar);
       applyButton =  view.findViewById(R.id.applyButton);
       clearDateButton = view.findViewById(R.id.clearDateButton);

       ImageButton filterButton = view.findViewById(R.id.filterButton);
       filterButton.setOnClickListener(v -> drawerLayout.openDrawer(Gravity.RIGHT));

       applyButton.setOnClickListener(v -> {
           applyFilters();
           drawerLayout.closeDrawer(Gravity.RIGHT);
       });

       calendarView.setOnDateChangeListener((calendarView, year, month, dayOfMonth) -> {
           Calendar calendar = Calendar.getInstance();
           calendar.set(year, month, dayOfMonth);
           selectedDateInMillis = calendar.getTimeInMillis();
       });

       clearDateButton.setOnClickListener(v -> {
           selectedDateInMillis = -1;
           loadCompanyRatings();
           calendarView.setDate(System.currentTimeMillis(), false, true);
       });

       return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        drawerLayout = view.findViewById(R.id.rating_drawer_layout);
    }

    private void loadCompanyRatings() {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null) {
            companyRepository.getCompanyByOwnerEmail(currentUser.getEmail()).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Company company = task.getResult();
                    if(company != null) {
                        companyRatingRepository.getRatingsByCompany(company.getId()).addOnCompleteListener(ratingTask -> {
                            if(ratingTask.isSuccessful()) {
                                //originalCompanyRatingList.clear();
                                List<CompanyRating> companyRatings = ratingTask.getResult();
                                originalCompanyRatingList.addAll(companyRatings);
                                companyRatingList.clear();
                                companyRatingList.addAll(companyRatings);
                                adapter.notifyDataSetChanged();
                            } else {
                                Log.e(TAG, "Error getting ratings: ", ratingTask.getException());
                            }
                        });
                    } else {
                        Log.e(TAG, "Company not found for owner email: " + currentUser.getEmail());
                    }
                } else {
                    Log.e(TAG, "Error getting company: ", task.getException());
                }
            });
        }
    }

    private void applyFilters() {
        List<CompanyRating> filteredRatings = getRatingsForSelectedDate();
        companyRatingList.clear();
        companyRatingList.addAll(filteredRatings);
        adapter.notifyDataSetChanged();
    }

    private List<CompanyRating> getRatingsForSelectedDate() {
        List<CompanyRating> ratingsForSelectedDate = new ArrayList<>();
        if (selectedDateInMillis != -1) {
            Calendar selectedCalendar = Calendar.getInstance();
            selectedCalendar.setTimeInMillis(selectedDateInMillis);

            for (CompanyRating rating : originalCompanyRatingList) {
                Calendar ratingCalendar = Calendar.getInstance();
                ratingCalendar.setTimeInMillis(rating.getDate().getTime());

                if (selectedCalendar.get(Calendar.YEAR) == ratingCalendar.get(Calendar.YEAR) &&
                        selectedCalendar.get(Calendar.MONTH) == ratingCalendar.get(Calendar.MONTH) &&
                        selectedCalendar.get(Calendar.DAY_OF_MONTH) == ratingCalendar.get(Calendar.DAY_OF_MONTH)) {

                    ratingsForSelectedDate.add(rating);
                }
            }
        }
        return ratingsForSelectedDate;
    }

    private void populateEmailToUserMap() {
        userRepository.getAllUsers().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<User> users = task.getResult();
                for (User user : users) {
                    String lowercaseEmail = user.getEmail().toLowerCase();
                    emailToUserMap.put(lowercaseEmail, user);
                }
                adapter.notifyDataSetChanged();
            } else {
                Log.e(TAG, "Error fetching users: ", task.getException());
            }
        });
    }

    @Override
    public void onReportButtonClick(CompanyRating companyRating) {
        Fragment reportFragment = ReportCompanyRatingFragment.newInstance(companyRating);

        getParentFragmentManager()
                .beginTransaction()
                .replace(R.id.vendor_fragment_container, reportFragment)
                .addToBackStack(null)
                .commit();
    }


}