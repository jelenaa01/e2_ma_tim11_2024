package com.example.eventplanner.fragments.productsOverview;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import com.example.eventplanner.model.OrganiserEvent;
import com.example.eventplanner.repository.interfaces.BudgetRepositoryInterface;
import com.example.eventplanner.repository.interfaces.OrganiserEventRepositoryInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class EventSelectionDialog extends DialogFragment {

    private static final String TAG = "EventSelectionDialog";
    private List<OrganiserEvent> organiserEvents;
    private BudgetRepositoryInterface budgetRepository;
    private OrganiserEventRepositoryInterface organiserEventRepository;
    private String productID;
    private double productPrice;

    public EventSelectionDialog(BudgetRepositoryInterface budgetRepository,
                                OrganiserEventRepositoryInterface organiserEventRepository,
                                String productID, double productPrice) {
        this.budgetRepository = budgetRepository;
        this.organiserEventRepository = organiserEventRepository;
        this.productID = productID;
        this.productPrice = productPrice;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());

        organiserEventRepository.getByOrganiserEmail(getCurrentUserEmail()).addOnCompleteListener(new OnCompleteListener<List<OrganiserEvent>>() {
            @Override
            public void onComplete(@NonNull Task<List<OrganiserEvent>> task) {
                if (task.isSuccessful()) {
                    organiserEvents = task.getResult();
                    if (isAdded()) {
                        String[] eventNames = new String[organiserEvents.size()];
                        for (int i = 0; i < organiserEvents.size(); i++) {
                            eventNames[i] = organiserEvents.get(i).getName();
                        }

                        builder.setTitle("Select Event")
                                .setItems(eventNames, (dialog, which) -> {
                                    OrganiserEvent selectedEvent = organiserEvents.get(which);
                                    addProductToBudget(selectedEvent.getBudgetId());
                                });
                        builder.create().show();
                    }
                } else {
                    Log.e(TAG, "Error fetching events", task.getException());
                }
            }
        });

        return builder.create();
    }

    private void addProductToBudget(String budgetId) {
        budgetRepository.addProductToBudget(budgetId, productID, productPrice).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Log.d(TAG, "Product successfully added to budget.");
            } else {
                Log.e(TAG, "Error adding product to budget.", task.getException());
            }
        });
    }

    private String getCurrentUserEmail() {
        // Return the current user's email. Implement this method based on your app's user management.
        return FirebaseAuth.getInstance().getCurrentUser().getEmail();
    }
}
