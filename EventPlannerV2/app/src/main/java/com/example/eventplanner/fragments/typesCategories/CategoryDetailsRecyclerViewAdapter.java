package com.example.eventplanner.fragments.typesCategories;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.eventplanner.R;
import com.example.eventplanner.database.DBHandler;

import com.example.eventplanner.model.Subcategory;

import java.util.List;

public class CategoryDetailsRecyclerViewAdapter extends RecyclerView.Adapter<CategoryDetailsRecyclerViewAdapter.ViewHolder> {

    private List<Subcategory> subcategories;
    private Context context;
    private DBHandler dbHandler;

    public CategoryDetailsRecyclerViewAdapter(List<Subcategory> subcategories, Context context) {
        this.context = context;
        this.dbHandler = DBHandler.getInstance(context);
        this.subcategories = subcategories;
    }


    @NonNull
    @Override
    public CategoryDetailsRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_category_details, parent, false);
        return new CategoryDetailsRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryDetailsRecyclerViewAdapter.ViewHolder holder, int position) {
        Subcategory subcategory = subcategories.get(position);
        holder.tvName.setText(subcategory.getName());
        holder.tvDescription.setText(subcategory.getDescription());

        if (subcategory.isActive()) {
            holder.tvStatus.setText("ACTIVE");

        } else {
            holder.tvStatus.setText("INACTIVE");

        }
    }

    @Override
    public int getItemCount() {
        return subcategories.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView tvName;
        public final TextView tvDescription;
        public final TextView tvStatus;
        public ViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvSubcategoryName);
            tvDescription = view.findViewById(R.id.tvSubcategoryDescription);
            tvStatus = view.findViewById(R.id.tvSubcategoryStatus);
        }
    }

}