package com.example.eventplanner.model;

import java.io.Serializable;

public class WorkingTime implements Serializable {
    public enum Days {
        MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
    }

    private Days workingDays;
    private TimeInterval workingHours;

    public WorkingTime() {}
    public WorkingTime(Days workingDays, TimeInterval workingHours) {
        this.workingDays = workingDays;
        this.workingHours = workingHours;
    }

    public Days getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(Days workingDays) {
        this.workingDays = workingDays;
    }

    public TimeInterval getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(TimeInterval workingHours) {
        this.workingHours = workingHours;
    }

    @Override
    public String toString() {
        return "CompanyWorkingHours{" +
                "workingDays=" + workingDays +
                ", workingHours=" + workingHours +
                '}';
    }
}
