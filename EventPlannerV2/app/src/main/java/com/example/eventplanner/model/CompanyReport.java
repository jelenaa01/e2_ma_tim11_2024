package com.example.eventplanner.model;

import java.util.UUID;

public class CompanyReport {
    private String Id;
    private String OrganizerEmail;
    private String CompanyName;
    private String Description;

    public CompanyReport() {
        Id = UUID.randomUUID().toString();
    }

    public CompanyReport(String organizerEmail, String companyName, String description) {
        Id = UUID.randomUUID().toString();
        OrganizerEmail = organizerEmail;
        CompanyName = companyName;
        Description = description;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getOrganizerEmail() {
        return OrganizerEmail;
    }

    public void setOrganizerEmail(String organizerEmail) {
        OrganizerEmail = organizerEmail;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}
