package com.example.eventplanner.model;

import java.util.Objects;

public class Guest {
    private String name;
    private String ageRange;
    private boolean invited;
    private boolean acceptedInvitation;
    private boolean isVegan;
    private boolean isVegetarian;
    private int eventId;

    public Guest () {}

    public Guest(String name, String ageRange, boolean invited, boolean acceptedInvitation, boolean isVegan, boolean isVegetarian, int eventId) {
        this.name = name;
        this.ageRange = ageRange;
        this.invited = invited;
        this.acceptedInvitation = acceptedInvitation;
        this.isVegan = isVegan;
        this.isVegetarian = isVegetarian;
        this.eventId = eventId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAgeRange() {
        return ageRange;
    }

    public void setAgeRange(String ageRange) {
        this.ageRange = ageRange;
    }

    public boolean isInvited() {
        return invited;
    }

    public void setInvited(boolean invited) {
        this.invited = invited;
    }

    public boolean hasAcceptedInvitation() {
        return acceptedInvitation;
    }

    public void setAcceptedInvitation(boolean acceptedInvitation) {
        this.acceptedInvitation = acceptedInvitation;
    }

    public boolean isVegan() {
        return isVegan;
    }

    public void setVegan(boolean vegan) {
        isVegan = vegan;
    }

    public boolean isVegetarian() {
        return isVegetarian;
    }

    public void setVegetarian(boolean vegetarian) {
        isVegetarian = vegetarian;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    @Override
    public String toString() {
        return "Guest{" +
                "name='" + name + '\'' +
                ", ageRange='" + ageRange + '\'' +
                ", invited=" + invited +
                ", acceptedInvitation=" + acceptedInvitation +
                ", isVegan=" + isVegan +
                ", isVegetarian=" + isVegetarian +
                ", eventId=" + eventId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Guest guest = (Guest) o;
        return invited == guest.invited && acceptedInvitation == guest.acceptedInvitation && isVegan == guest.isVegan && isVegetarian == guest.isVegetarian && eventId == guest.eventId && Objects.equals(name, guest.name) && Objects.equals(ageRange, guest.ageRange);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, ageRange, invited, acceptedInvitation, isVegan, isVegetarian, eventId);
    }
}
