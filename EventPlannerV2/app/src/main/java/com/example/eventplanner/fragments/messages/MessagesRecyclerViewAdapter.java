package com.example.eventplanner.fragments.messages;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.event.GuestsRecyclerViewAdapter;
import com.example.eventplanner.model.Message;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class MessagesRecyclerViewAdapter extends RecyclerView.Adapter<MessagesRecyclerViewAdapter.ViewHolder> {

    private final List<Message> messages;
    private OnItemClickListener listener;
    private OnEmailClickListener emailClickListener;
    public MessagesRecyclerViewAdapter(List<Message> messages) {
        this.messages = messages;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Message message = messages.get(position);
        holder.tvSenderEmail.setText(message.getSender());
        holder.tvTime.setText(message.getTimestamp());
        holder.tvContent.setText(message.getContent());
        holder.bind(message, listener, emailClickListener);
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }
    public interface OnItemClickListener {
        void onItemClick(Message message);
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnEmailClickListener {
        void onItemClick(String email);
    }
    public void setOnEmailClickListener(OnEmailClickListener emailClickListener) {
        this.emailClickListener = emailClickListener;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvSenderEmail;
        public TextView tvTime;
        public TextView tvContent;



        public ViewHolder(View itemView) {
            super(itemView);
            tvSenderEmail = itemView.findViewById(R.id.tvSenderEmail);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvContent = itemView.findViewById(R.id.tvContent);
        }

        public void bind(final Message message, final OnItemClickListener listener, final OnEmailClickListener email) {
            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(message);
                }
            });

            tvSenderEmail.setOnClickListener(v -> {
                if (email != null) {
                    email.onItemClick((String) tvSenderEmail.getText());
                }
            });
        }
    }
}
