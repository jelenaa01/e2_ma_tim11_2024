package com.example.eventplanner.utils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.navigation.NavigationView;

public class FragmentUtils {

    public static void replaceFragment(FragmentManager fragmentManager, Fragment fragment, int containerId, NavigationView navigationView, int menuItemId, String title) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(containerId, fragment);
        fragmentTransaction.addToBackStack(null); // Optional: Add the transaction to the back stack
        fragmentTransaction.commit();

        // Set the toolbar title
        ((AppCompatActivity) navigationView.getContext()).getSupportActionBar().setTitle(title);

        // Uncheck all menu items
        for (int i = 0; i < navigationView.getMenu().size(); i++) {
            navigationView.getMenu().getItem(i).setChecked(false);
        }

        // Check the selected menu item
        navigationView.getMenu().findItem(menuItemId).setChecked(true);
    }
}

