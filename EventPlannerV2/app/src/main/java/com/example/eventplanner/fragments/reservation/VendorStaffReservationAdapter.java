package com.example.eventplanner.fragments.reservation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Reservation;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.VendorStaffEvent;
import com.example.eventplanner.repository.interfaces.NotificationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.PackageRepositoryInterface;
import com.example.eventplanner.repository.interfaces.ReservationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.StaffEventRepositoryInterface;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

public class VendorStaffReservationAdapter extends RecyclerView.Adapter<VendorStaffReservationAdapter.ViewHolder> {
    private static final String TAG = "VendorStaffReservationAdapter";
    private List<Reservation> reservationList;
    private List<Reservation> originalReservationList;
    private Map<String, User> emailToUserMap;
    private Map<String, Service> serviceIdToNameMap;
    private Map<String, Package> packageIdToNameMap;
    private VendorStaffReservationAdapter.OnItemClickListener listener;
    private ReservationRepositoryInterface reservationRepository;
    private StaffEventRepositoryInterface staffEventRepository;
    private NotificationRepositoryInterface notificationRepository;

    public VendorStaffReservationAdapter(List<Reservation> items, Map<String, User> emailToUserMap, Map<String, Service> serviceIdToNameMap, Map<String, Package> packageIdToNameMap,
                                         ReservationRepositoryInterface reservationRepository, StaffEventRepositoryInterface staffEventRepository, NotificationRepositoryInterface notificationRepository) {
        this.reservationList = items;
        this.originalReservationList = new ArrayList<>(items);
        this.emailToUserMap = emailToUserMap;
        this.serviceIdToNameMap = serviceIdToNameMap;
        this.packageIdToNameMap = packageIdToNameMap;
        this.reservationRepository = reservationRepository;
        this.staffEventRepository = staffEventRepository;
        this.notificationRepository = notificationRepository;
    }

    @NonNull
    @Override
    public VendorStaffReservationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reservation_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VendorStaffReservationAdapter.ViewHolder holder, int position) {
        Reservation reservation = reservationList.get(position);

        User organizer = emailToUserMap.get(reservation.getOrganizerEmail().toLowerCase());
        Service service = serviceIdToNameMap.get(reservation.getServiceId().toLowerCase());
        Package packageObj = packageIdToNameMap.get(reservation.getPackageId().toLowerCase());

        String organizerFullName = (organizer != null) ? organizer.getFirstName() + " " + organizer.getLastName() : "";
        String serviceName = (service != null) ? service.getName() : "";
        String packageName = (packageObj != null) ? packageObj.getName() : "";

        holder.serviceName.setText("Service: " + serviceName);
        holder.packageName.setText("Package: " + packageName);
        holder.organizerFullName.setText("Reserver: " + organizerFullName);
        holder.staffFullName.setVisibility(View.GONE);
        holder.status.setText("Status: " + reservation.getReservationStatus().toString());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        String formattedDate = dateFormat.format(reservation.getDate());
        String formattedStartTime = formatTime(reservation.getStartTime());
        String formattedEndTime = formatTime(reservation.getEndTime());

        holder.date.setText("Date: " + formattedDate);
        holder.time.setText("Time: " + formattedStartTime + "-" + formattedEndTime);

        if (isManualApprovalNeeded(reservation)) {
            Log.d(TAG, "Manual approval is needed for reservation ID: " + reservation.getId());
            holder.btnAccept.setVisibility(View.VISIBLE);
            holder.btnDecline.setVisibility(View.VISIBLE);
        } else {
            Log.d(TAG, "Manual approval is not needed for reservation ID: " + reservation.getId());
            holder.btnAccept.setVisibility(View.GONE);
            holder.btnDecline.setVisibility(View.GONE);
        }

        holder.btnDecline.setOnClickListener(v -> {
            if(listener != null) {
                listener.onItemClick(reservation);
            }
            if(reservation.getReservationStatus() == Reservation.ReservationStatus.NEW || reservation.getReservationStatus() == Reservation.ReservationStatus.ACCEPTED) {
                int canellationDeadline = getCancellationDeadline(reservation);
                if(canCancelReservation(reservation.getDate().toString(), canellationDeadline)) {
                    reservationRepository.changeReservationStatus(reservation.getId(), Reservation.ReservationStatus.CANCELLED_BY_PUP);
                    updateReservationStatus(reservation.getId(), Reservation.ReservationStatus.CANCELLED_BY_PUP);
                    Log.d(TAG, "Reservation succesfully canceled");

                    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                    if(currentUser != null) {
                        Notification notification = new Notification(UUID.randomUUID().toString(), "Reservation cancellation", "Vendor staff " + currentUser.getEmail() + " canceled reservation", reservation.getOrganizerEmail(), Notification.NotificationStatus.CREATED, Timestamp.now());
                        notificationRepository.createNotification(notification);
                    }

                    Notification notification = new Notification(UUID.randomUUID().toString(), "Company rating", "You can rate company", reservation.getOrganizerEmail(), Notification.NotificationStatus.CREATED, Timestamp.now());
                    notificationRepository.createNotification(notification);

                    String staffEventId = reservation.getStaffEventId();
                    if(staffEventId != null) {
                        staffEventRepository.removeStaffEvent(staffEventId);
                        Log.d(TAG, "Successfully remove staff event");
                        reservationRepository.updateReservationStaffEventId(reservation.getId(), "");
                        Log.d(TAG, "Staff event id successfully remove from reservation");
                    } else {
                        Log.d(TAG, "Event staff id not found");
                    }

                } else {
                    Log.d(TAG, "Error during canceling reservation");
                }
            }
        });

        holder.btnAccept.setOnClickListener(v -> {
            if(listener != null) {
                listener.onItemClick(reservation);
            }
            if(reservation.getReservationStatus() == Reservation.ReservationStatus.NEW) {
                try {
                    reservationRepository.changeReservationStatus(reservation.getId(), Reservation.ReservationStatus.ACCEPTED);
                    updateReservationStatus(reservation.getId(), Reservation.ReservationStatus.ACCEPTED);
                    Log.d(TAG, "Reservation successfully accepted");

                    VendorStaffEvent staffEvent = new VendorStaffEvent(UUID.randomUUID().toString(), serviceName, reservation.getDate(), reservation.getStartTime(), reservation.getEndTime(), VendorStaffEvent.Type.RESERVED, reservation.getVendorStaffEmail());
                    staffEventRepository.addStaffEvent(staffEvent);
                    Log.d(TAG, "Successfully added new event for vendor staff");

                    reservationRepository.updateReservationStaffEventId(reservation.getId(), staffEvent.getId()); // Ažurirajte rezervaciju sa novim eventId
                    Log.d(TAG, "Staff event id successfully added to reservation");


                } catch (Exception e) {
                    Log.e(TAG, "Error while accepting reservation ", e);
                }
            } else {
                Log.d(TAG, "Reservation status is not NEW, cannot accept reservation with ID: " + reservation.getId());
            }
        });

    }

    public Filter getFilter() {
        return reservationFilter;
    }

    private Filter reservationFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Reservation> filteredReservations = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredReservations.addAll(originalReservationList);
            } else {
                String filterPattern = constraint.toString().replaceAll("\\s+", "").toLowerCase().trim();

            for (Reservation item : originalReservationList) {
                User organizer = emailToUserMap.get(item.getOrganizerEmail().toLowerCase());
                Service service = serviceIdToNameMap.get(item.getServiceId().toLowerCase());

                String organizerFullName = (organizer != null) ? (organizer.getFirstName() + " " + organizer.getLastName()).toLowerCase() : "";
                String organizerFullNameReverse = (organizer != null) ? (organizer.getLastName() + " " + organizer.getFirstName()).toLowerCase() : "";
                String serviceName = (service != null) ? (service.getName().toLowerCase()) : "";

                if (organizerFullName.contains(filterPattern) || organizerFullNameReverse.contains(filterPattern) || organizer.getFirstName().toLowerCase().contains(filterPattern) ||
                        organizer.getLastName().toLowerCase().contains(filterPattern) ||
                        serviceName.contains(filterPattern)) {
                    filteredReservations.add(item);
                }
            }
        }

            FilterResults results = new FilterResults();
            results.values = filteredReservations;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            reservationList.clear();
            reservationList.addAll((List<Reservation>) results.values);
            notifyDataSetChanged();
        }
    };

    @Override
    public int getItemCount() {
        return reservationList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView serviceName;
        public TextView packageName;
        public TextView staffFullName;
        public TextView organizerFullName;
        public TextView status;
        public TextView date;
        public TextView time;
        public ImageButton btnDecline;
        public ImageButton btnAccept;

        public ViewHolder(View view) {
            super(view);
            serviceName = view.findViewById(R.id.reservation_serviceName);
            packageName = view.findViewById(R.id.reservation_packageName);
            staffFullName = view.findViewById(R.id.reservation_staff);
            organizerFullName = view.findViewById(R.id.reservation_organizer);
            status = view.findViewById(R.id.reservation_status);
            date = view.findViewById(R.id.reservation_date);
            time = view.findViewById(R.id.reservation_time);
            btnDecline = view.findViewById(R.id.reservation_btnDecline);
            btnAccept = view.findViewById(R.id.reservation_btnAccept);
        }
    }

    public void setOnItemClickListener(VendorStaffReservationAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(Reservation reservation);
    }

    private String formatTime(String time) {
        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            Date date = inputFormat.parse(time);

            SimpleDateFormat outputFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            return outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return time;
        }
    }

    public void setOriginalReservationList(List<Reservation> originalList) {
        this.originalReservationList = new ArrayList<>(originalList);
    }

    public void updateData(List<Reservation> newReservations) {
        reservationList.clear();
        reservationList.addAll(newReservations);
        notifyDataSetChanged();
    }


    private int getCancellationDeadline(Reservation reservation) {
        if (reservation.getPackageId() != null) {
            Package packageObj = packageIdToNameMap.get(reservation.getPackageId().toLowerCase());
            if (packageObj != null) {
                return packageObj.getCancellationDeadline();
            }
        } else {
            Service service = serviceIdToNameMap.get(reservation.getServiceId().toLowerCase());
            if (service != null) {
                return service.getCancellationDeadline();
            }
        }
        return 0;
    }

    private boolean canCancelReservation(String reservationDate, int cancellationDeadlineDays) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
        try {
            Date reservationDateObj = dateFormat.parse(reservationDate);

            long currentTimeMillis = System.currentTimeMillis();

            long deadlineSeconds = cancellationDeadlineDays * 24 * 60 * 60;
            long deadlineMillis = deadlineSeconds * 1000;

            long deadlineDateMillis = reservationDateObj.getTime() - deadlineMillis;

            if (currentTimeMillis <= deadlineDateMillis) {
                Log.d(TAG, "Cancellation deadline hasn't passed");
                return true;
            } else {
                Log.d(TAG, "Cancellation deadline has passed");
                return false;
            }
        } catch (ParseException e) {
            Log.e(TAG, "Error parsing reservation date: " + e.getMessage());
            return false;
        }
    }

    public void updateReservationStatus(String reservationId, Reservation.ReservationStatus newStatus) {
        for (int i = 0; i < reservationList.size(); i++) {
            Reservation reservation = reservationList.get(i);
            if (reservation.getId().equals(reservationId)) {
                reservation.setReservationStatus(newStatus);
                notifyItemChanged(i);
                break;
            }
        }
    }

    public boolean isManualApprovalNeeded(Reservation reservation) {
        if (reservation.getPackageId() != null) {
            Package packageItem = packageIdToNameMap.get(reservation.getPackageId().toLowerCase());
            if (packageItem != null) {
                if (packageItem.getConfirmationMode() == Package.ConfirmationMode.MANUAL) {
                    Log.d(TAG, "Manual approval needed for Package ID: " + reservation.getPackageId());
                    return true;
                } else {
                    Log.d(TAG, "Automatic approval for Package ID: " + reservation.getPackageId());
                }
            } else {
                Log.e(TAG, "Package not found for Package ID: " + reservation.getPackageId());
            }
        }

        if (reservation.getServiceId() != null) {
            Service service = serviceIdToNameMap.get(reservation.getServiceId().toLowerCase());
            if (service != null) {
               if (service.getConfirmationMode() == Service.ConfirmationModeService.MANUAL) {
                    Log.d(TAG, "Manual approval needed for Service ID: " + reservation.getServiceId());
                    return true;
               } else {
                    Log.d(TAG, "Automatic approval for Service ID: " + reservation.getServiceId());
               }
            } else {
                Log.e(TAG, "Service not found for Service ID: " + reservation.getServiceId());
            }
        }

        return false;
    }

}
