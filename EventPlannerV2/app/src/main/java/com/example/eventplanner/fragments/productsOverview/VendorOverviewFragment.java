package com.example.eventplanner.fragments.productsOverview;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.reports.ReportCompanyFragment;
import com.example.eventplanner.fragments.reports.ReportUserFragment;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.NotificationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class VendorOverviewFragment extends Fragment {

    private static final String ARG_SELECTED_VENDOR = "selected-vendor";
    private User selectedVendor;
    private User loggedInUser;
    private Button reportUserBtn;
    private UserRepositoryInterface userRepository;
    private FirebaseAuth mAuth;

    public VendorOverviewFragment() { mAuth = FirebaseAuth.getInstance(); }

    public static VendorOverviewFragment newInstance(User selectedVendor) {
        VendorOverviewFragment fragment = new VendorOverviewFragment();

        Bundle args = new Bundle();
        args.putSerializable(ARG_SELECTED_VENDOR, selectedVendor);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            selectedVendor = (User) getArguments().getSerializable(ARG_SELECTED_VENDOR);
        }

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);

        getLoggedInUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vendor_overview, container, false);

        TextView nameTextView = view.findViewById(R.id.name4);
        TextView lastNameTextView = view.findViewById(R.id.lastName4);
        TextView emailTextView = view.findViewById(R.id.email4);
        TextView phoneTextView = view.findViewById(R.id.phone4);

        if (selectedVendor != null) {
            nameTextView.setText(selectedVendor.getFirstName());
            lastNameTextView.setText(selectedVendor.getLastName());
            emailTextView.setText(selectedVendor.getEmail());
            phoneTextView.setText(selectedVendor.getPhone());
        }

        reportUserBtn = view.findViewById(R.id.reportButton);
        reportUserBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToReportUserPage();
            }
        });

        return view;
    }

    private void navigateToReportUserPage() {
        ReportUserFragment fragment = new ReportUserFragment(selectedVendor.getEmail());
        FragmentTransaction transaction = getParentFragmentManager().beginTransaction();

        if (loggedInUser != null) {
            switch (loggedInUser.getRole()) {
                case VENDOR:
                    transaction.replace(R.id.vendor_fragment_container, fragment);
                    break;
                case ORGANISER:
                    transaction.replace(R.id.organizer_fragment_container, fragment);
                    break;
                case VENDOR_STAFF:
                    transaction.replace(R.id.vendor_staff_fragment_container, fragment);
                    break;
                case ADMIN:
                    transaction.replace(R.id.admin_fragment_container, fragment);
                    break;
                default:
                    Log.e("navigateToCompanyOverview", "Unknown user role.");
                    return;
            }
        }

        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void getLoggedInUser() {
        if (mAuth.getCurrentUser() != null) {
            userRepository.getUserByEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<User>() {
                @Override
                public void onComplete(@NonNull Task<User> task) {
                    if (task.isSuccessful()) {
                        loggedInUser = task.getResult();
                        if (loggedInUser.getRole() == User.UserRole.ADMIN) {
                            reportUserBtn.setVisibility(View.GONE);
                        }
                    } else {
                        Log.e("TAG", "Failed to get logged in vendor: " + task.getException());
                    }
                }
            });
        }
    }
}