package com.example.eventplanner.model;

import java.util.ArrayList;
import java.util.UUID;
import java.io.Serializable;

public class Product implements Serializable {
    private String Id;
    private String CategoryId;
    private String SubCategoryId;
    private String CompanyId;
    private String Name;
    private String Description;
    private ArrayList<String> Gallery;
    private ArrayList<String> EventTypes;
    private boolean isAvailable;
    private boolean isVisible;
    private double Price;
    private double Discount;
    private double PriceWithDiscount;

    public Product() {
        Id = UUID.randomUUID().toString();
    }

    public Product(String categoryId, String subCategoryId, String name, String description, ArrayList<String> gallery, ArrayList<String> eventTypes, boolean isAvailable, boolean isVisible, double price, double discount, String companyId) {
        Id = UUID.randomUUID().toString();
        CategoryId = categoryId;
        SubCategoryId = subCategoryId;
        CompanyId = companyId;
        Name = name;
        Description = description;
        Gallery = gallery;
        EventTypes = eventTypes;
        this.isAvailable = isAvailable;
        this.isVisible = isVisible;
        Price = price;
        Discount = discount;
        PriceWithDiscount = (Price * Discount) / 100;
    }

    public String getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(String companyId) {
        CompanyId = companyId;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(String categoryId) {
        CategoryId = categoryId;
    }

    public String getSubCategoryId() {
        return SubCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        SubCategoryId = subCategoryId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public ArrayList<String> getGallery() {
        return Gallery;
    }

    public void setGallery(ArrayList<String> gallery) {
        Gallery = gallery;
    }

    public ArrayList<String> getEventTypes() {
        return EventTypes;
    }

    public void setEventTypes(ArrayList<String> eventTypes) {
        EventTypes = eventTypes;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public double getDiscount() {
        return Discount;
    }

    public void setDiscount(double discount) {
        Discount = discount;
    }

    public double getPriceWithDiscount() {
        return PriceWithDiscount;
    }

    public void setPriceWithDiscount(double priceWithDiscount) {
        PriceWithDiscount = priceWithDiscount;
    }
}
