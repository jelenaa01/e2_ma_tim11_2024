package com.example.eventplanner.fragments.reservation;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.SearchView;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Reservation;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.NotificationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.PackageRepositoryInterface;
import com.example.eventplanner.repository.interfaces.ReservationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.ServiceRepositoryInterface;
import com.example.eventplanner.repository.interfaces.StaffEventRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VendorStaffReservationFragment extends Fragment {
    private static final String TAG = "VendorStaffReservationFragment";
    private List<Reservation> reservationList;
    private List<Reservation> originalReservationList;
    private VendorStaffReservationAdapter adapter;
    private DrawerLayout drawerLayout;
    private SearchView searchView;
    private CheckBox checkBoxNew;
    private CheckBox checkBoxCancelledByPup;
    private CheckBox checkBoxCancelledByOd;
    private CheckBox checkBoxCancelledByAdmin;
    private CheckBox checkBoxAccepted;
    private CheckBox checkBoxRealized;
    private Button applyButton;
    private Map<String, User> emailToUserMap;
    private Map<String, Service> serviceIdToNameMap;
    private Map<String, Package> packageIdToNameMap;
    private ReservationRepositoryInterface reservationRepository;
    private UserRepositoryInterface userRepository;
    private ServiceRepositoryInterface serviceRepository;
    private PackageRepositoryInterface packageRepository;
    private StaffEventRepositoryInterface staffEventRepository;
    private NotificationRepositoryInterface notificationRepository;
    private FirebaseAuth mAuth;

    public VendorStaffReservationFragment() {}

    public static VendorStaffReservationFragment newInstance() {
        VendorStaffReservationFragment fragment = new VendorStaffReservationFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        reservationRepository = eventPlannerApp.getDIContainer().resolve(ReservationRepositoryInterface.class);
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);
        serviceRepository = eventPlannerApp.getDIContainer().resolve(ServiceRepositoryInterface.class);
        packageRepository = eventPlannerApp.getDIContainer().resolve(PackageRepositoryInterface.class);
        staffEventRepository = eventPlannerApp.getDIContainer().resolve(StaffEventRepositoryInterface.class);
        notificationRepository = eventPlannerApp.getDIContainer().resolve(NotificationRepositoryInterface.class);

        mAuth = FirebaseAuth.getInstance();

    }

    @SuppressLint("MissingInflatedId")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_vendor_staff_reservation_list, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.staff_reservation_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        reservationList = new ArrayList<>();
        originalReservationList = new ArrayList<>();
        emailToUserMap = new HashMap<>();
        serviceIdToNameMap = new HashMap<>();
        packageIdToNameMap = new HashMap<>();
        adapter = new VendorStaffReservationAdapter(reservationList, emailToUserMap, serviceIdToNameMap, packageIdToNameMap, reservationRepository, staffEventRepository, notificationRepository);
        recyclerView.setAdapter(adapter);

        loadStaffReservations();
        populateEmailToUserMap();
        populateServiceIdToNameMap();
        populatePackageIdToNameMap();

        searchView = view.findViewById(R.id.staff_reservation_searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });

        drawerLayout = view.findViewById(R.id.staff_reservation_drawer_layout);

        checkBoxNew = view.findViewById(R.id.checkbox_new);
        checkBoxCancelledByPup = view.findViewById(R.id.checkbox_cancelled_by_pup);
        checkBoxCancelledByOd = view.findViewById(R.id.checkbox_cancelled_by_od);
        checkBoxCancelledByAdmin = view.findViewById(R.id.checkbox_cancelled_by_admin);
        checkBoxAccepted = view.findViewById(R.id.checkbox_accepted);
        checkBoxRealized = view.findViewById(R.id.checkbox_realized);
        applyButton = view.findViewById(R.id.applyButton);

        ImageButton filterButton = view.findViewById(R.id.filterButton);
        filterButton.setOnClickListener(v -> drawerLayout.openDrawer(Gravity.RIGHT));

        applyButton.setOnClickListener(v -> {
            applyFilters();
            drawerLayout.closeDrawer(Gravity.RIGHT);
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        drawerLayout = view.findViewById(R.id.staff_reservation_drawer_layout);
    }

    private void loadStaffReservations() {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null) {
            reservationRepository.getReservationByStaff(currentUser.getEmail()).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    List<Reservation> reservations = task.getResult();
                    originalReservationList.addAll(reservations);
                    reservationList.clear();
                    reservationList.addAll(reservations);
                    adapter.setOriginalReservationList(reservations);
                    adapter.notifyDataSetChanged();
                } else {
                    Log.e(TAG, "Error getting staff's reservations: ", task.getException());
                }
            });
        }

    }

    private void populateEmailToUserMap() {
        userRepository.getAllUsers().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<User> users = task.getResult();
                for (User user : users) {
                    String lowercaseEmail = user.getEmail().toLowerCase();
                    emailToUserMap.put(lowercaseEmail, user);
                }
                adapter.notifyDataSetChanged();
            } else {
                Log.e(TAG, "Error fetching users: ", task.getException());
            }
        });
    }

    private void populateServiceIdToNameMap() {
        serviceRepository.getAllServices().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<Service> services = task.getResult();
                for (Service service : services) {
                    String lowercaseId = service.getId().toLowerCase();
                    serviceIdToNameMap.put(lowercaseId, service);
                }
                adapter.notifyDataSetChanged();
            } else {
                Log.e(TAG, "Error fetching services: ", task.getException());
            }
        });
    }

    private void populatePackageIdToNameMap() {
        packageRepository.getAllPackages().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<Package> packages = task.getResult();
                for (Package pkg : packages) {
                    if (pkg != null && pkg.getId() != null) {
                        String lowercaseId = pkg.getId().toLowerCase();
                        packageIdToNameMap.put(lowercaseId, pkg);
                    } else {
                        Log.e(TAG, "Package or Package ID is null");
                    }
                }
                adapter.notifyDataSetChanged();
            } else {
                Log.e(TAG, "Error fetching packages: ", task.getException());
            }
        });
    }

    private void applyFilters() {
        List<Reservation> filteredReservations = new ArrayList<>();

        boolean isAnyCheckboxChecked = checkBoxNew.isChecked() || checkBoxCancelledByPup.isChecked() ||
                checkBoxCancelledByOd.isChecked() || checkBoxCancelledByAdmin.isChecked() ||
                checkBoxAccepted.isChecked() || checkBoxRealized.isChecked();

        if (!isAnyCheckboxChecked) {
            loadStaffReservations();
        } else {
            for (Reservation reservation : originalReservationList) {
                if ((checkBoxNew.isChecked() && reservation.getReservationStatus() == Reservation.ReservationStatus.NEW) ||
                        (checkBoxCancelledByPup.isChecked() && reservation.getReservationStatus() == Reservation.ReservationStatus.CANCELLED_BY_PUP) ||
                        (checkBoxCancelledByOd.isChecked() && reservation.getReservationStatus() == Reservation.ReservationStatus.CANCELLED_BY_ORGANIZER) ||
                        (checkBoxCancelledByAdmin.isChecked() && reservation.getReservationStatus() == Reservation.ReservationStatus.CANCELLED_BY_ADMIN) ||
                        (checkBoxAccepted.isChecked() && reservation.getReservationStatus() == Reservation.ReservationStatus.ACCEPTED) ||
                        (checkBoxRealized.isChecked() && reservation.getReservationStatus() == Reservation.ReservationStatus.REALIZED)) {
                    filteredReservations.add(reservation);
                }
            }
        }

        adapter.updateData(filteredReservations);
    }

}