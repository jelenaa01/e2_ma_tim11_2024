package com.example.eventplanner.activities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.notifications.NotificationFragment;
import com.example.eventplanner.fragments.productsOverview.ProductsOverviewFragment;
import com.example.eventplanner.fragments.authentication.RegistrationRequestsFragment;
import com.example.eventplanner.fragments.profiles.VendorStaffProfileFragment;
import com.example.eventplanner.fragments.reservation.VendorStaffReservationFragment;
import com.example.eventplanner.fragments.serviceProduct.ServiceFragment;
import com.example.eventplanner.repository.interfaces.NotificationRepositoryInterface;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.FavoriteItemRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

public class VendorStaffActivity extends AppCompatActivity {
    private static final String TAG = "VendorStaffActivity";
    private static final String CHANNEL_ID = "EVENT_PLANNER_NOTIFICATIONS";
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Toolbar toolbar;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private ImageView ivUserProfile;
    private TextView tvUserName;
    private UserRepositoryInterface userRepository;

    private NotificationRepositoryInterface notificationRepository;

    private ImageButton notificationButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_staff);

        EventPlannerApp eventPlannerApp = (EventPlannerApp) this.getApplication();
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        EventPlannerApp eventPlannerApp1 = (EventPlannerApp) getApplication();
        notificationRepository = eventPlannerApp1.getDIContainer().resolve(NotificationRepositoryInterface.class);

        toolbar = findViewById(R.id.vendor_staff_toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.vendor_staff_drawer_layout);
        navigationView = findViewById(R.id.vendor_staff_nav_view);

        View headerView = navigationView.getHeaderView(0);
        ivUserProfile = headerView.findViewById(R.id.nav_header_imageView);
        tvUserName = headerView.findViewById(R.id.nav_header_textView);

        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            String userId = currentUser.getUid();

            DocumentReference userRef = db.collection("users").document(userId);

            userRef.get().addOnSuccessListener(documentSnapshot -> {
                if (documentSnapshot.exists()) {
                    String firstName = documentSnapshot.getString("firstName");
                    String lastName = documentSnapshot.getString("lastName");
                    String profilePhotoPath = documentSnapshot.getString("profilePhotoPath");

                    tvUserName.setText(firstName + " " + lastName);

                    if (profilePhotoPath != null && !profilePhotoPath.isEmpty()) {
                        Glide.with(VendorStaffActivity.this)
                                .load(profilePhotoPath)
                                .placeholder(R.drawable.profileplaceholder)
                                .error(R.drawable.profileplaceholder)
                                .into(ivUserProfile);
                    } else {
                        ivUserProfile.setImageResource(R.drawable.profileplaceholder);
                    }
                }
            });
        }

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        );
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        // Set default fragment
        replaceFragment(new ServiceFragment(), getString(R.string.event_type_list), R.id.vendor_staff_nav_item1);
        notificationButton = findViewById(R.id.notification_button);

        // Call method to check for unread notifications and update the button
        checkAndChangeNotificationButton();

        // Set onClickListener for the notification button
        notificationButton.setOnClickListener(v -> {
            // Replace the fragment when notification button is clicked
            replaceFragment(new NotificationFragment(), "Notifications", R.id.organizer_nav_item1);
        });
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                drawerLayout.closeDrawers();

                int itemId = menuItem.getItemId();
                /*if (itemId == R.id.vendor_staff_nav_item1) {
                    replaceFragment(new RegistrationRequestsFragment(), getString(R.string.view_registration_requests), R.id.vendor_staff_nav_item1);
                } else*/ if (itemId == R.id.vendor_staff_nav_item1) {
                    replaceFragment(new VendorStaffReservationFragment(), getString(R.string.view_reservations), R.id.vendor_staff_nav_item2);
                } else if (itemId == R.id.vendor_staff_nav_item2) {
                    replaceFragment(new ServiceFragment(), getString(R.string.view_services), R.id.vendor_staff_nav_item1);
                }
                else if (itemId == R.id.vendor_staff_nav_item3) {
                    replaceFragment(new ProductsOverviewFragment(), "View Products", R.id.vendor_staff_nav_item3);
                }
                else if (itemId == R.id.vendor_staff_nav_item5) {
                    userRepository.getUserByEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<User>() {
                        @Override
                        public void onComplete(@NonNull Task<User> task) {
                            if (task.isSuccessful()) {
                                Fragment fragment = VendorStaffProfileFragment.newInstance(task.getResult());
                                FragmentManager fragmentManager = getSupportFragmentManager();
                                FragmentTransaction transaction = fragmentManager.beginTransaction();
                                transaction.replace(R.id.vendor_staff_fragment_container, fragment);
                                transaction.addToBackStack(null);
                                transaction.commit();
                            } else {
                                Log.e("CompanyOverviewFragment", "Failed to get logged in vendor: " + task.getException());
                            }
                        }
                    });
                }
                else if (itemId == R.id.vendor_staff_nav_item4) {
                    mAuth.signOut();
                    Intent intent = new Intent(VendorStaffActivity.this, UnauthActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }

                return true;
            }
        });

        // Create notification channel
        createNotificationChannel();

        // Listen for unread notifications
        listenForCreatedNotifications();

        // Listen for shown notifications
        listenForShownNotifications();
    }

    private void replaceFragment(Fragment fragment, String title, int menuItemId) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.vendor_staff_fragment_container, fragment)
                .commit();

        setTitle(title);
    }
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Event Planner Notifications";
            String description = "Channel for Event Planner notifications";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void listenForCreatedNotifications() {
        notificationRepository.listenForCreatedNotifications((snapshots, e) -> {
            if (e != null) {
                Log.e(TAG, "listenForCreatedNotifications: Error listening for notifications", e);
                return;
            }

            if (snapshots != null && !snapshots.isEmpty()) {
                Log.d(TAG, "listenForCreatedNotifications: Unread notifications found");

                // Change the image button when a new notification is found
                notificationButton.setImageResource(R.drawable.notificationactive);

                for (QueryDocumentSnapshot document : snapshots) {
                    // Retrieve notification content from the document snapshot
                    String title = document.getString("title");
                    String message = document.getString("text");

                    // Show the notification
                    showNotification(title, message);

                    // Update the notification as read
                    String notificationId = document.getId();
                    notificationRepository.setNotificationAsShown(notificationId);
                }
            } else {
                Log.d(TAG, "listenForCreatedNotifications: No unread notifications");

            }
        });
    }

    private void listenForShownNotifications() {
        notificationRepository.listenForShownNotifications((snapshots, e) -> {
            if (e != null) {
                Log.e(TAG, "listenForShownNotifications: Error listening for notifications", e);
                return;
            }

            if (snapshots != null && !snapshots.isEmpty()) {
                Log.d(TAG, "listenForShownNotifications: Shown notifications found");
                // Change the image button when a "shown" notification is found
                notificationButton.setImageResource(R.drawable.notificationactive);
            } else {
                Log.d(TAG, "listenForShownNotifications: No shown notifications");
                // Change the image button back to the default state if no "shown" notifications are found
                notificationButton.setImageResource(R.drawable.notification);
            }
        });
    }

    private void checkAndChangeNotificationButton() {
        notificationRepository.getLoggedUserUnreadNotifications()
                .addOnSuccessListener(notifications -> {
                    if (!notifications.isEmpty()) {
                        // Change the notification button image if there are unread notifications
                        notificationButton.setImageResource(R.drawable.notificationactive);
                    } else {
                        // Change the notification button image to default if there are no unread notifications
                        notificationButton.setImageResource(R.drawable.notification);
                    }
                })
                .addOnFailureListener(e -> {
                    // Handle failure to retrieve notifications
                    Log.e(TAG, "checkAndChangeNotificationButton: Error retrieving unread notifications", e);
                });
    }
    private void showNotification(String title, String message) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.calendar)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, builder.build());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        notificationRepository.removeListener();
    }
}