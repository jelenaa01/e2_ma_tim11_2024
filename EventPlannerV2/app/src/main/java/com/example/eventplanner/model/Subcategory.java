package com.example.eventplanner.model;

import java.io.Serializable;

public class Subcategory implements Serializable {

    public enum Type {
        SERVICE,
        PRODUCT
    }

    private int id;
    private String name;
    private String description;
    private Type type;
    private int categoryId;
    private boolean isActive;

    public Subcategory(int id, String name, String description, Type type, int categoryId, boolean isActive) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.type = type;
        this.categoryId = categoryId;
        this.isActive = isActive;
    }

    // Getters and Setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getCategoryId() {
        return categoryId;
    }
    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
