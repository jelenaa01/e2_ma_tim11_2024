package com.example.eventplanner.repository.interfaces;

import android.util.Log;

import com.example.eventplanner.model.Budget;
import com.example.eventplanner.model.OrganiserEvent;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;

public interface BudgetRepositoryInterface {
    void add(Budget budget);
    Task<Budget> getBudgetById(String budgetId);
    Task<Void> addProductToBudget(String budgetId, String productID, double addedPrice);
}
