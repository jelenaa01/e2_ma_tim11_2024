package com.example.eventplanner.fragments.productsOverview;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.FavoriteItem;
import com.example.eventplanner.model.OrganiserEvent;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.ProductRepository;
import com.example.eventplanner.repository.interfaces.BudgetRepositoryInterface;
import com.example.eventplanner.repository.interfaces.CompanyRepositoryInterface;
import com.example.eventplanner.repository.interfaces.FavoriteItemRepositoryInterface;
import com.example.eventplanner.repository.interfaces.OrganiserEventRepositoryInterface;
import com.example.eventplanner.repository.interfaces.ProductRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.example.eventplanner.utils.FragmentUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class ProductOverviewFragment extends Fragment {

    private static final String ARG_SELECTED_PRODUCT = "selected-product";

    private Product selectedProduct;
    private ProductRepositoryInterface productRepository;
    private CompanyRepositoryInterface companyRepository;
    private User loggedInUser;
    private UserRepositoryInterface userRepository;
    private FavoriteItemRepositoryInterface favoriteItemRepository;

    private BudgetRepositoryInterface budgetRepository;
    private OrganiserEventRepositoryInterface organiserEventRepository;
    private FirebaseAuth mAuth;

    private Button addToBudgetButton;

    private Button editPriceButton;
    public ProductOverviewFragment() { mAuth = FirebaseAuth.getInstance(); }

    public static ProductOverviewFragment newInstance(Product selectedProduct) {
        ProductOverviewFragment fragment = new ProductOverviewFragment();

        Bundle args = new Bundle();
        args.putSerializable(ARG_SELECTED_PRODUCT, selectedProduct);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            selectedProduct = (Product) getArguments().getSerializable(ARG_SELECTED_PRODUCT);
        }

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        productRepository = eventPlannerApp.getDIContainer().resolve(ProductRepositoryInterface.class);
        companyRepository = eventPlannerApp.getDIContainer().resolve(CompanyRepositoryInterface.class);
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);
        favoriteItemRepository = eventPlannerApp.getDIContainer().resolve(FavoriteItemRepositoryInterface.class);
        budgetRepository = eventPlannerApp.getDIContainer().resolve(BudgetRepositoryInterface.class);
        organiserEventRepository = eventPlannerApp.getDIContainer().resolve(OrganiserEventRepositoryInterface.class);

        getLoggedInUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vendor_product_details, container, false);

        if (selectedProduct == null){
            Log.e("VendorProductDetailsFragment", "Error getting documents: ");
        }

        TextView tvName = view.findViewById(R.id.nameTextView2);
        TextView tvDescription = view.findViewById(R.id.descriptionTextView2);
        TextView tvPrice = view.findViewById(R.id.priceTextView2);
        TextView tvCompany = view.findViewById(R.id.companyTextView2);
        TextView tvVendor = view.findViewById(R.id.vendorTextView2);
        TextView tvAvailabilty = view.findViewById(R.id.availabilityTextView2);

        tvName.setText(selectedProduct.getName());
        tvDescription.setText(selectedProduct.getDescription());
        tvPrice.setText(String.valueOf(selectedProduct.getPrice()));
        tvCompany.setText(selectedProduct.getCompanyId());
        if (!selectedProduct.isAvailable()) {
            tvAvailabilty.setText("This product currently isn't available");
        }

        companyRepository.getVendorByCompanyName(selectedProduct.getCompanyId())
                .addOnSuccessListener(new OnSuccessListener<User>() {
                    @Override
                    public void onSuccess(User user) {
                        String name = user.getFirstName();
                        String lastName = user.getLastName();
                        String fullName = name + " " + lastName;
                        tvVendor.setText(fullName);
                    }
                });

        tvCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Navigate to Company Overview page
                navigateToCompanyOverview(selectedProduct.getCompanyId());
            }
        });

        Button addProductToFavoritesButton = view.findViewById(R.id.addProductToFavoritesButton);
        addProductToFavoritesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addProductToFavorites();
                addProductToFavoritesButton.setVisibility(View.GONE);
            }
        });

        favoriteItemRepository.existsByNameAndEmail(selectedProduct.getName(), mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<Boolean>() {
            @Override
            public void onComplete(@NonNull Task<Boolean> task) {
                if (task.isSuccessful()) {
                    boolean isItemInFavorites = task.getResult();
                    if (isItemInFavorites) {
                        addProductToFavoritesButton.setVisibility(View.GONE);
                    } else {
                        addProductToFavoritesButton.setVisibility(View.VISIBLE);
                    }
                } else {
                    Log.e("TAG", "Error getting favorite items", task.getException());
                }
            }
        });

        addToBudgetButton = view.findViewById(R.id.addToBudgetButton);
        addToBudgetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEventSelectionDialog();
            }
        });

        editPriceButton = view.findViewById(R.id.editPrice);
        editPriceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditPriceDialog dialog = new EditPriceDialog(selectedProduct, productRepository);
                dialog.show(getParentFragmentManager(), "EditPriceDialog");
            }
        });
        return view;
    }

    private void showEventSelectionDialog() {
        if (mAuth.getCurrentUser() != null) {
            organiserEventRepository.getByOrganiserEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<List<OrganiserEvent>>() {
                @Override
                public void onComplete(@NonNull Task<List<OrganiserEvent>> task) {
                    if (task.isSuccessful()) {
                        List<OrganiserEvent> events = task.getResult();
                        double newPrice = 0;

                        if(selectedProduct.getDiscount() > 0)
                        {
                            newPrice = selectedProduct.getPrice() - selectedProduct.getPrice() * selectedProduct.getDiscount() / 100;
                        }
                        else
                        {
                            newPrice = selectedProduct.getPrice();
                        }


                        EventSelectionDialog dialog = new EventSelectionDialog(budgetRepository, organiserEventRepository, selectedProduct.getId(), newPrice);
                        dialog.show(getParentFragmentManager(), "EventSelectionDialog");
                    } else {
                        Log.e("ProductOverviewFragment", "Error fetching events", task.getException());
                    }
                }
            });
        }
    }

    private void navigateToCompanyOverview(String companyId) {
        if (companyRepository == null || selectedProduct == null || selectedProduct.getCompanyId() == null) {
            Log.e("navigateToCompanyOverview", "Required data is missing.");
            return;
        }

        companyRepository.getCompanyByName(selectedProduct.getCompanyId())
                .addOnSuccessListener(new OnSuccessListener<Company>() {
                    @Override
                    public void onSuccess(Company company) {
                        CompanyOverviewFragment companyOverviewFragment = CompanyOverviewFragment.newInstance(company);

                        FragmentTransaction transaction = getParentFragmentManager().beginTransaction();

                        if (loggedInUser != null) {
                            switch (loggedInUser.getRole()) {
                                case VENDOR:
                                    transaction.replace(R.id.vendor_fragment_container, companyOverviewFragment);
                                    break;
                                case ORGANISER:
                                    transaction.replace(R.id.organizer_fragment_container, companyOverviewFragment);
                                    break;
                                case VENDOR_STAFF:
                                    transaction.replace(R.id.vendor_staff_fragment_container, companyOverviewFragment);
                                    break;
                                case ADMIN:
                                    transaction.replace(R.id.admin_fragment_container, companyOverviewFragment);
                                    break;
                                default:
                                    Log.e("navigateToCompanyOverview", "Unknown user role.");
                                    return;
                            }

                            transaction.addToBackStack(null);
                            transaction.commit();
                        } else {
                            Log.e("navigateToCompanyOverview", "Logged in user is null.");
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("navigateToCompanyOverview", "Failed to get company: ", e);
                    }
                });
    }


    private void navigateToVendorOverview(String companyId) {
        companyRepository.getVendorByCompanyName(selectedProduct.getCompanyId())
                .addOnSuccessListener(new OnSuccessListener<User>() {
                    @Override
                    public void onSuccess(User user) {
                        VendorOverviewFragment vendorOverviewFragment = new VendorOverviewFragment().newInstance(user);

                        FragmentTransaction transaction = getParentFragmentManager().beginTransaction();
                        transaction.replace(R.id.vendor_fragment_container, vendorOverviewFragment);

                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                });
    }

    private void getLoggedInUser() {
        if (mAuth.getCurrentUser() != null) {
            userRepository.getUserByEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<User>() {
                @Override
                public void onComplete(@NonNull Task<User> task) {
                    if (task.isSuccessful()) {
                        loggedInUser = task.getResult();

                        if(loggedInUser.getRole() != User.UserRole.ORGANISER)
                        {
                            addToBudgetButton.setVisibility(View.INVISIBLE);

                        }
                        if(!(loggedInUser.getRole() == User.UserRole.VENDOR || loggedInUser.getRole() == User.UserRole.VENDOR_STAFF)) {
                            editPriceButton.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        Log.e("CompanyOverviewFragment", "Failed to get logged in vendor: " + task.getException());
                    }
                }
            });
        }
    }

    private void addProductToFavorites() {
        if (mAuth.getCurrentUser() != null) {
            FavoriteItem favoriteItem = new FavoriteItem();
            favoriteItem.setUserEmail(loggedInUser.getEmail());
            favoriteItem.setType("product");
            favoriteItem.setItemName(selectedProduct.getName());
            favoriteItemRepository.addFavoriteItem(favoriteItem);
        }
    }
}