package com.example.eventplanner.fragments.serviceProduct;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.NonNull;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.repository.ServiceRepository;
import com.example.eventplanner.repository.interfaces.ServiceRepositoryInterface;

public class EditPriceDialog extends Dialog {

    private EditText editTextPrice;
    private EditText editTextDiscount;
    private Button buttonUpdate;
    private Service service;
    private ServiceRepositoryInterface serviceRepository;

    public EditPriceDialog(@NonNull Context context, Service service, ServiceRepositoryInterface serviceRepository) {
        super(context);
        this.service = service;
        this.serviceRepository = serviceRepository;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_price_dialog);

        editTextPrice = findViewById(R.id.editTextPrice);
        editTextDiscount = findViewById(R.id.editTextDiscount);
        buttonUpdate = findViewById(R.id.buttonUpdate);

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get new price and discount values
                double newPrice = Double.parseDouble(editTextPrice.getText().toString());
                int newDiscount = Integer.parseInt(editTextDiscount.getText().toString());

                // Update the service with new values
                service.setTotalPrice(newPrice);
                service.setDiscount(newDiscount);

                // Call the repository to update the service
                serviceRepository.updateService(service);

                // Dismiss the dialog
                dismiss();
            }
        });
    }
}
