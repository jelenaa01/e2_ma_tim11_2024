package com.example.eventplanner.fragments.servicesOverview;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.repository.interfaces.ServiceRepositoryInterface;

import java.util.ArrayList;
import java.util.List;

public class ServicesRecyclerViewAdapter extends RecyclerView.Adapter<ServicesRecyclerViewAdapter.ViewHolder> {

    private final List<Service> services;
    private final ServiceRepositoryInterface serviceRepository;
    private OnItemClickListener listener;

    public ServicesRecyclerViewAdapter(List<Service> services, ServiceRepositoryInterface serviceRepository) {
        this.services = new ArrayList<>(services);
        this.serviceRepository = serviceRepository;
    }

    @NonNull
    @Override
    public ServicesRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_item, parent, false);
        return new ServicesRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServicesRecyclerViewAdapter.ViewHolder holder, int position) {
        Service service = services.get(position);
        holder.tvName.setText(service.getName());
        holder.tvCategory.setText(service.getCategory());
        holder.bind(service, listener);
    }

    @Override
    public int getItemCount() {
        return services.size();
    }

    public void setOnItemClickListener(ServicesRecyclerViewAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(Service service);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.serviceNameTextView1);
            tvCategory = itemView.findViewById(R.id.serviceDescriptionTextView1);
        }

        public void bind(final Service service, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(service);
                }
            });
        }
    }
}
