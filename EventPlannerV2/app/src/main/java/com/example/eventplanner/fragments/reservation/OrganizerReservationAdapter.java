package com.example.eventplanner.fragments.reservation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Reservation;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.NotificationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.ReservationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.StaffEventRepositoryInterface;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

public class OrganizerReservationAdapter extends RecyclerView.Adapter<OrganizerReservationAdapter.ViewHolder> {
    private static final String TAG = "OrganizerReservationAdapter";
    private List<Reservation> reservationList;
    private Map<String, User> emailToUserMap;
    private Map<String, Service> serviceIdToNameMap;
    private Map<String, Package> packageIdToNameMap;
    private OrganizerReservationAdapter.OnItemClickListener listener;
    private ReservationRepositoryInterface reservationRepository;
    private StaffEventRepositoryInterface staffEventRepository;
    private NotificationRepositoryInterface notificationRepository;

    public OrganizerReservationAdapter(List<Reservation> items, Map<String, User> emailToUserMap, Map<String, Service> serviceIdToNameMap, Map<String, Package> packageIdToNameMap,
                                       ReservationRepositoryInterface reservationRepository, StaffEventRepositoryInterface staffEventRepository, NotificationRepositoryInterface notificationRepository) {
        this.reservationList = items;
        this.emailToUserMap = emailToUserMap;
        this.serviceIdToNameMap = serviceIdToNameMap;
        this.packageIdToNameMap = packageIdToNameMap;
        this.reservationRepository = reservationRepository;
        this.staffEventRepository = staffEventRepository;
        this.notificationRepository = notificationRepository;
    }

    @NonNull
    @Override
    public OrganizerReservationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reservation_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrganizerReservationAdapter.ViewHolder holder, int position) {
        Reservation reservation = reservationList.get(position);

        User staff = emailToUserMap.get(reservation.getVendorStaffEmail().toLowerCase());
        Service service = serviceIdToNameMap.get(reservation.getServiceId().toLowerCase());
        Package packageObj = packageIdToNameMap.get(reservation.getPackageId().toLowerCase());

        String staffFullName = (staff != null) ? staff.getFirstName() + " " + staff.getLastName() : "";
        String serviceName = (service != null) ? service.getName() : "";
        String packageName = (packageObj != null) ? packageObj.getName() : "";

        holder.serviceName.setText("Service: " + serviceName);
        holder.packageName.setText("Package: " + packageName);
        holder.organizerFullName.setVisibility(View.GONE);
        holder.staffFullName.setText("Staff: " + staffFullName);
        holder.status.setText("Status: " + reservation.getReservationStatus().toString());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        String formattedDate = dateFormat.format(reservation.getDate());
        String formattedStartTime = formatTime(reservation.getStartTime());
        String formattedEndTime = formatTime(reservation.getEndTime());


        holder.date.setText("Date: " + formattedDate);
        holder.time.setText("Time: " + formattedStartTime + "-" + formattedEndTime);

        holder.btnDecline.setOnClickListener(v -> {
            if(listener != null) {
                listener.onItemClick(reservation);
            }
            if(reservation.getReservationStatus() == Reservation.ReservationStatus.NEW || reservation.getReservationStatus() == Reservation.ReservationStatus.ACCEPTED) {
                int canellationDeadline = getCancellationDeadline(reservation);
                if(canCancelReservation(reservation.getDate().toString(), canellationDeadline)) {
                    reservationRepository.changeReservationStatus(reservation.getId(), Reservation.ReservationStatus.CANCELLED_BY_ORGANIZER);
                    updateReservationStatus(reservation.getId(), Reservation.ReservationStatus.CANCELLED_BY_ORGANIZER);
                    Log.d(TAG, "Reservation successfully canceled");

                    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                    if(currentUser != null) {
                        Notification notification = new Notification(UUID.randomUUID().toString(), "Reservation cancellation", "Organizer " + currentUser.getEmail() + " canceled reservation", reservation.getVendorStaffEmail(), Notification.NotificationStatus.CREATED, Timestamp.now());
                        notificationRepository.createNotification(notification);
                    }

                    String staffEventId = reservation.getStaffEventId();
                    if(staffEventId != null) {
                        staffEventRepository.removeStaffEvent(staffEventId);
                        Log.d(TAG, "Successfully remove staff event");
                        reservationRepository.updateReservationStaffEventId(reservation.getId(), "");
                        Log.d(TAG, "Staff event id successfully remove from reservation");
                    } else {
                        Log.d(TAG, "Event staff id not found");
                    }

                } else {
                    Log.d(TAG, "Error during canceling reservation");
                }
            }
        });

        holder.btnAccept.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return reservationList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView serviceName;
        public TextView packageName;
        public TextView staffFullName;
        public TextView organizerFullName;
        public TextView status;
        public TextView date;
        public TextView time;
        public ImageButton btnDecline;
        public ImageButton btnAccept;

        public ViewHolder(View view) {
            super(view);
            serviceName = view.findViewById(R.id.reservation_serviceName);
            packageName = view.findViewById(R.id.reservation_packageName);
            staffFullName = view.findViewById(R.id.reservation_staff);
            organizerFullName = view.findViewById(R.id.reservation_organizer);
            status = view.findViewById(R.id.reservation_status);
            date = view.findViewById(R.id.reservation_date);
            time = view.findViewById(R.id.reservation_time);
            btnDecline = view.findViewById(R.id.reservation_btnDecline);
            btnAccept = view.findViewById(R.id.reservation_btnAccept);
        }
    }

    public void setOnItemClickListener(OrganizerReservationAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(Reservation reservation);
    }

    private String formatTime(String time) {
        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            Date date = inputFormat.parse(time);

            SimpleDateFormat outputFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            return outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return time;
        }
    }

    public void updateData(List<Reservation> newReservations) {
        reservationList.clear();
        reservationList.addAll(newReservations);
        notifyDataSetChanged();
    }


    private int getCancellationDeadline(Reservation reservation) {
        if (reservation.getPackageId() != null) {
            Package packageObj = packageIdToNameMap.get(reservation.getPackageId().toLowerCase());
            if (packageObj != null) {
                return packageObj.getCancellationDeadline();
            }
        } else {
            Service service = serviceIdToNameMap.get(reservation.getServiceId().toLowerCase());
            if (service != null) {
                return service.getCancellationDeadline();
            }
        }
        return 0;
    }

    private boolean canCancelReservation(String reservationDate, int cancellationDeadlineDays) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
        try {
            Date reservationDateObj = dateFormat.parse(reservationDate);

            long currentTimeMillis = System.currentTimeMillis();

            long deadlineSeconds = cancellationDeadlineDays * 24 * 60 * 60;
            long deadlineMillis = deadlineSeconds * 1000;

            long deadlineDateMillis = reservationDateObj.getTime() - deadlineMillis;

            if (currentTimeMillis <= deadlineDateMillis) {
                Log.d(TAG, "Cancellation deadline hasn't passed");
                return true;
            } else {
                Log.d(TAG, "Cancellation deadline has passed");
                return false;
            }
        } catch (ParseException e) {
            Log.e(TAG, "Error parsing reservation date: " + e.getMessage());
            return false;
        }
    }

    public void updateReservationStatus(String reservationId, Reservation.ReservationStatus newStatus) {
        for (int i = 0; i < reservationList.size(); i++) {
            Reservation reservation = reservationList.get(i);
            if (reservation.getId().equals(reservationId)) {
                reservation.setReservationStatus(newStatus);
                notifyItemChanged(i);
                break;
            }
        }
    }

}
