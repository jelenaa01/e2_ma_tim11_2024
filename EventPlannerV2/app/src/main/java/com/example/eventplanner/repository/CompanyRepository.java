package com.example.eventplanner.repository;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.CompanyRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Transaction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CompanyRepository implements CompanyRepositoryInterface {
    private static final String TAG = "CompanyRepository";
    private FirebaseFirestore db;
    private UserRepository userRepository;

    public CompanyRepository(FirebaseFirestore firestore) {
        this.db = firestore;
        userRepository = new UserRepository(firestore);
    }

    @Override
    public Task<Company> getCompanyByOwnerEmail(String ownerEmail) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("companies")
                .whereEqualTo("ownerEmail", ownerEmail)
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
                            return documentSnapshot.toObject(Company.class);
                        }
                    }
                    return null;
                });
    }


    @Override
    public void addCompany(Company company) {
        db.collection("companies")
                .add(company)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Company successfully added");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error adding company", e);
                });
    }

    @Override
    public Task<List<User>> getCompanyEmployees(String ownerEmail) {
        return getCompanyByOwnerEmail(ownerEmail).onSuccessTask(company -> {
            if (company != null) {
                return Tasks.whenAllSuccess(company.getEmployeeEmails().stream()
                        .map(email -> userRepository.getUserByEmail(email))
                        .collect(Collectors.toList()));
            }
            return Tasks.forResult(Collections.emptyList());
        });
    }

    @Override
    public void addEmployeeToCompany(String ownerEmail, String employeeEmail) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("companies")
                .whereEqualTo("ownerEmail", ownerEmail)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        DocumentSnapshot documentSnapshot = queryDocumentSnapshots.getDocuments().get(0);
                        String companyId = documentSnapshot.getId();
                        DocumentReference companyRef = db.collection("companies").document(companyId);

                        db.runTransaction((Transaction.Function<Void>) transaction -> {
                            DocumentSnapshot snapshot = transaction.get(companyRef);
                            Company company = snapshot.toObject(Company.class);

                            if (company != null) {
                                if (company.getEmployeeEmails() == null) {
                                    company.setEmployeeEmails(new ArrayList<>());
                                }
                                company.getEmployeeEmails().add(employeeEmail);
                                transaction.update(companyRef, "employeeEmails", company.getEmployeeEmails());
                            } else {
                                throw new FirebaseFirestoreException("Company not found", FirebaseFirestoreException.Code.NOT_FOUND);
                            }

                            return null;
                        }).addOnSuccessListener(aVoid -> {
                            Log.d(TAG, "Employee added to company successfully");
                        }).addOnFailureListener(e -> {
                            Log.e(TAG, "Error adding employee to company", e);
                        });
                    } else {
                        Log.d(TAG, "No company found for owner: " + ownerEmail);
                    }
                }).addOnFailureListener(e -> {
                    Log.e(TAG, "Error finding company", e);
                });
    }

    @Override
    public Task<Company> getCompanyByName(String companyName) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("companies")
                .whereEqualTo("name", companyName)
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
                            return documentSnapshot.toObject(Company.class);
                        }
                    }
                    return null;
                });
    }

    @Override
    public Task<Void> updateCompany(Company company) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        return db.collection("companies")
                .whereEqualTo("email", company.getEmail())
                .get()
                .continueWithTask(task -> {
                    if (task.isSuccessful() && !task.getResult().isEmpty()) {
                        DocumentSnapshot documentSnapshot = task.getResult().getDocuments().get(0);
                        DocumentReference companyRef = documentSnapshot.getReference();
                        return companyRef.set(company);
                    } else {
                        throw new Exception("Company not found or multiple companies found with the same name");
                    }
                })
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Company successfully updated");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error updating company", e);
                });
    }

    @Override
    public Task<User> getVendorByCompanyName(String companyName) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        // First, get the company by its name
        return getCompanyByName(companyName).continueWithTask(companyTask -> {
            Company company = companyTask.getResult();

            if (company != null) {
                // If the company is found, get the vendor by the company's owner email
                return db.collection("users")
                        .whereEqualTo("email", company.getOwnerEmail())
                        .get()
                        .continueWith(task -> {
                            if (task.isSuccessful()) {
                                QuerySnapshot querySnapshot = task.getResult();
                                if (querySnapshot != null && !querySnapshot.isEmpty()) {
                                    DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
                                    return documentSnapshot.toObject(User.class);
                                }
                            }
                            return null;
                        });
            } else {
                // If no company is found, return a failed task
                return Tasks.forException(new Exception("Company not found"));
            }
        });
    }
}
