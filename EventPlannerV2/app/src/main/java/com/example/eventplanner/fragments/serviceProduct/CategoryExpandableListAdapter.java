package com.example.eventplanner.fragments.serviceProduct;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Category;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CategoryExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<Category> listDataHeader; // header titles
    private HashMap<String, List<String>> listDataChild; // child data
    private List<String> selectedCategories = new ArrayList<>();
    private HashMap<String, List<String>> selectedSubcategories = new HashMap<>();

    public CategoryExpandableListAdapter(Context context, List<Category> listDataHeader,
                                         HashMap<String, List<String>> listChildData) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        this.listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.listDataChild.get(this.listDataHeader.get(groupPosition).getName())
                .get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.filter_list_item, null);
        }

        CheckBox checkBox = convertView.findViewById(R.id.checkbox);
        checkBox.setText(childText);

        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                if (!selectedCategories.contains(listDataHeader.get(groupPosition).getName())) {
                    selectedCategories.add(listDataHeader.get(groupPosition).getName());
                    selectedSubcategories.put(listDataHeader.get(groupPosition).getName(), new ArrayList<>());
                }
                selectedSubcategories.get(listDataHeader.get(groupPosition).getName()).add(childText);
            } else {
                selectedSubcategories.get(listDataHeader.get(groupPosition).getName()).remove(childText);
                if (selectedSubcategories.get(listDataHeader.get(groupPosition).getName()).isEmpty()) {
                    selectedCategories.remove(listDataHeader.get(groupPosition).getName());
                    selectedSubcategories.remove(listDataHeader.get(groupPosition).getName());
                }
            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.listDataChild.get(this.listDataHeader.get(groupPosition).getName())
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = ((Category) getGroup(groupPosition)).getName();
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.filter_list_group, null);
        }

        TextView lblListHeader = convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public List<String> getSelectedCategories() {
        return selectedCategories;
    }

    public HashMap<String, List<String>> getSelectedSubcategories() {
        return selectedSubcategories;
    }
}

