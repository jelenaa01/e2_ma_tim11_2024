package com.example.eventplanner.fragments.productsOverview;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.companyRating.AddCompanyRatingFragment;
import com.example.eventplanner.fragments.companyRating.CompanyRatingAdapter;
import com.example.eventplanner.fragments.event.AddStaffEventFragment;
import com.example.eventplanner.fragments.messages.ChatFragment;
import com.example.eventplanner.fragments.reports.ReportCompanyFragment;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.CompanyRating;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.CompanyRatingRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.example.eventplanner.utils.FragmentUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CompanyOverviewFragment extends Fragment {

    private static final String ARG_SELECTED_COMPANY = "selected-company";
    private Company selectedCompany;
    private User loggedInUser;
    private UserRepositoryInterface userRepository;
    private FirebaseAuth mAuth;
    Button reportCompanyButton, rateCompanyButton, messageCompanyButton;
    private CompanyRatingAdapter adapter;
    private List<CompanyRating> companyRatingList;
    private Map<String, User> emailToUserMap;
    private CompanyRatingRepositoryInterface companyRatingRepository;

    public CompanyOverviewFragment() {
        mAuth = FirebaseAuth.getInstance();
    }

    public static CompanyOverviewFragment newInstance(Company selectedCompany) {
        CompanyOverviewFragment fragment = new CompanyOverviewFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_SELECTED_COMPANY, selectedCompany);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            selectedCompany = (Company) getArguments().getSerializable(ARG_SELECTED_COMPANY);
        }

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);
        companyRatingRepository = eventPlannerApp.getDIContainer().resolve(CompanyRatingRepositoryInterface.class);
    }

    @Nullable
    @Override
    @SuppressLint("MissingInflatedId")
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_company_overview, container, false);
        if (selectedCompany == null) {
            Log.e("CompanyOverviewFragment", "Selected company is null");
            return view;
        }

        TextView tvName = view.findViewById(R.id.name3);
        TextView tvEmail = view.findViewById(R.id.email3);
        TextView tvAddress = view.findViewById(R.id.address3);
        TextView tvPhone = view.findViewById(R.id.phone3);
        TextView tvDescription = view.findViewById(R.id.description3);

        tvName.setText(selectedCompany.getName());
        tvEmail.setText(selectedCompany.getEmail());
        tvAddress.setText(selectedCompany.getAddress());
        tvPhone.setText(selectedCompany.getPhoneNumber());
        tvDescription.setText(selectedCompany.getDescription());

        getLoggedInUser();
        updateViewForLoggedInUser(view);

        RecyclerView recyclerView = view.findViewById(R.id.company_rating_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        companyRatingList = new ArrayList<>();
        emailToUserMap = new HashMap<>();
        adapter = new CompanyRatingAdapter(companyRatingList, emailToUserMap);
        recyclerView.setAdapter(adapter);

        loadCompanyRatings();
        populateEmailToUserMap();

        return view;
    }

    private void updateViewForLoggedInUser(View view) {
        if (loggedInUser != null && loggedInUser.getRole() == User.UserRole.ORGANISER) {
            reportCompanyButton = view.findViewById(R.id.reportCompanyButton);
            reportCompanyButton.setVisibility(View.VISIBLE);
            reportCompanyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigateToReportCompanyPage();
                }
            });

            rateCompanyButton = view.findViewById(R.id.rateCompanyBtn);
            rateCompanyButton.setVisibility(View.VISIBLE);
            rateCompanyButton.setOnClickListener(v -> FragmentUtils.replaceFragment(
                    getParentFragmentManager(),
                    AddCompanyRatingFragment.newInstance(selectedCompany),
                    R.id.organizer_fragment_container,
                    (NavigationView) getActivity().findViewById(R.id.organizer_nav_view),
                    R.id.organizer_nav_item4,
                    getString(R.string.add_comapny_rating)
            ));

            messageCompanyButton = view.findViewById(R.id.messageCompanyBtn);
            messageCompanyButton.setVisibility(View.VISIBLE);
            messageCompanyButton.setOnClickListener(v -> FragmentUtils.replaceFragment(
                    getParentFragmentManager(),
                    ChatFragment.newInstance(selectedCompany.getOwnerEmail()),
                    R.id.organizer_fragment_container,
                    (NavigationView) getActivity().findViewById(R.id.organizer_nav_view),
                    R.id.organizer_nav_item4,
                    selectedCompany.getOwnerEmail()
            ));
        }
    }

    private void navigateToReportCompanyPage() {
        ReportCompanyFragment fragment = ReportCompanyFragment.newInstance(selectedCompany);
        FragmentTransaction transaction = getParentFragmentManager().beginTransaction();

        if (loggedInUser != null) {
            switch (loggedInUser.getRole()) {
                case VENDOR:
                    transaction.replace(R.id.vendor_fragment_container, fragment);
                    break;
                case ORGANISER:
                    transaction.replace(R.id.organizer_fragment_container, fragment);
                    break;
                case VENDOR_STAFF:
                    transaction.replace(R.id.vendor_staff_fragment_container, fragment);
                    break;
                case ADMIN:
                    transaction.replace(R.id.admin_fragment_container, fragment);
                    break;
                default:
                    Log.e("navigateToCompanyOverview", "Unknown user role.");
                    return;
                }
            }

        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void getLoggedInUser() {
        if (mAuth.getCurrentUser() != null) {
            userRepository.getUserByEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<User>() {
                @Override
                public void onComplete(@NonNull Task<User> task) {
                    if (task.isSuccessful()) {
                        loggedInUser = task.getResult();
                        // Make sure the view is updated after getting the user data
                        if (getView() != null) {
                            updateViewForLoggedInUser(getView());
                        }
                    } else {
                        Log.e("CompanyOverviewFragment", "Failed to get logged in user: " + task.getException());
                    }
                }
            });
        }
    }

    private void loadCompanyRatings() {
         if(selectedCompany != null) {
             companyRatingRepository.getRatingsByCompany(selectedCompany.getId())
                     .addOnCompleteListener(ratingTask -> {
                         if(ratingTask.isSuccessful()) {
                                List<CompanyRating> companyRatings = ratingTask.getResult();
                                companyRatingList.clear();
                                companyRatingList.addAll(companyRatings);
                                adapter.notifyDataSetChanged();
                         } else {
                                Log.e("CompanyOverviewFragment", "Error getting ratings: ", ratingTask.getException());
                         }
                     });
         } else {
             Log.e("CompanyOverviewFragment", "Company not found ");
         }
    }

    private void populateEmailToUserMap() {
        userRepository.getAllUsers().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<User> users = task.getResult();
                for (User user : users) {
                    String lowercaseEmail = user.getEmail().toLowerCase();
                    emailToUserMap.put(lowercaseEmail, user);
                }
                adapter.notifyDataSetChanged();
            } else {
                Log.e("CompanyOverviewFragment", "Error fetching users: ", task.getException());
            }
        });
    }

}
