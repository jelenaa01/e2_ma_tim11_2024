package com.example.eventplanner.fragments.typesCategories;

import android.app.AlertDialog;
import android.database.Cursor;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.eventplanner.R;
import com.example.eventplanner.database.DBHandler;
import com.example.eventplanner.model.EventType;

import java.util.ArrayList;
import java.util.List;

public class EventTypeFragment extends Fragment {

    private List<EventType> eventTypes;
    private EventTypeRecyclerViewAdapter adapter;
    private DBHandler dbHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.event_type_list, container, false);

        dbHandler = DBHandler.getInstance(getContext());

        RecyclerView recyclerView = view.findViewById(R.id.eventTypesRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        eventTypes = getAllEventTypesFromDatabase();

        adapter = new EventTypeRecyclerViewAdapter(eventTypes, getContext());
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new EventTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(EventType eventType) {
                // Handle item click if needed
            }
        });

        // Floating Action Button to add new EventType
        view.findViewById(R.id.fabAddEventType).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddEventTypeDialog();
            }
        });

        return view;
    }

    private List<EventType> getAllEventTypesFromDatabase() {
        List<EventType> eventTypes = new ArrayList<>();
        Cursor cursor = dbHandler.getAllEventTypes();

        if (cursor != null && cursor.moveToFirst()) {
            do {
                int idIndex = cursor.getColumnIndex(DBHandler.EVENT_TYPE_ID_COL);
                int nameIndex = cursor.getColumnIndex(DBHandler.EVENT_TYPE_NAME_COL);
                int descriptionIndex = cursor.getColumnIndex(DBHandler.EVENT_TYPE_DESCRIPTION_COL);
                int isActiveIndex = cursor.getColumnIndex(DBHandler.EVENT_TYPE_IS_ACTIVE_COL);

                int id = idIndex != -1 ? cursor.getInt(idIndex) : 0;
                String name = cursor.getString(nameIndex);
                String description = cursor.getString(descriptionIndex);
                boolean isActive = cursor.getInt(isActiveIndex) == 1;

                EventType eventType = new EventType(id, name, description, isActive);
                eventTypes.add(eventType);
            } while (cursor.moveToNext());

            cursor.close();
        }

        return eventTypes;
    }
    private void showAddEventTypeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_add_event_type, null);
        EditText editTextName = view.findViewById(R.id.editEventTypeTextName);
        EditText editTextDescription = view.findViewById(R.id.editEventTypeTextDescription);
        Button buttonCancel = view.findViewById(R.id.buttonEventTypeCancel);
        Button buttonAdd = view.findViewById(R.id.buttonEventTypeAdd);

        builder.setView(view);

        AlertDialog dialog = builder.create();

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editTextName.getText().toString();
                String description = editTextDescription.getText().toString();

                if (!name.isEmpty() && !description.isEmpty()) {
                    dbHandler.addEventType(name, description, true); // Add EventType to database
                    eventTypes.clear();
                    eventTypes.addAll(getAllEventTypesFromDatabase()); // Refresh the list from database
                    adapter.notifyDataSetChanged(); // Notify the adapter about the data set change
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }


}
