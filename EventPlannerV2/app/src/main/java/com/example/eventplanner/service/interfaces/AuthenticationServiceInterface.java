package com.example.eventplanner.service.interfaces;

import com.example.eventplanner.model.User;

public interface AuthenticationServiceInterface {
    public User getLoggedUser();

}
