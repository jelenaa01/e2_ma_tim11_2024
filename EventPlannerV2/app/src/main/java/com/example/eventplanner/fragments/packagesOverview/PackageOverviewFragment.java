package com.example.eventplanner.fragments.packagesOverview;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.model.FavoriteItem;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.FavoriteItemRepositoryInterface;
import com.example.eventplanner.repository.interfaces.PackageRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class PackageOverviewFragment extends Fragment {
    private static final String ARG_SELECTED_PACKAGE = "selected-package";
    private static final String TAG = "PackagesOverviewFragment";

    private Package selectedPackage;
    private PackageRepositoryInterface packageRepository;
    private User loggedInUser;
    private UserRepositoryInterface userRepository;
    private FavoriteItemRepositoryInterface favoriteItemRepository;
    private FirebaseAuth mAuth;

    public PackageOverviewFragment() { mAuth = FirebaseAuth.getInstance(); }

    public static PackageOverviewFragment newInstance(Package selectedPackage) {
        PackageOverviewFragment fragment = new PackageOverviewFragment();

        Bundle args = new Bundle();
        args.putSerializable(ARG_SELECTED_PACKAGE, selectedPackage);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            selectedPackage = (Package) getArguments().getSerializable(ARG_SELECTED_PACKAGE);
        }

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        packageRepository = eventPlannerApp.getDIContainer().resolve(PackageRepositoryInterface.class);
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);
        favoriteItemRepository = eventPlannerApp.getDIContainer().resolve(FavoriteItemRepositoryInterface.class);

        getLoggedInUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_organiser_package_details, container, false);

        if (selectedPackage == null){
            Log.e("VendorProductDetailsFragment", "Error getting Service: ");
        }

        TextView tvName = view.findViewById(R.id.packageNameTextView2);
        TextView tvDescription = view.findViewById(R.id.packageDescriptionTextView2);
        TextView tvPrice = view.findViewById(R.id.packagePriceTextView2);
        TextView tvAvailability = view.findViewById(R.id.packageAvailabilityTextView2);

        tvName.setText(selectedPackage.getName());
        tvDescription.setText(selectedPackage.getDescription());
        tvPrice.setText(String.valueOf(selectedPackage.getPrice()));
        if (!selectedPackage.isAvailable()) {
            tvAvailability.setText("This package currently isn't available.");
        }

        Button addPackageToFavoritesButton = view.findViewById(R.id.addPackageToFavoritesButton);
        addPackageToFavoritesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPackageToFavorites();
                addPackageToFavoritesButton.setVisibility(View.GONE);
            }
        });

        favoriteItemRepository.existsByNameAndEmail(selectedPackage.getName(), mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<Boolean>() {
            @Override
            public void onComplete(@NonNull Task<Boolean> task) {
                if (task.isSuccessful()) {
                    boolean isItemInFavorites = task.getResult();
                    if (isItemInFavorites) {
                        addPackageToFavoritesButton.setVisibility(View.GONE);
                    } else {
                        addPackageToFavoritesButton.setVisibility(View.VISIBLE);
                    }
                } else {
                    Log.e(TAG, "Error getting favorite items", task.getException());
                }
            }
        });

        return view;
    }

    private void getLoggedInUser() {
        if (mAuth.getCurrentUser() != null) {
            userRepository.getUserByEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<User>() {
                @Override
                public void onComplete(@NonNull Task<User> task) {
                    if (task.isSuccessful()) {
                        loggedInUser = task.getResult();
                    } else {
                        Log.e("ServiceOverviewFragment", "Failed to get logged in user: " + task.getException());
                    }
                }
            });
        }
    }

    private void addPackageToFavorites() {
        if (mAuth.getCurrentUser() != null) {
            FavoriteItem favoriteItem = new FavoriteItem();
            favoriteItem.setUserEmail(loggedInUser.getEmail());
            favoriteItem.setType("package");
            favoriteItem.setItemName(selectedPackage.getName());
            favoriteItemRepository.addFavoriteItem(favoriteItem);
        }
    }
}
