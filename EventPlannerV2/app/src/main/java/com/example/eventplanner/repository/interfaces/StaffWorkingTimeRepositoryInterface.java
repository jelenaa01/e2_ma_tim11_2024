package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.VendorStaffWorkingTime;
import com.example.eventplanner.model.WorkingTime;
import com.google.android.gms.tasks.Task;

import java.util.Date;
import java.util.List;

public interface StaffWorkingTimeRepositoryInterface {
    void addWorkingTime(VendorStaffWorkingTime vendorStaffWorkingTime);
    Task<VendorStaffWorkingTime> getWorkingTimeByStaff(String email);
    void updateWorkingTime(String email, List<WorkingTime> workingTimes, Date fromDate, Date toDate);
}
