package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.Reservation;
import com.google.android.gms.tasks.Task;

import java.util.List;

public interface ReservationRepositoryInterface {
    void createReservation(Reservation reservation);
    Task<List<Reservation>> getAll();
    Task<List<Reservation>> getReservationByOragnizer(String organizerEmail);
    Task<List<Reservation>> getReservationByStaff(String staffEmail);
    void changeReservationStatus(String reservationId, Reservation.ReservationStatus newStatus);
    void updateReservationStaffEventId(String reservationId, String staffEventId);
    Task<Boolean> doesReservationExistWithEmailAndDatePassed(String email);
    void cancelReservationsByOrganizerEmail(String organizerEmail);
}
