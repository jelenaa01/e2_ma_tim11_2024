package com.example.eventplanner.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.model.User;

import java.util.ArrayList;
import java.util.List;

public class DBHandler extends SQLiteOpenHelper {

    // Database constants
    private static final String DB_NAME = "eventplannerdb";
    private static final int DB_VERSION = 9;  // Updated DB version

    // User table constants
    private static final String USER_TABLE_NAME = "users";
    private static final String USER_ID_COL = "id";
    private static final String USER_EMAIL_COL = "email";
    private static final String USER_PASSWORD_COL = "password";
    private static final String USER_FIRST_NAME_COL = "first_name";
    private static final String USER_LAST_NAME_COL = "last_name";
    private static final String USER_ADDRESS_COL = "address";
    private static final String USER_PHONE_COL = "phone";
    private static final String USER_PROFILE_PHOTO_COL = "profile_photo";
    private static final String USER_ROLE_COL = "role";  // New column for user role

    // Category table constants
    private static final String CATEGORY_TABLE_NAME = "categories";
    private static final String CATEGORY_ID_COL = "id";
    private static final String CATEGORY_NAME_COL = "name";
    private static final String CATEGORY_DESCRIPTION_COL = "description";
    private static final String CATEGORY_IS_ACTIVE_COL = "is_active";

    // Subcategory table constants
    private static final String SUBCATEGORY_TABLE_NAME = "subcategories";
    private static final String SUBCATEGORY_ID_COL = "id";
    private static final String SUBCATEGORY_NAME_COL = "name";
    private static final String SUBCATEGORY_DESCRIPTION_COL = "description";
    private static final String SUBCATEGORY_TYPE_COL = "type";
    private static final String SUBCATEGORY_IS_ACTIVE_COL = "is_active";
    private static final String CATEGORY_ID_FK = "category_id";

    // EventType table constants
    public static final String EVENT_TYPE_TABLE_NAME = "event_types";
    public static final String EVENT_TYPE_ID_COL = "id";
    public static final String EVENT_TYPE_NAME_COL = "name";
    public static final String EVENT_TYPE_DESCRIPTION_COL = "description";
    public static final String EVENT_TYPE_IS_ACTIVE_COL = "is_active";


    // Singleton instance
    private static DBHandler instance;

    private DBHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static synchronized DBHandler getInstance(Context context) {
        if (instance == null) {
            instance = new DBHandler(context.getApplicationContext());
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createUserTable(db);
        createCategoryTable(db);
        createSubcategoryTable(db);
        createEventTypeTable(db);
    }

    private void createUserTable(SQLiteDatabase db) {
        String createUserTableQuery = "CREATE TABLE " + USER_TABLE_NAME + " ("
                + USER_ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + USER_EMAIL_COL + " TEXT UNIQUE,"
                + USER_PASSWORD_COL + " TEXT,"
                + USER_FIRST_NAME_COL + " TEXT,"
                + USER_LAST_NAME_COL + " TEXT,"
                + USER_ADDRESS_COL + " TEXT,"
                + USER_PHONE_COL + " TEXT,"
                + USER_PROFILE_PHOTO_COL + " TEXT,"
                + USER_ROLE_COL + " TEXT)";  // Added the new role column
        db.execSQL(createUserTableQuery);
    }

    private void createCategoryTable(SQLiteDatabase db) {
        String createCategoryTableQuery = "CREATE TABLE " + CATEGORY_TABLE_NAME + " ("
                + CATEGORY_ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + CATEGORY_NAME_COL + " TEXT,"
                + CATEGORY_DESCRIPTION_COL + " TEXT,"
                + CATEGORY_IS_ACTIVE_COL + " INTEGER)";
        db.execSQL(createCategoryTableQuery);
    }

    private void createSubcategoryTable(SQLiteDatabase db) {
        String createSubcategoryTableQuery = "CREATE TABLE " + SUBCATEGORY_TABLE_NAME + " ("
                + SUBCATEGORY_ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + SUBCATEGORY_NAME_COL + " TEXT,"
                + SUBCATEGORY_DESCRIPTION_COL + " TEXT,"
                + SUBCATEGORY_TYPE_COL + " TEXT,"
                + SUBCATEGORY_IS_ACTIVE_COL + " INTEGER,"
                + CATEGORY_ID_FK + " INTEGER,"
                + "FOREIGN KEY(" + CATEGORY_ID_FK + ") REFERENCES " + CATEGORY_TABLE_NAME + "(" + CATEGORY_ID_COL + "))";
        db.execSQL(createSubcategoryTableQuery);
    }

    private void createEventTypeTable(SQLiteDatabase db) {
        String createEventTypeTableQuery = "CREATE TABLE " + EVENT_TYPE_TABLE_NAME + " ("
                + EVENT_TYPE_ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + EVENT_TYPE_NAME_COL + " TEXT,"
                + EVENT_TYPE_DESCRIPTION_COL + " TEXT,"
                + EVENT_TYPE_IS_ACTIVE_COL + " INTEGER)";
        db.execSQL(createEventTypeTableQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTables(db);
        onCreate(db);
    }

    private void dropTables(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + CATEGORY_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + SUBCATEGORY_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + EVENT_TYPE_TABLE_NAME);
    }

    public User getUserByEmailAndPassword(String email, String password) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + USER_TABLE_NAME + " WHERE " + USER_EMAIL_COL + "=? AND " + USER_PASSWORD_COL + "=?", new String[]{email, password});
        User user = getUserFromCursor(cursor);
        cursor.close();
        db.close();
        return user;
    }

    public User getUserById(int userId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + USER_TABLE_NAME + " WHERE " + USER_ID_COL + "=?", new String[]{String.valueOf(userId)});
        User user = getUserFromCursor(cursor);
        cursor.close();
        db.close();
        return user;
    }
    private User getUserFromCursor(Cursor cursor) {
        User user = null;
        if (cursor.moveToFirst()) {
           // int idIndex = cursor.getColumnIndex(USER_ID_COL);
            int emailIndex = cursor.getColumnIndex(USER_EMAIL_COL);
            int passwordIndex = cursor.getColumnIndex(USER_PASSWORD_COL);
            int firstNameIndex = cursor.getColumnIndex(USER_FIRST_NAME_COL);
            int lastNameIndex = cursor.getColumnIndex(USER_LAST_NAME_COL);
            int addressIndex = cursor.getColumnIndex(USER_ADDRESS_COL);
            int phoneIndex = cursor.getColumnIndex(USER_PHONE_COL);
            int profilePhotoIndex = cursor.getColumnIndex(USER_PROFILE_PHOTO_COL);
            int roleIndex = cursor.getColumnIndex(USER_ROLE_COL);

            if (/*idIndex != -1 && */emailIndex != -1 && passwordIndex != -1 && firstNameIndex != -1 && lastNameIndex != -1
                    && addressIndex != -1 && phoneIndex != -1 && profilePhotoIndex != -1  && roleIndex != -1) {

               // String id = cursor.getString(idIndex);
                String userEmail = cursor.getString(emailIndex);
                String userPassword = cursor.getString(passwordIndex);
                String firstName = cursor.getString(firstNameIndex);
                String lastName = cursor.getString(lastNameIndex);
                String address = cursor.getString(addressIndex);
                String phone = cursor.getString(phoneIndex);
                String profilePhoto = cursor.getString(profilePhotoIndex);
                String roleStr = cursor.getString(roleIndex);  // Added this line
                User.UserRole role = User.UserRole.valueOf(roleStr);  // Convert string to enum

                user = new User( userEmail, userPassword, firstName, lastName, address, phone, profilePhoto, role, true);  // Updated to include role
            }
        }
        return user;
    }

    public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USER_EMAIL_COL, user.getEmail());
        values.put(USER_PASSWORD_COL, user.getPassword());
        values.put(USER_FIRST_NAME_COL, user.getFirstName());
        values.put(USER_LAST_NAME_COL, user.getLastName());
        values.put(USER_ADDRESS_COL, user.getAddress());
        values.put(USER_PHONE_COL, user.getPhone());
        values.put(USER_PROFILE_PHOTO_COL, user.getProfilePhoto());
        values.put(USER_ROLE_COL, user.getRole().name());

        long result = db.insert(USER_TABLE_NAME, null, values);
        db.close();

        if (result != -1) {
            Log.d("DBHandler", "User added: " + user);
        } else {
            Log.d("DBHandler", "Failed to add user: " + user.getEmail());
        }
    }

    public void updateUserRole(int userId, User.UserRole role) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USER_ROLE_COL, role.name());
        String[] whereArgs = {String.valueOf(userId)};
        db.update(USER_TABLE_NAME, values, USER_ID_COL + "=?", whereArgs);
        db.close();

        Log.d("DBHandler", "Role updated: " + role.name());
    }


    public void addEventType(String eventName, String eventDescription, boolean isActive) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(EVENT_TYPE_NAME_COL, eventName);
        values.put(EVENT_TYPE_DESCRIPTION_COL, eventDescription);
        values.put(EVENT_TYPE_IS_ACTIVE_COL, isActive ? 1 : 0);
        db.insert(EVENT_TYPE_TABLE_NAME, null, values);
        db.close();
    }

    public Cursor getAllEventTypes() {
        SQLiteDatabase db = this.getReadableDatabase();
        return db.query(EVENT_TYPE_TABLE_NAME, null, null, null, null, null, null);
    }

    public void updateEventTypeStatus(int id, boolean isActive) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(EVENT_TYPE_IS_ACTIVE_COL, isActive ? 1 : 0);
        String[] whereArgs = {String.valueOf(id)};
        db.update(EVENT_TYPE_TABLE_NAME, values, EVENT_TYPE_ID_COL + "=?", whereArgs);
        db.close();
    }

    public void updateEventTypeDescription(int id, String description) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(EVENT_TYPE_DESCRIPTION_COL, description);
        String[] whereArgs = {String.valueOf(id)};
        db.update(EVENT_TYPE_TABLE_NAME, values, EVENT_TYPE_ID_COL + "=?", whereArgs);
        db.close();
    }

    public void addCategory(Category category) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CATEGORY_NAME_COL, category.getName());
        values.put(CATEGORY_DESCRIPTION_COL, category.getDescription());
        values.put(CATEGORY_IS_ACTIVE_COL, category.isActive() ? 1 : 0);

        long result = db.insert(CATEGORY_TABLE_NAME, null, values);
        db.close();

        if (result != -1) {
            Log.d("DBHandler", "Category added: " + category);
        } else {
            Log.d("DBHandler", "Failed to add category: " + category.getName());
        }
    }

    public List<Category> getAllCategories() {
        List<Category> categories = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + CATEGORY_TABLE_NAME, null);

        if (cursor.moveToFirst()) {
            do {
                int idIndex = cursor.getColumnIndex(CATEGORY_ID_COL);
                int nameIndex = cursor.getColumnIndex(CATEGORY_NAME_COL);
                int descriptionIndex = cursor.getColumnIndex(CATEGORY_DESCRIPTION_COL);
                int isActiveIndex = cursor.getColumnIndex(CATEGORY_IS_ACTIVE_COL);

                if (idIndex != -1 && nameIndex != -1 && descriptionIndex != -1 && isActiveIndex != -1) {
                    int id = cursor.getInt(idIndex);
                    String name = cursor.getString(nameIndex);
                    String description = cursor.getString(descriptionIndex);
                    boolean isActive = cursor.getInt(isActiveIndex) == 1;

                    Category category = new Category(id, name, description, isActive);
                    categories.add(category);
                }

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return categories;
    }

    public void deactivateCategory(int categoryId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CATEGORY_IS_ACTIVE_COL, 0);
        String[] whereArgs = {String.valueOf(categoryId)};
        db.update(CATEGORY_TABLE_NAME, values, CATEGORY_ID_COL + "=?", whereArgs);
        db.close();

        Log.d("DBHandler", "Category deactivated: " + categoryId);
    }

    public void activateCategory(int categoryId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CATEGORY_IS_ACTIVE_COL, 1);
        String[] whereArgs = {String.valueOf(categoryId)};
        db.update(CATEGORY_TABLE_NAME, values, CATEGORY_ID_COL + "=?", whereArgs);
        db.close();

        Log.d("DBHandler", "Category activated: " + categoryId);
    }

    public void addSubcategory(Subcategory subcategory) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SUBCATEGORY_NAME_COL, subcategory.getName());
        values.put(SUBCATEGORY_DESCRIPTION_COL, subcategory.getDescription());
        values.put(SUBCATEGORY_TYPE_COL, subcategory.getType().name());
        values.put(SUBCATEGORY_IS_ACTIVE_COL, subcategory.isActive() ? 1 : 0);
        values.put(CATEGORY_ID_FK, subcategory.getCategoryId());

        long result = db.insert(SUBCATEGORY_TABLE_NAME, null, values);
        db.close();

        if (result != -1) {
            Log.d("DBHandler", "Subcategory added: " + subcategory);
        } else {
            Log.d("DBHandler", "Failed to add subcategory: " + subcategory.getName());
        }
    }

    public List<Subcategory> getSubcategoriesForCategory(int categoryId) {
        List<Subcategory> subcategories = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String[] selectionArgs = {String.valueOf(categoryId)};
        Cursor cursor = db.rawQuery("SELECT * FROM " + SUBCATEGORY_TABLE_NAME + " WHERE " + CATEGORY_ID_FK + "=?", selectionArgs);

        if (cursor.moveToFirst()) {
            do {
                int idIndex = cursor.getColumnIndex(SUBCATEGORY_ID_COL);
                int nameIndex = cursor.getColumnIndex(SUBCATEGORY_NAME_COL);
                int descriptionIndex = cursor.getColumnIndex(SUBCATEGORY_DESCRIPTION_COL);
                int typeIndex = cursor.getColumnIndex(SUBCATEGORY_TYPE_COL);
                int isActiveIndex = cursor.getColumnIndex(SUBCATEGORY_IS_ACTIVE_COL);

                if (idIndex != -1 && nameIndex != -1 && descriptionIndex != -1 && typeIndex != -1 && isActiveIndex != -1) {
                    int id = cursor.getInt(idIndex);
                    String name = cursor.getString(nameIndex);
                    String description = cursor.getString(descriptionIndex);
                    Subcategory.Type type = Subcategory.Type.valueOf(cursor.getString(typeIndex));
                    boolean isActive = cursor.getInt(isActiveIndex) == 1;

                    Subcategory subcategory = new Subcategory(id, name, description, type, categoryId, isActive);
                    subcategories.add(subcategory);
                }

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return subcategories;
    }

    public List<Subcategory> getAllSubcategories() {
        List<Subcategory> subcategories = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + SUBCATEGORY_TABLE_NAME, null);

        if (cursor.moveToFirst()) {
            do {
                int idIndex = cursor.getColumnIndex(SUBCATEGORY_ID_COL);
                int nameIndex = cursor.getColumnIndex(SUBCATEGORY_NAME_COL);
                int descriptionIndex = cursor.getColumnIndex(SUBCATEGORY_DESCRIPTION_COL);
                int typeIndex = cursor.getColumnIndex(SUBCATEGORY_TYPE_COL);
                int isActiveIndex = cursor.getColumnIndex(SUBCATEGORY_IS_ACTIVE_COL);
                int categoryIdIndex = cursor.getColumnIndex(CATEGORY_ID_FK);

                if (idIndex != -1 && nameIndex != -1 && descriptionIndex != -1 && typeIndex != -1 && isActiveIndex != -1 && categoryIdIndex != -1) {
                    int id = cursor.getInt(idIndex);
                    String name = cursor.getString(nameIndex);
                    String description = cursor.getString(descriptionIndex);
                    Subcategory.Type type = Subcategory.Type.valueOf(cursor.getString(typeIndex));
                    boolean isActive = cursor.getInt(isActiveIndex) == 1;
                    int categoryId = cursor.getInt(categoryIdIndex);

                    Subcategory subcategory = new Subcategory(id, name, description, type, categoryId, isActive);
                    subcategories.add(subcategory);
                }

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return subcategories;
    }

    public void deactivateSubcategory(int subcategoryId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SUBCATEGORY_IS_ACTIVE_COL, 0);
        String[] whereArgs = {String.valueOf(subcategoryId)};
        db.update(SUBCATEGORY_TABLE_NAME, values, SUBCATEGORY_ID_COL + "=?", whereArgs);
        db.close();

        Log.d("DBHandler", "Subcategory deactivated: " + subcategoryId);
    }

    public void activateSubcategory(int subcategoryId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SUBCATEGORY_IS_ACTIVE_COL, 1);
        String[] whereArgs = {String.valueOf(subcategoryId)};
        db.update(SUBCATEGORY_TABLE_NAME, values, SUBCATEGORY_ID_COL + "=?", whereArgs);
        db.close();

        Log.d("DBHandler", "Subcategory activated: " + subcategoryId);
    }
}
