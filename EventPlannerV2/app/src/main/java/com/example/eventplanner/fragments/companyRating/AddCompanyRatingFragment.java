package com.example.eventplanner.fragments.companyRating;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.event.AddStaffEventFragment;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.CompanyRating;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.CompanyRatingRepositoryInterface;
import com.example.eventplanner.repository.interfaces.NotificationRepositoryInterface;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Date;
import java.util.UUID;

public class AddCompanyRatingFragment extends Fragment {

    private static final String TAG = "AddCompanyRatingFragment";
    private static final String ARG_SELECTED_COMPANY = "selected-company";
    private Company selectedCompany;
    private EditText commentEditText;
    private RatingBar ratingBar;
    private int userRating = 0;
    private CompanyRatingRepositoryInterface companyRatingRepository;
    private NotificationRepositoryInterface notificationRepository;
    private FirebaseAuth mAuth;

    public AddCompanyRatingFragment() {}

    public static AddCompanyRatingFragment newInstance(Company selectedCompany) {
        AddCompanyRatingFragment fragment = new AddCompanyRatingFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_SELECTED_COMPANY, selectedCompany);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            selectedCompany = (Company) getArguments().getSerializable(ARG_SELECTED_COMPANY);
        }

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        companyRatingRepository = eventPlannerApp.getDIContainer().resolve(CompanyRatingRepositoryInterface.class);
        notificationRepository = eventPlannerApp.getDIContainer().resolve(NotificationRepositoryInterface.class);

        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_company_rating, container, false);

        commentEditText = view.findViewById(R.id.commentEditText);
        ratingBar = view.findViewById(R.id.ratingBar);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                userRating = (int)rating;
                Log.d(TAG, "User Rating: " + userRating);
            }
        });

        Button rateButton = view.findViewById(R.id.btnRate);
        rateButton.setOnClickListener(v -> addRating());

        return view;
    }

    private boolean validate() {
        String comment = commentEditText.getText().toString();
        userRating = (int)ratingBar.getRating();

        if (comment.isEmpty() || userRating == 0) {
            Toast.makeText(getContext(), "Please fill in all the fields", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void addRating() {
        if(!validate()) {
            return;
        }
        FirebaseUser currentUser = mAuth.getCurrentUser();
        String comment = commentEditText.getText().toString().trim();

        if(currentUser != null) {
            CompanyRating companyRating = new CompanyRating(UUID.randomUUID().toString(), userRating, comment, selectedCompany.getId(), currentUser.getEmail(), new Date());
            companyRatingRepository.addCompanyRating(companyRating);

            Notification notification = new Notification(UUID.randomUUID().toString(), "Rate company", "Organizer " + currentUser.getEmail() + " rate your company", selectedCompany.getOwnerEmail(), Notification.NotificationStatus.CREATED, Timestamp.now());
            notificationRepository.createNotification(notification);

            Toast.makeText(getContext(), "Your rating successfully added", Toast.LENGTH_SHORT).show();
            getParentFragmentManager().popBackStack();
        }
    }

}