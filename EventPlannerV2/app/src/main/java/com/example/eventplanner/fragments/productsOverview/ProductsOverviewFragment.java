package com.example.eventplanner.fragments.productsOverview;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.ProductRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProductsOverviewFragment extends Fragment {

    private static final String TAG = "ProductsOverviewFragment";

    private List<Product> products;
    private ProductsRecyclerViewAdapter adapter;

    private ProductRepositoryInterface productRepository;
    private User loggedInUser;
    private UserRepositoryInterface userRepository;
    private FirebaseAuth mAuth;

    public ProductsOverviewFragment() {
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        productRepository = eventPlannerApp.getDIContainer().resolve(ProductRepositoryInterface.class);
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);

        getLoggedInUser();

        generateDummyData();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vendor_products, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.productRecyclerView1);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        productRepository.getAllVisible()
                .addOnSuccessListener(ps -> {
                    this.products = ps;
                    // Sorting the products after fetching them
                    Collections.sort(products, new Comparator<Product>() {
                        @Override
                        public int compare(Product o1, Product o2) {
                            String name1 = o1.getName();
                            String name2 = o2.getName();
                            if (name1 == null && name2 == null) {
                                return 0;
                            } else if (name1 == null) {
                                return -1;
                            } else if (name2 == null) {
                                return 1;
                            } else {
                                return name1.compareToIgnoreCase(name2);
                            }
                        }
                    });

                    adapter = new ProductsRecyclerViewAdapter(products, productRepository);
                    recyclerView.setAdapter(adapter);

                    if (adapter != null) {
                        adapter.setOnItemClickListener(product -> {
                            navigateToPage(product);
                        });
                    }

                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Failed to fetch products", e);
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                });

        return view;
    }

    private void navigateToPage(Product product){
        if (product == null) {
            Log.e(TAG, "Product is null, cannot navigate");
            return;
        }

        ProductOverviewFragment fragment = ProductOverviewFragment.newInstance(product);

        FragmentTransaction transaction = getParentFragmentManager().beginTransaction();

        if (loggedInUser != null) {
            switch (loggedInUser.getRole()) {
                case VENDOR:
                    transaction.replace(R.id.vendor_fragment_container, fragment);
                    break;
                case ORGANISER:
                    transaction.replace(R.id.organizer_fragment_container, fragment);
                    break;
                case VENDOR_STAFF:
                    transaction.replace(R.id.vendor_staff_fragment_container, fragment);
                    break;
                case ADMIN:
                    transaction.replace(R.id.admin_fragment_container, fragment);
                    break;
                default:
                    Log.e(TAG, "Unknown user role.");
                    return;
            }

            transaction.addToBackStack(null);
            transaction.commit();
        } else {
            Log.e(TAG, "Logged in user is null.");
        }
    }

    private void getLoggedInUser() {
        if (mAuth.getCurrentUser() != null) {
            userRepository.getUserByEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<User>() {
                @Override
                public void onComplete(@NonNull Task<User> task) {
                    if (task.isSuccessful()) {
                        loggedInUser = task.getResult();
                    } else {
                        Log.e("CompanyOverviewFragment", "Failed to get logged in vendor: " + task.getException());
                    }
                }
            });
        }
    }

    private void generateDummyData(){
        // Dummy product 1
        ArrayList<String> gallery1 = new ArrayList<>();
        gallery1.add("https://example.com/gallery1/image1.jpg");
        gallery1.add("https://example.com/gallery1/image2.jpg");

        ArrayList<String> eventTypes1 = new ArrayList<>();
        eventTypes1.add("Wedding");
        eventTypes1.add("Corporate Event");

        Product product1 = new Product(
                "Category1",
                "SubCategory1",
                "Product 1",
                "Description for Product 1",
                gallery1,
                eventTypes1,
                true,
                true,
                100.0,
                10.0,
                "Company"
        );

        // Dummy product 2
        ArrayList<String> gallery2 = new ArrayList<>();
        gallery2.add("https://example.com/gallery2/image1.jpg");
        gallery2.add("https://example.com/gallery2/image2.jpg");

        ArrayList<String> eventTypes2 = new ArrayList<>();
        eventTypes2.add("Birthday");
        eventTypes2.add("Anniversary");

        Product product2 = new Product(
                "Category2",
                "SubCategory2",
                "Product 2",
                "Description for Product 2",
                gallery2,
                eventTypes2,
                true,
                false,
                200.0,
                15.0,
                "Company"
        );

        // Dummy product 3
        ArrayList<String> gallery3 = new ArrayList<>();
        gallery3.add("https://example.com/gallery3/image1.jpg");
        gallery3.add("https://example.com/gallery3/image2.jpg");

        ArrayList<String> eventTypes3 = new ArrayList<>();
        eventTypes3.add("Concert");
        eventTypes3.add("Festival");

        Product product3 = new Product(
                "Category3",
                "SubCategory3",
                "Product 3",
                "Description for Product 3",
                gallery3,
                eventTypes3,
                false,
                true,
                300.0,
                20.0,
                "Company"
        );

        productRepository.addProduct(product1);
        productRepository.addProduct(product2);
        productRepository.addProduct(product3);
    }
}
