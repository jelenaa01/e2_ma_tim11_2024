package com.example.eventplanner.model;

import java.io.Serializable;
import java.time.Period;
import java.util.List;
import java.util.UUID;

public class Service implements Serializable {
    public enum ConfirmationModeService {
        AUTOMATIC,
        MANUAL
    }
    private String id;
    protected String category;
    protected String subcategory;
    protected String name;
    protected String description;
    protected List<String> gallery;
    protected double pricePerHour;
    protected double totalPrice;
    protected int durationInHours;
    protected String location;
    protected double discount;
    protected List<String> personnel;
    protected List<String> eventType;
    protected int bookingDeadline;
    protected int cancellationDeadline;
    protected ConfirmationModeService confirmationMode;
    protected boolean available;
    protected boolean visible;

    public Service() {
        this.id = UUID.randomUUID().toString();
    }

    public Service(String category, String subcategory, String name, String description, List<String> gallery,
                   double pricePerHour, double totalPrice, int durationInHours, String location,
                   double discount, List<String> personnel, List<String> eventType, int bookingDeadline,
                   int cancellationDeadline, ConfirmationModeService  confirmationMode, boolean available, boolean visible) {
        this.id = UUID.randomUUID().toString();
        this.category = category;
        this.subcategory = subcategory;
        this.name = name;
        this.description = description;
        this.gallery = gallery;
        this.pricePerHour = pricePerHour;
        this.totalPrice = totalPrice;
        this.durationInHours = durationInHours;
        this.location = location;
        this.discount = discount;
        this.personnel = personnel;
        this.eventType = eventType;
        this.bookingDeadline = bookingDeadline;
        this.cancellationDeadline = cancellationDeadline;
        this. confirmationMode =  confirmationMode;
        this.available = available;
        this.visible = visible;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getGallery() {
        return gallery;
    }

    public double getPricePerHour() {
        return pricePerHour;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public int getDurationInHours() {
        return durationInHours;
    }

    public String getLocation() {
        return location;
    }

    public double getDiscount() {
        return discount;
    }

    public List<String> getPersonnel() {
        return personnel;
    }

    public List<String> getEventType() {
        return eventType;
    }

    public int getBookingDeadline() {
        return bookingDeadline;
    }

    public int getCancellationDeadline() {
        return cancellationDeadline;
    }

    public ConfirmationModeService getConfirmationMode() {
        return confirmationMode;
    }

    public boolean isAvailable() {
        return available;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setGallery(List<String> gallery) {
        this.gallery = gallery;
    }

    public void setPricePerHour(double pricePerHour) {
        this.pricePerHour = pricePerHour;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void setDurationInHours(int durationInHours) {
        this.durationInHours = durationInHours;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public void setPersonnel(List<String> personnel) {
        this.personnel = personnel;
    }

    public void setEventType(List<String> eventType) {
        this.eventType = eventType;
    }

    public void setBookingDeadline(int bookingDeadline) {
        this.bookingDeadline = bookingDeadline;
    }

    public void setCancellationDeadline(int cancellationDeadline) {
        this.cancellationDeadline = cancellationDeadline;
    }

    public void setConfirmationMode(ConfirmationModeService confirmationMode) {
        this.confirmationMode = confirmationMode;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}