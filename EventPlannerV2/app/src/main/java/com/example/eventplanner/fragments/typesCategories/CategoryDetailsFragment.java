package com.example.eventplanner.fragments.typesCategories;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.eventplanner.R;
import com.example.eventplanner.database.DBHandler;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Subcategory;

import java.util.ArrayList;
import java.util.List;

public class CategoryDetailsFragment extends Fragment {

    private static final String ARG_SELECTED_CATEGORY = "selected-category";
    private Category selectedCategory;
    private List<Subcategory> subcategories;
    private CategoryDetailsRecyclerViewAdapter adapter;
    private DBHandler dbHandler;


    public static CategoryDetailsFragment newInstance(Category category) {
        CategoryDetailsFragment fragment = new CategoryDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_SELECTED_CATEGORY, category);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            selectedCategory = (Category) getArguments().getSerializable(ARG_SELECTED_CATEGORY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_details_list, container, false);

        dbHandler = DBHandler.getInstance(getContext());

      /*  tvCategoryName = view.findViewById(R.id.tvCategoryName);
        tvCategoryDescription = view.findViewById(R.id.tvCategoryDescription);
        tvCategoryStatus = view.findViewById(R.id.tvCategoryStatus);
        tvSubcategoryCount = view.findViewById(R.id.tvSubcategoryCount);*/

        RecyclerView recyclerView = view.findViewById(R.id.subcategoriesRecyclerView);

        /*tvCategoryName.setText(selectedCategory.getName());
        tvCategoryDescription.setText(selectedCategory.getDescription());
        tvCategoryStatus.setText(selectedCategory.isActive() ? "ACTIVE" : "INACTIVE");*/

        subcategories = dbHandler.getSubcategoriesForCategory(selectedCategory.getId());
        //tvSubcategoryCount.setText(subcategories.size() + " Subcategories");

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new CategoryDetailsRecyclerViewAdapter(subcategories, getContext());
        recyclerView.setAdapter(adapter);

        // Floating Action Button to add new Subcategory
        view.findViewById(R.id.fabAddSubcategory).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddSubcategoryDialog();
            }
        });

        return view;
    }

    private void showAddSubcategoryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_add_subcategory, null);
        EditText editTextName = view.findViewById(R.id.editSubcategoryTextName);
        EditText editTextDescription = view.findViewById(R.id.editSubcategoryTextDescription);
        Button buttonCancel = view.findViewById(R.id.buttonSubcategoryCancel);
        Button buttonAdd = view.findViewById(R.id.buttonSubcategoryAdd);

        builder.setView(view);

        AlertDialog dialog = builder.create();

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editTextName.getText().toString();
                String description = editTextDescription.getText().toString();

                if (!name.isEmpty() && !description.isEmpty()) {
                    dbHandler.addSubcategory(new Subcategory(1, name, description, Subcategory.Type.SERVICE, selectedCategory.getId(), true)); // Add Subcategory to database
                    subcategories.clear();
                    subcategories.addAll(dbHandler.getSubcategoriesForCategory(selectedCategory.getId())); // Refresh the list from database
                    //tvSubcategoryCount.setText(subcategories.size() + " Subcategories");
                    adapter.notifyDataSetChanged(); // Notify the adapter about the data set change
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }
}
