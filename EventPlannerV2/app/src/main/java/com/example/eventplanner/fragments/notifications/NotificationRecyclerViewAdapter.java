package com.example.eventplanner.fragments.notifications;

import static com.example.eventplanner.model.Notification.NotificationStatus.READ;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.eventplanner.databinding.FragmentNotificationBinding;
import com.example.eventplanner.model.Notification;
import com.google.firebase.Timestamp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class NotificationRecyclerViewAdapter extends RecyclerView.Adapter<NotificationRecyclerViewAdapter.ViewHolder> {

    private final List<Notification> mAllNotifications;
    private final List<Notification> mDisplayedNotifications;

    private final OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(Notification notification);
    }

    public NotificationRecyclerViewAdapter(List<Notification> items, OnItemClickListener listener) {
        mAllNotifications = new ArrayList<>(items);
        mDisplayedNotifications = new ArrayList<>(items);
        this.listener = listener;
    }

    @NonNull
    @Override
    public NotificationRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(FragmentNotificationBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Notification notification = mDisplayedNotifications.get(position);
        Log.d("NotificationAdapter", "Binding notification: " + notification.getTitle());
        holder.mTitleView.setText(notification.getTitle());
        holder.mTextView.setText(notification.getText());

        // Set text style based on the notification status
        if (notification.getStatus() == Notification.NotificationStatus.SHOWN) {
            holder.mTitleView.setTypeface(null, Typeface.BOLD);
            holder.mTextView.setTypeface(null, Typeface.BOLD);
        } else if (notification.getStatus() == Notification.NotificationStatus.READ) {
            holder.mTitleView.setTypeface(null, Typeface.NORMAL);
            holder.mTextView.setTypeface(null, Typeface.NORMAL);
        }

        // Calculate and set the relative timestamp
        String relativeTimestamp = getRelativeTimeAgo(notification.getTimeStamp());
        holder.mTimestampView.setText("Sent " + relativeTimestamp);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(notification);
            }
        });
    }

    @Override
    public int getItemCount() {
        Log.d("NotificationAdapter", "Item count: " + mDisplayedNotifications.size());
        return mDisplayedNotifications.size();
    }

    public void updateNotifications(List<Notification> notifications) {
        mAllNotifications.clear();
        mAllNotifications.addAll(notifications);
        showAllNotifications();  // Display all notifications after update
    }

    public void showAllNotifications() {
        Log.d("NotificationAdapter", "Showing all notifications");
        mDisplayedNotifications.clear();
        mDisplayedNotifications.addAll(mAllNotifications);
        notifyDataSetChanged();
    }

    public void showOnlyReadNotifications() {
        Log.d("NotificationAdapter", "Showing only read notifications");
        mDisplayedNotifications.clear();
        for (Notification notification : mAllNotifications) {
            if (notification.getStatus() == Notification.NotificationStatus.READ) {
                mDisplayedNotifications.add(notification);
            }
        }
        notifyDataSetChanged();
    }

    public void showOnlyUnreadNotifications() {
        Log.d("NotificationAdapter", "Showing only unread notifications");
        mDisplayedNotifications.clear();
        for (Notification notification : mAllNotifications) {
            if (notification.getStatus() != Notification.NotificationStatus.READ) {
                mDisplayedNotifications.add(notification);
            }
        }
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView mTitleView;
        public final TextView mTextView;
        public final TextView mTimestampView;

        public ViewHolder(FragmentNotificationBinding binding) {
            super(binding.getRoot());
            mTitleView = binding.tvNotificationTitle;
            mTextView = binding.tvNotificationText;
            mTimestampView = binding.tvNotificationTimestamp;
        }
    }

    private String getRelativeTimeAgo(Timestamp timestamp) {
        long time = timestamp.toDate().getTime();
        long now = new Date().getTime();

        final long diff = now - time;
        if (diff < TimeUnit.MINUTES.toMillis(1)) {
            return "just now";
        } else if (diff < TimeUnit.MINUTES.toMillis(60)) {
            long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);
            return minutes == 1 ? "a minute ago" : minutes + " minutes ago";
        } else if (diff < TimeUnit.HOURS.toMillis(24)) {
            long hours = TimeUnit.MILLISECONDS.toHours(diff);
            return hours == 1 ? "an hour ago" : hours + " hours ago";
        } else if (diff < TimeUnit.DAYS.toMillis(7)) {
            long days = TimeUnit.MILLISECONDS.toDays(diff);
            return days == 1 ? "a day ago" : days + " days ago";
        } else if (diff < TimeUnit.DAYS.toMillis(30)) {
            long weeks = TimeUnit.MILLISECONDS.toDays(diff) / 7;
            return weeks == 1 ? "a week ago" : weeks + " weeks ago";
        } else if (diff < TimeUnit.DAYS.toMillis(365)) {
            long months = TimeUnit.MILLISECONDS.toDays(diff) / 30;
            return months == 1 ? "a month ago" : months + " months ago";
        } else {
            long years = TimeUnit.MILLISECONDS.toDays(diff) / 365;
            return years == 1 ? "a year ago" : years + " years ago";
        }
    }
}
