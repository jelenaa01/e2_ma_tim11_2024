package com.example.eventplanner.repository;

import android.util.Log;

import com.example.eventplanner.model.FavoriteItem;
import com.example.eventplanner.repository.interfaces.FavoriteItemRepositoryInterface;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class FavoriteItemRepository implements FavoriteItemRepositoryInterface {
    private static final String TAG = "FavoriteItemRepository";
    private final FirebaseFirestore db;

    public FavoriteItemRepository(FirebaseFirestore firestore) {
        this.db = firestore;
    }

    @Override
    public void addFavoriteItem(FavoriteItem favoriteItem) {
        if (favoriteItem == null) {
            Log.e(TAG, "Favorite Item is null.");
            return;
        }
        db.collection("favorite_items")
                .add(favoriteItem)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Favorite Item successfully added.");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error adding Favorite Item.", e);
                });
    }

    @Override
    public Task<List<FavoriteItem>> getByEmail(String email) {
        return db.collection("favorite_items")
                .whereEqualTo("userEmail", email)
                .get()
                .continueWith(task -> {
                    List<FavoriteItem> favoriteItems = new ArrayList<>();
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                FavoriteItem favoriteItem = documentSnapshot.toObject(FavoriteItem.class);
                                favoriteItems.add(favoriteItem);
                            }
                        }
                    }
                    return favoriteItems;
                });
    }

    @Override
    public Task<Boolean> existsByNameAndEmail(String name, String email) {
        return db.collection("favorite_items")
                .whereEqualTo("itemName", name)
                .whereEqualTo("userEmail", email)
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        return querySnapshot != null && !querySnapshot.isEmpty();
                    }
                    return false;
                });
    }

    @Override
    public void removeByNameAndEmail(String name, String email) {
        Query query = db.collection("favorite_items")
                .whereEqualTo("itemName", name)
                .whereEqualTo("userEmail", email);

        query.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    document.getReference().delete().addOnSuccessListener(aVoid -> {
                        Log.d("Firestore", "DocumentSnapshot successfully deleted!");
                    }).addOnFailureListener(e -> {
                        Log.w("Firestore", "Error deleting document", e);
                    });
                }
            } else {
                Log.w("Firestore", "Error getting documents: ", task.getException());
            }
        });
    }
}
