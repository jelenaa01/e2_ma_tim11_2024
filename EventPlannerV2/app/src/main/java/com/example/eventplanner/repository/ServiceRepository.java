package com.example.eventplanner.repository;

import android.util.Log;

import com.example.eventplanner.model.Service;
import com.example.eventplanner.repository.interfaces.ServiceRepositoryInterface;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ServiceRepository implements ServiceRepositoryInterface {
    private static final String TAG = "ServiceRepository";

    private FirebaseFirestore db;

    public ServiceRepository(FirebaseFirestore firestore) {
        this.db = firestore;
    }

    @Override
    public void addService(Service service) {
        db.collection("services")
                .add(service)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Service successfully added");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error adding service", e);
                });
    }

    @Override
    public Task<List<Service>> getAllServices() {
        return db.collection("services")
                .get()
                .continueWith(task -> {
                    List<Service> services = new ArrayList<>();
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                Service service = documentSnapshot.toObject(Service.class);
                                services.add(service);
                            }
                        }
                    }
                    return services;
                });
    }

    public Task<List<Service>> getAll() {
        return db.collection("services")
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        return querySnapshot.getDocuments().stream()
                                .map(documentSnapshot -> documentSnapshot.toObject(Service.class))
                                .collect(Collectors.toList());
                    } else {
                        Log.e(TAG, "Error getting services.", task.getException());
                        return null;
                    }
                });
    }

    @Override
    public Task<List<Service>> getAllVisibleServices() {
        return db.collection("services")
                .whereEqualTo("visible", true)
                .get()
                .continueWith(task -> {
                    List<Service> services = new ArrayList<>();
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                Service service = documentSnapshot.toObject(Service.class);
                                services.add(service);
                            }
                        }
                    }
                    return services;
                });
    }

    @Override
    public Task<Service> getById(String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("services")
                .whereEqualTo("id", id)
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
                            return documentSnapshot.toObject(Service.class);
                        }
                    }
                    return null;
                });
    }

    @Override
    public Task<Service> getByName(String itemName) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("services")
                .whereEqualTo("name", itemName)
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
                            return documentSnapshot.toObject(Service.class);
                        }
                    }
                    return null;
                });
    }


    @Override
    public Task<Void> updateService(Service service) {
        if (service == null || service.getId() == null) {
            Log.e(TAG, "Service or Service ID is null.");
            return Tasks.forException(new IllegalArgumentException("Service or Service ID is null."));
        }

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("services")
                .whereEqualTo("id", service.getId())
                .get()
                .continueWithTask(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
                            String documentId = documentSnapshot.getId();

                            // Update the document with the new service data
                            return db.collection("services")
                                    .document(documentId)
                                    .set(service);
                        } else {
                            return Tasks.forException(new IllegalStateException("Service with ID " + service.getId() + " not found."));
                        }
                    } else {
                        // Handle failure
                        Exception e = task.getException();
                        if (e != null) {
                            Log.e(TAG, "Error updating Service: " + e.getMessage());
                        }
                        return Tasks.forException(e);
                    }
                });
    }

}
