package com.example.eventplanner.fragments.authentication;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.dto.RegistrationRequestDTO;
import com.example.eventplanner.model.RegistrationRequest;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.CompanyRepositoryInterface;
import com.example.eventplanner.repository.interfaces.RegistrationRequestRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.example.eventplanner.service.EmailService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class RegistrationRequestsRecyclerViewAdapter extends RecyclerView.Adapter<RegistrationRequestsRecyclerViewAdapter.ViewHolder> implements Filterable {

    private List<RegistrationRequestDTO> registrationRequests;
    private List<RegistrationRequestDTO> filteredRequests;
    private RegistrationRequestRepositoryInterface registrationRequestRepository;
    private UserRepositoryInterface userRepository;
    private CompanyRepositoryInterface companyRepository;
    private Context context;
    private OnItemClickListener listener; // Declare the listener
    private FirebaseAuth mAuth;

    public RegistrationRequestsRecyclerViewAdapter(List<RegistrationRequestDTO> registrationRequests,
                                                   RegistrationRequestRepositoryInterface registrationRequestRepository,
                                                   UserRepositoryInterface userRepository,
                                                   CompanyRepositoryInterface companyRepository,
                                                   Context context) {
        this.registrationRequests = registrationRequests;
        this.filteredRequests = new ArrayList<>(registrationRequests);
        this.registrationRequestRepository = registrationRequestRepository;
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
        this.context = context;
        this.mAuth = FirebaseAuth.getInstance();
    }
    // Setter method to initialize the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    // Interface for item click listener
    public interface OnItemClickListener {
        void onItemClick(RegistrationRequestDTO requestDTO);
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.registration_request, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        RegistrationRequestDTO request = filteredRequests.get(position);
        holder.companyName.setText(request.getCompanyName());
        holder.fullName.setText(request.getFirstName() + " " + request.getLastName());
        holder.companyEmail.setText(request.getCompanyAddress());
        holder.userEmail.setText(request.getEmail());
        Timestamp timestamp = request.getTimestamp();
        long seconds = timestamp.getSeconds();
        Date timestampDate = new Date(seconds * 1000); // Convert seconds to milliseconds
        holder.timestamp.setText(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.US).format(timestampDate));

        holder.btnAccept.setOnClickListener(v -> {
            userRepository.getUserByEmail(request.getEmail())
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            User retrievedUser = task.getResult();
                            if (retrievedUser != null) {
                                verifyEmail(retrievedUser);
                                registrationRequestRepository.setRequestStatusAccepted(request.getRequestId());
                                Toast.makeText(context, "Activation link sent to " + request.getEmail(), Toast.LENGTH_SHORT).show();
                            } else {
                                Log.e("ADMIN_ACTIVITY", "User not found with email");
                            }
                        } else {
                            Log.e("ADMIN_ACTIVITY", "Failed to retrieve user", task.getException());
                        }
                    });
        });

        holder.btnDecline.setOnClickListener(v -> {
            showRejectionDialog(request);
        });
    }


    private void showRejectionDialog(RegistrationRequestDTO requestDTO) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogView = inflater.inflate(R.layout.dialog_rejection_reason, null);
        builder.setView(dialogView);

        AlertDialog dialog = builder.create();
        dialog.show();

        EditText editTextReason = dialogView.findViewById(R.id.editTextReason);
        Button buttonSubmit = dialogView.findViewById(R.id.buttonSubmit);

        buttonSubmit.setOnClickListener(v -> {
            String reason = editTextReason.getText().toString().trim();
            if (!reason.isEmpty()) {
                EmailService.sendRejectionEmail(requestDTO.getEmail(), reason);
                Toast.makeText(context, "Rejection reason sent to  " + requestDTO.getEmail(), Toast.LENGTH_SHORT).show();
                registrationRequestRepository.setRequestStatusRejected(requestDTO.getRequestId()).addOnCompleteListener(statusTask -> {
                    if (statusTask.isSuccessful()) {
                        Log.d("Adapter", "Request status updated to REJECTED");
                    } else {
                        Log.e("Adapter", "Error updating request status", statusTask.getException());
                    }
                });
                dialog.dismiss();
                if (listener != null) {
                    listener.onItemClick(requestDTO);
                }
            } else {
                Toast.makeText(context, "Please enter a reason for rejection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void verifyEmail(User user) {
        mAuth.signInWithEmailAndPassword(user.getEmail(), user.getPassword())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser firebaseUser = mAuth.getCurrentUser();
                            if (firebaseUser != null) {
                                firebaseUser.sendEmailVerification()
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Log.d("ADAPTER", "Verification email sent to: " + user.getEmail());
                                                } else {
                                                    Log.e("ADAPTER", "Failed to send verification email.", task.getException());
                                                }
                                                mAuth.signOut();
                                            }
                                        });
                            } else {
                                Log.e("ADAPTER", "Firebase user is null after sign in.");
                                mAuth.signOut();
                            }
                        } else {
                            Log.e("ADAPTER", "Failed to authenticate user: " + user.getEmail(), task.getException());
                            mAuth.signOut();
                        }
                    }
                });
    }

    @Override
    public int getItemCount() {
        return filteredRequests.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView companyName;
        public TextView fullName;
        public TextView companyEmail;
        public TextView userEmail;
        public TextView timestamp;
        public Button btnAccept;
        public Button btnDecline;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            companyName = itemView.findViewById(R.id.tvCompanyName);
            fullName = itemView.findViewById(R.id.tvFullName);
            companyEmail = itemView.findViewById(R.id.tvCompanyEmail);
            userEmail = itemView.findViewById(R.id.tvUserEmail);
            timestamp = itemView.findViewById(R.id.tvTimestamp);
            btnAccept = itemView.findViewById(R.id.btnAccept);
            btnDecline = itemView.findViewById(R.id.btnDecline);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                if (charString.isEmpty()) {
                    filteredRequests = new ArrayList<>(registrationRequests);
                } else {
                    List<RegistrationRequestDTO> filteredList = new ArrayList<>();
                    for (RegistrationRequestDTO request : registrationRequests) {
                        if (request.getCompanyName().toLowerCase().contains(charString.toLowerCase()) ||
                                request.getFirstName().toLowerCase().contains(charString.toLowerCase()) ||
                                request.getLastName().toLowerCase().contains(charString.toLowerCase()) ||
                                request.getCompanyAddress().toLowerCase().contains(charString.toLowerCase()) ||
                                request.getEmail().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(request);
                        }
                    }
                    filteredRequests = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredRequests;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredRequests = (ArrayList<RegistrationRequestDTO>) results.values;
                notifyDataSetChanged();
            }
        };
    }


    public Filter getDateFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                if (charString.isEmpty()) {
                    filteredRequests = new ArrayList<>(registrationRequests);
                } else {
                    String[] dates = charString.split(",");
                    if (dates.length != 2) {
                        Log.e("DateFilter", "Invalid date range format. Expected format: startDate,endDate");
                        return new FilterResults();
                    }

                    String startDateString = dates[0].trim();
                    String endDateString = dates[1].trim();

                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
                        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                        Date startDate = sdf.parse(startDateString);
                        Date endDate = sdf.parse(endDateString);

                        // Log the startDate and endDate
                        Log.d("DateFilter", "Start Date: " + startDate);
                        Log.d("DateFilter", "End Date: " + endDate);

                        List<RegistrationRequestDTO> filteredList = new ArrayList<>();
                        for (RegistrationRequestDTO request : registrationRequests) {
                            Date requestDate = request.getTimestamp().toDate();

                            // Convert request date to UTC
                            SimpleDateFormat utcSdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
                            utcSdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date requestDateUtc = utcSdf.parse(utcSdf.format(requestDate));

                            // Log the requestDate
                            Log.d("DateFilter", "Request Date: " + requestDateUtc);

                            if (!requestDateUtc.before(startDate) && !requestDateUtc.after(endDate)) {
                                filteredList.add(request);
                            }
                        }
                        filteredRequests = filteredList;
                    } catch (Exception e) {
                        Log.e("DateFilter", "Error parsing dates", e);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredRequests;

                // Log the size of the filtered list
                Log.d("DateFilter", "Filtered List Size: " + filteredRequests.size());

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredRequests = (ArrayList<RegistrationRequestDTO>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
