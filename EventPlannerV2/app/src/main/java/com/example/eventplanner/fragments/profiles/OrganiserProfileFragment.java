package com.example.eventplanner.fragments.profiles;

import static android.content.ContentValues.TAG;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.activities.OrganizerActivity;
import com.example.eventplanner.activities.UnauthActivity;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.ReservationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import org.w3c.dom.Text;

public class OrganiserProfileFragment extends Fragment {

    private TextView tvEmail;
    private EditText etName;
    private EditText etLastName;
    private EditText etAddress;
    private EditText etPhone;
    private EditText etCurrentPassword;
    private EditText etNewPassword1;
    private EditText etNewPassword2;
    private User loggedInUser;
    private FirebaseAuth mAuth;
    private UserRepositoryInterface userRepository;
    private ReservationRepositoryInterface reservationRepository;

    public OrganiserProfileFragment() { mAuth = FirebaseAuth.getInstance(); }

    public static OrganiserProfileFragment newInstance() {
        OrganiserProfileFragment fragment = new OrganiserProfileFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_organiser_profile, container, false);

        Button saveChangesButton = view.findViewById(R.id.saveOrganiserChangesButton);
        saveChangesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveChanges();
            }
        });

        Button updatePasswordButton = view.findViewById(R.id.saveOrganiserPasswordButton);
        updatePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePassword();
            }
        });

        Button deactivateAccountButton = view.findViewById(R.id.deactivateAccountButton);

        String userEmail = mAuth.getCurrentUser().getEmail(); // Method to get current user's email, replace it with your implementation

        reservationRepository.doesReservationExistWithEmailAndDatePassed(userEmail).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                if (task.getResult()) {
                    deactivateAccountButton.setVisibility(View.GONE);
                } else {
                    deactivateAccountButton.setVisibility(View.VISIBLE);
                }
            } else {
                Log.e(TAG, "Error getting reservations for user: " + userEmail, task.getException());
            }
        });
        deactivateAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deactivateAccount();
            }
        });

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);
        reservationRepository = eventPlannerApp.getDIContainer().resolve(ReservationRepositoryInterface.class);

        getLoggedInUser(new OnUserFetchListener() {
            @Override
            public void onUserFetch(User user) {
                loggedInUser = user;
                // Inflate the view only after user data is fetched
                if (getView() != null) {
                    setupViews(getView());
                }
            }
        });
    }

    interface OnUserFetchListener {
        void onUserFetch(User user);
    }

    private void setupViews(View view) {
        tvEmail = view.findViewById(R.id.tvOrganiserEmail);
        etName = view.findViewById(R.id.editOrganiserName);
        etLastName = view.findViewById(R.id.editOrganiserLastName);
        etAddress = view.findViewById(R.id.editOrganiserAddress);
        etPhone = view.findViewById(R.id.editOrganiserPhone);
        etCurrentPassword = view.findViewById(R.id.organiserCurrentPassword);
        etNewPassword1 = view.findViewById(R.id.organiserNewPassword1);
        etNewPassword2 = view.findViewById(R.id.organiserNewPassword2);

        tvEmail.setText(loggedInUser.getEmail());
        etName.setText(loggedInUser.getFirstName());
        etLastName.setText(loggedInUser.getLastName());
        etAddress.setText(loggedInUser.getAddress());
        etPhone.setText(loggedInUser.getPhone());
    }

    private void deactivateAccount() {
        loggedInUser.setActive(false);
        userRepository.updateUser(loggedInUser).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Intent intent;
                intent = new Intent(getActivity(), UnauthActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Handle failure
                Log.e("OrganiserProfileFragment", "Failed to deactivate account: " + e.getMessage());
            }
        });
    }

    private void updatePassword() {
        String currentPassword = etCurrentPassword.getText().toString();
        String newPassword1 = etNewPassword1.getText().toString();
        String newPassword2 = etNewPassword2.getText().toString();

        if (currentPassword.equals(loggedInUser.getPassword()) && newPassword1.equals(newPassword2)) {
            loggedInUser.setPassword(newPassword1);
            mAuth.getCurrentUser().updatePassword(newPassword1).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) { }
                }
            });
            userRepository.updateUser(loggedInUser).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    etCurrentPassword.setText("");
                    etNewPassword1.setText("");
                    etNewPassword2.setText("");
                    Toast.makeText(getActivity(), "Password updated successfully", Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    // Handle failure
                    Log.e("OrganiserProfileFragment", "Failed to update password: " + e.getMessage());
                    Toast.makeText(getActivity(), "Failed to update password", Toast.LENGTH_SHORT).show();
                }
            });
        } else if (!currentPassword.equals(loggedInUser.getPassword())) {
            Toast.makeText(getActivity(), "Incorrect password!", Toast.LENGTH_SHORT).show();
        } else if (!newPassword1.equals(newPassword2)) {
            Toast.makeText(getActivity(), "Passwords do not match!", Toast.LENGTH_SHORT).show();
        }
    }

    private void saveChanges() {
        String updatedName = etName.getText().toString();
        String updatedLastName = etLastName.getText().toString();
        String updatedAddress = etAddress.getText().toString();
        String updatedPhone = etPhone.getText().toString();

        loggedInUser.setFirstName(updatedName);
        loggedInUser.setLastName(updatedLastName);
        loggedInUser.setAddress(updatedAddress);
        loggedInUser.setPhone(updatedPhone);

        userRepository.updateUser(loggedInUser).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getActivity(), "Changes saved successfully", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Handle failure
                Log.e("OrganiserProfileFragment", "Failed to save changes: " + e.getMessage());
                Toast.makeText(getActivity(), "Failed to save changes", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getLoggedInUser(OnUserFetchListener listener) {
        if (mAuth.getCurrentUser() != null) {
            userRepository.getUserByEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<User>() {
                @Override
                public void onComplete(@NonNull Task<User> task) {
                    if (task.isSuccessful()) {
                        User user = task.getResult();
                        if (listener != null) {
                            listener.onUserFetch(user);
                        }
                    } else {
                        Log.e("CompanyOverviewFragment", "Failed to get logged in vendor: " + task.getException());
                    }
                }
            });
        }
    }
}
