package com.example.eventplanner.fragments.typesCategories;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.eventplanner.R;
import com.example.eventplanner.database.DBHandler;
import com.example.eventplanner.databinding.FragmentCategoryBinding;
import com.example.eventplanner.model.Category;


import java.util.ArrayList;
import java.util.List;
public class CategoryRecyclerViewAdapter extends RecyclerView.Adapter<CategoryRecyclerViewAdapter.ViewHolder> {

    private List<Category> categories;
    private CategoryRecyclerViewAdapter.OnItemClickListener listener;
    private Context context;
    private DBHandler dbHandler;

    public CategoryRecyclerViewAdapter(List<Category> categories, Context context) {
        this.context = context;
        this.dbHandler = DBHandler.getInstance(context);
        this.categories = categories;
    }


    @NonNull
    @Override
    public CategoryRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_category, parent, false);
        return new CategoryRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryRecyclerViewAdapter.ViewHolder holder, int position) {
        Category category = categories.get(position);
        holder.tvName.setText(category.getName());
        holder.tvDescription.setText(category.getDescription());
        holder.tvSubcategoryCount.setText("Number Of Subcategories: " + dbHandler.getSubcategoriesForCategory(category.getId()).size());

        if (category.isActive()) {
            holder.tvStatus.setText("ACTIVE");

        } else {
            holder.tvStatus.setText("INACTIVE");

        }

        holder.itemView.setOnClickListener(v -> {
            if (listener != null) {
                listener.onItemClick(category);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public void setOnItemClickListener(CategoryRecyclerViewAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(Category eventType);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView tvName;
        public final TextView tvDescription;
        public final TextView tvStatus;
        public final TextView tvSubcategoryCount;
        public ViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvCategoryName);
            tvDescription = view.findViewById(R.id.tvCategoryDescription);
            tvStatus = view.findViewById(R.id.tvCategoryStatus);
            tvSubcategoryCount = view.findViewById(R.id.tvSubcategoryCount);
        }
    }

}