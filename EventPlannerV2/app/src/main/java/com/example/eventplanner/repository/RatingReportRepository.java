package com.example.eventplanner.repository;

import android.util.Log;

import com.example.eventplanner.model.CompanyRatingReport;
import com.example.eventplanner.model.Reservation;
import com.example.eventplanner.repository.interfaces.RatingReportRepositoryInterface;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class RatingReportRepository implements RatingReportRepositoryInterface {
    private static final String TAG = "RatingReportRepository";
    private FirebaseFirestore db;

    public RatingReportRepository(FirebaseFirestore firestore) {
        this.db = firestore;
    }

    @Override
    public void addReport(CompanyRatingReport ratingReport) {
        db.collection("companyRatingReports")
                .add(ratingReport)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Report for rating successfully added");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error adding report for rating", e);
                });
    }

    @Override
    public Task<List<CompanyRatingReport>> getAll() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("companyRatingReports").get().continueWith(task -> {
            List<CompanyRatingReport> ratingReports = new ArrayList<>();
            if(task.isSuccessful()) {
                QuerySnapshot querySnapshot = task.getResult();
                if(querySnapshot != null && !querySnapshot.isEmpty()) {
                    for(DocumentSnapshot documentSnapshot: querySnapshot.getDocuments()) {
                        CompanyRatingReport ratingReport = documentSnapshot.toObject(CompanyRatingReport.class);
                        ratingReports.add(ratingReport);
                    }
                }
            }
            return ratingReports;
        });
    }

    @Override
    public void changeReportStatus(String reportId, CompanyRatingReport.RatingReportStatus newStatus) {
       FirebaseFirestore db = FirebaseFirestore.getInstance();
       db.collection("companyRatingReports")
               .whereEqualTo("id", reportId)
               .get().addOnSuccessListener(queryDocumentSnapshots -> {
                   if(!queryDocumentSnapshots.isEmpty()) {
                       DocumentSnapshot documentSnapshot = queryDocumentSnapshots.getDocuments().get(0);
                       String docId = documentSnapshot.getId();
                       DocumentReference documentReference = db.collection("companyRatingReports").document(docId);
                       documentReference.update("reportStatus", newStatus.name())
                               .addOnSuccessListener(aVoid -> {
                                   Log.d(TAG, "Report status updated");
                               })
                               .addOnFailureListener(e -> {
                                   Log.e(TAG, "Error during report status update", e);
                               });
                   } else {
                       Log.d(TAG, "No rating report found for id: " + reportId);
                   }
               })
               .addOnFailureListener(e -> {
                   Log.e(TAG, "Error finding rating report", e);
               });
    }

}
