package com.example.eventplanner.fragments.event;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.serviceProduct.ServiceFragment;
import com.example.eventplanner.fragments.vendorStaff.VendorStaffFragment;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.VendorStaffEvent;
import com.example.eventplanner.model.VendorStaffWorkingTime;
import com.example.eventplanner.model.WorkingTime;
import com.example.eventplanner.repository.interfaces.NotificationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.StaffEventRepositoryInterface;
import com.example.eventplanner.repository.interfaces.StaffWorkingTimeRepositoryInterface;
import com.example.eventplanner.utils.FragmentUtils;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.Timestamp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class AddStaffEventFragment extends Fragment {

    private static final String ARG_SELECTED_STAFF = "selected-staff";

    private User selectedStaff;
    private EditText nameEditText, dateEditText, fromTimeEditText, toTimeEditText;
    private StaffEventRepositoryInterface staffEventRepository;
    private StaffWorkingTimeRepositoryInterface staffWorkingTimeRepository;
    private NotificationRepositoryInterface notificationRepository;

    public AddStaffEventFragment() {
        // Required empty public constructor
    }

    public static AddStaffEventFragment newInstance(User selectedStaff) {
        AddStaffEventFragment fragment = new AddStaffEventFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_SELECTED_STAFF, selectedStaff);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            selectedStaff = (User) getArguments().getSerializable(ARG_SELECTED_STAFF);
        }

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        staffEventRepository = eventPlannerApp.getDIContainer().resolve(StaffEventRepositoryInterface.class);
        staffWorkingTimeRepository = eventPlannerApp.getDIContainer().resolve(StaffWorkingTimeRepositoryInterface.class);
        notificationRepository = eventPlannerApp.getDIContainer().resolve(NotificationRepositoryInterface.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_staff_event, container, false);

        nameEditText = view.findViewById(R.id.nameEditText);
        dateEditText = view.findViewById(R.id.dateEditText);
        fromTimeEditText = view.findViewById(R.id.fromTimeEditText);
        toTimeEditText = view.findViewById(R.id.toTimeEditText);

        setupDatePicker(dateEditText);
        setupTimePicker(fromTimeEditText);
        setupTimePicker(toTimeEditText);

        Button addButton = view.findViewById(R.id.btnAdd);
        addButton.setOnClickListener(v -> addEvent());

        return view;
    }

    private void setupTimePicker(EditText editText) {
        editText.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(
                    getContext(),
                    (view, hourOfDay, minuteOfHour) -> {
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minuteOfHour);
                        editText.setText(String.format(Locale.getDefault(), "%02d:%02d", hourOfDay, minuteOfHour));
                    },
                    hour,
                    minute,
                    android.text.format.DateFormat.is24HourFormat(getContext())
            );
            timePickerDialog.show();
        });
    }

    private void setupDatePicker(EditText editText) {
        editText.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(
                    getContext(),
                    (view, year1, monthOfYear, dayOfMonth) -> {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
                        String formattedDate = dateFormat.format(calendar.getTime());
                        editText.setText(formattedDate);
                    },
                    year,
                    month,
                    day
            );
            datePickerDialog.show();
        });
    }

    private boolean validate() {

        String name = nameEditText.getText().toString();
        String date = dateEditText.getText().toString();
        String startTime = fromTimeEditText.getText().toString();
        String endTime = toTimeEditText.getText().toString();

        if (name.isEmpty() || date.isEmpty() || startTime.isEmpty() || endTime.isEmpty()) {
            Toast.makeText(getContext(), "Please fill in all the fields", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


    private void addEvent() {
        if (!validate()) {
            return;
        }

        String name = nameEditText.getText().toString().trim();
        String dateString = dateEditText.getText().toString().trim();
        String startTime = fromTimeEditText.getText().toString().trim();
        String endTime = toTimeEditText.getText().toString().trim();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        Date date = null;
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Invalid date format", Toast.LENGTH_SHORT).show();
            return;
        }

        VendorStaffEvent newEvent = new VendorStaffEvent(UUID.randomUUID().toString(), name, date, startTime, endTime, VendorStaffEvent.Type.OCCUPIED, selectedStaff.getEmail());

        Task<List<VendorStaffEvent>> existingEventsTask = staffEventRepository.getEventsByStaff(selectedStaff.getEmail());
        Task<VendorStaffWorkingTime> workingTimeTask = staffWorkingTimeRepository.getWorkingTimeByStaff(selectedStaff.getEmail());

        Tasks.whenAllComplete(existingEventsTask, workingTimeTask).addOnCompleteListener(tasks -> {
            if (existingEventsTask.isSuccessful() && workingTimeTask.isSuccessful() &&
                    existingEventsTask.getResult() != null && workingTimeTask.getResult() != null) {

                List<VendorStaffEvent> existingEvents = existingEventsTask.getResult();
                VendorStaffWorkingTime workingTime = workingTimeTask.getResult();

                if (isOverlapping(existingEvents, newEvent)) {
                    Toast.makeText(getContext(), "Event overlaps with existing event", Toast.LENGTH_SHORT).show();
                } else if (!isWithinWorkingHours(newEvent, workingTime)) {
                    Toast.makeText(getContext(), "Event is outside of working hours", Toast.LENGTH_SHORT).show();
                } else {
                    staffEventRepository.addStaffEvent(newEvent);
                    Toast.makeText(getContext(), "Staff Event Created!", Toast.LENGTH_SHORT).show();

                    Notification notification = new Notification(UUID.randomUUID().toString(), "New event for staff", "Vendor added new event for you", newEvent.getVendorStaffEmail(), Notification.NotificationStatus.CREATED, Timestamp.now());
                    notificationRepository.createNotification(notification);

                    FragmentUtils.replaceFragment(
                            getParentFragmentManager(),
                            new VendorStaffFragment(),
                            R.id.vendor_fragment_container,
                            getActivity().findViewById(R.id.vendor_nav_view),
                            R.id.vendor_nav_item1,
                            getString(R.string.view_staff)
                    );
                }
            } else {
                Toast.makeText(getContext(), "Failed to fetch existing events or working time", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean isWithinWorkingHours(VendorStaffEvent event, VendorStaffWorkingTime workingTime) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

        try {
            Date eventStart = timeFormat.parse(event.getStartTime());
            Date eventEnd = timeFormat.parse(event.getEndTime());

            for (WorkingTime wt : workingTime.getWorkingTime()) {
                Date workingDayStart = timeFormat.parse(wt.getWorkingHours().getStartTime());
                Date workingDayEnd = timeFormat.parse(wt.getWorkingHours().getEndTime());

                if ((eventStart.equals(workingDayStart) || eventStart.after(workingDayStart)) &&
                        (eventEnd.equals(workingDayEnd) || eventEnd.before(workingDayEnd))) {
                    return true;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }

    private boolean isOverlapping(List<VendorStaffEvent> existingEvents, VendorStaffEvent newEvent) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        for (VendorStaffEvent event : existingEvents) {
            if (event.getDate().equals(newEvent.getDate())) {
                try {
                    Date existingStart = timeFormat.parse(event.getStartTime());
                    Date existingEnd = timeFormat.parse(event.getEndTime());
                    Date newStart = timeFormat.parse(newEvent.getStartTime());
                    Date newEnd = timeFormat.parse(newEvent.getEndTime());

                    if ((newStart.before(existingEnd) && newEnd.after(existingStart)) ||
                            (newStart.equals(existingStart) || newEnd.equals(existingEnd))) {
                        return true;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

}