package com.example.eventplanner.fragments.authentication;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.serviceProduct.ServiceFragment;
import com.example.eventplanner.fragments.vendorStaff.VendorStaffFragment;
import com.example.eventplanner.model.TimeInterval;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.VendorStaffWorkingTime;
import com.example.eventplanner.model.WorkingTime;
import com.example.eventplanner.repository.interfaces.CompanyRepositoryInterface;
import com.example.eventplanner.repository.interfaces.StaffWorkingTimeRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.example.eventplanner.utils.FragmentUtils;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class VendorStaffRegistrationFragment extends Fragment {

    private int currentStep = 1;
    private LinearLayout layoutStep1, layoutStep2, layoutStep3, layoutStep4, layoutStep5,layoutStep6, layoutStep7, layoutStep8;
    private EditText etFirstName, etLastName, etEmail, etAddress, etPhoneNumber, etPassword, etConfirmPassword, etCompanyName, etCompanyDescription, etCompanyAddress ,etCompanyEmail, etCompanyPhoneNumber,etCategory, etEventType;
    private ImageButton btnPrevious, btnNext;
    private ImageView ivUploadedPhoto, ivUploadedCompanyPhoto;

    private Spinner spnMonStart, spnMonEnd, spnTueStart, spnTueEnd, spnWedStart, spnWedEnd, spnThuStart, spnThuEnd, spnFriStart, spnFriEnd, spnSatStart, spnSatEnd, spnSunStart, spnSunEnd;

    private ListView lvCategories, lvEventTypes;
    private List<String> categoriesList = new ArrayList<>();
    private List<String> eventTypesList = new ArrayList<>();

    private FrameLayout layoutUploadPhoto, layoutUploadCompanyPhoto;
    private ActivityResultLauncher<String> requestPermissionLauncher;
    private ActivityResultLauncher<Intent> pickImageLauncher;
    private ActivityResultLauncher<Intent> pickCompanyImageLauncher;

    private UserRepositoryInterface userRepository;
    private CompanyRepositoryInterface companyRepository;
    private StaffWorkingTimeRepositoryInterface staffWorkingTimeRepository;

    public VendorStaffRegistrationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);
        companyRepository = eventPlannerApp.getDIContainer().resolve(CompanyRepositoryInterface.class);
        staffWorkingTimeRepository = eventPlannerApp.getDIContainer().resolve(StaffWorkingTimeRepositoryInterface.class);

        // Initialize the permission launcher
        requestPermissionLauncher =
                registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                    if (isGranted) {
                        // Permission is granted. Open the gallery.
                        openGallery();
                    } else {
                        // Permission denied.
                        Toast.makeText(getContext(), "Permission denied. Can't access gallery.", Toast.LENGTH_LONG).show();
                    }
                });

        // Initialize the pick image launcher
        pickImageLauncher =
                registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                        result -> {
                            if (result.getResultCode() == getActivity().RESULT_OK && result.getData() != null) {
                                ivUploadedPhoto.setImageURI(result.getData().getData());
                                ivUploadedPhoto.setVisibility(View.VISIBLE);
                            }
                        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vendor_staff_registration, container, false);

        initViews(view);
        btnPrevious.setOnClickListener(v -> navigateToPreviousStep());
        btnNext.setOnClickListener(v -> {

            if(validateStep()) {
                if (currentStep == 3) {
                    saveUser();
                } else {
                    navigateToNextStep();
                }
            }
        });

        return view;
    }

    private void initViews(View view) {
        layoutStep1 = view.findViewById(R.id.vendor_staff_layout_step1);
        layoutStep2 = view.findViewById(R.id.vendor_staff_layout_step2);
        layoutStep3 = view.findViewById(R.id.vendor_staff_layout_step3);

        etFirstName = view.findViewById(R.id.vendor_staff_et_first_name);
        etLastName = view.findViewById(R.id.vendor_staff_et_last_name);
        etEmail = view.findViewById(R.id.vendor_staff_et_email);
        etAddress = view.findViewById(R.id.vendor_staff_et_address);
        etPhoneNumber = view.findViewById(R.id.vendor_staff_et_phone_number);
        etPassword = view.findViewById(R.id.vendor_staff_et_password);
        etConfirmPassword = view.findViewById(R.id.vendor_staff_et_confirm_password);

        btnPrevious = view.findViewById(R.id.vendor_staff_btn_previous);
        btnNext = view.findViewById(R.id.vendor_staff_btn_next);

        ivUploadedPhoto = view.findViewById(R.id.vendor_staff_iv_uploaded_photo);

        spnMonStart = view.findViewById(R.id.vendor_staff_spn_mon_start);
        spnMonEnd = view.findViewById(R.id.vendor_staff_spn_mon_end);
        spnTueStart = view.findViewById(R.id.vendor_staff_spn_tue_start);
        spnTueEnd = view.findViewById(R.id.vendor_staff_spn_tue_end);
        spnWedStart = view.findViewById(R.id.vendor_staff_spn_wed_start);
        spnWedEnd = view.findViewById(R.id.vendor_staff_spn_wed_end);
        spnThuStart = view.findViewById(R.id.vendor_staff_spn_thu_start);
        spnThuEnd = view.findViewById(R.id.vendor_staff_spn_thu_end);
        spnFriStart = view.findViewById(R.id.vendor_staff_spn_fri_start);
        spnFriEnd = view.findViewById(R.id.vendor_staff_spn_fri_end);
        spnSatStart = view.findViewById(R.id.vendor_staff_spn_sat_start);
        spnSatEnd = view.findViewById(R.id.vendor_staff_spn_sat_end);
        spnSunStart = view.findViewById(R.id.vendor_staff_spn_sun_start);
        spnSunEnd = view.findViewById(R.id.vendor_staff_spn_sun_end);

        // Initialize the FrameLayout for photo upload
        layoutUploadPhoto = view.findViewById(R.id.vendor_staff_layout_upload_photo);


        // Set OnClickListener for layoutUploadPhoto
        if (layoutUploadPhoto != null) {
            layoutUploadPhoto.setOnClickListener(v -> requestGalleryPermission());
        }

    }


    private void navigateToPreviousStep() {
        if (currentStep > 1) {
            currentStep--;
            updateStepVisibility();
        }
    }

    private void navigateToNextStep() {
        if (currentStep < 3) {
            currentStep++;
            updateStepVisibility();
        } else {
            FragmentUtils.replaceFragment(
                    getParentFragmentManager(),
                    new ServiceFragment(),
                    R.id.vendor_fragment_container,
                    (NavigationView) getActivity().findViewById(R.id.vendor_nav_view),
                    R.id.vendor_nav_item1,
                    getString(R.string.view_services)
            );

        }
    }

    private void updateStepVisibility() {
        layoutStep1.setVisibility(currentStep == 1 ? View.VISIBLE : View.GONE);
        layoutStep2.setVisibility(currentStep == 2 ? View.VISIBLE : View.GONE);
        layoutStep3.setVisibility(currentStep == 3 ? View.VISIBLE : View.GONE);
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickImageLauncher.launch(intent);
    }

    private void requestGalleryPermission() {
        requestPermissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE);
    }


    private boolean validateStep() {
        switch (currentStep) {
            case 1:
                return validateStep1();
            case 2:
                return validateStep2();
            case 3:
                return validateStep3();
            default:
                return true;
        }
    }

    private boolean validateStep1() {
        if (etFirstName.getText().toString().isEmpty() ||
                etLastName.getText().toString().isEmpty() ||
                etEmail.getText().toString().isEmpty() ||
                etAddress.getText().toString().isEmpty() ||
                etPhoneNumber.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Please fill in all the fields.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateStep2() {
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();

        if (password.isEmpty() || confirmPassword.isEmpty()) {
            Toast.makeText(getContext(), "Please enter both password and confirm password.", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (password.length() < 4) {
            Toast.makeText(getContext(), "Password must be at least 4 characters.", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!password.equals(confirmPassword)) {
            Toast.makeText(getContext(), "Passwords do not match.", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }
    private boolean validateStep3() {
        // Retrieve the selected start and end times from spinners
        int monStart, monEnd, tueStart, tueEnd, wedStart, wedEnd, thuStart, thuEnd,
                friStart, friEnd, satStart, satEnd, sunStart, sunEnd;

        try {
            monStart = spnMonStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnMonStart.getSelectedItem().toString());
            monEnd = spnMonEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnMonEnd.getSelectedItem().toString());
            tueStart = spnTueStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnTueStart.getSelectedItem().toString());
            tueEnd = spnTueEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnTueEnd.getSelectedItem().toString());
            wedStart = spnWedStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnWedStart.getSelectedItem().toString());
            wedEnd = spnWedEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnWedEnd.getSelectedItem().toString());
            thuStart = spnThuStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnThuStart.getSelectedItem().toString());
            thuEnd = spnThuEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnThuEnd.getSelectedItem().toString());
            friStart = spnFriStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnFriStart.getSelectedItem().toString());
            friEnd = spnFriEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnFriEnd.getSelectedItem().toString());
            satStart = spnSatStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnSatStart.getSelectedItem().toString());
            satEnd = spnSatEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnSatEnd.getSelectedItem().toString());
            sunStart = spnSunStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnSunStart.getSelectedItem().toString());
            sunEnd = spnSunEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnSunEnd.getSelectedItem().toString());
        } catch (NumberFormatException e) {

            Toast.makeText(getContext(), "Invalid time format.", Toast.LENGTH_SHORT).show();
            return false;
        }

        boolean allTimesEmpty = (monStart == -1 || monEnd == -1 || tueStart == -1 || tueEnd == -1 || wedStart == -1 || wedEnd == -1 ||
                                 thuStart == -1 || thuEnd == -1 || friStart == -1 || friEnd == -1 || satStart == -1 || satEnd == -1 ||
                                 sunStart == -1 || sunEnd == -1);

        if (!allTimesEmpty) {
            return true;
        }

        if (monStart != -1 && monEnd != -1 && monStart >= monEnd) {
            Toast.makeText(getContext(), "Monday: Start time must be before end time.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (tueStart != -1 && tueEnd != -1 && tueStart >= tueEnd) {
            Toast.makeText(getContext(), "Tuesday: Start time must be before end time.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (wedStart != -1 && wedEnd != -1 && wedStart >= wedEnd) {
            Toast.makeText(getContext(), "Wednesday: Start time must be before end time.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (thuStart != -1 && thuEnd != -1 && thuStart >= thuEnd) {
            Toast.makeText(getContext(), "Thursday: Start time must be before end time.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (friStart != -1 && friEnd != -1 && friStart >= friEnd) {
            Toast.makeText(getContext(), "Friday: Start time must be before end time.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (satStart != -1 && satEnd != -1 && satStart >= satEnd) {
            Toast.makeText(getContext(), "Saturday: Start time must be before end time.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (sunStart != -1 && sunEnd != -1 && sunStart >= sunEnd) {
            Toast.makeText(getContext(), "Sunday: Start time must be before end time.", Toast.LENGTH_SHORT).show();
            return false;
        }

        createWorkingTimeForUser(monStart, monEnd, tueStart, tueEnd, wedStart, wedEnd,
                thuStart, thuEnd, friStart, friEnd, satStart, satEnd,
                sunStart, sunEnd);

        return true;
    }

    private void saveUser() {
        if (!validateStep()) {
            return;
        }

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser == null) {
            Toast.makeText(getContext(), "No logged in user found.", Toast.LENGTH_SHORT).show();
            return;
        }

        String currentUserEmail = currentUser.getEmail();
        Log.d("LoggedInUserEmail", "Logged-in user email: " + currentUserEmail);

        String firstName = etFirstName.getText().toString().trim();
        String lastName = etLastName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String address = etAddress.getText().toString().trim();
        String phoneNumber = etPhoneNumber.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        String profilePhotoPath = savePhotoToInternalStorage(ivUploadedPhoto);
        User user = new User(email, password, firstName, lastName, address, phoneNumber, profilePhotoPath, User.UserRole.VENDOR_STAFF, false);

        userRepository.addUser(user)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser currentUserAfterRegistration = FirebaseAuth.getInstance().getCurrentUser();
                        if (currentUserAfterRegistration != null) {
                            currentUserAfterRegistration.sendEmailVerification()
                                    .addOnCompleteListener(emailTask -> {
                                        if (emailTask.isSuccessful()) {
                                            Toast.makeText(getContext(), "Activation link sent to " + email, Toast.LENGTH_SHORT).show();
                                            addUserToCompanyAndCreateWorkingTime(currentUserEmail, user);
                                            FragmentUtils.replaceFragment(
                                                    getParentFragmentManager(),
                                                    new VendorStaffFragment(),
                                                    R.id.vendor_fragment_container,
                                                    getActivity().findViewById(R.id.vendor_nav_view),
                                                    R.id.vendor_nav_item1,
                                                    getString(R.string.view_staff)
                                            );
                                        } else {
                                            Toast.makeText(getContext(), "Failed to send verification email.", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        } else {
                            Log.e("AuthError", "FirebaseUser is null after registration");
                        }
                    } else {
                        Toast.makeText(getContext(), "Registration failed: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void addUserToCompanyAndCreateWorkingTime(String loggedInUserEmail, User user) {
        companyRepository.getCompanyByOwnerEmail(loggedInUserEmail)
                .addOnSuccessListener(company -> {
                    if (company != null) {
                        Log.d("CompanyData", "Company data: " + company.toString());
                        companyRepository.addEmployeeToCompany(loggedInUserEmail, user.getEmail());

                        List<WorkingTime> workingTimes = createWorkingTimeForUser(
                                spnMonStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnMonStart.getSelectedItem().toString()),
                                spnMonEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnMonEnd.getSelectedItem().toString()),
                                spnTueStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnTueStart.getSelectedItem().toString()),
                                spnTueEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnTueEnd.getSelectedItem().toString()),
                                spnWedStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnWedStart.getSelectedItem().toString()),
                                spnWedEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnWedEnd.getSelectedItem().toString()),
                                spnThuStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnThuStart.getSelectedItem().toString()),
                                spnThuEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnThuEnd.getSelectedItem().toString()),
                                spnFriStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnFriStart.getSelectedItem().toString()),
                                spnFriEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnFriEnd.getSelectedItem().toString()),
                                spnSatStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnSatStart.getSelectedItem().toString()),
                                spnSatEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnSatEnd.getSelectedItem().toString()),
                                spnSunStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnSunStart.getSelectedItem().toString()),
                                spnSunEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnSunEnd.getSelectedItem().toString())
                        );

                        VendorStaffWorkingTime vendorStaffWorkingTime = new VendorStaffWorkingTime();
                        vendorStaffWorkingTime.setVendorStaffEmail(user.getEmail());
                        vendorStaffWorkingTime.setFromDate(Calendar.getInstance().getTime());
                        vendorStaffWorkingTime.setToDate(null);
                        if(workingTimes == null || workingTimes.isEmpty()) {
                            vendorStaffWorkingTime.setWorkingTime(workingTimes);
                        } else {
                            vendorStaffWorkingTime.setWorkingTime(company.getCompanyWorkingTime());
                        }
                        staffWorkingTimeRepository.addWorkingTime(vendorStaffWorkingTime);
                    } else {
                        Log.e("CompanyData", "Company not found for owner with email: " + loggedInUserEmail);
                    }
                })
                .addOnFailureListener(e -> {
                    Log.e("CompanyFetchError", "Error finding company: " + e.getMessage(), e);
                });

    }

    private List<WorkingTime> createWorkingTimeForUser(int monStart, int monEnd, int tueStart, int tueEnd,
                                          int wedStart, int wedEnd, int thuStart, int thuEnd,
                                          int friStart, int friEnd, int satStart, int satEnd,
                                          int sunStart, int sunEnd) {

        List<WorkingTime> workingTimeList = new ArrayList<>();

        if(monStart != 1 && monEnd != -1) {
            WorkingTime mondayWorkingTime = new WorkingTime(WorkingTime.Days.MONDAY, new TimeInterval(String.valueOf(monStart), String.valueOf(monEnd)));
            workingTimeList.add(mondayWorkingTime);
        }
        if(tueStart != -1 && tueEnd != -1) {
            WorkingTime tuesdayWorkingTime = new WorkingTime(WorkingTime.Days.TUESDAY, new TimeInterval(String.valueOf(tueStart), String.valueOf(tueEnd)));
            workingTimeList.add(tuesdayWorkingTime);
        }
        if(wedStart != -1 && wedEnd != -1) {
            WorkingTime wednesdayWorkingTime = new WorkingTime(WorkingTime.Days.WEDNESDAY, new TimeInterval(String.valueOf(wedStart), String.valueOf(wedEnd)));
            workingTimeList.add(wednesdayWorkingTime);
        }
        if(thuStart != -1 && thuEnd != -1) {
            WorkingTime thursdayWorkingTime = new WorkingTime(WorkingTime.Days.THURSDAY, new TimeInterval(String.valueOf(thuStart), String.valueOf(thuEnd)));
            workingTimeList.add(thursdayWorkingTime);
        }
        if(friStart != -1 && friEnd != -1) {
            WorkingTime fridayWorkingTime = new WorkingTime(WorkingTime.Days.FRIDAY, new TimeInterval(String.valueOf(friStart), String.valueOf(friEnd)));
            workingTimeList.add(fridayWorkingTime);
        }
        if(satStart != -1 && satEnd != -1) {
            WorkingTime saturdayWorkingTime = new WorkingTime(WorkingTime.Days.SATURDAY, new TimeInterval(String.valueOf(satStart), String.valueOf(satEnd)));
            workingTimeList.add(saturdayWorkingTime);
        }
        if(sunStart != -1 && sunEnd != -1) {
            WorkingTime sundayWorkingTime = new WorkingTime(WorkingTime.Days.SUNDAY, new TimeInterval(String.valueOf(sunStart), String.valueOf(sunEnd)));
            workingTimeList.add(sundayWorkingTime);
        }
        return workingTimeList;
    }


    private String savePhotoToInternalStorage(ImageView imageView) {
        BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
        Bitmap bitmap = drawable.getBitmap();

        ContextWrapper wrapper = new ContextWrapper(getActivity().getApplicationContext()); // Use getActivity().getApplicationContext() here
        File file = wrapper.getDir("EventPlannerImages", Context.MODE_PRIVATE);
        file = new File(file, "profile_photo.jpg");

        try {
            OutputStream stream = null;
            stream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.flush();
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return file.getAbsolutePath();
    }
}