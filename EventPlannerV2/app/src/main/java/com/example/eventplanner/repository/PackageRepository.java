package com.example.eventplanner.repository;

import android.util.Log;

import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.repository.interfaces.PackageRepositoryInterface;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class PackageRepository implements PackageRepositoryInterface {
    private static final String TAG = "PackageRepository";
    private FirebaseFirestore db;

    public PackageRepository(FirebaseFirestore firestore) {
        this.db = firestore;
    }

    @Override
    public void addPackage(Package newPackage) {
        db.collection("packages")
                .add(newPackage)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Package successfully added");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error adding package", e);
                });
    }

    @Override
    public Task<List<Package>> getAllPackages() {
        return db.collection("packages")
                .get()
                .continueWith(task -> {
                    List<Package> packages = new ArrayList<>();
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                Package packageObj = documentSnapshot.toObject(Package.class);
                                packages.add(packageObj);
                            }
                        }
                    }
                    return packages;
                });
    }

    @Override
    public Task<List<Package>> getAllVisiblePackages() {
        return db.collection("packages")
                .whereEqualTo("visible", true)
                .get()
                .continueWith(task -> {
                    List<Package> packages = new ArrayList<>();
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                Package packageObj = documentSnapshot.toObject(Package.class);
                                packages.add(packageObj);
                            }
                        }
                    }
                    return packages;
                });
    }

    @Override
    public Task<Package> getByName(String itemName) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("packages")
                .whereEqualTo("name", itemName)
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
                            return documentSnapshot.toObject(Package.class);
                        }
                    }
                    return null;
                });
    }
}
