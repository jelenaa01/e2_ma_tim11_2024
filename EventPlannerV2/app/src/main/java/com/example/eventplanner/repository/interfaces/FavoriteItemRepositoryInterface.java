package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.FavoriteItem;
import com.google.android.gms.tasks.Task;

import java.util.List;

public interface FavoriteItemRepositoryInterface {
    public void addFavoriteItem(FavoriteItem favoriteItem);
    public Task<List<FavoriteItem>> getByEmail(String email);

    Task<Boolean> existsByNameAndEmail(String name, String email);

    void removeByNameAndEmail(String name, String email);
}
