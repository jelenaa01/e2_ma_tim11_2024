package com.example.eventplanner.model;

import java.io.Serializable;
import java.time.Period;
import java.util.List;

public class Package implements Serializable {
    public enum ConfirmationMode {
        AUTOMATIC,
        MANUAL
    }
    private String id;
    private String name;
    private String description;
    private double price;
    private double discount;
    private List<String> images;
    private boolean visible;
    private boolean available;
    private String category;
    private List<Service> services;
    private List<String> subcategories;
    private List<String> eventTypes;
    private int bookingDeadline;
    private int cancellationDeadline;
    private ConfirmationMode confirmationMode;

    public Package() {}

    public Package(String id, String name, String description, double price, double discount, List<String> images,
                   boolean visible, boolean available, String category, List<Service> services, List<String> subcategories,
                   List<String> eventTypes, int bookingDeadline, int cancellationDeadline, ConfirmationMode confirmationMode) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.images = images;
        this.visible = visible;
        this.available = available;
        this.category = category;
        this.services = services;
        this.subcategories = subcategories;
        this.eventTypes = eventTypes;
        this.bookingDeadline = bookingDeadline;
        this.cancellationDeadline = cancellationDeadline;
        this.confirmationMode = confirmationMode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public List<String> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<String> subcategories) {
        this.subcategories = subcategories;
    }

    public List<String> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(List<String> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public int getBookingDeadline() {
        return bookingDeadline;
    }

    public void setBookingDeadline(int bookingDeadline) {
        this.bookingDeadline = bookingDeadline;
    }

    public int getCancellationDeadline() {
        return cancellationDeadline;
    }

    public void setCancellationDeadline(int cancellationDeadline) {
        this.cancellationDeadline = cancellationDeadline;
    }

    public ConfirmationMode getConfirmationMode() {
        return confirmationMode;
    }

    public void setConfirmationMode(ConfirmationMode confirmationMode) {
        this.confirmationMode = confirmationMode;
    }

    @Override
    public String toString() {
        return "Package{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", discount=" + discount +
                ", images=" + images +
                ", visible=" + visible +
                ", available=" + available +
                ", category='" + category + '\'' +
                ", services=" + services +
                ", subcategories=" + subcategories +
                ", eventTypes=" + eventTypes +
                ", bookingDeadline='" + bookingDeadline + '\'' +
                ", cancellationDeadline='" + cancellationDeadline + '\'' +
                ", confirmationMode=" + confirmationMode +
                '}';
    }
}
