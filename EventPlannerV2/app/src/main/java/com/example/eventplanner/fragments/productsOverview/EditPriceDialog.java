package com.example.eventplanner.fragments.productsOverview;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import com.example.eventplanner.R;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.repository.interfaces.ProductRepositoryInterface;

public class EditPriceDialog extends DialogFragment {

    private Product selectedProduct;
    private ProductRepositoryInterface productRepository;

    public EditPriceDialog(Product selectedProduct, ProductRepositoryInterface productRepository) {
        this.selectedProduct = selectedProduct;
        this.productRepository = productRepository;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_edit_price);

        EditText priceEditText = dialog.findViewById(R.id.priceEditText);
        EditText discountEditText = dialog.findViewById(R.id.discountEditText);
        Button saveButton = dialog.findViewById(R.id.saveButton);

        priceEditText.setText(String.valueOf(selectedProduct.getPrice()));
        discountEditText.setText(String.valueOf(selectedProduct.getDiscount()));

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double newPrice = Double.parseDouble(priceEditText.getText().toString());
                double newDiscount = Double.parseDouble(discountEditText.getText().toString());

                selectedProduct.setPrice(newPrice);
                selectedProduct.setDiscount(newDiscount);

                productRepository.updateProduct(selectedProduct)
                        .addOnSuccessListener(aVoid -> {
                            dismiss();
                            // You can add any additional actions like refreshing the UI here
                        })
                        .addOnFailureListener(e -> {
                            // Handle the error
                        });
            }
        });

        return dialog;
    }
}
