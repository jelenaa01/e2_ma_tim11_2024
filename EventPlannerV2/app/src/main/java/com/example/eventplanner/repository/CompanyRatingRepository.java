package com.example.eventplanner.repository;

import android.util.Log;

import com.example.eventplanner.model.CompanyRating;
import com.example.eventplanner.repository.interfaces.CompanyRatingRepositoryInterface;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class CompanyRatingRepository implements CompanyRatingRepositoryInterface {
    private static final String TAG = "CompanyRatingRepository";
    private FirebaseFirestore db;

    public CompanyRatingRepository(FirebaseFirestore firestore) {
        this.db = firestore;
    }

    @Override
    public void addCompanyRating(CompanyRating rating) {
        db.collection("companyRatings")
                .add(rating)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Company rating successfully added");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error adding company rating", e);
                });
    }

    @Override
    public Task<List<CompanyRating>> getRatingsByCompany(String companyId) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("companyRatings")
                .whereEqualTo("companyId", companyId)
                .get()
                .continueWith(task -> {
                    List<CompanyRating> companyRatings = new ArrayList<>();
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                CompanyRating rating = documentSnapshot.toObject(CompanyRating.class);
                                companyRatings.add(rating);
                            }
                        }
                    }
                    return companyRatings;
                });
    }

    @Override
    public void removeCompanyRating(String ratingId) {
        db.collection("companyRatings")
                .whereEqualTo("id", ratingId)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        DocumentSnapshot documentSnapshot = queryDocumentSnapshots.getDocuments().get(0);
                        String docId = documentSnapshot.getId();
                        DocumentReference docRef = db.collection("companyRatings").document(docId);

                        docRef.delete()
                                .addOnSuccessListener(aVoid -> {
                                    Log.d(TAG, "Company rating deleted");
                                })
                                .addOnFailureListener(e -> {
                                    Log.e(TAG, "Error during company rating delete", e);
                                });
                    } else {
                        Log.d(TAG, "No company rating found for id: " + ratingId);
                    }
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error finding company rating", e);
                });
    }

    @Override
    public Task<List<CompanyRating>> getAll() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("companyRatings")
                .get()
                .continueWith(task -> {
                    List<CompanyRating> companyRatings = new ArrayList<>();
                    if(task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if(querySnapshot != null && !querySnapshot.isEmpty()) {
                            for(DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                CompanyRating companyRating = documentSnapshot.toObject(CompanyRating.class);
                                companyRatings.add(companyRating);
                    }
                }
            }
            return companyRatings;
        });
    }

}
