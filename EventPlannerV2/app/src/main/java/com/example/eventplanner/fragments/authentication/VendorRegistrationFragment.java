package com.example.eventplanner.fragments.authentication;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.serviceProduct.ServiceFragment;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.RegistrationRequest;
import com.example.eventplanner.model.TimeInterval;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.WorkingTime;
import com.example.eventplanner.repository.StaffWorkingTimeRepository;
import com.example.eventplanner.repository.interfaces.CompanyRepositoryInterface;
import com.example.eventplanner.repository.interfaces.NotificationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.RegistrationRequestRepositoryInterface;
import com.example.eventplanner.repository.interfaces.StaffWorkingTimeRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.example.eventplanner.service.EmailService;
import com.example.eventplanner.utils.FragmentUtils;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.Timestamp;

import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class VendorRegistrationFragment extends Fragment {

    private int currentStep = 1;
    private LinearLayout layoutStep1, layoutStep2, layoutStep3, layoutStep4, layoutStep5,layoutStep6, layoutStep7, layoutStep8;
    private EditText etFirstName, etLastName, etEmail, etAddress, etPhoneNumber, etPassword, etConfirmPassword, etCompanyName, etCompanyDescription, etCompanyAddress ,etCompanyEmail, etCompanyPhoneNumber,etCategory, etEventType;
    private ImageButton btnPrevious, btnNext;
    private ImageView ivUploadedPhoto, ivUploadedCompanyPhoto;

    private Spinner spnMonStart, spnMonEnd, spnTueStart, spnTueEnd, spnWedStart, spnWedEnd, spnThuStart, spnThuEnd, spnFriStart, spnFriEnd, spnSatStart, spnSatEnd, spnSunStart, spnSunEnd;

    private ListView lvCategories, lvEventTypes;
    private List<String> categoriesList = new ArrayList<>();
    private List<String> eventTypesList = new ArrayList<>();

    private FrameLayout layoutUploadPhoto, layoutUploadCompanyPhoto;
    private ActivityResultLauncher<String> requestPermissionLauncher;
    private ActivityResultLauncher<Intent> pickImageLauncher;
    private ActivityResultLauncher<Intent> pickCompanyImageLauncher;
    private CompanyRepositoryInterface companyRepository;
    private StaffWorkingTimeRepositoryInterface workingTimeRepository;
    private UserRepositoryInterface userRepository;
    private RegistrationRequestRepositoryInterface registrationRequestRepository;
    private NotificationRepositoryInterface notificationRepository;
    public VendorRegistrationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);
        companyRepository = eventPlannerApp.getDIContainer().resolve(CompanyRepositoryInterface.class);

        notificationRepository = eventPlannerApp.getDIContainer().resolve(NotificationRepositoryInterface.class);

        registrationRequestRepository = eventPlannerApp.getDIContainer().resolve(RegistrationRequestRepositoryInterface.class);
        // Initialize the permission launcher
        requestPermissionLauncher =
                registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                    if (isGranted) {
                        // Permission is granted. Open the gallery.
                        openGallery();
                    } else {
                        // Permission denied.
                        Toast.makeText(getContext(), "Permission denied. Can't access gallery.", Toast.LENGTH_LONG).show();
                    }
                });

        // Initialize the pick image launcher
        pickImageLauncher =
                registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                        result -> {
                            if (result.getResultCode() == getActivity().RESULT_OK && result.getData() != null) {
                                ivUploadedPhoto.setImageURI(result.getData().getData());
                                ivUploadedPhoto.setVisibility(View.VISIBLE);
                            }
                        });

        pickCompanyImageLauncher =
                registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                        result -> {
                            if (result.getResultCode() == getActivity().RESULT_OK && result.getData() != null) {
                                ivUploadedCompanyPhoto.setImageURI(result.getData().getData());
                                ivUploadedCompanyPhoto.setVisibility(View.VISIBLE);
                            }
                        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vendor_registration, container, false);

        initViews(view);
        btnPrevious.setOnClickListener(v -> navigateToPreviousStep());
        btnNext.setOnClickListener(v -> {

            if(validateStep()) {
                if (currentStep == 6) {
                    saveUser();
                } else {
                    navigateToNextStep();
                }
            }
        });

        return view;
    }

    private void initViews(View view) {
        layoutStep1 = view.findViewById(R.id.vendor_layout_step1);
        layoutStep2 = view.findViewById(R.id.vendor_layout_step2);
        layoutStep3 = view.findViewById(R.id.vendor_layout_step3);
        layoutStep4 = view.findViewById(R.id.vendor_layout_step4);
        layoutStep5 = view.findViewById(R.id.vendor_layout_step5);
        layoutStep6 = view.findViewById(R.id.vendor_layout_step6);

        etFirstName = view.findViewById(R.id.vendor_et_first_name);
        etLastName = view.findViewById(R.id.vendor_et_last_name);
        etEmail = view.findViewById(R.id.vendor_et_email);
        etAddress = view.findViewById(R.id.vendor_et_address);
        etPhoneNumber = view.findViewById(R.id.vendor_et_phone_number);
        etPassword = view.findViewById(R.id.vendor_et_password);
        etConfirmPassword = view.findViewById(R.id.vendor_et_confirm_password);
        etCategory = view.findViewById(R.id.vendor_et_category);
        etEventType = view.findViewById(R.id.vendor_et_event_type);

        etCompanyName = view.findViewById(R.id.vendor_et_company_name);
        etCompanyDescription = view.findViewById(R.id.vendor_et_company_description);
        etCompanyAddress = view.findViewById(R.id.vendor_et_company_address);
        etCompanyEmail = view.findViewById(R.id.vendor_et_company_email);
        etCompanyPhoneNumber = view.findViewById(R.id.vendor_et_company_phone_number);

        btnPrevious = view.findViewById(R.id.vendor_btn_previous);
        btnNext = view.findViewById(R.id.vendor_btn_next);

        lvCategories = view.findViewById(R.id.vendor_lv_categories);
        lvEventTypes = view.findViewById(R.id.vendor_lv_event_types);

        ivUploadedPhoto = view.findViewById(R.id.vendor_iv_uploaded_photo);
        ivUploadedCompanyPhoto = view.findViewById(R.id.vendor_iv_uploaded_company_photo);

        spnMonStart = view.findViewById(R.id.vendor_spn_mon_start);
        spnMonEnd = view.findViewById(R.id.vendor_spn_mon_end);
        spnTueStart = view.findViewById(R.id.vendor_spn_tue_start);
        spnTueEnd = view.findViewById(R.id.vendor_spn_tue_end);
        spnWedStart = view.findViewById(R.id.vendor_spn_wed_start);
        spnWedEnd = view.findViewById(R.id.vendor_spn_wed_end);
        spnThuStart = view.findViewById(R.id.vendor_spn_thu_start);
        spnThuEnd = view.findViewById(R.id.vendor_spn_thu_end);
        spnFriStart = view.findViewById(R.id.vendor_spn_fri_start);
        spnFriEnd = view.findViewById(R.id.vendor_spn_fri_end);
        spnSatStart = view.findViewById(R.id.vendor_spn_sat_start);
        spnSatEnd = view.findViewById(R.id.vendor_spn_sat_end);
        spnSunStart = view.findViewById(R.id.vendor_spn_sun_start);
        spnSunEnd = view.findViewById(R.id.vendor_spn_sun_end);

        // Initialize the FrameLayout for photo upload
        layoutUploadPhoto = view.findViewById(R.id.vendor_layout_upload_photo);
        layoutUploadCompanyPhoto = view.findViewById(R.id.vendor_layout_upload_company_photo);

        // Set OnClickListener for layoutUploadPhoto
        if (layoutUploadPhoto != null) {
            layoutUploadPhoto.setOnClickListener(v -> requestGalleryPermission());
        }

        // Set OnClickListener for layoutUploadCompanyPhoto
        if (layoutUploadCompanyPhoto != null) {
            layoutUploadCompanyPhoto.setOnClickListener(v -> requestGalleryPermission());
        }

        // Set OnClickListener for Add Category button
        Button btnAddCategory = view.findViewById(R.id.vendor_btn_add_category);
        btnAddCategory.setOnClickListener(v -> {
            String category = etCategory.getText().toString().trim();
            if (!category.isEmpty()) {
                categoriesList.add(category);
                ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
                        android.R.layout.simple_list_item_1, categoriesList);
                lvCategories.setAdapter(adapter);
                etCategory.setText("");
            }
        });

        // Set OnClickListener for Add Event Type button
        Button btnAddEventType = view.findViewById(R.id.vendor_btn_add_event_type);
        btnAddEventType.setOnClickListener(v -> {
            String eventType = etEventType.getText().toString().trim();
            if (!eventType.isEmpty()) {
                eventTypesList.add(eventType);
                ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
                        android.R.layout.simple_list_item_1, eventTypesList);
                lvEventTypes.setAdapter(adapter);
                etEventType.setText("");
            }
        });
    }


    private void navigateToPreviousStep() {
        if (currentStep > 1) {
            currentStep--;
            updateStepVisibility();
        }
    }

    private void navigateToNextStep() {
        if (currentStep < 6) {
            currentStep++;
            updateStepVisibility();
        } else {
            FragmentUtils.replaceFragment(
                    getParentFragmentManager(),
                    new ServiceFragment(),
                    R.id.fragment_container,
                    (NavigationView) getActivity().findViewById(R.id.nav_view),
                    R.id.nav_item1,
                    getString(R.string.login)
            );

            Toast.makeText(getContext(), "Registration Request Sent!", Toast.LENGTH_LONG).show();
        }
    }

    private void updateStepVisibility() {
        layoutStep1.setVisibility(currentStep == 1 ? View.VISIBLE : View.GONE);
        layoutStep2.setVisibility(currentStep == 2 ? View.VISIBLE : View.GONE);
        layoutStep3.setVisibility(currentStep == 3 ? View.VISIBLE : View.GONE);
        layoutStep4.setVisibility(currentStep == 4 ? View.VISIBLE : View.GONE);
        layoutStep5.setVisibility(currentStep == 5 ? View.VISIBLE : View.GONE);
        layoutStep6.setVisibility(currentStep == 6 ? View.VISIBLE : View.GONE);
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickImageLauncher.launch(intent);
    }

    private void requestGalleryPermission() {
        requestPermissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    private void openMap() {
        // Implement map opening code here
    }

    private boolean validateStep() {
        switch (currentStep) {
            case 1:
                return validateStep1();
            case 2:
                return validateStep2();
            case 3:
                return validateStep3();
            case 4:
                return validateStep4();
            case 5:
                return validateStep5();
            case 6:
                return validateStep6();
            default:
                return true;
        }
    }

    private boolean validateStep1() {
        if (etFirstName.getText().toString().isEmpty() ||
                etLastName.getText().toString().isEmpty() ||
                etEmail.getText().toString().isEmpty() ||
                etAddress.getText().toString().isEmpty() ||
                etPhoneNumber.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Please fill in all the fields.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateStep2() {
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();

        if (password.isEmpty() || confirmPassword.isEmpty()) {
            Toast.makeText(getContext(), "Please enter both password and confirm password.", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (password.length() < 4) {
            Toast.makeText(getContext(), "Password must be at least 4 characters.", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!password.equals(confirmPassword)) {
            Toast.makeText(getContext(), "Passwords do not match.", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private boolean validateStep3() {
        if (etCompanyName.getText().toString().isEmpty() ||
                etCompanyDescription.getText().toString().isEmpty() ||
                etCompanyEmail.getText().toString().isEmpty() ||
                etCompanyAddress.getText().toString().isEmpty() ||
                etCompanyPhoneNumber.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Please fill in all the fields.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateStep4() {
        if (categoriesList.isEmpty()) {
            Toast.makeText(getContext(), "Please add at least one category.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateStep5() {
        return true;
    }
    private boolean validateStep6() {
        // Retrieve the selected start and end times from spinners
        int monStart, monEnd, tueStart, tueEnd, wedStart, wedEnd, thuStart, thuEnd,
                friStart, friEnd, satStart, satEnd, sunStart, sunEnd;

        try {
            monStart = spnMonStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnMonStart.getSelectedItem().toString());
            monEnd = spnMonEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnMonEnd.getSelectedItem().toString());
            tueStart = spnTueStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnTueStart.getSelectedItem().toString());
            tueEnd = spnTueEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnTueEnd.getSelectedItem().toString());
            wedStart = spnWedStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnWedStart.getSelectedItem().toString());
            wedEnd = spnWedEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnWedEnd.getSelectedItem().toString());
            thuStart = spnThuStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnThuStart.getSelectedItem().toString());
            thuEnd = spnThuEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnThuEnd.getSelectedItem().toString());
            friStart = spnFriStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnFriStart.getSelectedItem().toString());
            friEnd = spnFriEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnFriEnd.getSelectedItem().toString());
            satStart = spnSatStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnSatStart.getSelectedItem().toString());
            satEnd = spnSatEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnSatEnd.getSelectedItem().toString());
            sunStart = spnSunStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnSunStart.getSelectedItem().toString());
            sunEnd = spnSunEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnSunEnd.getSelectedItem().toString());
        } catch (NumberFormatException e) {

            Toast.makeText(getContext(), "Invalid time format.", Toast.LENGTH_SHORT).show();
            return false;
        }

        // Validate each day's start and end time
        if (monStart != -1 && monEnd != -1 && monStart >= monEnd) {
            Toast.makeText(getContext(), "Monday: Start time must be before end time.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (tueStart != -1 && tueEnd != -1 && tueStart >= tueEnd) {
            Toast.makeText(getContext(), "Tuesday: Start time must be before end time.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (wedStart != -1 && wedEnd != -1 && wedStart >= wedEnd) {
            Toast.makeText(getContext(), "Wednesday: Start time must be before end time.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (thuStart != -1 && thuEnd != -1 && thuStart >= thuEnd) {
            Toast.makeText(getContext(), "Thursday: Start time must be before end time.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (friStart != -1 && friEnd != -1 && friStart >= friEnd) {
            Toast.makeText(getContext(), "Friday: Start time must be before end time.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (satStart != -1 && satEnd != -1 && satStart >= satEnd) {
            Toast.makeText(getContext(), "Saturday: Start time must be before end time.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (sunStart != -1 && sunEnd != -1 && sunStart >= sunEnd) {
            Toast.makeText(getContext(), "Sunday: Start time must be before end time.", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void saveUser() {
        if (!validateStep()) {
            return;
        }

        String firstName = etFirstName.getText().toString().trim();
        String lastName = etLastName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String address = etAddress.getText().toString().trim();
        String phoneNumber = etPhoneNumber.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        String profilePhotoPath = savePhotoToInternalStorage(ivUploadedPhoto);  // Save photo to internal storage

        String companyName = etCompanyName.getText().toString().trim();
        String companyEmail = etCompanyEmail.getText().toString().trim();
        String companyAddress = etCompanyAddress.getText().toString().trim();
        String companyPhone = etCompanyPhoneNumber.getText().toString().trim();
        String companyDescription = etCompanyDescription.getText().toString().trim();
        String companyPhotoPath = savePhotoToInternalStorage(ivUploadedPhoto);
        String ownerEmail = etEmail.getText().toString().trim();
        List<String> employeeEmails = new ArrayList<String>();

        int monStart, monEnd, tueStart, tueEnd, wedStart, wedEnd, thuStart, thuEnd,
                friStart, friEnd, satStart, satEnd, sunStart, sunEnd;

        monStart = spnMonStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnMonStart.getSelectedItem().toString());
        monEnd = spnMonEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnMonEnd.getSelectedItem().toString());
        tueStart = spnTueStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnTueStart.getSelectedItem().toString());
        tueEnd = spnTueEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnTueEnd.getSelectedItem().toString());
        wedStart = spnWedStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnWedStart.getSelectedItem().toString());
        wedEnd = spnWedEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnWedEnd.getSelectedItem().toString());
        thuStart = spnThuStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnThuStart.getSelectedItem().toString());
        thuEnd = spnThuEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnThuEnd.getSelectedItem().toString());
        friStart = spnFriStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnFriStart.getSelectedItem().toString());
        friEnd = spnFriEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnFriEnd.getSelectedItem().toString());
        satStart = spnSatStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnSatStart.getSelectedItem().toString());
        satEnd = spnSatEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnSatEnd.getSelectedItem().toString());
        sunStart = spnSunStart.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnSunStart.getSelectedItem().toString());
        sunEnd = spnSunEnd.getSelectedItem().toString().equals("-") ? -1 : Integer.parseInt(spnSunEnd.getSelectedItem().toString());

        WorkingTime monday = new WorkingTime(WorkingTime.Days.MONDAY, new TimeInterval(Integer.toString(monStart), Integer.toString(monEnd)));
        WorkingTime tuesday = new WorkingTime(WorkingTime.Days.TUESDAY, new TimeInterval(Integer.toString(tueStart), Integer.toString(tueEnd)));
        WorkingTime wednesday = new WorkingTime(WorkingTime.Days.WEDNESDAY, new TimeInterval(Integer.toString(wedStart), Integer.toString(wedEnd)));
        WorkingTime thursday = new WorkingTime(WorkingTime.Days.THURSDAY, new TimeInterval(Integer.toString(thuStart), Integer.toString(thuEnd)));
        WorkingTime friday = new WorkingTime(WorkingTime.Days.FRIDAY, new TimeInterval(Integer.toString(friStart), Integer.toString(friEnd)));
        WorkingTime saturday = new WorkingTime(WorkingTime.Days.SATURDAY, new TimeInterval(Integer.toString(satStart), Integer.toString(satEnd)));
        WorkingTime sunday = new WorkingTime(WorkingTime.Days.SUNDAY, new TimeInterval(Integer.toString(sunStart), Integer.toString(sunEnd)));

        List<WorkingTime> workingTimeList = new ArrayList<WorkingTime>();

        workingTimeList.add(monday);
        workingTimeList.add(tuesday);
        workingTimeList.add(wednesday);
        workingTimeList.add(thursday);
        workingTimeList.add(friday);
        workingTimeList.add(saturday);
        workingTimeList.add(sunday);

        Company company = new Company(UUID.randomUUID().toString(),companyName, companyEmail, companyAddress, companyPhone, companyDescription, companyPhotoPath, email, new ArrayList<String>(), workingTimeList );
        companyRepository.addCompany(company);

        User user = new User(email, password, firstName, lastName, address, phoneNumber, profilePhotoPath, User.UserRole.VENDOR, false);
        userRepository.addUser(user);

        registrationRequestRepository.createRegistrationRequest(new RegistrationRequest(UUID.randomUUID().toString(), email, RegistrationRequest.RequestStatus.PENDING, Timestamp.now()));
        Toast.makeText(getContext(), "Request sent!", Toast.LENGTH_SHORT).show();

        Notification notification = new Notification(UUID.randomUUID().toString(), "New Registration Request", "Vendor " + firstName + " " + lastName + " has sent a registration request", "admin@gmail.com", Notification.NotificationStatus.CREATED, Timestamp.now() );
        notificationRepository.createNotification(notification);

        FragmentUtils.replaceFragment(
                getParentFragmentManager(),
                new LoginFragment(),
                R.id.unauth_fragment_container,
                getActivity().findViewById(R.id.unauth_nav_view), // Make sure nav_view is initialized before calling
                R.id.unauth_nav_item4,
                getString(R.string.login)
        );
    }
    private String savePhotoToInternalStorage(ImageView imageView) {
        BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
        Bitmap bitmap = drawable.getBitmap();

        ContextWrapper wrapper = new ContextWrapper(getActivity().getApplicationContext()); // Use getActivity().getApplicationContext() here
        File file = wrapper.getDir("EventPlannerImages", Context.MODE_PRIVATE);
        file = new File(file, "profile_photo.jpg");

        try {
            OutputStream stream = null;
            stream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.flush();
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return file.getAbsolutePath();
    }


}