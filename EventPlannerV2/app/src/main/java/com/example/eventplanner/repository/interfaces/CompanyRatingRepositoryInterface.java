package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.CompanyRating;
import com.google.android.gms.tasks.Task;

import java.util.List;

public interface CompanyRatingRepositoryInterface {
    void addCompanyRating(CompanyRating rating);
    Task<List<CompanyRating>> getRatingsByCompany(String companyId);
    void removeCompanyRating(String ratingId);
    Task<List<CompanyRating>> getAll();
}
