package com.example.eventplanner.fragments.profiles;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.CompanyRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class VendorProfileFragment extends Fragment {

    private TextView tvEmail;
    private EditText etName;
    private EditText etLastName;
    private EditText etAddress;
    private EditText etPhone;
    private EditText etCurrentPassword;
    private EditText etNewPassword1;
    private EditText etNewPassword2;
    private TextView tvCompanyEmail;
    private EditText etCompanyName;
    private EditText etCompanyDescription;
    private EditText etCompanyAddress;
    private EditText etCompanyPhone;
    private User loggedInUser;
    private Company company;
    private FirebaseAuth mAuth;
    private UserRepositoryInterface userRepository;
    private CompanyRepositoryInterface companyRepository;

    public VendorProfileFragment() { mAuth = FirebaseAuth.getInstance(); }

    public static VendorProfileFragment newInstance() {
        VendorProfileFragment fragment = new VendorProfileFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vendor_profile, container, false);

        Button saveChangesButton = view.findViewById(R.id.saveOrganiserChangesButton);
        saveChangesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveChanges();
            }
        });

        Button saveCompanyChangesButton = view.findViewById(R.id.saveCompanyChangesButton);
        saveCompanyChangesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveCompanyChanges();
            }
        });

        Button updatePasswordButton = view.findViewById(R.id.saveOrganiserPasswordButton);
        updatePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePassword();
            }
        });

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);
        companyRepository = eventPlannerApp.getDIContainer().resolve(CompanyRepositoryInterface.class);

        getCompany();
        getLoggedInUser(new OnUserFetchListener() {
            @Override
            public void onUserFetch(User user) {
                loggedInUser = user;
                // Inflate the view only after user data is fetched
                if (getView() != null) {
                    setupViews(getView());
                }
            }
        });
    }

    interface OnUserFetchListener {
        void onUserFetch(User user);
    }

    private void setupViews(View view) {
        tvEmail = view.findViewById(R.id.tvOrganiserEmail);
        etName = view.findViewById(R.id.editOrganiserName);
        etLastName = view.findViewById(R.id.editOrganiserLastName);
        etAddress = view.findViewById(R.id.editOrganiserAddress);
        etPhone = view.findViewById(R.id.editOrganiserPhone);
        etCurrentPassword = view.findViewById(R.id.organiserCurrentPassword);
        etNewPassword1 = view.findViewById(R.id.organiserNewPassword1);
        etNewPassword2 = view.findViewById(R.id.organiserNewPassword2);

        tvEmail.setText(loggedInUser.getEmail());
        etName.setText(loggedInUser.getFirstName());
        etLastName.setText(loggedInUser.getLastName());
        etAddress.setText(loggedInUser.getAddress());
        etPhone.setText(loggedInUser.getPhone());

        tvCompanyEmail = view.findViewById(R.id.tvCompanyEmail);
        etCompanyName = view.findViewById(R.id.editCompanyName);
        etCompanyDescription = view.findViewById(R.id.editCompanyDescription);
        etCompanyAddress = view.findViewById(R.id.editCompanyAddress);
        etCompanyPhone = view.findViewById(R.id.editCompanyPhone);

        tvCompanyEmail.setText(company.getEmail());
        etCompanyName.setText(company.getName());
        etCompanyDescription.setText(company.getDescription());
        etCompanyAddress.setText(company.getAddress());
        etCompanyPhone.setText(company.getPhoneNumber());
    }

    private void updatePassword() {
        String currentPassword = etCurrentPassword.getText().toString();
        String newPassword1 = etNewPassword1.getText().toString();
        String newPassword2 = etNewPassword2.getText().toString();

        if (currentPassword.equals(loggedInUser.getPassword()) && newPassword1.equals(newPassword2)) {
            loggedInUser.setPassword(newPassword1);
            mAuth.getCurrentUser().updatePassword(newPassword1).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) { }
                }
            });
            userRepository.updateUser(loggedInUser).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    etCurrentPassword.setText("");
                    etNewPassword1.setText("");
                    etNewPassword2.setText("");
                    Toast.makeText(getActivity(), "Password updated successfully", Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    // Handle failure
                    Log.e("OrganiserProfileFragment", "Failed to update password: " + e.getMessage());
                    Toast.makeText(getActivity(), "Failed to update password", Toast.LENGTH_SHORT).show();
                }
            });
        } else if (!currentPassword.equals(loggedInUser.getPassword())) {
            Toast.makeText(getActivity(), "Incorrect password!", Toast.LENGTH_SHORT).show();
        } else if (!newPassword1.equals(newPassword2)) {
            Toast.makeText(getActivity(), "Passwords do not match!", Toast.LENGTH_SHORT).show();
        }
    }

    private void saveChanges() {
        String updatedName = etName.getText().toString();
        String updatedLastName = etLastName.getText().toString();
        String updatedAddress = etAddress.getText().toString();
        String updatedPhone = etPhone.getText().toString();

        loggedInUser.setFirstName(updatedName);
        loggedInUser.setLastName(updatedLastName);
        loggedInUser.setAddress(updatedAddress);
        loggedInUser.setPhone(updatedPhone);

        userRepository.updateUser(loggedInUser).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getActivity(), "Changes saved successfully", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Handle failure
                Log.e("OrganiserProfileFragment", "Failed to save changes: " + e.getMessage());
                Toast.makeText(getActivity(), "Failed to save changes", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void saveCompanyChanges() {
        String updatedName = etCompanyName.getText().toString();
        String updatedLastName = etCompanyDescription.getText().toString();
        String updatedAddress = etCompanyAddress.getText().toString();
        String updatedPhone = etCompanyPhone.getText().toString();

        company.setName(updatedName);
        company.setDescription(updatedLastName);
        company.setAddress(updatedAddress);
        company.setPhoneNumber(updatedPhone);

        companyRepository.updateCompany(company).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getActivity(), "Changes saved successfully", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Handle failure
                Log.e("OrganiserProfileFragment", "Failed to save changes: " + e.getMessage());
                Toast.makeText(getActivity(), "Failed to save changes", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getCompany() {
        companyRepository.getCompanyByOwnerEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<Company>() {
            @Override
            public void onComplete(@NonNull Task<Company> task) {
                if (task.isSuccessful()) {
                    company = task.getResult();
                } else {
                    Log.e("CompanyOverviewFragment", "Failed to get logged in vendor: " + task.getException());
                }
            }
        });
    }

    private void getLoggedInUser(OnUserFetchListener listener) {
        if (mAuth.getCurrentUser() != null) {
            userRepository.getUserByEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<User>() {
                @Override
                public void onComplete(@NonNull Task<User> task) {
                    if (task.isSuccessful()) {
                        User user = task.getResult();
                        if (listener != null) {
                            listener.onUserFetch(user);
                        }
                    } else {
                        Log.e("CompanyOverviewFragment", "Failed to get logged in vendor: " + task.getException());
                    }
                }
            });
        }
    }
}
