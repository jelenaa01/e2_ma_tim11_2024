package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.CompanyRatingReport;
import com.google.android.gms.tasks.Task;

import java.util.List;

public interface RatingReportRepositoryInterface {
    void addReport(CompanyRatingReport companyRatingReport);
    Task<List<CompanyRatingReport>> getAll();
    void changeReportStatus(String reportId, CompanyRatingReport.RatingReportStatus newStatus);
}
