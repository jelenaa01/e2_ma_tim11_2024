package com.example.eventplanner.fragments.vendorStaff;

import android.content.Context;
import android.os.Bundle;

import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.serviceProduct.MyServiceRecyclerViewAdapter;
import com.example.eventplanner.fragments.serviceProduct.ServiceDetailsFragment;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.CompanyRepositoryInterface;
import com.example.eventplanner.repository.interfaces.StaffWorkingTimeRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.example.eventplanner.utils.FragmentUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class VendorStaffFragment extends Fragment {

    private List<User> vendorStaff = new ArrayList<>();
    private VendorStaffRecyclerViewAdapter adapter;
    private DrawerLayout drawerLayout;
    private SearchView searchView;
    private CompanyRepositoryInterface companyRepository;
    private UserRepositoryInterface userRepository;

    @Override
    public void onCreate(@org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        companyRepository = eventPlannerApp.getDIContainer().resolve(CompanyRepositoryInterface.class);
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vendor_staff_list, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.vendor_staff_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String currentUserEmail = currentUser.getEmail();
        companyRepository.getCompanyEmployees(currentUserEmail)
                .addOnSuccessListener(vendorStaffs -> {
                    this.vendorStaff = vendorStaffs;
                    adapter = new VendorStaffRecyclerViewAdapter(vendorStaff, userRepository);
                    recyclerView.setAdapter(adapter);

                    if(adapter != null) {
                        adapter.setOnItemClickListener(staff -> {
                            FragmentUtils.replaceFragment(
                                    getParentFragmentManager(),
                                    VendorStaffDetailsFragment.newInstance(staff),
                                    R.id.vendor_fragment_container,
                                    (NavigationView) getActivity().findViewById(R.id.vendor_nav_view),
                                    R.id.vendor_nav_item1,
                                    getString(R.string.view_staff)
                            );
                        });
                    }

                })
                .addOnFailureListener(e -> {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                });


        searchView = view.findViewById(R.id.vendor_staff_searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });

        Collections.sort(vendorStaff, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o1.getLastName().compareToIgnoreCase(o2.getLastName());
            }
        });

        drawerLayout = view.findViewById(R.id.vendor_staff_drawer_layout);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        drawerLayout = view.findViewById(R.id.vendor_staff_drawer_layout);
    }
}