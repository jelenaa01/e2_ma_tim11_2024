package com.example.eventplanner.fragments.companyRating;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.CompanyRating;
import com.example.eventplanner.model.Reservation;
import com.example.eventplanner.model.User;
import com.example.eventplanner.utils.FragmentUtils;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;


import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class CompanyRatingAdapter  extends RecyclerView.Adapter<CompanyRatingAdapter .ViewHolder>{
    private static final String TAG = "CompanyRatingAdapter";
    private List<CompanyRating> companyRatingList;
    private CompanyRatingAdapter.OnItemClickListener listener;
    private Map<String, User> emailToUserMap;
    private OnReportButtonClickListener reportButtonClickListener;

    public CompanyRatingAdapter(List<CompanyRating> items, Map<String, User> emailToUserMap) {
        this.companyRatingList = items;
        this.emailToUserMap = emailToUserMap;
    }

    public void setOnReportButtonClickListener(OnReportButtonClickListener listener) {
        this.reportButtonClickListener = listener;
    }

    @NonNull
    @Override
    public CompanyRatingAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.company_rating_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CompanyRatingAdapter.ViewHolder holder, int position) {
        CompanyRating companyRating = companyRatingList.get(position);

        User organizer = emailToUserMap.get(companyRating.getOrganizerEmail().toLowerCase());

        String organizerFullName = (organizer != null) ? organizer.getFirstName() + " " + organizer.getLastName() : "";

        holder.comment.setText("Comment: " + companyRating.getComment());
        holder.rating.setText("Rating: " + companyRating.getRating());
        holder.organizerFullName.setText("Rate by: " + organizerFullName);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        String formattedDate = dateFormat.format(companyRating.getDate());
        holder.date.setText("Date: " + formattedDate);

        checkUserRoleAndToggleVisibility(holder.btnReport);
        holder.btnReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reportButtonClickListener != null) {
                    reportButtonClickListener.onReportButtonClick(companyRating);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return companyRatingList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView comment;
        public TextView rating;
        public TextView organizerFullName;
        public TextView date;
        public ImageButton btnReport;

        public ViewHolder(View view) {
            super(view);
            comment = view.findViewById(R.id.rating_comment);
            rating = view.findViewById(R.id.rating);
            organizerFullName = view.findViewById(R.id.rating_organizer);
            date = view.findViewById(R.id.rating_date);
            btnReport = view.findViewById(R.id.rating_btnReport);
        }
    }

    public void setOnItemClickListener(CompanyRatingAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(CompanyRating companyRating);
    }

    public interface OnReportButtonClickListener {
        void onReportButtonClick(CompanyRating companyRating);
    }

    private void checkUserRoleAndToggleVisibility(ImageButton button) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            FirebaseFirestore db = FirebaseFirestore.getInstance();

            DocumentReference userRef = db.collection("users").document(currentUser.getUid());
            userRef.get().addOnSuccessListener(documentSnapshot -> {
                if (documentSnapshot.exists()) {
                    User user = documentSnapshot.toObject(User.class);
                    if (user != null && user.getRole() == User.UserRole.VENDOR) {
                        button.setVisibility(View.VISIBLE);
                    } else {
                        button.setVisibility(View.GONE);
                    }
                }
            }).addOnFailureListener(e -> {
                button.setVisibility(View.GONE);
            });
        } else {
            button.setVisibility(View.GONE);
        }
    }


}
