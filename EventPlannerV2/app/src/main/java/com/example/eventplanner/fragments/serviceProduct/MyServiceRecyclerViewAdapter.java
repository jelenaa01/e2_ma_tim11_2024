package com.example.eventplanner.fragments.serviceProduct;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.eventplanner.R;
import com.example.eventplanner.model.Service;

import java.util.ArrayList;
import java.util.List;
public class MyServiceRecyclerViewAdapter extends RecyclerView.Adapter<MyServiceRecyclerViewAdapter.ViewHolder> {

    private final List<Service> services;
    public List<Service> servicesFull; // Full copy of the services list

    private OnItemClickListener listener;

    public MyServiceRecyclerViewAdapter(List<Service> items) {
        services = new ArrayList<>(items); // Make a copy of the items
        servicesFull = new ArrayList<>(items); // Initialize servicesFull with a copy of the full list
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_service_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Service service = services.get(position);
        holder.tvName.setText(service.getName());
        holder.tvDescription.setText(service.getDescription());
        holder.tvPrice.setText(String.valueOf(service.getPricePerHour()));

        holder.btnEdit.setOnClickListener(v -> {
            // Handle edit button click
        });

        holder.btnDelete.setOnClickListener(v -> {
            // Handle delete button click
            Service deletedService = services.remove(position);
            servicesFull.remove(deletedService);
            notifyDataSetChanged();
        });


        holder.itemView.setOnClickListener(v -> {
            if (listener != null) {
                listener.onItemClick(service);
            }
        });
    }

    public Filter getFilter() {
        return serviceFilter;
    }

    private Filter serviceFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Service> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(servicesFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (Service item : servicesFull) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            services.clear();
            services.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    @Override
    public int getItemCount() {
        return services.size();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(Service service);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView tvName;
        public final TextView tvDescription;
        public final TextView tvPrice;
        public final ImageButton btnEdit;
        public final ImageButton btnDelete;

        public ViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);
            tvDescription = view.findViewById(R.id.tvDescription);
            tvPrice = view.findViewById(R.id.tvPrice);
            btnEdit = view.findViewById(R.id.btnEdit);
            btnDelete = view.findViewById(R.id.btnDelete);
        }
    }
}