package com.example.eventplanner.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class VendorStaffWorkingTime implements Serializable {
    private String vendorStaffEmail;
   private List<WorkingTime> workingTime;
   private Date fromDate;
   private Date toDate;

    public VendorStaffWorkingTime() {}

    public VendorStaffWorkingTime(String vendorStaffEmail, List<WorkingTime> workingTime, Date fromDate, Date toDate) {
        this.vendorStaffEmail = vendorStaffEmail;
        this.workingTime = workingTime;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public String getVendorStaffEmail() {
        return vendorStaffEmail;
    }

    public void setVendorStaffEmail(String vendorStaffEmail) {
        this.vendorStaffEmail = vendorStaffEmail;
    }

    public List<WorkingTime> getWorkingTime() {
        return workingTime;
    }

    public void setWorkingTime(List<WorkingTime> workingTime) {
        this.workingTime = workingTime;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    @Override
    public String toString() {
        return "VendorStaffWorkingTime{" +
                "vendorStaffEmail='" + vendorStaffEmail + '\'' +
                ", workingTime=" + workingTime +
                ", fromDate=" + fromDate +
                ", toDate=" + toDate +
                '}';
    }
    
}
