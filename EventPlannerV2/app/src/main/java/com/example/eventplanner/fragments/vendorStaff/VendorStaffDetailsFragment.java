package com.example.eventplanner.fragments.vendorStaff;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.event.AddStaffEventFragment;
import com.example.eventplanner.model.TimeInterval;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.VendorStaffEvent;
import com.example.eventplanner.model.VendorStaffWorkingTime;
import com.example.eventplanner.model.WorkingTime;
import com.example.eventplanner.repository.interfaces.StaffEventRepositoryInterface;
import com.example.eventplanner.repository.interfaces.StaffWorkingTimeRepositoryInterface;
import com.example.eventplanner.utils.FragmentUtils;
import com.google.android.material.navigation.NavigationView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class VendorStaffDetailsFragment extends Fragment {

    private static final String ARG_SELECTED_STAFF = "selected-staff";

    private User selectedStaff;
    private RecyclerView eventsRecyclerView;
    private VendorStaffEventAdapter eventAdapter;
    private List<VendorStaffEvent> eventList;
    private StaffWorkingTimeRepositoryInterface staffWorkingTimeRepository;
    private StaffEventRepositoryInterface staffEventRepository;

    public VendorStaffDetailsFragment() {}

    public static VendorStaffDetailsFragment newInstance(User selectedStaff) {
        VendorStaffDetailsFragment fragment = new VendorStaffDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_SELECTED_STAFF, selectedStaff);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            selectedStaff = (User) getArguments().getSerializable(ARG_SELECTED_STAFF);
        }

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        staffWorkingTimeRepository = eventPlannerApp.getDIContainer().resolve(StaffWorkingTimeRepositoryInterface.class);
        staffEventRepository = eventPlannerApp.getDIContainer().resolve(StaffEventRepositoryInterface.class);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vendor_staff_details, container, false);

        if (selectedStaff != null) {
            TextView nameTextView = view.findViewById(R.id.nameTextView);
            TextView emailTextView = view.findViewById(R.id.emailTextView);
            TextView phoneTextView = view.findViewById(R.id.phoneTextView);
            TextView addressTextView = view.findViewById(R.id.addressTextView);
            ImageView profilePhotoImageView = view.findViewById(R.id.profilePhotoImageView);

            nameTextView.setText(selectedStaff.getLastName()  + " " + selectedStaff.getFirstName());
            emailTextView.setText(selectedStaff.getEmail());
            phoneTextView.setText(selectedStaff.getPhone());
            addressTextView.setText(selectedStaff.getAddress());
            // Set profile photo here, you can use a library like Glide or Picasso
            // Glide.with(this).load(selectedStaff.getProfilePhotoUrl()).into(profilePhotoImageView);

            EditText startTimeMondayEditText = view.findViewById(R.id.startTimeMondayEditText);
            EditText endTimeMondayEditText = view.findViewById(R.id.endTimeMondayEditText);
            EditText startTimeTuesdayEditText = view.findViewById(R.id.startTimeTuesdayEditText);
            EditText endTimeTuesdayEditText = view.findViewById(R.id.endTimeTuesdayEditText);
            EditText startTimeWednesdayEditText = view.findViewById(R.id.startTimeWednesdayEditText);
            EditText endTimeWednesdayEditText = view.findViewById(R.id.endTimeWednesdayEditText);
            EditText startTimeThursdayEditText = view.findViewById(R.id.startTimeThursdayEditText);
            EditText endTimeThursdayEditText = view.findViewById(R.id.endTimeThursdayEditText);
            EditText startTimeFridayEditText = view.findViewById(R.id.startTimeFridayEditText);
            EditText endTimeFridayEditText = view.findViewById(R.id.endTimeFridayEditText);
            EditText startTimeSaturdayEditText = view.findViewById(R.id.startTimeSaturdayEditText);
            EditText endTimeSaturdayEditText = view.findViewById(R.id.endTimeSaturdayEditText);
            EditText startTimeSundayEditText = view.findViewById(R.id.startTimeSundayEditText);
            EditText endTimeSundayEditText = view.findViewById(R.id.endTimeSundayEditText);

            EditText fromDateEditText = view.findViewById(R.id.fromDateEditText);
            EditText toDateEditText = view.findViewById(R.id.toDateEditText);

            setupTimePicker(startTimeMondayEditText);
            setupTimePicker(endTimeMondayEditText);
            setupTimePicker(startTimeTuesdayEditText);
            setupTimePicker(endTimeTuesdayEditText);
            setupTimePicker(startTimeWednesdayEditText);
            setupTimePicker(endTimeWednesdayEditText);
            setupTimePicker(startTimeThursdayEditText);
            setupTimePicker(endTimeThursdayEditText);
            setupTimePicker(startTimeFridayEditText);
            setupTimePicker(endTimeFridayEditText);
            setupTimePicker(startTimeSaturdayEditText);
            setupTimePicker(endTimeSaturdayEditText);
            setupTimePicker(startTimeSundayEditText);
            setupTimePicker(endTimeSundayEditText);

            setupDatePicker(fromDateEditText);
            setupDatePicker(toDateEditText);

            staffWorkingTimeRepository.getWorkingTimeByStaff(selectedStaff.getEmail()).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    VendorStaffWorkingTime workingTime = task.getResult();
                    if (workingTime != null) {
                        fillWorkingTime(workingTime, view);
                        fromDateEditText.setText(formatDate(workingTime.getFromDate()));
                        toDateEditText.setText(formatDate(workingTime.getToDate()));
                    }
                } else {
                    Log.e("VendorStaffDetailsFragment", "Error getting documents: ", task.getException());
                }
            });

            RecyclerView eventsRecyclerView = view.findViewById(R.id.eventsRecyclerView);
            eventsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

            eventList = new ArrayList<>();
            eventAdapter = new VendorStaffEventAdapter(eventList);
            eventsRecyclerView.setAdapter(eventAdapter);

            loadEvents();
        }

        ImageButton updateButton = view.findViewById(R.id.working_time_btnUpdate);
        updateButton.setOnClickListener(v -> saveWorkingTime(view));

        ImageButton addButton = view.findViewById(R.id.event_btnAdd);
        addButton.setOnClickListener(a -> FragmentUtils.replaceFragment(
                getParentFragmentManager(),
                AddStaffEventFragment.newInstance(selectedStaff),
                R.id.vendor_fragment_container,
                (NavigationView) getActivity().findViewById(R.id.vendor_nav_view),
                R.id.vendor_nav_item1,
                getString(R.string.add_staff_event)
        ));

        updateDateRange(view);

        return view;
    }

    private void setupTimePicker(EditText editText) {
        editText.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(
                    getContext(),
                    (view, hourOfDay, minuteOfHour) -> {
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minuteOfHour);
                        editText.setText(String.format(Locale.getDefault(), "%02d:%02d", hourOfDay, minuteOfHour));
                    },
                    hour,
                    minute,
                    android.text.format.DateFormat.is24HourFormat(getContext())
            );
            timePickerDialog.show();
        });
    }

    private void setupDatePicker(EditText editText) {
        editText.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(
                    getContext(),
                    (view, year1, monthOfYear, dayOfMonth) -> {
                        String formattedDate = formatDate(calendar.getTime());
                        editText.setText(formattedDate);
                    },
                    year,
                    month,
                    day
            );
            datePickerDialog.show();
        });
    }


    private void fillWorkingTime(VendorStaffWorkingTime vendorStaffWorkingTime, View view) {
        for (WorkingTime workingTime : vendorStaffWorkingTime.getWorkingTime()) {
            switch (workingTime.getWorkingDays()) {
                case MONDAY:
                    setWorkingTime(view.findViewById(R.id.startTimeMondayEditText), view.findViewById(R.id.endTimeMondayEditText), workingTime);
                    break;
                case TUESDAY:
                    setWorkingTime(view.findViewById(R.id.startTimeTuesdayEditText), view.findViewById(R.id.endTimeTuesdayEditText), workingTime);
                    break;
                case WEDNESDAY:
                    setWorkingTime(view.findViewById(R.id.startTimeWednesdayEditText), view.findViewById(R.id.endTimeWednesdayEditText), workingTime);
                    break;
                case THURSDAY:
                    setWorkingTime(view.findViewById(R.id.startTimeThursdayEditText), view.findViewById(R.id.endTimeThursdayEditText), workingTime);
                    break;
                case FRIDAY:
                    setWorkingTime(view.findViewById(R.id.startTimeFridayEditText), view.findViewById(R.id.endTimeFridayEditText), workingTime);
                    break;
                case SATURDAY:
                    setWorkingTime(view.findViewById(R.id.startTimeSaturdayEditText), view.findViewById(R.id.endTimeSaturdayEditText), workingTime);
                    break;
                case SUNDAY:
                    setWorkingTime(view.findViewById(R.id.startTimeSundayEditText), view.findViewById(R.id.endTimeSundayEditText), workingTime);
                    break;
            }
        }
    }

    private void setWorkingTime(EditText startTimeEditText, EditText endTimeEditText, WorkingTime workingTime) {
        startTimeEditText.setText(workingTime.getWorkingHours().getStartTime());
        endTimeEditText.setText(workingTime.getWorkingHours().getEndTime());
    }

    private void saveWorkingTime(View view) {
        List<WorkingTime> workingTimes = new ArrayList<>();

        workingTimes.add(createWorkingTime(view, R.id.startTimeMondayEditText, R.id.endTimeMondayEditText, WorkingTime.Days.MONDAY));
        workingTimes.add(createWorkingTime(view, R.id.startTimeTuesdayEditText, R.id.endTimeTuesdayEditText, WorkingTime.Days.TUESDAY));
        workingTimes.add(createWorkingTime(view, R.id.startTimeWednesdayEditText, R.id.endTimeWednesdayEditText, WorkingTime.Days.WEDNESDAY));
        workingTimes.add(createWorkingTime(view, R.id.startTimeThursdayEditText, R.id.endTimeThursdayEditText, WorkingTime.Days.THURSDAY));
        workingTimes.add(createWorkingTime(view, R.id.startTimeFridayEditText, R.id.endTimeFridayEditText, WorkingTime.Days.FRIDAY));
        workingTimes.add(createWorkingTime(view, R.id.startTimeSaturdayEditText, R.id.endTimeSaturdayEditText, WorkingTime.Days.SATURDAY));
        workingTimes.add(createWorkingTime(view, R.id.startTimeSundayEditText, R.id.endTimeSundayEditText, WorkingTime.Days.SUNDAY));

        EditText fromDateEditText = view.findViewById(R.id.fromDateEditText);
        EditText toDateEditText = view.findViewById(R.id.toDateEditText);
        Date fromDate = null;
        Date toDate = null;
        if (!TextUtils.isEmpty(fromDateEditText.getText()) || !TextUtils.isEmpty(toDateEditText.getText())) {
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
                fromDate = dateFormat.parse(fromDateEditText.getText().toString());
                toDate = dateFormat.parse(toDateEditText.getText().toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        staffWorkingTimeRepository.updateWorkingTime(selectedStaff.getEmail(), workingTimes, fromDate, toDate);
    }

    private WorkingTime createWorkingTime(View view, int startTimeId, int endTimeId, WorkingTime.Days workingDay) {
        EditText startTimeEditText = view.findViewById(startTimeId);
        EditText endTimeEditText = view.findViewById(endTimeId);
        String startTime = startTimeEditText.getText().toString();
        String endTime = endTimeEditText.getText().toString();
        return new WorkingTime(workingDay, new TimeInterval(startTime, endTime));
    }

    private String formatDate(Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        return sdf.format(date);
    }

    private void loadEvents() {
        staffEventRepository.getEventsByStaff(selectedStaff.getEmail()).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<VendorStaffEvent> events = task.getResult();
                eventList.clear();
                eventList.addAll(events);
                eventAdapter.notifyDataSetChanged();
            } else {
                Log.e("VendorStaffDetailsFragment", "Error getting events: ", task.getException());
            }
        });
    }


    private void updateDateRange(View view) {
        TextView dateRangeTextView = view.findViewById(R.id.dateRangeTextView);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        Date startDate = calendar.getTime();
        calendar.add(Calendar.DAY_OF_WEEK, 6);
        Date endDate = calendar.getTime();

        String dateRange = formatDate(startDate) + "-" + formatDate(endDate);

        dateRangeTextView.setText(dateRange);
    }

}