package com.example.eventplanner.repository;

import android.content.Context;
import android.database.Cursor;

import com.example.eventplanner.database.DBHandler;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.repository.interfaces.CategoryRepositoryInterface;

import java.util.ArrayList;
import java.util.List;

public class CategoryRepository implements CategoryRepositoryInterface {

    private final DBHandler dbHandler;

    public CategoryRepository(DBHandler dbHandler) {
        this.dbHandler = dbHandler;
    }

    public void addCategory(Category category) {
        dbHandler.addCategory(category);
    }

    public List<Category> getAllCategories() {
        return dbHandler.getAllCategories();
    }

    public void deactivateCategory(int categoryId) {
        dbHandler.deactivateCategory(categoryId);
    }

    public void activateCategory(int categoryId) {
        dbHandler.activateCategory(categoryId);
    }
}
