package com.example.eventplanner.repository;

import android.util.Log;

import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.UserReport;
import com.example.eventplanner.repository.interfaces.UserReportRepositoryInterface;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class UserReportRepository implements UserReportRepositoryInterface {
    private static final String TAG = "UserReportRepository";
    private FirebaseFirestore db;

    public UserReportRepository(FirebaseFirestore firestore) {
        this.db = firestore;
    }
    @Override
    public Task<List<UserReport>> getAllPending() {
        return db.collection("userReport")
                .get()
                .continueWith(task -> {
                    List<UserReport> services = new ArrayList<>();
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                UserReport service = documentSnapshot.toObject(UserReport.class);
                                services.add(service);
                            }
                        }
                    }
                    return services;
                });
    }

    @Override
    public void update(UserReport report) {
        db.collection("userReport")
                .whereEqualTo("userEmail", report.getUserEmail())
                .whereEqualTo("vendorEmail", report.getVendorEmail())
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful() && task.getResult() != null && !task.getResult().isEmpty()) {
                        for (DocumentSnapshot document : task.getResult().getDocuments()) {
                            db.collection("userReport")
                                    .document(document.getId())
                                    .set(report)
                                    .addOnSuccessListener(aVoid -> {
                                        Log.d(TAG, "Report updated successfully.");
                                    })
                                    .addOnFailureListener(e -> {
                                        Log.e(TAG, "Error updating report", e);
                                    });
                        }
                    } else {
                        Log.e(TAG, "No matching report found or error occurred", task.getException());
                    }
                });
    }

}
