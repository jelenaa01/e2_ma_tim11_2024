package com.example.eventplanner.repository;

import android.util.Log;

import com.example.eventplanner.model.Message;
import com.example.eventplanner.repository.interfaces.MessageRepositoryInterface;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MessageRepository implements MessageRepositoryInterface {
    private static final String TAG = "MessageRepository";
    private FirebaseFirestore db;

    public MessageRepository(FirebaseFirestore firestore) {
        this.db = firestore;
    }
    @Override
    public void add(Message message) {
        db.collection("messages")
                .add(message)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Message successfully added.");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error adding Message.", e);
                });
    }

    @Override
    public Task<List<Message>> getBySenderAndRecipient(String firstEmail, String secondEmail) {
        TaskCompletionSource<List<Message>> taskCompletionSource = new TaskCompletionSource<>();

        // Query for messages where the current user is the sender and the recipient is the other user
        Query query1 = db.collection("messages")
                .whereEqualTo("sender", firstEmail)
                .whereEqualTo("recipient", secondEmail);

        // Query for messages where the current user is the recipient and the sender is the other user
        Query query2 = db.collection("messages")
                .whereEqualTo("sender", secondEmail)
                .whereEqualTo("recipient", firstEmail);

        Task<List<Message>> task1 = query1.get().continueWith(task -> {
            if (task.isSuccessful()) {
                return task.getResult().toObjects(Message.class);
            } else {
                throw task.getException();
            }
        });

        Task<List<Message>> task2 = query2.get().continueWith(task -> {
            if (task.isSuccessful()) {
                return task.getResult().toObjects(Message.class);
            } else {
                throw task.getException();
            }
        });

        // Combine the results of both queries
        Task<List<Message>> combinedTask = Tasks.whenAllSuccess(task1, task2).continueWith(task -> {
            List<Message> combinedList = new ArrayList<>();
            combinedList.addAll(task1.getResult());
            combinedList.addAll(task2.getResult());

            Collections.sort(combinedList, Comparator.comparing(Message::getTimestamp));
            return combinedList;
        });

        combinedTask.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                taskCompletionSource.setResult(task.getResult());
            } else {
                taskCompletionSource.setException(task.getException());
            }
        });

        return taskCompletionSource.getTask();
    }
}
