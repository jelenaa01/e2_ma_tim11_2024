package com.example.eventplanner.fragments.event;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Guest;

import java.util.List;

public class GuestsRecyclerViewAdapter extends RecyclerView.Adapter<GuestsRecyclerViewAdapter.ViewHolder> {

    private final List<Guest> guests;
    private OnItemClickListener listener;
    private OnRemoveClickListener removeClickListener;
    private OnEditClickListener editClickListener;

    public GuestsRecyclerViewAdapter(List<Guest> guests) {
        this.guests = guests;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.guest_item, parent, false);
        return new GuestsRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Guest guest = guests.get(position);
        holder.tvName.setText(guest.getName());
        holder.tvGuestAge.setText(guest.getAgeRange());
        holder.tvGuestInvited.setText(guest.isInvited() ? "INVITED" : "NOT INVITED");

        holder.ibRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (removeClickListener != null) {
                    removeClickListener.onRemoveClick(guest);
                }
            }
        });

        holder.ibEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editClickListener != null) {
                    editClickListener.onEditClick(guest);
                }
            }
        });

        holder.bind(guest, listener);
    }

    public interface OnEditClickListener {
        void onEditClick(Guest guest);
    }

    public void setOnEditClickListener(OnEditClickListener listener) {
        this.editClickListener = listener;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnRemoveClickListener {
        void onRemoveClick(Guest guest);
    }

    public void setOnRemoveClickListener(OnRemoveClickListener listener) {
        this.removeClickListener = listener;
    }

    @Override
    public int getItemCount() {
        return guests.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Guest guest);
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvGuestAge;
        public TextView tvGuestInvited;
        public ImageButton ibRemove;
        public ImageButton ibEdit;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvGuestName);
            tvGuestAge = itemView.findViewById(R.id.tvGuestAge);
            tvGuestInvited = itemView.findViewById(R.id.tvGuestInvited);
            ibRemove = itemView.findViewById(R.id.btnGuestDelete);
            ibEdit = itemView.findViewById(R.id.btnGuestEdit);
        }

        public void bind(final Guest guest, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(guest);
                }
            });
        }
    }

}
