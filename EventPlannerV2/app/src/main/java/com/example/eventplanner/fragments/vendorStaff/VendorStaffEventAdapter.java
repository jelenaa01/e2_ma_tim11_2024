package com.example.eventplanner.fragments.vendorStaff;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.VendorStaffEvent;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class VendorStaffEventAdapter extends RecyclerView.Adapter<VendorStaffEventAdapter.ViewHolder> {
    private List<VendorStaffEvent> eventList;

    public VendorStaffEventAdapter(List<VendorStaffEvent> eventList) {
        this.eventList = eventList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vendor_staff_event_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        calendar.add(Calendar.DAY_OF_WEEK, position);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.(EEE)", Locale.getDefault());
        String formattedDate = dateFormat.format(calendar.getTime());

        holder.dateTextView.setText(formattedDate);

        VendorStaffEvent event = getEventForDate(calendar.getTime());

        if (event != null) {
            holder.eventNameTextView.setText(event.getEventName());
            holder.eventTypeTextView.setText(event.getEventType().toString());
            holder.fromTimeTextView.setText(formatTime(event.getStartTime()));
            holder.toTimeTextView.setText(formatTime(event.getEndTime()));
        } else {
            holder.eventNameTextView.setText("/");
            holder.eventTypeTextView.setText("/");
            holder.fromTimeTextView.setText("/");
            holder.toTimeTextView.setText("/");
        }
    }

    @Override
    public int getItemCount() {
        return 7;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView dateTextView;
        public TextView eventNameTextView;
        public TextView eventTypeTextView;
        public TextView fromTimeTextView;
        public TextView toTimeTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            dateTextView = itemView.findViewById(R.id.dateTextView);
            eventNameTextView = itemView.findViewById(R.id.eventNameTextView);
            eventTypeTextView = itemView.findViewById(R.id.eventTypeTextView);
            fromTimeTextView = itemView.findViewById(R.id.fromTimeTextText);
            toTimeTextView = itemView.findViewById(R.id.toTimeTextText);
        }
    }

    private String formatTime(String time) {
        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            Date date = inputFormat.parse(time);

            SimpleDateFormat outputFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            return outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return time;
        }
    }

    private VendorStaffEvent getEventForDate(Date date) {
        if (eventList != null) {
            for (VendorStaffEvent event : eventList) {
                if (event != null && isSameDay(event.getDate(), date)) {
                    return event;
                }
            }
        }
        return null;
    }

    private boolean isSameDay(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }
}
