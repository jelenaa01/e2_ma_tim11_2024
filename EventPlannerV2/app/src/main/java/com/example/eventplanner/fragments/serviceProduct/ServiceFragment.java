package com.example.eventplanner.fragments.serviceProduct;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.repository.interfaces.CategoryRepositoryInterface;
import com.example.eventplanner.repository.interfaces.ServiceRepositoryInterface;
import com.example.eventplanner.repository.interfaces.SubcategoryRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.example.eventplanner.utils.FragmentUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ServiceFragment extends Fragment {

    private static final String TAG = "ServiceFragment";

    private List<Service> services;
    private MyServiceRecyclerViewAdapter adapter;
    private DrawerLayout drawerLayout;
    private SearchView searchView;
    private ExpandableListView expandableListView;

    private Button applyButton;
    private CategoryRepositoryInterface categoryRepository;
    private SubcategoryRepositoryInterface subcategoryRepository;
    private ServiceRepositoryInterface serviceRepository;

    private UserRepositoryInterface userRepository;
    private List<Category> categories;
    private HashMap<String, List<String>> categorySubcategoryMap;
    private CategoryExpandableListAdapter expandableListAdapter;

    public ServiceFragment() { }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_service_list, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        categoryRepository = eventPlannerApp.getDIContainer().resolve(CategoryRepositoryInterface.class);
        subcategoryRepository = eventPlannerApp.getDIContainer().resolve(SubcategoryRepositoryInterface.class);
        serviceRepository = eventPlannerApp.getDIContainer().resolve(ServiceRepositoryInterface.class);
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);


        searchView = view.findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });




        services = new ArrayList<Service>() { };
        generateServices();
        loadServices();

        categories = categoryRepository.getAllCategories();
        categorySubcategoryMap = new HashMap<>();
        for (Category category : categories) {
            List<Subcategory> subcategories = subcategoryRepository.getSubcategoriesForCategory(category.getId());
            List<String> subcategoryNames = new ArrayList<>();
            for (Subcategory subcategory : subcategories) {
                subcategoryNames.add(subcategory.getName());
            }
            categorySubcategoryMap.put(category.getName(), subcategoryNames);
        }

        adapter = new MyServiceRecyclerViewAdapter(services);
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(service -> {
            // Replace current fragment with ServiceDetailsFragment
            FragmentUtils.replaceFragment(
                    getParentFragmentManager(),
                    ServiceDetailsFragment.newInstance(service, serviceRepository, userRepository),
                    R.id.unauth_fragment_container,
                    (NavigationView) getActivity().findViewById(R.id.unauth_nav_view),
                    R.id.unauth_nav_item1,
                    getString(R.string.view_services)
            );
        });

        FloatingActionButton fabAdd = view.findViewById(R.id.fabAdd);
        fabAdd.setOnClickListener(v -> {
            // ... (same as before)
        });

        ImageButton filterButton = view.findViewById(R.id.filterButton);

        drawerLayout = view.findViewById(R.id.drawer_layout);
        expandableListView = view.findViewById(R.id.expandableListView);

        filterButton.setOnClickListener(v -> drawerLayout.openDrawer(Gravity.RIGHT));

        loadFilterOptions();

        return view;
    }

    private void loadServices() {
        serviceRepository.getAll().addOnCompleteListener(new OnCompleteListener<List<Service>>() {
            @Override
            public void onComplete(@NonNull Task<List<Service>> task) {
                if (task.isSuccessful()) {
                    List<Service> services = task.getResult();
                    if (services != null) {
                        sortServices(services);
                        // Update your UI with the sorted list

                    } else {
                        Log.e(TAG, "Services list is null.");
                    }
                } else {
                    Log.e(TAG, "Error getting services.", task.getException());
                }
            }
        });
    }

    private void sortServices(List<Service> services) {
        if (services != null) {
            Collections.sort(services, new Comparator<Service>() {
                @Override
                public int compare(Service s1, Service s2) {
                    return s1.getName().compareTo(s2.getName());
                }
            });
        }
    }

    private void generateServices(){
        // add dummy data
    }

    private void loadFilterOptions() {
        expandableListAdapter = new CategoryExpandableListAdapter(getContext(), categories, categorySubcategoryMap);
        expandableListView.setAdapter(expandableListAdapter);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        drawerLayout = view.findViewById(R.id.drawer_layout);
        expandableListView = view.findViewById(R.id.expandableListView);
        applyButton = view.findViewById(R.id.applyButton);

        loadFilterOptions();

        applyButton.setOnClickListener(v -> {
            drawerLayout.closeDrawer(Gravity.RIGHT);

            List<String> selectedCategories = expandableListAdapter.getSelectedCategories();
            HashMap<String, List<String>> selectedSubcategoriesMap = expandableListAdapter.getSelectedSubcategories();

            // Filter services based on selected categories and subcategories
            List<Service> filteredServices = new ArrayList<>();
            for (Service service : services) {
                if (selectedCategories.contains(service.getCategory())) {
                    List<String> serviceSubcategories = selectedSubcategoriesMap.get(service.getCategory());
                    if (serviceSubcategories == null || serviceSubcategories.contains(service.getSubcategory())) {
                        filteredServices.add(service);
                    }
                }
            }

            //adapter.setServices(filteredServices);
            adapter.notifyDataSetChanged();
        });

        drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Apply the filter here
                List<String> selectedCategories = expandableListAdapter.getSelectedCategories();
                HashMap<String, List<String>> selectedSubcategoriesMap = expandableListAdapter.getSelectedSubcategories();

                // Apply filter based on selectedCategories and selectedSubcategories
                applyFilter(selectedCategories, selectedSubcategoriesMap);
            }
        });
    }

    private void applyFilter(List<String> selectedCategories, HashMap<String, List<String>> selectedSubcategoriesMap) {
        // Filter services based on selected categories and subcategories
        List<Service> filteredServices = new ArrayList<>();
        for (Service service : services) {
            if (selectedCategories.contains(service.getCategory())) {
                List<String> serviceSubcategories = selectedSubcategoriesMap.get(service.getCategory());
                if (serviceSubcategories == null || serviceSubcategories.contains(service.getSubcategory())) {
                    filteredServices.add(service);
                }
            }
        }

       // adapter.setServices(filteredServices);
        adapter.notifyDataSetChanged();
    }
}
