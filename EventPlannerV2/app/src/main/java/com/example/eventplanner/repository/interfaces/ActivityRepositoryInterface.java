package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.Activity;

import java.util.List;

public interface ActivityRepositoryInterface {
    List<Activity> getAll();
    List<Activity> getByEventId(int eventId);
    void add(Activity activity);
}
