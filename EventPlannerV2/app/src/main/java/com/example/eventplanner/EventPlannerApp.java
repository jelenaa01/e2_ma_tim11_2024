package com.example.eventplanner;

import android.app.Application;

import com.example.eventplanner.database.DBHandler;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Reservation;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.model.TimeInterval;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.WorkingTime;
import com.example.eventplanner.repository.ActivityRepository;
import com.example.eventplanner.repository.BudgetRepository;
import com.example.eventplanner.repository.CategoryRepository;
import com.example.eventplanner.repository.CompanyRatingRepository;
import com.example.eventplanner.repository.CompanyRepository;
import com.example.eventplanner.repository.EventTypeRepository;
import com.example.eventplanner.repository.FavoriteItemRepository;
import com.example.eventplanner.repository.GuestRepository;
import com.example.eventplanner.repository.MessageRepository;
import com.example.eventplanner.repository.NotificationRepository;
import com.example.eventplanner.repository.OrganiserEventRepository;
import com.example.eventplanner.repository.PackageRepository;
import com.example.eventplanner.repository.RatingReportRepository;
import com.example.eventplanner.repository.RegistrationRequestRepository;
import com.example.eventplanner.repository.ReservationRepository;
import com.example.eventplanner.repository.ServiceRepository;
import com.example.eventplanner.repository.StaffEventRepository;
import com.example.eventplanner.repository.ProductRepository;
import com.example.eventplanner.repository.StaffWorkingTimeRepository;
import com.example.eventplanner.repository.SubcategoryRepository;
import com.example.eventplanner.repository.UserRepository;
import com.example.eventplanner.repository.interfaces.ActivityRepositoryInterface;
import com.example.eventplanner.repository.interfaces.BudgetRepositoryInterface;
import com.example.eventplanner.repository.interfaces.CategoryRepositoryInterface;
import com.example.eventplanner.repository.interfaces.CompanyRatingRepositoryInterface;
import com.example.eventplanner.repository.interfaces.CompanyRepositoryInterface;
import com.example.eventplanner.repository.interfaces.EventTypeRepositoryInterface;
import com.example.eventplanner.repository.interfaces.FavoriteItemRepositoryInterface;
import com.example.eventplanner.repository.interfaces.GuestRepositoryInterface;
import com.example.eventplanner.repository.interfaces.MessageRepositoryInterface;
import com.example.eventplanner.repository.interfaces.NotificationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.OrganiserEventRepositoryInterface;
import com.example.eventplanner.repository.interfaces.PackageRepositoryInterface;
import com.example.eventplanner.repository.interfaces.RegistrationRequestRepositoryInterface;
import com.example.eventplanner.repository.interfaces.RatingReportRepositoryInterface;
import com.example.eventplanner.repository.interfaces.ReservationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.ServiceRepositoryInterface;
import com.example.eventplanner.repository.interfaces.StaffEventRepositoryInterface;
import com.example.eventplanner.repository.interfaces.ProductRepositoryInterface;
import com.example.eventplanner.repository.interfaces.StaffWorkingTimeRepositoryInterface;
import com.example.eventplanner.repository.interfaces.SubcategoryRepositoryInterface;
import com.example.eventplanner.repository.UserReportRepository;
import com.example.eventplanner.repository.interfaces.UserReportRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.example.eventplanner.utils.DIContainer;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventPlannerApp extends Application {

    private final DIContainer diContainer = new DIContainer();
    private FirebaseFirestore firestore;
    private DBHandler dbHandler;

    // Repositories
    private UserRepository userRepository;
    private EventTypeRepository eventTypeRepository;
    private CategoryRepository categoryRepository;
    private SubcategoryRepository subCategoryRepository;
    private CompanyRepository companyRepository;
    private StaffWorkingTimeRepository staffWorkingTimeRepository;
    private StaffEventRepository staffEventRepository;
    private ReservationRepository reservationRepository;
    private ServiceRepository serviceRepository;
    private NotificationRepository notificationRepository;
    private RegistrationRequestRepository registrationRequestRepository;
    private PackageRepository packageRepository;
    private CompanyRatingRepository companyRatingRepository;
    private RatingReportRepository ratingReportRepository;
    private BudgetRepositoryInterface budgetRepository;
    private OrganiserEventRepositoryInterface organiserEventRepository;
    @Override
    public void onCreate() {
        super.onCreate();

        initializeFirebaseAndDB();
        initializeRepositories();
        initializeServices();
        createInitialData();
    }

    private void initializeFirebaseAndDB() {
        firestore = FirebaseFirestore.getInstance();
        diContainer.register(FirebaseFirestore.class, firestore);

        dbHandler = DBHandler.getInstance(this);
        diContainer.register(DBHandler.class, dbHandler);
    }

    private void initializeRepositories() {
        userRepository = new UserRepository(firestore);
        diContainer.register(UserRepositoryInterface.class, userRepository);

        eventTypeRepository = new EventTypeRepository(dbHandler);
        diContainer.register(EventTypeRepositoryInterface.class, eventTypeRepository);

        categoryRepository = new CategoryRepository(dbHandler);
        diContainer.register(CategoryRepositoryInterface.class, categoryRepository);

        subCategoryRepository = new SubcategoryRepository(dbHandler);
        diContainer.register(SubcategoryRepositoryInterface.class, subCategoryRepository);

        companyRepository = new CompanyRepository(firestore);
        diContainer.register(CompanyRepositoryInterface.class, companyRepository);

        staffWorkingTimeRepository = new StaffWorkingTimeRepository(firestore);
        diContainer.register(StaffWorkingTimeRepositoryInterface.class, staffWorkingTimeRepository);

        staffEventRepository = new StaffEventRepository(firestore);

        ServiceRepository serviceRepository = new ServiceRepository(firestore);
        diContainer.register(ServiceRepositoryInterface.class, serviceRepository);

        ProductRepository productRepository = new ProductRepository(firestore);
        diContainer.register(ProductRepositoryInterface.class, productRepository);

        StaffEventRepository staffEventRepository = new StaffEventRepository(firestore);
        diContainer.register(StaffEventRepositoryInterface.class, staffEventRepository);

        MessageRepository messageRepository = new MessageRepository(firestore);
        diContainer.register(MessageRepositoryInterface.class, messageRepository);

        ActivityRepository activityRepository = new ActivityRepository(firestore);
        diContainer.register(ActivityRepositoryInterface.class, activityRepository);

        UserReportRepository userReportRepository = new UserReportRepository(firestore);
        diContainer.register(UserReportRepositoryInterface.class, userReportRepository);

        GuestRepository guestRepository = new GuestRepository(firestore);
        diContainer.register(GuestRepositoryInterface.class, guestRepository);

        reservationRepository = new ReservationRepository(firestore);
        diContainer.register(ReservationRepositoryInterface.class, reservationRepository);

        serviceRepository = new ServiceRepository(firestore);
        diContainer.register(ServiceRepositoryInterface.class, serviceRepository);

        notificationRepository = new NotificationRepository(firestore);
        diContainer.register(NotificationRepositoryInterface.class, notificationRepository);

        registrationRequestRepository = new RegistrationRequestRepository();
        diContainer.register(RegistrationRequestRepositoryInterface.class, registrationRequestRepository);

        packageRepository = new PackageRepository(firestore);
        PackageRepository packageRepository = new PackageRepository(firestore);
        diContainer.register(PackageRepositoryInterface.class, packageRepository);

        companyRatingRepository = new CompanyRatingRepository(firestore);
        diContainer.register(CompanyRatingRepositoryInterface.class, companyRatingRepository);

        ratingReportRepository = new RatingReportRepository(firestore);
        diContainer.register(RatingReportRepositoryInterface.class, ratingReportRepository);

        organiserEventRepository = new OrganiserEventRepository(firestore);
        diContainer.register(OrganiserEventRepositoryInterface.class, organiserEventRepository);

        budgetRepository = new BudgetRepository(firestore);
        diContainer.register(BudgetRepositoryInterface.class, budgetRepository);

    }

    private void initializeServices() {

    }

    private void createInitialData() {
        User admin = new User("admin@gmail.com", "admin1", "Admin", "Admin", "Admin address", "456", "", User.UserRole.ADMIN, true);
        User vendor = new User("vendor@gmail.com", "vendor", "Vendor", "Vendor", "Vendor address", "789", "", User.UserRole.VENDOR, true);
        User vendorStaff = new User("vendorstaff@gmail.com", "vendorstaff", "Vendor Staff", "Vendor Staff", "Vendor Staff address", "101", "", User.UserRole.VENDOR_STAFF, true);
        User organiser = new User("organiser@gmail.com", "organiser", "Organiser", "Organiser", "Organiser address", "123", "", User.UserRole.ORGANISER, true);

        userRepository.addUser(admin);
        userRepository.addUser(vendor);
        userRepository.addUser(vendorStaff);
        userRepository.addUser(organiser);

        Category category1 = new Category(1, "Category Name 1", "Description 1", true);
        Category category2 = new Category(2, "Category Name 2", "Description 2", true);

        categoryRepository.addCategory(category1);
        categoryRepository.addCategory(category2);

        Subcategory subcategory1 = new Subcategory(1, "Subcategory 1", "Description 1", Subcategory.Type.PRODUCT, 1, true);
        Subcategory subcategory2 = new Subcategory(2, "Subcategory 2", "Description 2", Subcategory.Type.PRODUCT, 1, true);

        FavoriteItemRepository favoriteItemRepository = new FavoriteItemRepository(firestore);
        diContainer.register(FavoriteItemRepositoryInterface.class, favoriteItemRepository);

        subCategoryRepository.addSubcategory(subcategory1);
        subCategoryRepository.addSubcategory(subcategory2);

        List<WorkingTime> companyWorkingTime = new ArrayList<>();
        companyWorkingTime.add(new WorkingTime(WorkingTime.Days.MONDAY, new TimeInterval("09:00", "16:00")));
        companyWorkingTime.add(new WorkingTime(WorkingTime.Days.TUESDAY, new TimeInterval("09:00", "16:00")));
        companyWorkingTime.add(new WorkingTime(WorkingTime.Days.WEDNESDAY, new TimeInterval("09:00", "16:00")));
        companyWorkingTime.add(new WorkingTime(WorkingTime.Days.THURSDAY, new TimeInterval("09:00", "16:00")));
        companyWorkingTime.add(new WorkingTime(WorkingTime.Days.FRIDAY, new TimeInterval("16:00", "22:00")));
        companyWorkingTime.add(new WorkingTime(WorkingTime.Days.SATURDAY, new TimeInterval("16:00", "22:00")));
        companyWorkingTime.add(new WorkingTime(WorkingTime.Days.SUNDAY, new TimeInterval("16:00", "22:00")));
        Company company = new Company("1", "Company", "company@gmail.com", "Company address", "021974630", "Description", "", "vendor@gmail.com", null, companyWorkingTime);

        // Uncomment to add the company
        // companyRepository.addCompany(company);

       /* Service service1 = new Service("1", "Foto i video", "Snimanje dronom", "Snimanje dronom", "Ovo je snimanje iz vazduha sa dronom", Collections.emptyList(), 3000, 6000, 2, "okolina Novog Sada", 0, Arrays.asList("Nikola, Anica"), Arrays.asList("vencanje, krstenje, 1 rodjendan"), 358, 2, Service.ConfirmationModeService.MANUAL, true, true);
        Service service2 = new Service("2", "Foto i video", "Videografija", "Snimanje kamerom 4k", "Ovo je snimanje iz vazduha sa dronom", Collections.emptyList(), 5000, 9000, 3, "okolina Novog Sada", 0, Arrays.asList("Nikola, Anica"), Arrays.asList("vencanje, krstenje, 1 rodjendan"), 358, 2, Service.ConfirmationModeService.MANUAL, true, true);
        Service service3 = new Service("3", "Foto i video", "Fotografisanje", "Fotografisanje događaja", "Fotografisanje događaja sa najboljim kamerama", Collections.emptyList(), 5000, 9000, 3, "okolina Novog Sada", 0, Arrays.asList("Nikola"), Arrays.asList("vencanje, krstenje, 1 rodjendan"), 358, 2, Service.ConfirmationModeService.MANUAL, true, true);
        Service service4 = new Service("4", "Catering", "Ketering za događaje", "Profesionalni catering", "Prilagođena hrana i piće za vaše posebne događaje", Collections.emptyList(), 6000, 12000, 4, "Novi Sad i okolina", 0, Arrays.asList("Mirko"), Arrays.asList("svadbe, korporativni događaji, rođendani"), 358, 2, Service.ConfirmationModeService.MANUAL, true, true);
        Service service5 = new Service("5", "Dekoracija", "Dekoracija prostora", "Profesionalna dekoracija", "Ulepšajte vaš prostor sa našom kreativnom dekoracijom", Collections.emptyList(), 5000, 10000, 3, "Vojvodina", 0, Arrays.asList("Mirko"), Arrays.asList("svadbe, rođendani, proslave"), 358, 2, Service.ConfirmationModeService.MANUAL, true, true);*/

        // Uncomment to add the services
        // serviceRepository.addService(service1);
        // serviceRepository.addService(service2);
        // serviceRepository.addService(service3);
        // serviceRepository.addService(service4);
        // serviceRepository.addService(service5);

        Reservation reservation1 = new Reservation("1", "1", "", "jecasubic@gmail.com", "organiser@gmail.com", Reservation.ReservationStatus.NEW, new Date(), "16:00", "19:00");
        Reservation reservation2 = new Reservation("2", "2", "", "jecasubic@gmail.com", "organiser@gmail.com", Reservation.ReservationStatus.ACCEPTED, new Date(), "18:00", "22:00");
        Reservation reservation3 = new Reservation("3", "3", "", "jecasubic@gmail.com", "organiser@gmail.com", Reservation.ReservationStatus.NEW, new Date(), "13:00", "17:00");
        Reservation reservation4 = new Reservation("4", "1", "1", "mi@gmail.com", "organiser@gmail.com", Reservation.ReservationStatus.ACCEPTED, new Date(), "15:00", "20:00");
        Reservation reservation5 = new Reservation("5", "2", "1", "n@gmail.com", "organiser@gmail.com", Reservation.ReservationStatus.ACCEPTED, new Date(), "17:00", "22:00");
        Reservation reservation6 = new Reservation("6", "3", "1", "mi@gmail.com", "organiser@gmail.com", Reservation.ReservationStatus.NEW, new Date(), "13:00", "15:00");

        // Uncomment to add the reservations
        // reservationRepository.createReservation(reservation1);
        // reservationRepository.createReservation(reservation2);
        // reservationRepository.createReservation(reservation3);
        // reservationRepository.createReservation(reservation4);
        // reservationRepository.createReservation(reservation5);
        // reservationRepository.createReservation(reservation6);

        /*Package package1 = new Package("1", "Snimanje događaja", "Ovaj paket uključuje sve potrebne stavke za dekoraciju vašeg venčanja.", 18000, 5, Collections.emptyList(), true, true, "Foto i video", Arrays.asList(service1, service2), Arrays.asList("vencanje", "krstenje", "1 rodjendan"), Arrays.asList("vencanje", "krstenje", "1 rodjendan"), 358, 2, Package.ConfirmationMode.MANUAL);
        Package package2 = new Package("2", "Catering i dekoracija", "Ovaj paket uključuje ketering i dekoraciju prostora za vaše posebne događaje.", 16000, 0, Collections.emptyList(), true, true, "Event services", Arrays.asList(service4, service5), Arrays.asList("svadbe", "korporativni događaji", "rođendani"), Arrays.asList("svadbe", "korporativni događaji", "rođendani"), 358, 2, Package.ConfirmationMode.MANUAL);*/

        // Uncomment to add the packages
        // packageRepository.addPackage(package1);
        // packageRepository.addPackage(package2);
        // dummy data replaced into a txt file
    }

    public DIContainer getDIContainer() {
        return diContainer;
    }
}
