package com.example.eventplanner.fragments.reports;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.productsOverview.CompanyOverviewFragment;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.CompanyReport;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.UserReport;
import com.example.eventplanner.repository.interfaces.NotificationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;

public class ReportCompanyFragment extends Fragment {

    private static final String ARG_SELECTED_COMPANY = "selected-company";
    private Company selectedCompany;
    private User loggedInUser;
    private UserRepositoryInterface userRepository;
    private NotificationRepositoryInterface notificationRepository;
    private FirebaseAuth mAuth;

    private EditText description;
    private Button button;

    public ReportCompanyFragment() { mAuth = FirebaseAuth.getInstance(); }

    public static ReportCompanyFragment newInstance(Company selectedCompany) {
        ReportCompanyFragment fragment = new ReportCompanyFragment();

        Bundle args = new Bundle();
        args.putSerializable(ARG_SELECTED_COMPANY, selectedCompany);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            selectedCompany = (Company) getArguments().getSerializable(ARG_SELECTED_COMPANY);
        }

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);
        notificationRepository = eventPlannerApp.getDIContainer().resolve(NotificationRepositoryInterface.class);

        getLoggedInUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report_company, container, false);

        initViews(view);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateAndSaveReport();
            }
        });

        return view;
    }

    private void validateAndSaveReport() {
        if (loggedInUser == null) {
            Toast.makeText(getActivity(), "No one is logged in.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (userRepository == null) {
            Toast.makeText(getActivity(), "UserRepository is null.", Toast.LENGTH_SHORT).show();
            return;
        }

        String _description = description.getText().toString().trim();

        if (_description.isEmpty()) {
            Toast.makeText(getActivity(), "Please fill out the description field.", Toast.LENGTH_SHORT).show();
            return;
        }

        CompanyReport companyReport = new CompanyReport(loggedInUser.getEmail(), selectedCompany.getName(), _description);

        userRepository.addCompanyReport(companyReport);
        Toast.makeText(getActivity(), "Report Added.", Toast.LENGTH_SHORT).show();

        Notification notification = new Notification("1", "New Report", "A new report has been added", "admin@gmail.com", Notification.NotificationStatus.CREATED, Timestamp.now() );
        notificationRepository.createNotification(notification);

        returnToPreviousPage();
    }

    private void returnToPreviousPage(){
        CompanyOverviewFragment companyOverviewFragment = CompanyOverviewFragment.newInstance(selectedCompany);

        FragmentTransaction transaction = getParentFragmentManager().beginTransaction();
        transaction.replace(R.id.organizer_fragment_container, companyOverviewFragment);

        transaction.addToBackStack(null);

        transaction.commit();
    }

    private void getLoggedInUser() {
        if (mAuth.getCurrentUser() != null) {
            userRepository.getUserByEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<User>() {
                @Override
                public void onComplete(@NonNull Task<User> task) {
                    if (task.isSuccessful()) {
                        loggedInUser = task.getResult();
                    } else {
                        Log.e("ReportCompanyFragment", "Failed to get logged in vendor: " + task.getException());
                    }
                }
            });
        }
    }

    private void initViews(View view) {
        description = view.findViewById(R.id.description5);
        button = view.findViewById(R.id.button5);
    }
}