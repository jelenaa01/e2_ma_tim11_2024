package com.example.eventplanner.model;

import java.io.Serializable;
import java.util.Date;

public class CompanyRatingReport implements Serializable {
    public enum RatingReportStatus {
        REPORTED,
        ACCEPTED,
        REJECTED
    }
    private String id;
    private String reason;
    private Date date;
    private String vendorEmail;
    private RatingReportStatus reportStatus;
    private String companyRatingId;

    public CompanyRatingReport() {}

    public CompanyRatingReport(String id, String reason, Date date, String vendorEmail, RatingReportStatus reportStatus, String companyRatingId) {
        this.id = id;
        this.reason = reason;
        this.date = date;
        this.vendorEmail = vendorEmail;
        this.reportStatus = reportStatus;
        this.companyRatingId = companyRatingId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getVendorEmail() {
        return vendorEmail;
    }

    public void setVendorEmail(String vendorEmail) {
        this.vendorEmail = vendorEmail;
    }

    public RatingReportStatus getReportStatus() {
        return reportStatus;
    }

    public void setReportStatus(RatingReportStatus reportStatus) {
        this.reportStatus = reportStatus;
    }

    public String getCompanyRatingId() {
        return companyRatingId;
    }

    public void setCompanyRatingId(String companyRatingId) {
        this.companyRatingId = companyRatingId;
    }

    @Override
    public String toString() {
        return "CompanyRatingReport{" +
                "id='" + id + '\'' +
                ", reason='" + reason + '\'' +
                ", date=" + date +
                ", vendorEmail='" + vendorEmail + '\'' +
                ", reportStatus=" + reportStatus +
                ", companyRatingId='" + companyRatingId + '\'' +
                '}';
    }

}
