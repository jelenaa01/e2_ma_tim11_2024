package com.example.eventplanner.repository;

import android.util.Log;

import com.example.eventplanner.model.VendorStaffEvent;
import com.example.eventplanner.repository.interfaces.StaffEventRepositoryInterface;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class StaffEventRepository implements StaffEventRepositoryInterface {
    private static final String TAG = "VendorStaffEventRepository";
    private FirebaseFirestore db;

    public StaffEventRepository(FirebaseFirestore firestore) {
        this.db = firestore;
    }

    @Override
    public void addStaffEvent(VendorStaffEvent vendorStaffEvent) {
        if (vendorStaffEvent == null) {
            Log.e(TAG, "Event for vendor staff is null");
            return;
        }
        db.collection("vendorStaffEvents")
                .add(vendorStaffEvent)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Event for vendor staff successfully added");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error adding event for vendor staff", e);
                });
    }

    @Override
    public Task<List<VendorStaffEvent>> getEventsByStaff(String email) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("vendorStaffEvents")
                .whereEqualTo("vendorStaffEmail", email)
                .get()
                .continueWith(task -> {
                    List<VendorStaffEvent> events = new ArrayList<>();
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                VendorStaffEvent event = documentSnapshot.toObject(VendorStaffEvent.class);
                                events.add(event);
                            }
                        }
                    }
                    return events;
                });
    }

    @Override
    public void removeStaffEvent(String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("vendorStaffEvents")
                .whereEqualTo("id", id)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        DocumentSnapshot documentSnapshot = queryDocumentSnapshots.getDocuments().get(0);
                        String docId = documentSnapshot.getId();
                        DocumentReference docRef = db.collection("vendorStaffEvents").document(docId);

                        docRef.delete()
                                .addOnSuccessListener(aVoid -> {
                                    Log.d(TAG, "Staff event deleted");
                                })
                                .addOnFailureListener(e -> {
                                    Log.e(TAG, "Error during staff event delete", e);
                                });
                    } else {
                        Log.d(TAG, "No staff event found for id: " + id);
                    }
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error finding staff event", e);
                });
    }

}
