package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.RegistrationRequest;
import com.google.android.gms.tasks.Task;

import java.util.List;

public interface RegistrationRequestRepositoryInterface {
    Task<Void> createRegistrationRequest(RegistrationRequest request);
    Task<List<RegistrationRequest>> getAllPendingRequests();
    Task<Void> setRequestStatusAccepted(String requestId);
    Task<Void> setRequestStatusRejected(String requestId);
    Task<RegistrationRequest> getRequestById(String requestId);

}
