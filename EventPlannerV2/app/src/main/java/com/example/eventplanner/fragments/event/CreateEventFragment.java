package com.example.eventplanner.fragments.event;

import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.serviceProduct.ServiceFragment;
import com.example.eventplanner.model.Activity;
import com.example.eventplanner.model.Budget;
import com.example.eventplanner.model.Guest;
import com.example.eventplanner.model.OrganiserEvent;
import com.example.eventplanner.model.TimeInterval;
import com.example.eventplanner.repository.interfaces.ActivityRepositoryInterface;
import com.example.eventplanner.repository.interfaces.BudgetRepositoryInterface;
import com.example.eventplanner.repository.interfaces.GuestRepositoryInterface;
import com.example.eventplanner.repository.interfaces.OrganiserEventRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.example.eventplanner.utils.FragmentUtils;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

import com.google.firebase.auth.FirebaseUser;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Paragraph;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

public class CreateEventFragment extends Fragment {

    private int currentStep = 1;
    private LinearLayout layoutStep1, layoutStep2, layoutStep3, layoutStep4, layoutStep5;
    private EditText eventName, eventDescription, maxGuests, eventAddress, radius;
    private EditText activityName, activityDescription, activityStart, activityEnd, activityLocation;
    private EditText guestName;
    private CheckBox guestInvited, guestAccepted, guestVegan, guestVegetarian;
    private Spinner guestAgeRange;
    private TextView eventDate;
    private RecyclerView recyclerViewActivities;
    private RecyclerView recyclerViewGuests;
    private ActivitiesRecyclerViewAdapter adapter;
    private GuestsRecyclerViewAdapter guestsAdapter;
    private Spinner privacyRules;
    private ImageButton btnPrevious, btnNext, btnAddActivity, btnGeneratePdf, btnAddGuest, btnGenerateGuestsPdf;

    private UserRepositoryInterface userRepository;
    private ActivityRepositoryInterface activityRepository;
    private BudgetRepositoryInterface budgetRepository;
    private OrganiserEventRepositoryInterface organiserEventRepository;
    private GuestRepositoryInterface guestRepository;
    private FirebaseAuth mAuth;
    private List<Activity> activities;
    private List<Guest> guests;
    private String selectedAgeRange;
    private boolean isAccepted = false;
    private boolean isInvited = false;
    private boolean isVegan = false;
    private boolean isVegetarian = false;

    public CreateEventFragment() {
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);
        activityRepository = eventPlannerApp.getDIContainer().resolve(ActivityRepositoryInterface.class);
        guestRepository = eventPlannerApp.getDIContainer().resolve(GuestRepositoryInterface.class);
        budgetRepository = eventPlannerApp.getDIContainer().resolve(BudgetRepositoryInterface.class);
        organiserEventRepository = eventPlannerApp.getDIContainer().resolve(OrganiserEventRepositoryInterface.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_event, container, false);

        initViews(view);

        recyclerViewActivities = view.findViewById(R.id.activitiesRecyclerView);
        recyclerViewActivities.setLayoutManager(new LinearLayoutManager(getContext()));
        activities = new ArrayList<>();
        adapter = new ActivitiesRecyclerViewAdapter(activities);
        recyclerViewActivities.setAdapter(adapter);

        recyclerViewGuests = view.findViewById(R.id.guestsRecyclerView);
        recyclerViewGuests.setLayoutManager(new LinearLayoutManager(getContext()));
        guests = new ArrayList<>();
        guestsAdapter = new GuestsRecyclerViewAdapter(guests);
        recyclerViewGuests.setAdapter(guestsAdapter);

        btnPrevious.setOnClickListener(v -> navigateToPreviousStep());
        btnNext.setOnClickListener(v -> {
            if (validateStep()) {
                if (currentStep == 5) {
                    saveEvent();
                } else {
                    navigateToNextStep();
                }
            }
        });
        btnAddActivity.setOnClickListener(v -> {
            addActivity();
        });
        btnGeneratePdf.setOnClickListener(v -> {
            generateAndSavePDF(activities);
        });

        btnAddGuest.setOnClickListener(v -> {
            addGuest();
        });
        btnGenerateGuestsPdf.setOnClickListener(v -> {
            generateAndSaveGuestsPDF(guests);
        });

        guestAgeRange.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                selectedAgeRange = parentView.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }
        });
        guestInvited.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                guestAccepted.setEnabled(isChecked);
                isInvited = isChecked;
                if (!isChecked) {
                    guestAccepted.setChecked(false);
                    isAccepted = false;
                }
            }
        });
        guestAccepted.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isAccepted = isChecked;
            }
        });
        guestVegetarian.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isVegetarian = isChecked;
            }
        });
        guestVegan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isVegan = isChecked;
            }
        });

        guestsAdapter.setOnRemoveClickListener(new GuestsRecyclerViewAdapter.OnRemoveClickListener() {
            @Override
            public void onRemoveClick(Guest guest) {
                guests.remove(guest);
                guestsAdapter.notifyDataSetChanged();
                guestRepository.remove(guest);
            }
        });

        guestsAdapter.setOnEditClickListener(new GuestsRecyclerViewAdapter.OnEditClickListener() {
            @Override
            public void onEditClick(Guest guest) {
                guestName.setText(guest.getName());
                setSelectedItemInSpinner(guestAgeRange, guest.getAgeRange());
                guestInvited.setChecked(guest.isInvited());
                guestAccepted.setChecked(guest.hasAcceptedInvitation());
                guestAccepted.setEnabled(guest.isInvited());
                guestVegetarian.setChecked(guest.isVegetarian());
                guestVegan.setChecked(guest.isVegan());

                recyclerViewGuests.scrollToPosition(0);
                guests.remove(guest);
                guestRepository.remove(guest);
            }
        });

        return view;
    }

    private void setSelectedItemInSpinner(Spinner spinner, String item) {
        ArrayAdapter<String> adapter = (ArrayAdapter<String>) spinner.getAdapter();
        for (int i = 0; i < adapter.getCount(); i++) {
            if (adapter.getItem(i).equals(item)) {
                spinner.setSelection(i);
                break;
            }
        }
    }

    public void generateAndSavePDF(List<Activity> activities) {
        try {
            String filePath = getContext().getExternalFilesDir(null) + "/activities.pdf";
            Log.d("PDF_PATH", "File path: " + filePath);
            PdfWriter writer = new PdfWriter(filePath);
            PdfDocument pdfDocument = new PdfDocument(writer);
            Document document = new Document(pdfDocument);

            Paragraph title = new Paragraph("AGENDA").setFontSize(16).setBold();
            document.add(title);

            Table table = new Table(5);
            table.addCell("Activity Name");
            table.addCell("Description");
            table.addCell("Start Time");
            table.addCell("End Time");
            table.addCell("Location");

            for (Activity activity : activities) {
                table.addCell(activity.getName());
                table.addCell(activity.getDescription());
                table.addCell(activity.getInterval().getStartTime());
                table.addCell(activity.getInterval().getEndTime());
                table.addCell(activity.getLocation());
            }

            document.add(table);

            document.close();

            Toast.makeText(getContext(), "PDF saved successfully!", Toast.LENGTH_SHORT).show();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Failed to create PDF: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void generateAndSaveGuestsPDF(List<Guest> guests) {
        try {
            String filePath = getContext().getExternalFilesDir(null) + "/guests.pdf";
            Log.d("PDF_PATH", "File path: " + filePath);
            PdfWriter writer = new PdfWriter(filePath);
            PdfDocument pdfDocument = new PdfDocument(writer);
            Document document = new Document(pdfDocument);

            Paragraph title = new Paragraph("GUESTS").setFontSize(16).setBold();
            document.add(title);

            Table table = new Table(4);
            table.addCell("Name");
            table.addCell("Age Range");
            table.addCell("Invited");
            table.addCell("Accepted Invitation");

            for (Guest guest : guests) {
                table.addCell(guest.getName());
                table.addCell(guest.getAgeRange());
                table.addCell(String.valueOf(guest.isInvited()));
                table.addCell(String.valueOf(guest.hasAcceptedInvitation()));
            }

            document.add(table);

            document.close();

            Toast.makeText(getContext(), "PDF saved successfully!", Toast.LENGTH_SHORT).show();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Failed to create PDF: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void addActivity() {
        TimeInterval interval = new TimeInterval();
        interval.setStartTime(String.valueOf(activityStart.getText()));
        interval.setEndTime(String.valueOf(activityEnd.getText()));

        Activity activity = new Activity();
        activity.setEventId(1);
        activity.setName(String.valueOf(activityName.getText()));
        activity.setDescription(String.valueOf(activityDescription.getText()));
        activity.setInterval(interval);
        activity.setLocation(String.valueOf(activityLocation.getText()));

        activityName.setText("");
        activityDescription.setText("");
        activityStart.setText("");
        activityEnd.setText("");
        activityLocation.setText("");

        activities.add(activity);
        activityRepository.add(activity);
        adapter.notifyDataSetChanged();
    }

    private void addGuest() {
        Guest guest = new Guest();
        guest.setEventId(1);
        guest.setName(String.valueOf(guestName.getText()));
        guest.setAgeRange(selectedAgeRange);
        guest.setInvited(isInvited);
        guest.setAcceptedInvitation(isAccepted);
        guest.setVegetarian(isVegetarian);
        guest.setVegan(isVegan);

        guestName.setText("");
        guestAgeRange.setSelection(0);
        guestInvited.setChecked(false);
        guestAccepted.setChecked(false);
        guestAccepted.setEnabled(false);
        guestVegetarian.setChecked(false);
        guestVegan.setChecked(false);

        guests.add(guest);
        guestRepository.add(guest);
        guestsAdapter.notifyDataSetChanged();
    }

    private void initViews(View view) {
        layoutStep1 = view.findViewById(R.id.create_event_layout_step1);
        layoutStep2 = view.findViewById(R.id.create_event_layout_step2);
        layoutStep3 = view.findViewById(R.id.create_event_layout_step3);
        layoutStep4 = view.findViewById(R.id.create_event_layout_step4);
        layoutStep5 = view.findViewById(R.id.create_event_layout_step5);

        eventName = view.findViewById(R.id.event_name);
        eventDescription = view.findViewById(R.id.event_description);
        maxGuests = view.findViewById(R.id.max_guests);
        privacyRules = view.findViewById(R.id.privacy_rules);
        eventAddress = view.findViewById(R.id.event_address);
        radius = view.findViewById(R.id.radius);
        eventDate = view.findViewById(R.id.event_date);

        guestName = view.findViewById(R.id.edit_text_guest_name);
        guestAgeRange = view.findViewById(R.id.spinner_age_range);
        guestInvited = view.findViewById(R.id.checkbox_invited);
        guestAccepted = view.findViewById(R.id.checkbox_accepted_invitation);
        guestVegan = view.findViewById(R.id.checkbox_vegan);
        guestVegetarian = view.findViewById(R.id.checkbox_vegetarian);

        activityName = view.findViewById(R.id.activity_name);
        activityDescription = view.findViewById(R.id.activity_description);
        activityStart = view.findViewById(R.id.activity_start);
        activityEnd = view.findViewById(R.id.activity_end);
        activityLocation = view.findViewById(R.id.activity_location);

        btnPrevious = view.findViewById(R.id.create_event_btn_previous);
        btnNext = view.findViewById(R.id.create_event_btn_next);
        btnAddActivity = view.findViewById(R.id.add_activity);
        btnGeneratePdf = view.findViewById(R.id.generate_pdf);
        btnAddGuest = view.findViewById(R.id.add_guest);
        btnGenerateGuestsPdf = view.findViewById(R.id.generate_guest_pdf);

        eventDate.setOnClickListener(this::showDatePickerDialog);
    }

    public void showDatePickerDialog(View v) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(
                getContext(),
                (DatePickerDialog.OnDateSetListener) (view, year1, monthOfYear, dayOfMonth) -> {
                    String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year1;
                    eventDate.setText(date);
                },
                year, month, day);

        datePickerDialog.show();
    }

    private void navigateToPreviousStep() {
        if (currentStep > 1) {
            currentStep--;
            updateStepVisibility();
        }
    }

    private void navigateToNextStep() {
        if (currentStep < 5) {
            currentStep++;
            updateStepVisibility();
        } else {
            FragmentUtils.replaceFragment(
                    getParentFragmentManager(),
                    new ServiceFragment(),
                    R.id.organizer_fragment_container,
                    (NavigationView) getActivity().findViewById(R.id.organizer_nav_view),
                    R.id.organizer_nav_item1,
                    getString(R.string.view_services)
            );
        }
    }

    private void updateStepVisibility() {
        layoutStep1.setVisibility(currentStep == 1 ? View.VISIBLE : View.GONE);
        layoutStep2.setVisibility(currentStep == 2 ? View.VISIBLE : View.GONE);
        layoutStep3.setVisibility(currentStep == 3 ? View.VISIBLE : View.GONE);
        layoutStep4.setVisibility(currentStep == 4 ? View.VISIBLE : View.GONE);
        layoutStep5.setVisibility(currentStep == 5 ? View.VISIBLE : View.GONE);
    }

    private boolean validateStep() {
        switch (currentStep) {
            case 1:
                return true;//validateStep1();
            case 2:
                return validateStep2();
            case 3:
                return validateStep3();
            case 4:
                return validateStep4();
            case 5:
                return validateStep5();
            default:
                return true;
        }
    }

    private boolean validateStep1() {

        String name = eventName.getText().toString();
        String description = eventDescription.getText().toString();
        String guests = maxGuests.getText().toString();
        String address = eventAddress.getText().toString();
        String radiusValue = radius.getText().toString();
        String date = eventDate.getText().toString();

        if (name.isEmpty() || description.isEmpty() || guests.isEmpty() || address.isEmpty() || radiusValue.isEmpty() || date.isEmpty()) {
            Toast.makeText(getContext(), "Please fill in all the fields", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private boolean validateStep2() {
        return true;
    }

    private boolean validateStep3() {
        return true;
    }

    private boolean validateStep4() {
        return true;
    }

    private boolean validateStep5() {
        return true;
    }

    private void saveEvent() {
        if (!validateStep()) {
            return;
        }

        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser == null) {
            Toast.makeText(getContext(), "User not authenticated", Toast.LENGTH_SHORT).show();
            return;
        }

        // Use currentUser to get the user's email or UID
        String userEmail = currentUser.getEmail();
        String userId = currentUser.getUid();

        // Convert EditText values to strings
        String eventNameStr = eventName.getText().toString();
        String eventDescriptionStr = eventDescription.getText().toString();
        String eventAddressStr = eventAddress.getText().toString();
        String maxGuestsStr = maxGuests.getText().toString();
        String radiusStr = radius.getText().toString();

        // Parse maxGuests and radius to integers
        int maxGuestsInt;
        int radiusInt;
        try {
            maxGuestsInt = Integer.parseInt(maxGuestsStr);
            radiusInt = Integer.parseInt(radiusStr);
        } catch (NumberFormatException e) {
            Toast.makeText(getContext(), "Invalid number format for max guests or radius", Toast.LENGTH_SHORT).show();
            return;
        }

        // Proceed with creating the Budget and Event
        Budget budget = new Budget(UUID.randomUUID().toString(), new ArrayList<String>());
        OrganiserEvent event = new OrganiserEvent(
                UUID.randomUUID().toString(),
                eventNameStr,
                eventDescriptionStr,
                maxGuestsInt,
                false,
                eventAddressStr,
                radiusInt,
                budget.getId(),
                userEmail // Assuming you want to store the user's email with the event
        );

        organiserEventRepository.add(event);
        budgetRepository.add(budget);

        Toast.makeText(getContext(), "Event Created!", Toast.LENGTH_SHORT).show();

        FragmentUtils.replaceFragment(
                getParentFragmentManager(),
                new ServiceFragment(),
                R.id.organizer_fragment_container,
                getActivity().findViewById(R.id.organizer_nav_view),
                R.id.organizer_nav_item1,
                getString(R.string.view_services)
        );
    }

}
