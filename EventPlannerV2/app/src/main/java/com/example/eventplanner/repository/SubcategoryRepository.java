package com.example.eventplanner.repository;

import android.content.Context;

import com.example.eventplanner.database.DBHandler;
import com.example.eventplanner.model.Subcategory;
import com.example.eventplanner.repository.interfaces.SubcategoryRepositoryInterface;

import java.util.List;

public class SubcategoryRepository implements SubcategoryRepositoryInterface {

    private final DBHandler dbHandler;

    public SubcategoryRepository(DBHandler dbHandler) {
        this.dbHandler = dbHandler;
    }
    public void addSubcategory(Subcategory subcategory) {
        dbHandler.addSubcategory(subcategory);
    }

    public List<Subcategory> getSubcategoriesForCategory(int categoryId) {
        return dbHandler.getSubcategoriesForCategory(categoryId);
    }

    public List<Subcategory> getAllSubcategories() {
        return dbHandler.getAllSubcategories();
    }

    public void deactivateSubcategory(int subcategoryId) {
        dbHandler.deactivateSubcategory(subcategoryId);
    }

    public void activateSubcategory(int subcategoryId) {
        dbHandler.activateSubcategory(subcategoryId);
    }
}
