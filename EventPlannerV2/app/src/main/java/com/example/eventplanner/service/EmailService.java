package com.example.eventplanner.service;

import android.util.Log;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailService {

    public static void sendRegistrationEmail(String userEmail, String userName) {
        new Thread(() -> {
            try {
                // Create email properties
                Properties properties = new Properties();
                properties.put("mail.smtp.host", "smtp.gmail.com");
                properties.put("mail.smtp.port", "465");
                properties.put("mail.smtp.ssl.enable", "true");
                properties.put("mail.smtp.auth", "true");

                // Gmail username and password
                String myEmail = "dmjmisa@gmail.com";
                String myPassword = "lqrk rhil pxyb scai";

                // Create a new session with an authenticator
                Session session = Session.getInstance(properties, new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(myEmail, myPassword);
                    }
                });

                // Create a new email message
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(myEmail));
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(userEmail));
                message.setSubject("Registration Successful");
                message.setText("Dear " + userName + ",\n\n" +
                        "Thank you for registering as an organizer with Event Planner.\n\n" +
                        "Best Regards,\n" +
                        "Event Planner Team");

                // Send the email
                Transport.send(message);

                // Log the email sent
                Log.d("Email", "Email sent successfully");

            } catch (Exception e) {
                Log.e("Email", "Error sending email", e);
            }
        }).start();
    }

    public static void sendRejectionEmail(String userEmail, String reason) {
        new Thread(() -> {
            try {
                // Create email properties
                Properties properties = new Properties();
                properties.put("mail.smtp.host", "smtp.gmail.com");
                properties.put("mail.smtp.port", "465");
                properties.put("mail.smtp.ssl.enable", "true");
                properties.put("mail.smtp.auth", "true");

                // Gmail username and password
                String myEmail = "dmjmisa@gmail.com";
                String myPassword = "lqrk rhil pxyb scai";

                // Create a new session with an authenticator
                Session session = Session.getInstance(properties, new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(myEmail, myPassword);
                    }
                });

                // Create a new email message
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(myEmail));
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(userEmail));
                message.setSubject("Registration Rejected");
                message.setText("Dear potential vendor, " + ",\n\n" +
                        "We regret to inform you that your registration as an organizer with Event Planner has been rejected.\n\n" +
                        "Reason: " + reason + "\n\n" +
                        "Best Regards,\n" +
                        "Event Planner Team");

                // Send the email
                Transport.send(message);

                // Log the email sent
                Log.d("Email", "Rejection email sent successfully");

            } catch (Exception e) {
                Log.e("Email", "Error sending rejection email", e);
            }
        }).start();
    }
}
