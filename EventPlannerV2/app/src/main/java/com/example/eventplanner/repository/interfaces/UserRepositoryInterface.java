package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.CompanyReport;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.UserReport;
import com.google.android.gms.tasks.Task;

import java.util.List;

public interface UserRepositoryInterface {
    Task<User> getUserByEmailAndPassword(String email, String password);
    Task<Void> addUser(User user);
    Task<User> getUserByEmail(String email);
    void deactivateStaffAccount(String email);
    void activateStaffAccount(String email);
    void addUserReport(UserReport userReport);
    void addCompanyReport(CompanyReport companyReport);
    Task<List<User>> getAllUsers();
    Task<List<User>> getPendingVendorUsers();
    Task<String> getPasswordByEmail(String email);
    Task<Void> updateUser(User loggedInUser);
    Task<Void> deactivateUserByEmail(String userEmail);
}
