package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.Category;

import java.util.List;

public interface CategoryRepositoryInterface {

    void addCategory(Category category);

    List<Category> getAllCategories();

    void deactivateCategory(int categoryId);

    void activateCategory(int categoryId);
}
