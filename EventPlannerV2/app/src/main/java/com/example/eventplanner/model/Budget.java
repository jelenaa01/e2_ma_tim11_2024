package com.example.eventplanner.model;

import java.util.ArrayList;
import java.util.List;

public class Budget {

    private String id;
    private List<String> productIds;
    private double totalPrice;
    public Budget(){}
    public Budget(String id, List<String> productIds) {
        this.id = id;
        this.productIds = productIds;
        this.totalPrice = 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<String> productIds) {
        this.productIds = productIds;
    }
    public void addProductIs(String productID) {
        if (this.productIds == null) {
            this.productIds = new ArrayList<>();
        }
        this.productIds.add(productID);
    }

    public void addPrice(double value)
    {
        this.totalPrice += value;
    }
}
