package com.example.eventplanner.fragments.typesCategories;

import android.app.AlertDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.eventplanner.R;
import com.example.eventplanner.database.DBHandler;
import com.example.eventplanner.fragments.vendorStaff.VendorStaffDetailsFragment;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.utils.FragmentUtils;
import com.google.android.material.navigation.NavigationView;


import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 */
public class CategoryFragment extends Fragment {

    private List<Category> categories;
    private CategoryRecyclerViewAdapter adapter;
    private DBHandler dbHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_list, container, false);

        dbHandler = DBHandler.getInstance(getContext());

        RecyclerView recyclerView = view.findViewById(R.id.categoriesRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        categories = dbHandler.getAllCategories();

        adapter = new CategoryRecyclerViewAdapter(categories, getContext());
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(category-> {
            FragmentUtils.replaceFragment(
                    getParentFragmentManager(),
                    CategoryDetailsFragment.newInstance(category),
                    R.id.admin_fragment_container,
                    (NavigationView) getActivity().findViewById(R.id.admin_nav_view),
                    R.id.admin_nav_item2,  // Assuming you want to update nav_item1, change this as per your requirement
                    getString(R.string.category_list)  // Set the title for the toolbar
            );
        });

        // Floating Action Button to add new EventType
        view.findViewById(R.id.fabAddCategory).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddCategoryDialog();
            }
        });

        return view;
    }
    private void showAddCategoryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_add_category, null);
        EditText editTextName = view.findViewById(R.id.editCategoryTextName);
        EditText editTextDescription = view.findViewById(R.id.editCategoryTextDescription);
        Button buttonCancel = view.findViewById(R.id.buttonCategoryCancel);
        Button buttonAdd = view.findViewById(R.id.buttonCategoryAdd);

        builder.setView(view);

        AlertDialog dialog = builder.create();

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editTextName.getText().toString();
                String description = editTextDescription.getText().toString();

                if (!name.isEmpty() && !description.isEmpty()) {
                    dbHandler.addCategory(new Category(1, name, description, true));
                    categories.clear();
                    categories.addAll(dbHandler.getAllCategories()); // Refresh the list from database
                    adapter.notifyDataSetChanged(); // Notify the adapter about the data set change
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }


}