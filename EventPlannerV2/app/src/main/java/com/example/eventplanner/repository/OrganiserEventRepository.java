package com.example.eventplanner.repository;

import android.util.Log;

import com.example.eventplanner.model.OrganiserEvent;
import com.example.eventplanner.repository.interfaces.OrganiserEventRepositoryInterface;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;

public class OrganiserEventRepository implements OrganiserEventRepositoryInterface {

    private static final String TAG = "OrganiserEventRepository";
    private FirebaseFirestore db;
    public OrganiserEventRepository(FirebaseFirestore firestore) {
        this.db = firestore;
    }
    @Override
    public List<OrganiserEvent> getAll() {
        return null;
    }

    @Override
    public List<OrganiserEvent> getByEventId(int eventId) {
        return null;
    }

    @Override
    public void add(OrganiserEvent organiserEvent) {
        db.collection("organiserEvents")
                .add(organiserEvent)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Event successfully added.");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error adding Event.", e);
                });
    }
    @Override
    public Task<List<OrganiserEvent>> getByOrganiserEmail(String organiserEmail) {
        TaskCompletionSource<List<OrganiserEvent>> taskCompletionSource = new TaskCompletionSource<>();

        db.collection("organiserEvents")
                .whereEqualTo("organiserEmail", organiserEmail)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<OrganiserEvent> events = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            OrganiserEvent event = document.toObject(OrganiserEvent.class);
                            events.add(event);
                        }
                        taskCompletionSource.setResult(events);
                    } else {
                        Log.e(TAG, "Error getting events by organiser email.", task.getException());
                        taskCompletionSource.setException(task.getException());
                    }
                });

        return taskCompletionSource.getTask();
    }


}
