package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.Subcategory;

import java.util.List;

public interface SubcategoryRepositoryInterface {

    void addSubcategory(Subcategory subcategory);

    List<Subcategory> getSubcategoriesForCategory(int categoryId);

    List<Subcategory> getAllSubcategories();

    void deactivateSubcategory(int subcategoryId);

    void activateSubcategory(int subcategoryId);
}
