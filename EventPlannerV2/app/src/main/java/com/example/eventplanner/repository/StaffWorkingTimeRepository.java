package com.example.eventplanner.repository;

import android.util.Log;

import com.example.eventplanner.model.User;
import com.example.eventplanner.model.VendorStaffWorkingTime;
import com.example.eventplanner.model.WorkingTime;
import com.example.eventplanner.repository.interfaces.StaffWorkingTimeRepositoryInterface;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StaffWorkingTimeRepository implements StaffWorkingTimeRepositoryInterface {
    private static final String TAG = "VendorStaffWorkingTimeRepository";
    private FirebaseFirestore db;

    public StaffWorkingTimeRepository(FirebaseFirestore firestore) {
        this.db = firestore;
    }

    @Override
    public void addWorkingTime(VendorStaffWorkingTime staffWorkingTime) {
        if (staffWorkingTime == null) {
            Log.e(TAG, "Working time for vendor staff is null");
            return;
        }
        db.collection("staffWorkingTime")
                .add(staffWorkingTime)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Working time for vendor staff successfully added");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error adding working time for vendor staff", e);
                });
    }

    @Override
    public Task<VendorStaffWorkingTime> getWorkingTimeByStaff(String email) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("staffWorkingTime")
                .whereEqualTo("vendorStaffEmail", email)
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
                            return documentSnapshot.toObject(VendorStaffWorkingTime.class);
                        }
                    }
                    return null;
                });
    }

    @Override
    public void updateWorkingTime(String email, List<WorkingTime> workingTimes, Date fromDate, Date toDate) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("staffWorkingTime")
                .whereEqualTo("vendorStaffEmail", email)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        DocumentSnapshot documentSnapshot = queryDocumentSnapshots.getDocuments().get(0);
                        String docId = documentSnapshot.getId();
                        DocumentReference docRef = db.collection("staffWorkingTime").document(docId);

                        Map<String, Object> data = new HashMap<>();
                        data.put("fromDate", fromDate); // Dodajemo datum od kada se primenjuje novo radno vreme
                        data.put("toDate", toDate); // Dodajemo datum do kada se primenjuje novo radno vreme
                        data.put("workingTime", workingTimes); // Ažuriramo radno vreme

                        docRef.set(data, SetOptions.merge()) // Koristimo SetOptions.merge() da bismo samo ažurirali polja koja su prosleđena, a ne celu dokumentaciju
                                .addOnSuccessListener(aVoid -> {
                                    Log.d(TAG, "Working time successfully updated");
                                })
                                .addOnFailureListener(e -> {
                                    Log.e(TAG, "Error during working time update", e);
                                });
                    } else {
                        Log.d(TAG, "No working time found for user with email: " + email);
                    }
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error finding working time", e);
                });
    }




}
