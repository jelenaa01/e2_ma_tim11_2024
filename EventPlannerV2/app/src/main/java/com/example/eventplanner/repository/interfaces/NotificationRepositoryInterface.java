package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

public interface NotificationRepositoryInterface {

    Task<Notification> createNotification(Notification notification);
    Task<List<Notification>> getAllLoggedUserNotifications();
    Task<List<Notification>> getLoggedUserUnreadNotifications();
    public Task<Void> setNotificationAsRead(String notificationId);
    Task<Void> setNotificationAsShown(String notificationId);
    public void removeListener();
    public void listenForCreatedNotifications(EventListener<QuerySnapshot> listener);
    public void listenForShownNotifications(EventListener<QuerySnapshot> listener);
}
