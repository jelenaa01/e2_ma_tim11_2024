package com.example.eventplanner.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Company implements Serializable {
    private String id;
    private String name;
    private String email;
    private String address;
    private String phoneNumber;
    private String description;
    private String companyPhoto;
    private String ownerEmail;
    private List<String> employeeEmails = new ArrayList<>();
    private List<WorkingTime> companyWorkingTime = new ArrayList<>();

    public Company() {}

    public Company(String id, String name, String email, String address, String phoneNumber, String description, String companyPhoto, String ownerEmail, List<String> employeeEmails, List<WorkingTime> companyWorkingTime) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.description = description;
        this.companyPhoto = companyPhoto;
        this.ownerEmail = ownerEmail;
        this.employeeEmails = employeeEmails;
        this.companyWorkingTime = companyWorkingTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCompanyPhoto() {
        return companyPhoto;
    }

    public void setCompanyPhoto(String companyPhoto) {
        this.companyPhoto = companyPhoto;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public List<String> getEmployeeEmails() {
        return employeeEmails;
    }

    public void setEmployeeEmails(List<String> employeeEmails) {
        this.employeeEmails = employeeEmails;
    }

    public List<WorkingTime> getCompanyWorkingTime() {
        return companyWorkingTime;
    }

    public void setCompanyWorkingTime(List<WorkingTime> companyWorkingTime) {
        this.companyWorkingTime = companyWorkingTime;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", description='" + description + '\'' +
                ", companyPhoto='" + companyPhoto + '\'' +
                ", ownerId='" + ownerEmail + '\'' +
                ", employees=" + employeeEmails +
                ", companyWorkingTime=" + companyWorkingTime +
                '}';
    }
}
