package com.example.eventplanner.fragments.reports;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.VendorActivity;
import com.example.eventplanner.model.CompanyRatingReport;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.CompanyRatingRepositoryInterface;
import com.example.eventplanner.repository.interfaces.NotificationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.RatingReportRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

public class RatingReportAdapter extends RecyclerView.Adapter<RatingReportAdapter.ViewHolder>{
    private static final String TAG = "RatingReportAdapter";
    private List<CompanyRatingReport> reportList;
    private Map<String, User> emailToUserMap;
    private RatingReportAdapter.OnItemClickListener listener;
    private RatingReportRepositoryInterface ratingReportRepository;
    private CompanyRatingRepositoryInterface companyRatingRepository;
    private UserRepositoryInterface userRepository;
    private NotificationRepositoryInterface notificationRepository;

    public RatingReportAdapter(List<CompanyRatingReport> items, Map<String, User> emailToUserMap, RatingReportRepositoryInterface ratingReportRepository,
                               CompanyRatingRepositoryInterface companyRatingRepository, UserRepositoryInterface userRepository, NotificationRepositoryInterface notificationRepository) {
        this.reportList = items;
        this.emailToUserMap = emailToUserMap;
        this.ratingReportRepository = ratingReportRepository;
        this.companyRatingRepository = companyRatingRepository;
        this.userRepository = userRepository;
        this.notificationRepository = notificationRepository;
    }

    @NonNull
    @Override
    public RatingReportAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rating_report_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RatingReportAdapter.ViewHolder holder, int position) {
        CompanyRatingReport ratingReport = reportList.get(position);

        User vendor = emailToUserMap.get(ratingReport.getVendorEmail().toLowerCase());

        String vendorFullName = (vendor != null) ? vendor.getFirstName() + " " + vendor.getLastName() : "";

        holder.vendorFullName.setText("Reported by: " + vendorFullName);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        String formattedDate = dateFormat.format(ratingReport.getDate());

        holder.date.setText("Date: " + formattedDate);
        holder.reason.setText("Reason: " + ratingReport.getReason());
        holder.status.setText("Status: " + ratingReport.getReportStatus().toString());


        holder.btnReject.setOnClickListener(v -> {
            if(listener != null) {
                listener.onItemClick(ratingReport);
            }

            ratingReportRepository.changeReportStatus(ratingReport.getId(), CompanyRatingReport.RatingReportStatus.REJECTED);
            updateReportStatus(ratingReport.getId(), CompanyRatingReport.RatingReportStatus.REJECTED);
            Log.d(TAG, "Report succesfully rejected");

            Notification notification = new Notification(UUID.randomUUID().toString(), "Reject report for company rating", "Reported rating submitted adheres to the rules or terms of service of the application.", ratingReport.getVendorEmail(),Notification.NotificationStatus.CREATED, Timestamp.now());
            notificationRepository.createNotification(notification);

        });

        holder.btnAccept.setOnClickListener(v -> {
            if(listener != null) {
                listener.onItemClick(ratingReport);
            }

            ratingReportRepository.changeReportStatus(ratingReport.getId(), CompanyRatingReport.RatingReportStatus.ACCEPTED);
            updateReportStatus(ratingReport.getId(), CompanyRatingReport.RatingReportStatus.ACCEPTED);
            Log.d(TAG, "Report succesfully accepted");
            companyRatingRepository.removeCompanyRating(ratingReport.getCompanyRatingId());
            Log.d(TAG, "Company rating successfully removed");

        });

        holder.btnVendorProfile.setOnClickListener(v -> {
            if (listener != null) {
                listener.onItemClick(ratingReport);
            }

            String vendorEmail = ratingReport.getVendorEmail();
            userRepository.getPasswordByEmail(vendorEmail)
                    .addOnSuccessListener(password -> {
                        FirebaseAuth mAuth = FirebaseAuth.getInstance();
                        mAuth.signInWithEmailAndPassword(vendorEmail, password)
                                .addOnCompleteListener(task -> {
                                    if (task.isSuccessful()) {
                                        Intent intent = new Intent(holder.itemView.getContext(), VendorActivity.class);
                                        holder.itemView.getContext().startActivity(intent);
                                    } else {
                                        Toast.makeText(holder.itemView.getContext(), "Failed to sign in as vendor.", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    })
                    .addOnFailureListener(e -> {
                        Toast.makeText(holder.itemView.getContext(), "Failed to fetch vendor password.", Toast.LENGTH_SHORT).show();
                    });
        });

    }

    @Override
    public int getItemCount() {
        return reportList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView vendorFullName;
        public TextView date;
        public TextView reason;
        public TextView status;
        public ImageButton btnReject;
        public ImageButton btnAccept;
        public Button btnVendorProfile;

        public ViewHolder(View view) {
            super(view);
            vendorFullName = view.findViewById(R.id.report_vendor);
            date = view.findViewById(R.id.report_date);
            reason = view.findViewById(R.id.report_reason);
            status = view.findViewById(R.id.report_status);
            btnReject = view.findViewById(R.id.report_btnReject);
            btnAccept = view.findViewById(R.id.report_btnAccept);
            btnVendorProfile = view.findViewById(R.id.report_btnVendorProfile);
        }
    }

    public void setOnItemClickListener(RatingReportAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(CompanyRatingReport ratingReport);
    }


    public void updateReportStatus(String reportId, CompanyRatingReport.RatingReportStatus newStatus) {
        for (int i = 0; i < reportList.size(); i++) {
            CompanyRatingReport report = reportList.get(i);
            if (report.getId().equals(reportId)) {
                report.setReportStatus(newStatus);
                notifyItemChanged(i);
                break;
            }
        }
    }

}
