package com.example.eventplanner.fragments.organiser;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.FavoriteItem;
import com.example.eventplanner.repository.interfaces.FavoriteItemRepositoryInterface;

import java.util.ArrayList;
import java.util.List;

public class FavoriteItemsRecyclerViewAdapter extends RecyclerView.Adapter<FavoriteItemsRecyclerViewAdapter.ViewHolder> {
    private final List<FavoriteItem> favoriteItems;
    private final FavoriteItemRepositoryInterface favoriteItemRepository;
    private OnItemClickListener listener;

    public FavoriteItemsRecyclerViewAdapter(List<FavoriteItem> favoriteItems, FavoriteItemRepositoryInterface favoriteItemRepository) {
        this.favoriteItems = new ArrayList<>(favoriteItems);
        this.favoriteItemRepository = favoriteItemRepository;
    }

    @NonNull
    @Override
    public FavoriteItemsRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.favorite_item, parent, false);
        return new FavoriteItemsRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteItemsRecyclerViewAdapter.ViewHolder holder, int position) {
        FavoriteItem favoriteItem = favoriteItems.get(position);
        holder.btnRemoveItem.setOnClickListener(v -> {
            removeItem(position, favoriteItem.getItemName(), favoriteItem.getUserEmail());
        });
        holder.tvItemName.setText(favoriteItem.getItemName());
        holder.tvItemType.setText(favoriteItem.getType());
        holder.tvItemEmail.setText(favoriteItem.getUserEmail());
        holder.bind(favoriteItem, listener);
    }

    public void removeItem(int position, String itemName, String userEmail) {
        favoriteItems.remove(position);
        favoriteItemRepository.removeByNameAndEmail(itemName, userEmail);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, favoriteItems.size());
    }

    @Override
    public int getItemCount() {
        return favoriteItems.size();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(FavoriteItem favoriteItem);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvItemName;
        public TextView tvItemType;
        public TextView tvItemEmail;
        public Button btnRemoveItem;

        public ViewHolder(View itemView) {
            super(itemView);
            tvItemName = itemView.findViewById(R.id.favoriteItemNameTextView1);
            tvItemType = itemView.findViewById(R.id.descriptiofavoriteItemTypeTextView1);
            tvItemEmail = itemView.findViewById(R.id.favoriteItemEmailTextView1);
            btnRemoveItem = itemView.findViewById(R.id.removeFavoriteItemButton);
        }

        public void bind(final FavoriteItem favoriteItem, final FavoriteItemsRecyclerViewAdapter.OnItemClickListener listener) {
            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(favoriteItem);
                }
            });
        }
    }
}
