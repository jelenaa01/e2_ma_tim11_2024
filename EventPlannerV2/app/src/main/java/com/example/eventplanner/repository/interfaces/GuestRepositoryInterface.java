package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.Guest;

public interface GuestRepositoryInterface {
    void add(Guest guest);
    void remove(Guest guest);
}
