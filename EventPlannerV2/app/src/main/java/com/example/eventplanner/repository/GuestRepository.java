package com.example.eventplanner.repository;

import android.util.Log;

import com.example.eventplanner.model.Guest;
import com.example.eventplanner.repository.interfaces.GuestRepositoryInterface;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class GuestRepository implements GuestRepositoryInterface {
    private static final String TAG = "GuestRepository";
    private FirebaseFirestore db;

    public GuestRepository(FirebaseFirestore firestore) {
        this.db = firestore;
    }

    @Override
    public void add(Guest guest) {
        db.collection("guests")
                .add(guest)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Guest successfully added.");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error adding Guest.", e);
                });
    }

    @Override
    public void remove(Guest guest) {
        Log.d(TAG, "Guest glup removed.");
        db.collection("guests")
                .whereEqualTo("name", guest.getName())
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Guest storedGuest = documentSnapshot.toObject(Guest.class);
                        Log.d(TAG, storedGuest.toString());
                        Log.d(TAG, guest.toString());
                        if (storedGuest != null && storedGuest.equals(guest)) {
                            db.collection("guests")
                                    .document(documentSnapshot.getId())
                                    .delete()
                                    .addOnSuccessListener(aVoid -> {
                                        Log.d(TAG, "Guest successfully removed.");
                                    })
                                    .addOnFailureListener(e -> {
                                        Log.e(TAG, "Error removing Guest.", e);
                                    });
                            break;
                        }
                    }
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error querying for Guest.", e);
                });
    }
}
