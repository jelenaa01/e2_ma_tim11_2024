package com.example.eventplanner.fragments.reports;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.UserReport;

import java.util.List;

public class ReportedUsersRecyclerViewAdapter extends RecyclerView.Adapter<ReportedUsersRecyclerViewAdapter.ViewHolder> {
    private final List<UserReport> reports;
    private OnItemClickListener listener;
    private OnReporteeClickListener reporteeClickListener;
    private OnReportedClickListener reportedClickListener;
    private OnAcceptClickListener acceptClickListener;
    private OnDenyClickListener denyClickListener;

    public ReportedUsersRecyclerViewAdapter(List<UserReport> reports) {
        this.reports = reports;
    }
    public interface OnItemClickListener {
        void onItemClick(UserReport userReport);
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
    public void setOnReporteeClickListener(OnReporteeClickListener reporteeClickListener) {
        this.reporteeClickListener = reporteeClickListener;
    }
    public void setOnReportedClickListener(OnReportedClickListener reportedClickListener) {
        this.reportedClickListener = reportedClickListener;
    }
    public void setOnAcceptClickListener(OnAcceptClickListener acceptClickListener) {
        this.acceptClickListener = acceptClickListener;
    }
    public void setOnDenyClickListener(OnDenyClickListener denyClickListener) {
        this.denyClickListener = denyClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_report_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        UserReport report = reports.get(position);
        holder.tvReportee.setText(report.getUserEmail());
        holder.tvReported.setText(report.getVendorEmail());
        holder.tvDate.setText(report.getDescription());
        holder.tvReason.setText(report.getDate());
        holder.tvStatus.setText(report.getStatus());

        if ("REPORTED".equals(report.getStatus())) {
            holder.ibAccept.setVisibility(View.VISIBLE);
            holder.ibDeny.setVisibility(View.VISIBLE);
        } else {
            holder.ibAccept.setVisibility(View.GONE);
            holder.ibDeny.setVisibility(View.GONE);
        }

        holder.bind(report, listener, reporteeClickListener, reportedClickListener, acceptClickListener, denyClickListener);
    }

    public interface OnReporteeClickListener {
        void onReporteeClick(String userEmail);
    }

    public interface OnReportedClickListener {
        void onReportedClick(String userEmail);
    }

    public interface OnAcceptClickListener {
        void onAcceptedClick(UserReport report);
    }

    public interface OnDenyClickListener {
        void onDeniedClick(UserReport report);
    }

    @Override
    public int getItemCount() {
        return reports.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvReportee;
        public TextView tvReported;
        public TextView tvDate;
        public TextView tvReason;
        public TextView tvStatus;
        public ImageButton ibAccept;
        public ImageButton ibDeny;

        public ViewHolder(View itemView) {
            super(itemView);
            tvReportee = itemView.findViewById(R.id.tvReportee);
            tvReported = itemView.findViewById(R.id.tvReported);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvReason = itemView.findViewById(R.id.tvReason);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            ibAccept = itemView.findViewById(R.id.ibAccept);
            ibDeny = itemView.findViewById(R.id.ibDeny);
        }

        public void bind(final UserReport userReport, final OnItemClickListener listener, final OnReporteeClickListener reportee, final OnReportedClickListener reported, final OnAcceptClickListener accept, final OnDenyClickListener deny) {
            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(userReport);
                }
            });

            tvReportee.setOnClickListener(v -> {
                if (reportee != null) {
                    reportee.onReporteeClick(userReport.getUserEmail());
                }
            });

            tvReported.setOnClickListener(v -> {
                if (reported != null) {
                    reported.onReportedClick(userReport.getVendorEmail());
                }
            });

            ibAccept.setOnClickListener(v -> {
                if (accept != null) {
                    accept.onAcceptedClick(userReport);
                }
            });

            ibDeny.setOnClickListener(v -> {
                if (deny != null) {
                    deny.onDeniedClick(userReport);
                }
            });
        }
    }
}
