package com.example.eventplanner.repository;

import android.util.Log;

import com.example.eventplanner.model.Notification;
import com.example.eventplanner.repository.interfaces.NotificationRepositoryInterface;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class NotificationRepository implements NotificationRepositoryInterface {

    private static final String TAG = "NotificationRepository";

    private final FirebaseFirestore db;
    private final FirebaseAuth mAuth;
    private final CollectionReference notificationsRef;
    private ListenerRegistration listenerRegistration;

    public NotificationRepository(FirebaseFirestore firestore) {
        this.db = firestore;
        this.mAuth = FirebaseAuth.getInstance();
        this.notificationsRef = db.collection("notifications");
    }

    @Override
    public Task<Notification> createNotification(Notification notification) {
        Log.d(TAG, "createNotification: Creating notification");
        DocumentReference newNotificationRef = notificationsRef.document();
        notification.setId(newNotificationRef.getId());
        return newNotificationRef.set(notification)
                .continueWithTask(task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "createNotification: Notification created successfully");
                        return Tasks.forResult(notification);
                    } else {
                        Log.e(TAG, "createNotification: Error creating notification", task.getException());
                        throw task.getException();
                    }
                });
    }

    @Override
    public Task<List<Notification>> getAllLoggedUserNotifications() {
        Log.d(TAG, "getAllLoggedUserNotifications: Retrieving all notifications for logged user");
        FirebaseUser user = mAuth.getCurrentUser();
        if (user == null) {
            Log.e(TAG, "getAllLoggedUserNotifications: User not logged in");
            return Tasks.forException(new Exception("User not logged in"));
        }
        String userEmail = user.getEmail();
        return notificationsRef.whereEqualTo("email", userEmail).get()
                .continueWithTask(task -> {
                    if (task.isSuccessful()) {
                        List<Notification> notifications = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            notifications.add(document.toObject(Notification.class));
                        }
                        Log.d(TAG, "getAllLoggedUserNotifications: Retrieved " + notifications.size() + " notifications");
                        return Tasks.forResult(notifications);
                    } else {
                        Log.e(TAG, "getAllLoggedUserNotifications: Error retrieving notifications", task.getException());
                        throw task.getException();
                    }
                });
    }

    @Override
    public Task<List<Notification>> getLoggedUserUnreadNotifications() {
        Log.d(TAG, "getLoggedUserUnreadNotifications: Retrieving unread notifications for logged user");
        FirebaseUser user = mAuth.getCurrentUser();
        if (user == null) {
            Log.e(TAG, "getLoggedUserUnreadNotifications: User not logged in");
            return Tasks.forException(new Exception("User not logged in"));
        }
        String userEmail = user.getEmail();
        return notificationsRef.whereEqualTo("email", userEmail).whereEqualTo("status", "SHOWN").get()
                .continueWithTask(task -> {
                    if (task.isSuccessful()) {
                        List<Notification> notifications = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            notifications.add(document.toObject(Notification.class));
                        }
                        Log.d(TAG, "getLoggedUserUnreadNotifications: Retrieved " + notifications.size() + " unread notifications");
                        return Tasks.forResult(notifications);
                    } else {
                        Log.e(TAG, "getLoggedUserUnreadNotifications: Error retrieving unread notifications", task.getException());
                        throw task.getException();
                    }
                });
    }

    @Override
    public Task<Void> setNotificationAsRead(String notificationId) {
        Log.d(TAG, "setNotificationAsRead: Setting notification as read");
        DocumentReference notificationRef = notificationsRef.document(notificationId);
        return notificationRef.update("status", "READ")
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "setNotificationAsRead: Notification marked as read");
                    } else {
                        Log.e(TAG, "setNotificationAsRead: Error setting notification as read", task.getException());
                        throw task.getException();
                    }
                    return null;
                });
    }

    @Override
    public Task<Void> setNotificationAsShown(String notificationId) {
        Log.d(TAG, "setNotificationAsRead: Setting notification as read");
        DocumentReference notificationRef = notificationsRef.document(notificationId);
        return notificationRef.update("status", "SHOWN")
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "setNotificationAsRead: Notification marked as SHOWN");
                    } else {
                        Log.e(TAG, "setNotificationAsRead: Error setting notification as SHOWN", task.getException());
                        throw task.getException();
                    }
                    return null;
                });
    }

    public void listenForCreatedNotifications(EventListener<QuerySnapshot> listener) {
        Log.d(TAG, "listenForCreatedNotifications: Setting up listener for created notifications");
        FirebaseUser user = mAuth.getCurrentUser();
        if (user == null) {
            Log.e(TAG, "listenForCreatedNotifications: User not logged in");
            return;
        }
        String userEmail = user.getEmail();
        Query query = notificationsRef.whereEqualTo("email", userEmail).whereEqualTo("status", "CREATED");
        listenerRegistration = query.addSnapshotListener(listener);
    }

    public void listenForShownNotifications(EventListener<QuerySnapshot> listener) {
        Log.d(TAG, "listenForShownNotifications: Setting up listener for shown notifications");
        FirebaseUser user = mAuth.getCurrentUser();
        if (user == null) {
            Log.e(TAG, "listenForShownNotifications: User not logged in");
            return;
        }
        String userEmail = user.getEmail();
        Query query = notificationsRef.whereEqualTo("email", userEmail).whereEqualTo("status", "SHOWN");
        listenerRegistration = query.addSnapshotListener(listener);
    }

    public void removeListener() {
        Log.d(TAG, "removeListener: Removing listener for notifications");
        if (listenerRegistration != null) {
            listenerRegistration.remove();
        }
    }
}
