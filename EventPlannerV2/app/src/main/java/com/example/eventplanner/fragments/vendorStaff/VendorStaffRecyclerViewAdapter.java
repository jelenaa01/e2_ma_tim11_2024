package com.example.eventplanner.fragments.vendorStaff;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;


public class VendorStaffRecyclerViewAdapter extends RecyclerView.Adapter<VendorStaffRecyclerViewAdapter.ViewHolder> {

    private final List<User> vendorStaff;
    public List<User> vendorStaffFull;

    private VendorStaffRecyclerViewAdapter.OnItemClickListener listener;
    private UserRepositoryInterface userRepository;

    public VendorStaffRecyclerViewAdapter(List<User> items, UserRepositoryInterface userRepository) {
        vendorStaff = new ArrayList<>(items);
        vendorStaffFull = new ArrayList<>(items);
        this.userRepository = userRepository;
    }

    @NonNull
    @Override
    public VendorStaffRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vendor_staff_item, parent, false);
        return new VendorStaffRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VendorStaffRecyclerViewAdapter.ViewHolder holder, int position) {
        User staff = vendorStaff.get(position);
        if (staff == null){
            Log.e("VendorStaffRecyclerView", "Staff is null.");
            return;
        }
        holder.tvName.setText(staff.getLastName() + " " + staff.getFirstName());
        holder.tvDescription.setText(staff.getEmail());

        if (staff.isActive()) {
            holder.tvName.setAlpha(1.0f);
            holder.tvDescription.setAlpha(1.0f);
            holder.btnDeactivate.setVisibility(View.VISIBLE);
            holder.btnActivate.setVisibility(View.GONE);
        } else {
            holder.tvName.setAlpha(0.5f);
            holder.tvDescription.setAlpha(0.5f);
            holder.btnDeactivate.setVisibility(View.GONE);
            holder.btnActivate.setVisibility(View.VISIBLE);
        }

        holder.btnDeactivate.setOnClickListener(v -> {
            if (listener != null) {
                listener.onItemClick(staff);
            }
            userRepository.deactivateStaffAccount(staff.getEmail());
        });

        holder.btnActivate.setOnClickListener(v -> {
            if(listener != null) {
                listener.onItemClick(staff);
            }
            userRepository.activateStaffAccount(staff.getEmail());
            FirebaseAuth.getInstance().signInWithEmailAndPassword(staff.getEmail(), staff.getPassword())
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            if (user != null) {
                                user.sendEmailVerification()
                                        .addOnCompleteListener(emailTask -> {
                                            if (emailTask.isSuccessful()) {
                                                Toast.makeText(holder.itemView.getContext(), "Activation link sent to " + user.getEmail(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(holder.itemView.getContext(), "Failed to send activation email.", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                            }
                        } else {
                            Toast.makeText(holder.itemView.getContext(), "Failed to sign in.", Toast.LENGTH_SHORT).show();
                        }
                    });

        });

        holder.itemView.setOnClickListener(v -> {
            if (listener != null) {
                listener.onItemClick(staff);
            }
        });
    }

    public Filter getFilter() {
        return staffFilter;
    }

    private Filter staffFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<User> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(vendorStaffFull);
            } else {
                // Remove spaces from the constraint and convert to lowercase
                String filterPattern = constraint.toString().replaceAll("\\s+", "").toLowerCase().trim();

                for (User item : vendorStaffFull) {
                    // Concatenate first name and last name without spaces and in lowercase
                    String fullName = (item.getFirstName() + item.getLastName()).toLowerCase();
                    String fullNameReverse = (item.getLastName() + item.getFirstName()).toLowerCase();

                    // Check if the user attributes contain the filter pattern
                    if (fullName.contains(filterPattern) || fullNameReverse.contains(filterPattern) ||
                            item.getFirstName().toLowerCase().contains(filterPattern) ||
                            item.getLastName().toLowerCase().contains(filterPattern) ||
                            item.getEmail().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }


        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            vendorStaff.clear();
            vendorStaff.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };


    @Override
    public int getItemCount() {
        return vendorStaff.size();
    }

    public void setOnItemClickListener(VendorStaffRecyclerViewAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(User staff);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView tvName;
        public final TextView tvDescription;
        public final ImageButton btnDeactivate;
        public final ImageButton btnActivate;

        public ViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.vendor_staff_tvName);
            tvDescription = view.findViewById(R.id.vendor_staff_tvDescription);
            btnDeactivate = view.findViewById(R.id.vendor_staff_btnDeactivate);
            btnActivate = view.findViewById(R.id.vendor_staff_btnActivate);
        }
    }
}