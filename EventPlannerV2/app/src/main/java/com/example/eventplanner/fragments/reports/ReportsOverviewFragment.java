package com.example.eventplanner.fragments.reports;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.productsOverview.VendorOverviewFragment;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.Reservation;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.UserReport;
import com.example.eventplanner.repository.interfaces.NotificationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.ReservationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserReportRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ReportsOverviewFragment extends Fragment implements ReportedUsersRecyclerViewAdapter.OnReporteeClickListener, ReportedUsersRecyclerViewAdapter.OnReportedClickListener, ReportedUsersRecyclerViewAdapter.OnAcceptClickListener, ReportedUsersRecyclerViewAdapter.OnDenyClickListener {

    private UserRepositoryInterface userRepository;
    private UserReportRepositoryInterface reportRepository;
    private FirebaseAuth mAuth;
    private RecyclerView reportsRecyclerView;
    private ReportedUsersRecyclerViewAdapter adapter;
    private List<UserReport> reports;
    private EditText etReason;
    private NotificationRepositoryInterface notificationRepository;
    private ReservationRepositoryInterface reservationRepository;
    private User loggedInUser;

    public ReportsOverviewFragment() {
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);
        reportRepository = eventPlannerApp.getDIContainer().resolve(UserReportRepositoryInterface.class);
        notificationRepository = eventPlannerApp.getDIContainer().resolve(NotificationRepositoryInterface.class);
        reservationRepository = eventPlannerApp.getDIContainer().resolve(ReservationRepositoryInterface.class);
        getLoggedInUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reports_overview, container, false);

        getReports(view);

        etReason = view.findViewById(R.id.edit_message);

        return view;
    }

    private void getReports(View view) {
        reports = new ArrayList<>();
        reportRepository.getAllPending()
                .addOnCompleteListener(new OnCompleteListener<List<UserReport>>() {
                    @Override
                    public void onComplete(@NonNull Task<List<UserReport>> task) {
                        if (task.isSuccessful()) {
                            reports = task.getResult();
                            reportsRecyclerView = view.findViewById(R.id.recycler_view_reports);
                            reportsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                            adapter = new ReportedUsersRecyclerViewAdapter(reports);
                            adapter.setOnReporteeClickListener(ReportsOverviewFragment.this);
                            adapter.setOnReportedClickListener(ReportsOverviewFragment.this);
                            adapter.setOnAcceptClickListener(ReportsOverviewFragment.this);
                            adapter.setOnDenyClickListener(ReportsOverviewFragment.this);
                            reportsRecyclerView.setAdapter(adapter);

                            for (UserReport message : reports) {
                                Log.d("ReportFragment", "Message: " + message);
                            }
                        } else {
                            Log.e("ReportFragment", "Error getting messages", task.getException());
                        }
                    }
                });
    }

    @Override
    public void onReporteeClick(String userEmail) {
        userRepository.getUserByEmail(userEmail)
                .addOnCompleteListener(new OnCompleteListener<User>() {
                    @Override
                    public void onComplete(@NonNull Task<User> task) {
                        navigateToUserPage(task.getResult());
                    }
                });
    }

    @Override
    public void onReportedClick(String userEmail) {
        userRepository.getUserByEmail(userEmail)
                .addOnCompleteListener(new OnCompleteListener<User>() {
                    @Override
                    public void onComplete(@NonNull Task<User> task) {
                        navigateToUserPage(task.getResult());
                    }
                });
    }

    private void navigateToUserPage(User user) {
        VendorOverviewFragment fragment = VendorOverviewFragment.newInstance(user);
        FragmentTransaction transaction = getParentFragmentManager().beginTransaction();

        if (loggedInUser != null) {
            switch (loggedInUser.getRole()) {
                case VENDOR:
                    transaction.replace(R.id.vendor_fragment_container, fragment);
                    break;
                case ORGANISER:
                    transaction.replace(R.id.organizer_fragment_container, fragment);
                    break;
                case VENDOR_STAFF:
                    transaction.replace(R.id.vendor_staff_fragment_container, fragment);
                    break;
                case ADMIN:
                    transaction.replace(R.id.admin_fragment_container, fragment);
                    break;
                default:
                    Log.e("navigateToCompanyOverview", "Unknown user role.");
                    return;
            }
        }

        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void getLoggedInUser() {
        if (mAuth.getCurrentUser() != null) {
            userRepository.getUserByEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<User>() {
                @Override
                public void onComplete(@NonNull Task<User> task) {
                    if (task.isSuccessful()) {
                        loggedInUser = task.getResult();
                    } else {
                        Log.e("TAG", "Failed to get logged in vendor: " + task.getException());
                    }
                }
            });
        }
    }

    @Override
    public void onAcceptedClick(UserReport report) {
        report.setStatus("ACCEPTED");
        userRepository.deactivateUserByEmail(report.getVendorEmail());
        adapter.notifyDataSetChanged();
        /*int position = reports.indexOf(report);
        if (position != -1) {
            reports.remove(position);
            adapter.notifyItemRemoved(position);
        }*/
        reportRepository.update(report);

        userRepository.getUserByEmail(report.getVendorEmail())
                .addOnCompleteListener(new OnCompleteListener<User>() {
                    @Override
                    public void onComplete(@NonNull Task<User> task) {
                        User user = task.getResult();
                        if (user.getRole() == User.UserRole.ORGANISER || user.getRole() == User.UserRole.VENDOR) {
                            reservationRepository.cancelReservationsByOrganizerEmail(user.getEmail());
                            reservationRepository.getReservationByOragnizer(user.getEmail())
                                    .addOnCompleteListener(new OnCompleteListener<List<Reservation>>() {
                                        @Override
                                        public void onComplete(@NonNull Task<List<Reservation>> task1) {
                                            List<Reservation> reservations = task1.getResult();
                                            for (Reservation reservation : reservations) {
                                                Notification notification = new Notification(UUID.randomUUID().toString(), "Event Cancelled", "Reason: Organiser got banned", reservation.getVendorStaffEmail(), Notification.NotificationStatus.CREATED, Timestamp.now() );
                                                notificationRepository.createNotification(notification);
                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    @Override
    public void onDeniedClick(UserReport report) {
        report.setStatus("DENIED");
        Notification notification = new Notification(UUID.randomUUID().toString(), "Report Denied", "Reason: " + etReason.getText(), report.getUserEmail(), Notification.NotificationStatus.CREATED, Timestamp.now() );
        notificationRepository.createNotification(notification);
        etReason.setText("");
        adapter.notifyDataSetChanged();
        /*int position = reports.indexOf(report);
        if (position != -1) {
            reports.remove(position);
            adapter.notifyItemRemoved(position);
        }*/
        reportRepository.update(report);
    }
}