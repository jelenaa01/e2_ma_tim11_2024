package com.example.eventplanner.fragments.serviceProduct;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.CategoryRepositoryInterface;
import com.example.eventplanner.repository.interfaces.ServiceRepositoryInterface;
import com.example.eventplanner.repository.interfaces.SubcategoryRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.Serializable;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ServiceDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ServiceDetailsFragment extends Fragment {

    private static final String ARG_SELECTED_SERVICE = "selected-service";
    Button btnEditPrice;
    private Service selectedService;
    private ServiceRepositoryInterface serviceRepository;
    private User loggedInUser;
    private UserRepositoryInterface userRepository;
    public ServiceDetailsFragment() {
        // Required empty public constructor
    }

    public static ServiceDetailsFragment newInstance(Service selectedService, ServiceRepositoryInterface serviceRepository, UserRepositoryInterface userRepository) {
        ServiceDetailsFragment fragment = new ServiceDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_SELECTED_SERVICE, selectedService);
        args.putSerializable("serviceRepository", (Serializable) serviceRepository);
        args.putSerializable("userRepository", (Serializable) userRepository);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            selectedService = (Service) getArguments().getSerializable(ARG_SELECTED_SERVICE);
            serviceRepository = (ServiceRepositoryInterface) getArguments().getSerializable("serviceRepository");
            userRepository = (UserRepositoryInterface) getArguments().getSerializable("userRepository");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_service_details, container, false);

        TextView tvCategory = view.findViewById(R.id.tvCategory);
        TextView tvSubcategory = view.findViewById(R.id.tvSubcategory);
        TextView tvName = view.findViewById(R.id.tvName);
        TextView tvDescription = view.findViewById(R.id.tvDescription);
        TextView tvPricePerHour = view.findViewById(R.id.tvPricePerHour);
        TextView tvTotalPrice = view.findViewById(R.id.tvTotalPrice);
        TextView tvDuration = view.findViewById(R.id.tvDuration);
        TextView tvLocation = view.findViewById(R.id.tvLocation);
        TextView tvPersonnel = view.findViewById(R.id.tvPersonnel);
        TextView tvEventType = view.findViewById(R.id.tvEventType);
        TextView tvBookingDeadline = view.findViewById(R.id.tvBookingDeadline);
        TextView tvCancellationDeadline = view.findViewById(R.id.tvCancellationDeadline);
        TextView tvAutoApproval = view.findViewById(R.id.tvAutoApproval);

        if (selectedService != null) {
            tvCategory.setText(selectedService.getCategory());
            tvSubcategory.setText(selectedService.getSubcategory());
            tvName.setText(selectedService.getName());
            tvDescription.setText(selectedService.getDescription());
            tvPricePerHour.setText(String.valueOf(selectedService.getPricePerHour()));
            tvTotalPrice.setText(String.valueOf(selectedService.getTotalPrice()));
            tvDuration.setText(String.valueOf(selectedService.getDurationInHours()));
            tvLocation.setText(selectedService.getLocation());
            tvPersonnel.setText(selectedService.getPersonnel().toString());
            tvEventType.setText(selectedService.getEventType().toString());
            tvBookingDeadline.setText(String.valueOf(selectedService.getBookingDeadline()));
            tvCancellationDeadline.setText(String.valueOf(selectedService.getCancellationDeadline()));
            tvAutoApproval.setText(String.valueOf(selectedService.getConfirmationMode()));
        }


        if (selectedService != null) {
            // Populate TextViews with service details...

            // Edit Price button click listener
            btnEditPrice = view.findViewById(R.id.btnEditPrice);

            btnEditPrice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Open a dialog or start an activity for editing price and discount
                    openEditPriceDialog();
                }
            });
        }

        return view;
    }

    private void openEditPriceDialog() {
        // Create and show the custom dialog
        EditPriceDialog editPriceDialog = new EditPriceDialog(getActivity(), selectedService, serviceRepository);
        editPriceDialog.show();
    }

    private void getLoggedInUser() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        if ( currentUser  != null) {
            userRepository.getUserByEmail( currentUser .getEmail()).addOnCompleteListener(new OnCompleteListener<User>() {
                @Override
                public void onComplete(@NonNull Task<User> task) {
                    if (task.isSuccessful()) {
                        loggedInUser = task.getResult();

                        if(!(loggedInUser.getRole() == User.UserRole.VENDOR || loggedInUser.getRole() == User.UserRole.VENDOR_STAFF)) {

                            btnEditPrice.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        Log.e("CompanyOverviewFragment", "Failed to get logged in vendor: " + task.getException());
                    }
                }
            });
        }
    }

}
