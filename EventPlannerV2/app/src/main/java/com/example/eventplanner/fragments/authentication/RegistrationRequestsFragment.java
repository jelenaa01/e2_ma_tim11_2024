package com.example.eventplanner.fragments.authentication;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.dto.RegistrationRequestDTO;
import com.example.eventplanner.model.RegistrationRequest;
import com.example.eventplanner.repository.interfaces.CompanyRepositoryInterface;
import com.example.eventplanner.repository.interfaces.RegistrationRequestRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class RegistrationRequestsFragment extends Fragment {

    private List<RegistrationRequest> registrationRequests = new ArrayList<>();
    private RegistrationRequestsRecyclerViewAdapter adapter;
    private DrawerLayout drawerLayout;
    private UserRepositoryInterface userRepository;
    private RegistrationRequestRepositoryInterface registrationRequestRepository;
    private CompanyRepositoryInterface companyRepository;
    private EditText startDateEditText, endDateEditText;
    private Calendar startDateCalendar = Calendar.getInstance();
    private Calendar endDateCalendar = Calendar.getInstance();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);
        registrationRequestRepository = eventPlannerApp.getDIContainer().resolve(RegistrationRequestRepositoryInterface.class);
        companyRepository = eventPlannerApp.getDIContainer().resolve(CompanyRepositoryInterface.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.registration_request_list, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.requestRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String currentUserEmail = currentUser.getEmail();
        registrationRequestRepository.getAllPendingRequests()
                .addOnSuccessListener(registrationRequests -> {
                    List<RegistrationRequestDTO> requestDTOs = new ArrayList<>();
                    for (RegistrationRequest request : registrationRequests) {
                        companyRepository.getCompanyByOwnerEmail(request.getEmail())
                                .addOnSuccessListener(company -> {
                                    userRepository.getUserByEmail(request.getEmail())
                                            .addOnSuccessListener(user -> {
                                                RegistrationRequestDTO dto = new RegistrationRequestDTO(
                                                        request.getId(),
                                                        request.getEmail(),
                                                        user.getFirstName(),
                                                        user.getLastName(),
                                                        company.getName(),
                                                        company.getAddress(),
                                                        request.getTimestamp()
                                                );
                                                requestDTOs.add(dto);
                                                if (requestDTOs.size() == registrationRequests.size()) {
                                                    adapter = new RegistrationRequestsRecyclerViewAdapter(requestDTOs, registrationRequestRepository, userRepository, companyRepository, getContext());
                                                    recyclerView.setAdapter(adapter);
                                                }
                                            });
                                });
                    }
                })
                .addOnFailureListener(e -> {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                });

        drawerLayout = view.findViewById(R.id.registration_requests_drawer_layout);

        startDateEditText = view.findViewById(R.id.startDate);
        endDateEditText = view.findViewById(R.id.endDate);

        startDateEditText.setOnClickListener(v -> {
            new DatePickerDialog(getContext(), (view1, year, monthOfYear, dayOfMonth) -> {
                startDateCalendar.set(Calendar.YEAR, year);
                startDateCalendar.set(Calendar.MONTH, monthOfYear);
                startDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(startDateEditText, startDateCalendar);
            }, startDateCalendar.get(Calendar.YEAR), startDateCalendar.get(Calendar.MONTH), startDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });

        endDateEditText.setOnClickListener(v -> {
            new DatePickerDialog(getContext(), (view12, year, monthOfYear, dayOfMonth) -> {
                endDateCalendar.set(Calendar.YEAR, year);
                endDateCalendar.set(Calendar.MONTH, monthOfYear);
                endDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(endDateEditText, endDateCalendar);
            }, endDateCalendar.get(Calendar.YEAR), endDateCalendar.get(Calendar.MONTH), endDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });

        Button applyFilterButton = view.findViewById(R.id.applyFilterButton);
        applyFilterButton.setOnClickListener(v -> {
            adapter.getDateFilter().filter(startDateEditText.getText().toString() + "," + endDateEditText.getText().toString());
            drawerLayout.closeDrawer(GravityCompat.END);
        });

        ImageButton filterButton = view.findViewById(R.id.requestFilterButton);
        filterButton.setOnClickListener(v -> {
            if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
                drawerLayout.closeDrawer(GravityCompat.END);
            } else {
                drawerLayout.openDrawer(GravityCompat.END);
            }
        });


        SearchView searchView = view.findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Filter the list based on the query
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // Filter the list based on the new text
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return view;
    }

    private void updateLabel(EditText editText, Calendar calendar) {
        String format = "MM/dd/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
        editText.setText(sdf.format(calendar.getTime()));
    }
}
