package com.example.eventplanner.model;

public class UserReport {
    private String id;
    private String vendorEmail;
    private String userEmail;
    private String description;
    private String date;
    private String status;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserReport() { }

    public UserReport(String id, String vendorEmail, String userEmail, String description, String date, String status) {
        this.id = id;
        this.vendorEmail = vendorEmail;
        this.userEmail = userEmail;
        this.description = description;
        this.date = date;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVendorEmail() {
        return vendorEmail;
    }

    public void setVendorEmail(String vendorEmail) {
        this.vendorEmail = vendorEmail;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "UserReport{" +
                "id='" + id + '\'' +
                ", vendorEmail='" + vendorEmail + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
