package com.example.eventplanner.fragments.authentication;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.example.eventplanner.service.EmailService;
import com.example.eventplanner.utils.FragmentUtils;

import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

// ... (imports and existing code)

public class OrganizerRegistrationFragment extends Fragment {
    private int currentStep = 1;
    private LinearLayout layoutStep1, layoutStep2, layoutStep3;
    private EditText etFirstName, etLastName, etEmail, etAddress, etPhoneNumber, etPassword, etConfirmPassword;
    private Button btnChooseLocation;
    private ImageButton btnPrevious, btnNext;
    private ImageView ivUploadedPhoto;
    private FrameLayout layoutUploadPhoto;
    private ActivityResultLauncher<String> requestPermissionLauncher;
    private ActivityResultLauncher<Intent> pickImageLauncher;
    private UserRepositoryInterface userRepository;

    public OrganizerRegistrationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize the permission launcher
        requestPermissionLauncher =
                registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                    if (isGranted) {
                        // Permission is granted. Open the gallery.
                        openGallery();
                    } else {
                        // Permission denied.
                        Toast.makeText(getContext(), "Permission denied. Can't access gallery.", Toast.LENGTH_LONG).show();
                    }
                });

        // Initialize the pick image launcher
        pickImageLauncher =
                registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                        result -> {
                            if (result.getResultCode() == getActivity().RESULT_OK && result.getData() != null) {
                                ivUploadedPhoto.setImageURI(result.getData().getData());
                                ivUploadedPhoto.setVisibility(View.VISIBLE);
                            }
                        });

        // Initialize UserRepository
        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_organizer_registration, container, false);

        initViews(view);
        btnPrevious.setOnClickListener(v -> navigateToPreviousStep());
        btnNext.setOnClickListener(v -> {
            if (validateStep()) {
                if (currentStep == 3) {
                    saveUser();
                } else {
                    navigateToNextStep();
                }
            }
        });

        // Setting click listener to the FrameLayout for uploading photo
        layoutUploadPhoto.setOnClickListener(v -> requestGalleryPermission());

        return view;
    }

    private void initViews(View view) {
        layoutStep1 = view.findViewById(R.id.layout_step1);
        layoutStep2 = view.findViewById(R.id.layout_step2);
        etFirstName = view.findViewById(R.id.et_first_name);
        etLastName = view.findViewById(R.id.et_last_name);
        etAddress = view.findViewById(R.id.et_address);
        etEmail = view.findViewById(R.id.et_email);
        etPhoneNumber = view.findViewById(R.id.et_phone_number);
        etPassword = view.findViewById(R.id.et_password);
        etConfirmPassword = view.findViewById(R.id.et_confirm_password);
        btnPrevious = view.findViewById(R.id.btn_previous);
        btnNext = view.findViewById(R.id.btn_next);
        ivUploadedPhoto = view.findViewById(R.id.iv_uploaded_photo);
        // Initialize the FrameLayout for photo upload
        layoutUploadPhoto = view.findViewById(R.id.layout_upload_photo);
    }

    private void navigateToPreviousStep() {
        if (currentStep > 1) {
            currentStep--;
            updateStepVisibility();
        }
    }

    private void navigateToNextStep() {
        if (currentStep < 2) {
            currentStep++;
            updateStepVisibility();
        }
    }

    private void updateStepVisibility() {
        layoutStep1.setVisibility(currentStep == 1 ? View.VISIBLE : View.GONE);
        layoutStep2.setVisibility(currentStep == 2 ? View.VISIBLE : View.GONE);
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickImageLauncher.launch(intent);
    }

    private void requestGalleryPermission() {
        requestPermissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    private boolean validateStep() {
        switch (currentStep) {
            case 1:
                return validateStep1();
            case 2:
                return validateStep2();
            default:
                return false;
        }
    }

    private boolean validateStep1() {
        if (etFirstName.getText().toString().isEmpty() ||
                etLastName.getText().toString().isEmpty() ||
                etEmail.getText().toString().isEmpty() ||
                etPhoneNumber.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Please fill in all the fields.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateStep2() {
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();

        if (password.isEmpty() || confirmPassword.isEmpty()) {
            Toast.makeText(getContext(), "Please enter both password and confirm password.", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (password.length() < 4) {
            Toast.makeText(getContext(), "Password must be at least 4 characters.", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!password.equals(confirmPassword)) {
            Toast.makeText(getContext(), "Passwords do not match.", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


    private void saveUser() {
        // Validate user input
        if (!validateStep()) {
            return;
        }

        String firstName = etFirstName.getText().toString().trim();
        String lastName = etLastName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String address = etAddress.getText().toString().trim();
        String phoneNumber = etPhoneNumber.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        String profilePhotoPath = savePhotoToInternalStorage(ivUploadedPhoto);  // Save photo to internal storage
        User user = new User(email, password, firstName, lastName, address, phoneNumber, profilePhotoPath, User.UserRole.ORGANISER, false);

        userRepository.addUser(user);

        EmailService.sendRegistrationEmail(email, firstName);

        Toast.makeText(getContext(), "Activation link sent to " + email, Toast.LENGTH_SHORT).show();

        FragmentUtils.replaceFragment(
                getParentFragmentManager(),
                new LoginFragment(),
                R.id.unauth_fragment_container,
                getActivity().findViewById(R.id.unauth_nav_view), // Make sure nav_view is initialized before calling
                R.id.unauth_nav_item4,
                getString(R.string.login)
        );
    }
    private String savePhotoToInternalStorage(ImageView imageView) {
        BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
        Bitmap bitmap = drawable.getBitmap();

        ContextWrapper wrapper = new ContextWrapper(getActivity().getApplicationContext()); // Use getActivity().getApplicationContext() here
        File file = wrapper.getDir("EventPlannerImages", Context.MODE_PRIVATE);
        file = new File(file, "profile_photo.jpg");

        try {
            OutputStream stream = null;
            stream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.flush();
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return file.getAbsolutePath();
    }


}
