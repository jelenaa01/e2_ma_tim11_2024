package com.example.eventplanner.repository;

import android.util.Log;

import com.example.eventplanner.model.RegistrationRequest;
import com.example.eventplanner.repository.interfaces.RegistrationRequestRepositoryInterface;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class RegistrationRequestRepository implements RegistrationRequestRepositoryInterface {

    private FirebaseFirestore db;
    private static final String TAG = "RegistrationRequestRepo";

    public RegistrationRequestRepository() {

        this.db = FirebaseFirestore.getInstance();
    }

    @Override
    public Task<Void> createRegistrationRequest(RegistrationRequest request) {
        return db.collection("registrationRequests")
                .add(request)
                .continueWith(task -> {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return null;
                });
    }

    @Override
    public Task<List<RegistrationRequest>> getAllPendingRequests() {
        return db.collection("registrationRequests")
                .whereEqualTo("status", RegistrationRequest.RequestStatus.PENDING)
                .get()
                .continueWith(task -> {
                    List<RegistrationRequest> requests = new ArrayList<>();
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null) {
                            for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                RegistrationRequest request = documentSnapshot.toObject(RegistrationRequest.class);
                                requests.add(request);
                            }
                        }
                    } else {
                        throw task.getException();
                    }
                    return requests;
                });
    }

    @Override
    public Task<Void> setRequestStatusAccepted(String requestId) {
// Assuming requestId is the value you want to search for in the "requestId" field
        Query query = db.collection("registrationRequests").whereEqualTo("id", requestId);

        return query.get().continueWithTask(task -> {
            if (task.isSuccessful()) {
                QuerySnapshot querySnapshot = task.getResult();
                if (!querySnapshot.isEmpty()) {
                    // Assuming you only expect one document to match the query
                    DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
                    DocumentReference requestRef = documentSnapshot.getReference();

                    // Now you can proceed with updating the status of the document
                    Log.d(TAG, "Document exists, proceeding with status update to ACCEPTED");
                    return requestRef.update("status", RegistrationRequest.RequestStatus.ACCEPTED)
                            .addOnSuccessListener(aVoid -> Log.d(TAG, "Request status successfully updated to ACCEPTED"))
                            .addOnFailureListener(e -> Log.e(TAG, "Error updating request status", e));
                } else {
                    Log.e(TAG, "Document not found for requestId: " + requestId);
                    throw new FirebaseFirestoreException("Document not found", FirebaseFirestoreException.Code.NOT_FOUND);
                }
            } else {
                Log.e(TAG, "Error fetching document for requestId: " + requestId, task.getException());
                throw task.getException();
            }
        });

    }

    @Override
    public Task<Void> setRequestStatusRejected(String requestId) {
        // Assuming requestId is the value you want to search for in the "requestId" field
        Query query = db.collection("registrationRequests").whereEqualTo("id", requestId);

        return query.get().continueWithTask(task -> {
            if (task.isSuccessful()) {
                QuerySnapshot querySnapshot = task.getResult();
                if (!querySnapshot.isEmpty()) {
                    // Assuming you only expect one document to match the query
                    DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
                    DocumentReference requestRef = documentSnapshot.getReference();

                    // Now you can proceed with updating the status of the document
                    Log.d(TAG, "Document exists, proceeding with status update to REJECTED");
                    return requestRef.update("status", RegistrationRequest.RequestStatus.REJECTED)
                            .addOnSuccessListener(aVoid -> Log.d(TAG, "Request status successfully updated to REJECTED"))
                            .addOnFailureListener(e -> Log.e(TAG, "Error updating request status", e));
                } else {
                    Log.e(TAG, "Document not found for requestId: " + requestId);
                    throw new FirebaseFirestoreException("Document not found", FirebaseFirestoreException.Code.NOT_FOUND);
                }
            } else {
                Log.e(TAG, "Error fetching document for requestId: " + requestId, task.getException());
                throw task.getException();
            }
        });
    }

    public Task<RegistrationRequest> getRequestById(String requestId) {
        DocumentReference requestRef = db.collection("registrationRequests").document(requestId);
        Log.d(TAG, "Fetching request with ID: " + requestId);
        return requestRef.get().continueWith(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot documentSnapshot = task.getResult();
                if (documentSnapshot != null && documentSnapshot.exists()) {
                    Log.d(TAG, "Request found: " + documentSnapshot.getData());
                    return documentSnapshot.toObject(RegistrationRequest.class);
                } else {
                    Log.d(TAG, "No request found with ID: " + requestId);
                    return null;
                }
            } else {
                Log.e(TAG, "Error fetching request with ID: " + requestId, task.getException());
                throw task.getException();
            }
        });
    }

}
