package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.Product;
import com.google.android.gms.tasks.Task;

import java.util.List;

public interface ProductRepositoryInterface {
    void addProduct(Product product);
    Task<List<Product>> getAll();
    Task<List<Product>> getAllVisible();
    Task<Product> getByName(String itemName);

    Task<Void> updateProduct(Product product);
}
