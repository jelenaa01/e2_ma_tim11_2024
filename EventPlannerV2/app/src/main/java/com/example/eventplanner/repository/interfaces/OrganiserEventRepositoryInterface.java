package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.OrganiserEvent;
import com.google.android.gms.tasks.Task;

import java.util.List;

public interface OrganiserEventRepositoryInterface {


    List<OrganiserEvent> getAll();

    List<OrganiserEvent> getByEventId(int eventId);

    void add(OrganiserEvent organiserEvent);
    Task<List<OrganiserEvent>> getByOrganiserEmail(String organiserEmail); // Update this method

}
