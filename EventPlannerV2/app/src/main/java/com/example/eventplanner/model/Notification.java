package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.Timestamp;

public class Notification implements Parcelable {

    public enum NotificationStatus {
        CREATED,
        SHOWN,
        READ
    }

    private String id;
    private String title;
    private String text;
    private String email;
    private NotificationStatus status;
    private Timestamp timeStamp;

    public Notification() {
        // Default constructor required for calls to DataSnapshot.getValue(Notification.class)
    }

    public Notification(String id, String title, String text, String email, NotificationStatus status, Timestamp timeStamp) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.email = email;
        this.status = status;
        this.timeStamp = timeStamp;
    }

    protected Notification(Parcel in) {
        id = in.readString();
        title = in.readString();
        text = in.readString();
        email = in.readString();
        status = NotificationStatus.valueOf(in.readString());
        timeStamp = in.readParcelable(Timestamp.class.getClassLoader());
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public NotificationStatus getStatus() {
        return status;
    }

    public void setStatus(NotificationStatus status) {
        this.status = status;
    }

    public Timestamp getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Timestamp timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(text);
        dest.writeString(email);
        dest.writeString(status.name());
        dest.writeParcelable(timeStamp, flags);
    }
}
