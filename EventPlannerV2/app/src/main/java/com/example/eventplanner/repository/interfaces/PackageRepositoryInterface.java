package com.example.eventplanner.repository.interfaces;

import com.example.eventplanner.model.Package;
import com.google.android.gms.tasks.Task;

import java.util.List;

public interface PackageRepositoryInterface {
    void addPackage(Package newPackage);
    Task<List<Package>> getAllPackages();
    Task<List<Package>> getAllVisiblePackages();
    Task<Package> getByName(String itemName);
}
