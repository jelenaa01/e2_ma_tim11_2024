package com.example.eventplanner.fragments.reports;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.model.CompanyRating;
import com.example.eventplanner.model.CompanyRatingReport;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.NotificationRepositoryInterface;
import com.example.eventplanner.repository.interfaces.RatingReportRepositoryInterface;
import com.example.eventplanner.repository.interfaces.StaffEventRepositoryInterface;
import com.example.eventplanner.repository.interfaces.StaffWorkingTimeRepositoryInterface;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Date;
import java.util.UUID;

public class ReportCompanyRatingFragment extends Fragment {
    private static final String ARG_SELECTED_RATING = "selected-rating";
    private CompanyRating selectedRating;
    private EditText reasonEditText;
    private Button  reportButton;
    private RatingReportRepositoryInterface ratingReportRepository;
    private NotificationRepositoryInterface notificationRepository;
    private FirebaseAuth mAuth;

    public ReportCompanyRatingFragment() {}

    public static ReportCompanyRatingFragment newInstance(CompanyRating selectedRating) {
        ReportCompanyRatingFragment fragment = new ReportCompanyRatingFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_SELECTED_RATING, selectedRating);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            selectedRating = (CompanyRating) getArguments().getSerializable(ARG_SELECTED_RATING);
        }

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        ratingReportRepository = eventPlannerApp.getDIContainer().resolve(RatingReportRepositoryInterface.class);
        notificationRepository = eventPlannerApp.getDIContainer().resolve(NotificationRepositoryInterface.class);

        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report_company_rating, container, false);

        reasonEditText = view.findViewById(R.id.report_reason);
        reportButton = view.findViewById(R.id.reportBtn);
        reportButton.setOnClickListener(v -> addReport());

        return view;
    }

    private void addReport() {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        String reason = reasonEditText.getText().toString().trim();

        if(!reason.isEmpty() && currentUser != null) {
            CompanyRatingReport ratingReport = new CompanyRatingReport(UUID.randomUUID().toString(), reason, new Date(), currentUser.getEmail(), CompanyRatingReport.RatingReportStatus.REPORTED, selectedRating.getId());
            ratingReportRepository.addReport(ratingReport);
            Toast.makeText(getContext(), "Report successfully added", Toast.LENGTH_SHORT).show();

            Notification notification = new Notification(UUID.randomUUID().toString(), "Report company rating", "Vendor " + ratingReport.getVendorEmail() + " report rating for his company", "admin@gmail.com", Notification.NotificationStatus.CREATED, Timestamp.now());
            notificationRepository.createNotification(notification);
        }
        getParentFragmentManager().popBackStack();
    }

}