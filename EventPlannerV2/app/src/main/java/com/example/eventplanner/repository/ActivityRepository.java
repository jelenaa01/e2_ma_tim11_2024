package com.example.eventplanner.repository;

import android.util.Log;

import com.example.eventplanner.model.Activity;
import com.example.eventplanner.repository.interfaces.ActivityRepositoryInterface;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class ActivityRepository implements ActivityRepositoryInterface {
    private static final String TAG = "ActivityRepository";
    private FirebaseFirestore db;

    public ActivityRepository(FirebaseFirestore firestore) {
        this.db = firestore;
    }

    @Override
    public List<Activity> getAll() {
        return null;
    }

    @Override
    public List<Activity> getByEventId(int eventId) {
        return null;
    }

    @Override
    public void add(Activity activity) {
        db.collection("activities")
                .add(activity)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Activity successfully added.");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error adding Activity.", e);
                });
    }
}
