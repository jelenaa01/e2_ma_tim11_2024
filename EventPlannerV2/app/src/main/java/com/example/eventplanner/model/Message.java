package com.example.eventplanner.model;

import java.time.LocalDateTime;
import java.util.Objects;

public class Message {
    private String sender;
    private String recipient;
    private String timestamp;
    private String content;
    private Boolean isRead;

    public Message(String sender, String recipient, String timestamp, String content, Boolean isRead) {
        this.sender = sender;
        this.recipient = recipient;
        this.timestamp = timestamp;
        this.content = content;
        this.isRead = isRead;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getRead() {
        return isRead;
    }

    @Override
    public String toString() {
        return "Message{" +
                "sender='" + sender + '\'' +
                ", recipient='" + recipient + '\'' +
                ", timestamp=" + timestamp +
                ", content='" + content + '\'' +
                ", isRead=" + isRead +
                '}';
    }

    public void setRead(Boolean read) {
        isRead = read;
    }

    public Message() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return Objects.equals(sender, message.sender) && Objects.equals(recipient, message.recipient) && Objects.equals(timestamp, message.timestamp) && Objects.equals(content, message.content) && Objects.equals(isRead, message.isRead);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sender, recipient, timestamp, content, isRead);
    }
}
