package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.UUID;

import com.google.firebase.Timestamp;

public class RegistrationRequest implements Parcelable {

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(email);
        dest.writeString(status.name());
        dest.writeParcelable(timestamp, flags);
    }

    public enum RequestStatus {
    PENDING,
    ACCEPTED,
    REJECTED,
    }

    private String id;
    private String email;

    private RequestStatus status;

    private Timestamp timestamp;

    public RegistrationRequest(){}

    public RegistrationRequest(String id, String email, RequestStatus status, Timestamp timestamp) {
        this.id = UUID.randomUUID().toString();
        this.email = email;
        this.status = status;
        this.timestamp = timestamp;
    }
    protected RegistrationRequest(Parcel in) {
        id = in.readString();
        email = in.readString();
        status = RegistrationRequest.RequestStatus.valueOf(in.readString());
        timestamp = in.readParcelable(Timestamp.class.getClassLoader());
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
