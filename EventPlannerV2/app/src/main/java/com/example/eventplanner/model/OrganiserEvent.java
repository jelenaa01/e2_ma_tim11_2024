package com.example.eventplanner.model;

import java.sql.Timestamp;

public class OrganiserEvent {

    private String id;

    private String name;

    private String description;

    private int maxGuests;

    private boolean isPrivate;

    private String address;
    private int radius;
    private Timestamp date;
    private String agendaId;
    private String budgetId;
    private String organiserEmail;

    public OrganiserEvent(){}
    public OrganiserEvent(String id, String name, String description, int maxGuests, boolean isPrivate, String address, int radius, String budgetId, String organiserEmail) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.maxGuests = maxGuests;
        this.isPrivate = isPrivate;
        this.address = address;
        this.radius = radius;
        //this.agendaId = agendaId;
        this.budgetId = budgetId;
        this.organiserEmail = organiserEmail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMaxGuests() {
        return maxGuests;
    }

    public void setMaxGuests(int maxGuests) {
        this.maxGuests = maxGuests;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getAgendaId() {
        return agendaId;
    }

    public void setAgendaId(String agendaId) {
        this.agendaId = agendaId;
    }

    public String getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(String budgetId) {
        this.budgetId = budgetId;
    }

    public String getOrganiserEmail() {
        return organiserEmail;
    }

    public void setOrganiserEmail(String organiserEmail) {
        this.organiserEmail = organiserEmail;
    }
}
