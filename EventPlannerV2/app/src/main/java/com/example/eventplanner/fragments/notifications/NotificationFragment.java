package com.example.eventplanner.fragments.notifications;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.NotificationRepository;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class NotificationFragment extends Fragment implements SensorEventListener, NotificationRecyclerViewAdapter.OnItemClickListener {

    private RecyclerView recyclerView;
    private DrawerLayout drawerLayout;
    private NotificationRepository notificationRepository;
    private FirebaseFirestore db;
    private List<Notification> notifications = new ArrayList<>();
    private NotificationRecyclerViewAdapter notificationAdapter;
    private CheckBox checkboxRead;
    private CheckBox checkboxUnread;
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private long lastShakeTime = 0;
    private int shakeCount = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notificationRepository = new NotificationRepository(FirebaseFirestore.getInstance());

        // Sensor setup
        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager != null) {
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification_list, container, false);

        checkboxRead = view.findViewById(R.id.checkbox_read);
        checkboxUnread = view.findViewById(R.id.checkbox_unread);

        Button applyButton = view.findViewById(R.id.notificationApplyButton);
        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean showRead = checkboxRead.isChecked();
                boolean showUnread = checkboxUnread.isChecked();

                Log.d("NotificationFragment", "Read: " + showRead + ", Unread: " + showUnread);

                if (showRead && showUnread) {
                    notificationAdapter.showAllNotifications();
                } else if (showRead) {
                    notificationAdapter.showOnlyReadNotifications();
                } else if (showUnread) {
                    notificationAdapter.showOnlyUnreadNotifications();
                } else {
                    notificationAdapter.showAllNotifications();
                }
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.notificationRecyclerView);
        drawerLayout = view.findViewById(R.id.notificationDrawer_layout);
        ImageButton filterButton = view.findViewById(R.id.notificationFilterButton);

        notificationAdapter = new NotificationRecyclerViewAdapter(notifications, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(notificationAdapter);

        filterButton.setOnClickListener(v -> drawerLayout.openDrawer(GravityCompat.END));

        fetchNotifications();
    }

    private void fetchNotifications() {
        Log.d("NotificationFragment", "Retrieving all notifications for logged user");
        notificationRepository.getAllLoggedUserNotifications().addOnCompleteListener(new OnCompleteListener<List<Notification>>() {
            @Override
            public void onComplete(@NonNull Task<List<Notification>> task) {
                if (task.isSuccessful()) {
                    notifications.clear();
                    notifications.addAll(task.getResult());
                    Log.d("NotificationFragment", "Fetched " + notifications.size() + " notifications");
                    notificationAdapter.updateNotifications(notifications);  // Ensure adapter is updated
                } else {
                    Log.e("NotificationFragment", "Error fetching notifications", task.getException());
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (accelerometer != null) {
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            float gX = x / SensorManager.GRAVITY_EARTH;
            float gY = y / SensorManager.GRAVITY_EARTH;
            float gZ = z / SensorManager.GRAVITY_EARTH;

            // gForce will be close to 1 when there is no movement.
            float gForce = (float) Math.sqrt(gX * gX + gY * gY + gZ * gZ);

            if (gForce > 2.5) {
                final long now = System.currentTimeMillis();
                if (lastShakeTime + 1000 > now) {
                    return;
                }

                lastShakeTime = now;
                shakeCount++;

                switch (shakeCount % 3) {
                    case 1:
                        checkboxRead.setChecked(true);
                        checkboxUnread.setChecked(false);
                        notificationAdapter.showOnlyReadNotifications();
                        break;
                    case 2:
                        checkboxRead.setChecked(false);
                        checkboxUnread.setChecked(true);
                        notificationAdapter.showOnlyUnreadNotifications();
                        break;
                    case 0:
                        checkboxRead.setChecked(true);
                        checkboxUnread.setChecked(true);
                        notificationAdapter.showAllNotifications();
                        break;
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Not used
    }


    @Override
    public void onItemClick(Notification notification) {
        // Open NotificationDetailFragment with the clicked notification

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            DocumentReference userRef = FirebaseFirestore.getInstance().collection("users").document(user.getUid());
            userRef.get().addOnSuccessListener(documentSnapshot -> {
                if (documentSnapshot.exists()) {
                    User userData = documentSnapshot.toObject(User.class);
                    handleNextPage(userData, notification);
                } else {
                }
            }).addOnFailureListener(e -> {
            });
        }
    }

    private void handleNextPage(User userData, Notification notification) {

        FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if(userData.getRole() == User.UserRole.ADMIN)
        {
            fragmentTransaction.replace(R.id.admin_fragment_container, NotificationDetailFragment.newInstance(notification));

        }
        else if(userData.getRole() == User.UserRole.ORGANISER)
        {
            fragmentTransaction.replace(R.id.organizer_fragment_container, NotificationDetailFragment.newInstance(notification));
        }
        else if(userData.getRole() == User.UserRole.VENDOR)
        {
            fragmentTransaction.replace(R.id.vendor_fragment_container, NotificationDetailFragment.newInstance(notification));
        }
        else if(userData.getRole() == User.UserRole.VENDOR_STAFF)
        {
            fragmentTransaction.replace(R.id.vendor_staff_fragment_container, NotificationDetailFragment.newInstance(notification));
        }
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

}
