package com.example.eventplanner.fragments.organiser;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.packagesOverview.PackageOverviewFragment;
import com.example.eventplanner.fragments.productsOverview.ProductOverviewFragment;
import com.example.eventplanner.fragments.productsOverview.ProductsRecyclerViewAdapter;
import com.example.eventplanner.fragments.servicesOverview.ServiceOverviewFragment;
import com.example.eventplanner.model.FavoriteItem;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.FavoriteItemRepositoryInterface;
import com.example.eventplanner.repository.interfaces.PackageRepositoryInterface;
import com.example.eventplanner.repository.interfaces.ProductRepositoryInterface;
import com.example.eventplanner.repository.interfaces.ServiceRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class FavoriteItemsFragment extends Fragment {
    private static final String TAG = "ProductsOverviewFragment";
    private List<FavoriteItem> favoriteItems;
    private FavoriteItemsRecyclerViewAdapter adapter;
    private FavoriteItemRepositoryInterface favoriteItemRepository;
    private User loggedInUser;
    private UserRepositoryInterface userRepository;
    private ProductRepositoryInterface productRepository;
    private ServiceRepositoryInterface serviceRepository;
    private PackageRepositoryInterface packageRepository;
    private FirebaseAuth mAuth;

    public FavoriteItemsFragment() {
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        favoriteItemRepository = eventPlannerApp.getDIContainer().resolve(FavoriteItemRepositoryInterface.class);
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);
        productRepository = eventPlannerApp.getDIContainer().resolve(ProductRepositoryInterface.class);
        serviceRepository = eventPlannerApp.getDIContainer().resolve(ServiceRepositoryInterface.class);
        packageRepository = eventPlannerApp.getDIContainer().resolve(PackageRepositoryInterface.class);

        getLoggedInUser();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite_items, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.favoriteItemRecyclerView1);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        favoriteItemRepository.getByEmail(mAuth.getCurrentUser().getEmail())
                .addOnSuccessListener(ps -> {
                    this.favoriteItems = ps;
                    // Sorting the products after fetching them
                    Collections.sort(favoriteItems, new Comparator<FavoriteItem>() {
                        @Override
                        public int compare(FavoriteItem o1, FavoriteItem o2) {
                            String name1 = o1.getItemName();
                            String name2 = o2.getItemName();
                            if (name1 == null && name2 == null) {
                                return 0;
                            } else if (name1 == null) {
                                return -1;
                            } else if (name2 == null) {
                                return 1;
                            } else {
                                return name1.compareToIgnoreCase(name2);
                            }
                        }
                    });

                    adapter = new FavoriteItemsRecyclerViewAdapter(favoriteItems, favoriteItemRepository);
                    recyclerView.setAdapter(adapter);

                    if (adapter != null) {
                        adapter.setOnItemClickListener(product -> {
                            navigateToPage(product);
                        });
                    }

                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Failed to fetch products", e);
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                });

        return view;
    }

    private void navigateToPage(FavoriteItem favoriteItem){
        if (favoriteItem == null) {
            Log.e(TAG, "Product is null, cannot navigate");
            return;
        }

        if (Objects.equals(favoriteItem.getType(), "product")) {
            productRepository.getByName(favoriteItem.getItemName()).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Product product = task.getResult();
                    ProductOverviewFragment fragment = ProductOverviewFragment.newInstance(product);

                    FragmentTransaction transaction = getParentFragmentManager().beginTransaction();

                    if (loggedInUser != null) {
                        switch (loggedInUser.getRole()) {
                            case VENDOR:
                                transaction.replace(R.id.vendor_fragment_container, fragment);
                                break;
                            case ORGANISER:
                                transaction.replace(R.id.organizer_fragment_container, fragment);
                                break;
                            case VENDOR_STAFF:
                                transaction.replace(R.id.vendor_staff_fragment_container, fragment);
                                break;
                            case ADMIN:
                                transaction.replace(R.id.admin_fragment_container, fragment);
                                break;
                            default:
                                Log.e(TAG, "Unknown user role.");
                                return;
                        }

                        transaction.addToBackStack(null);
                        transaction.commit();
                    } else {
                        Log.e(TAG, "Logged in user is null.");
                    }
                } else {
                    Exception exception = task.getException();
                    if (exception != null) {
                    }
                }
            });
        } else if (Objects.equals(favoriteItem.getType(), "service")) {
            serviceRepository.getByName(favoriteItem.getItemName()).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Service service = task.getResult();
                    ServiceOverviewFragment fragment = ServiceOverviewFragment.newInstance(service);

                    FragmentTransaction transaction = getParentFragmentManager().beginTransaction();

                    if (loggedInUser != null) {
                        switch (loggedInUser.getRole()) {
                            case VENDOR:
                                transaction.replace(R.id.vendor_fragment_container, fragment);
                                break;
                            case ORGANISER:
                                transaction.replace(R.id.organizer_fragment_container, fragment);
                                break;
                            case VENDOR_STAFF:
                                transaction.replace(R.id.vendor_staff_fragment_container, fragment);
                                break;
                            case ADMIN:
                                transaction.replace(R.id.admin_fragment_container, fragment);
                                break;
                            default:
                                Log.e(TAG, "Unknown user role.");
                                return;
                        }

                        transaction.addToBackStack(null);
                        transaction.commit();
                    } else {
                        Log.e(TAG, "Logged in user is null.");
                    }
                } else {
                    Exception exception = task.getException();
                    if (exception != null) {
                    }
                }
            });
        } else if (Objects.equals(favoriteItem.getType(), "package")) {
            packageRepository.getByName(favoriteItem.getItemName()).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Package myPackage = task.getResult();
                    PackageOverviewFragment fragment = PackageOverviewFragment.newInstance(myPackage);

                    FragmentTransaction transaction = getParentFragmentManager().beginTransaction();

                    if (loggedInUser != null) {
                        switch (loggedInUser.getRole()) {
                            case VENDOR:
                                transaction.replace(R.id.vendor_fragment_container, fragment);
                                break;
                            case ORGANISER:
                                transaction.replace(R.id.organizer_fragment_container, fragment);
                                break;
                            case VENDOR_STAFF:
                                transaction.replace(R.id.vendor_staff_fragment_container, fragment);
                                break;
                            case ADMIN:
                                transaction.replace(R.id.admin_fragment_container, fragment);
                                break;
                            default:
                                Log.e(TAG, "Unknown user role.");
                                return;
                        }

                        transaction.addToBackStack(null);
                        transaction.commit();
                    } else {
                        Log.e(TAG, "Logged in user is null.");
                    }
                } else {
                    Exception exception = task.getException();
                    if (exception != null) {
                    }
                }
            });
        }
    }

    private void getLoggedInUser() {
        if (mAuth.getCurrentUser() != null) {Log.e("CompanyOverviewFragment", "glup");
            userRepository.getUserByEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<User>() {
                @Override
                public void onComplete(@NonNull Task<User> task) {Log.e("CompanyOverviewFragment", "glup1");
                    if (task.isSuccessful()) {Log.e("CompanyOverviewFragment", task.getResult().getEmail());
                        loggedInUser = task.getResult();
                    } else {
                        Log.e("CompanyOverviewFragment", "Failed to get logged in vendor: " + task.getException());
                    }
                }
            });
        }
    }
}
