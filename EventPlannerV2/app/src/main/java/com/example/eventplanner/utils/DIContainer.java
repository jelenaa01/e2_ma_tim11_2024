package com.example.eventplanner.utils;

import java.util.HashMap;
import java.util.Map;

public class DIContainer {
    private final Map<Class<?>, Object> dependencies = new HashMap<>();

    public <T> void register(Class<T> clazz, T instance) {
        dependencies.put(clazz, instance);
    }

    public <T> T resolve(Class<T> clazz) {
        return clazz.cast(dependencies.get(clazz));
    }
}
