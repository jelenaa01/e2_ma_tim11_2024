package com.example.eventplanner.fragments.servicesOverview;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.model.FavoriteItem;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.example.eventplanner.repository.interfaces.FavoriteItemRepositoryInterface;
import com.example.eventplanner.repository.interfaces.ServiceRepositoryInterface;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ServiceOverviewFragment extends Fragment {

    private static final String ARG_SELECTED_SERVICE = "selected-service";

    private Service selectedService;
    private ServiceRepositoryInterface serviceRepository;
    private User loggedInUser;
    private UserRepositoryInterface userRepository;
    private FavoriteItemRepositoryInterface favoriteItemRepository;
    private FirebaseAuth mAuth;

    public ServiceOverviewFragment() { mAuth = FirebaseAuth.getInstance(); }

    public static ServiceOverviewFragment newInstance(Service selectedService) {
        ServiceOverviewFragment fragment = new ServiceOverviewFragment();

        Bundle args = new Bundle();
        args.putSerializable(ARG_SELECTED_SERVICE, selectedService);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            selectedService = (Service) getArguments().getSerializable(ARG_SELECTED_SERVICE);
        }

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        serviceRepository = eventPlannerApp.getDIContainer().resolve(ServiceRepositoryInterface.class);
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);
        favoriteItemRepository = eventPlannerApp.getDIContainer().resolve(FavoriteItemRepositoryInterface.class);

        getLoggedInUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_organiser_service_details, container, false);

        if (selectedService == null){
            Log.e("VendorProductDetailsFragment", "Error getting Service: ");
        }

        TextView tvName = view.findViewById(R.id.serviceNameTextView2);
        TextView tvDescription = view.findViewById(R.id.serviceDescriptionTextView2);
        TextView tvPrice = view.findViewById(R.id.servicePriceTextView2);
        TextView tvAvailability = view.findViewById(R.id.serviceAvailabilityTextView2);

        tvName.setText(selectedService.getName());
        tvDescription.setText(selectedService.getDescription());
        tvPrice.setText(String.valueOf(selectedService.getTotalPrice()));
        if (!selectedService.isAvailable()) {
            tvAvailability.setText("This service currently isn't available.");
        }

        Button addServiceToFavoritesButton = view.findViewById(R.id.addServiceToFavoritesButton);
        addServiceToFavoritesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addServiceToFavorites();
                addServiceToFavoritesButton.setVisibility(View.GONE);
            }
        });

        favoriteItemRepository.existsByNameAndEmail(selectedService.getName(), mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<Boolean>() {
            @Override
            public void onComplete(@NonNull Task<Boolean> task) {
                if (task.isSuccessful()) {
                    boolean isItemInFavorites = task.getResult();
                    if (isItemInFavorites) {
                        addServiceToFavoritesButton.setVisibility(View.GONE);
                    } else {
                        addServiceToFavoritesButton.setVisibility(View.VISIBLE);
                    }
                } else {
                    Log.e("TAG", "Error getting favorite items", task.getException());
                }
            }
        });

        return view;
    }

    private void getLoggedInUser() {
        if (mAuth.getCurrentUser() != null) {
            userRepository.getUserByEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<User>() {
                @Override
                public void onComplete(@NonNull Task<User> task) {
                    if (task.isSuccessful()) {
                        loggedInUser = task.getResult();
                    } else {
                        Log.e("ServiceOverviewFragment", "Failed to get logged in user: " + task.getException());
                    }
                }
            });
        }
    }

    private void addServiceToFavorites() {
        if (mAuth.getCurrentUser() != null) {
            FavoriteItem favoriteItem = new FavoriteItem();
            favoriteItem.setUserEmail(loggedInUser.getEmail());
            favoriteItem.setType("service");
            favoriteItem.setItemName(selectedService.getName());
            favoriteItemRepository.addFavoriteItem(favoriteItem);
        }
    }
}
