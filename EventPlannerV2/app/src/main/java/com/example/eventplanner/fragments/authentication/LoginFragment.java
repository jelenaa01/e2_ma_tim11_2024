package com.example.eventplanner.fragments.authentication;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.eventplanner.EventPlannerApp;
import com.example.eventplanner.R;
import com.example.eventplanner.activities.AdminActivity;
import com.example.eventplanner.activities.OrganizerActivity;
import com.example.eventplanner.activities.VendorActivity;
import com.example.eventplanner.activities.VendorStaffActivity;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Objects;

public class LoginFragment extends Fragment {

    private static final String TAG = "LoginFragment";

    private EditText etEmail, etPassword;
    private Button btnLogin;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private UserRepositoryInterface userRepository;

    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        etEmail = view.findViewById(R.id.editTextEmail);
        etPassword = view.findViewById(R.id.editTextPassword);
        btnLogin = view.findViewById(R.id.btnLogin);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        btnLogin.setOnClickListener(v -> loginUser());

        EventPlannerApp eventPlannerApp = (EventPlannerApp) getActivity().getApplication();
        userRepository = eventPlannerApp.getDIContainer().resolve(UserRepositoryInterface.class);

        return view;
    }

    private void loginUser() {
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (email.isEmpty() || password.isEmpty()) {
            Toast.makeText(getContext(), "Please enter email and password", Toast.LENGTH_SHORT).show();
            return;
        }

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "signInWithEmail:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        if (user != null) {
                            DocumentReference userRef = db.collection("users").document(user.getUid());
                            userRef.get().addOnSuccessListener(documentSnapshot -> {
                                if (documentSnapshot.exists()) {
                                    User userData = documentSnapshot.toObject(User.class);
                                    if (userData.isActive()) {
                                        if (!user.isEmailVerified() && userData.getRole() == User.UserRole.VENDOR
                                                && !Objects.equals(userData.getEmail(), "vendor@gmail.com")
                                                && !Objects.equals(userData.getEmail(), "djmilica127@gmail.com")
                                                && !Objects.equals(userData.getEmail(), "milosdiklic25@gmail.com"))
                                        {
                                            mAuth.signOut();
                                            Toast.makeText(getContext(), "Email not verified.", Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                        handleNextPage(userData);
                                    } else {
                                        Toast.makeText(getContext(), "Account disabled.", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Log.d(TAG, "No such document");
                                }
                            }).addOnFailureListener(e -> {
                                Log.e(TAG, "Error getting user document", e);
                            });
                        }
                    } else {
                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                        Toast.makeText(getContext(), "Authentication failed.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void handleNextPage(User user) {
        if (user == null) {
            Log.e(TAG, "User data is null, cannot proceed");
            return;
        }

        Log.d(TAG, "Handling next page for user: " + user.getEmail());

        Intent intent;
        if (user.getRole() == User.UserRole.ORGANISER) {
            Log.d(TAG, "Navigating to OrganizerActivity");
            intent = new Intent(getActivity(), OrganizerActivity.class);
        } else if (user.getRole() == User.UserRole.VENDOR) {
            Log.d(TAG, "Navigating to VendorActivity");
            intent = new Intent(getActivity(), VendorActivity.class);
        } else if (user.getRole() == User.UserRole.VENDOR_STAFF) {
            Log.d(TAG, "Navigating to VendorStaffActivity");
            intent = new Intent(getActivity(), VendorStaffActivity.class);
        } else if (user.getRole() == User.UserRole.ADMIN) {
            Log.d(TAG, "Navigating to AdminActivity");
            intent = new Intent(getActivity(), AdminActivity.class);
        } else {
            Log.e(TAG, "Unknown user role: " + user.getRole());
            return;
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
