package com.example.eventplanner.model;

import java.io.Serializable;

public class FavoriteItem implements Serializable {
    private String userEmail;
    private String type;
    private String itemName;

    public FavoriteItem() {}

    public FavoriteItem(String userEmail, String type, String itemName) {
        this.userEmail = userEmail;
        this.type = type;
        this.itemName = itemName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
