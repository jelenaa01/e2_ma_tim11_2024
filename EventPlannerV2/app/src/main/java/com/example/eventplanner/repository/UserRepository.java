package com.example.eventplanner.repository;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.eventplanner.database.DBHandler;
import com.example.eventplanner.model.CompanyReport;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.UserReport;
import com.example.eventplanner.model.VendorStaffWorkingTime;
import com.example.eventplanner.repository.interfaces.UserRepositoryInterface;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements UserRepositoryInterface {

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;

    private static final String TAG = "UserRepository";

    public UserRepository(FirebaseFirestore firestore) {
        this.db = firestore;
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public Task<User> getUserByEmailAndPassword(String email, String password) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("users")
                .whereEqualTo("email", email)
                .whereEqualTo("password", password)
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
                            return documentSnapshot.toObject(User.class);
                        }
                    }
                    return null;
                });
    }

    public Task<Void> addUser(User user) {
        Task<Void> userCreationTask = mAuth.createUserWithEmailAndPassword(user.getEmail(), user.getPassword())
                .continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    FirebaseUser firebaseUser = mAuth.getCurrentUser();
                    if (firebaseUser == null) {
                        throw new IllegalStateException("Firebase user is null after creation");
                    }

                    String uid = firebaseUser.getUid();
                    DocumentReference userRef = db.collection("users").document(uid);
                    return userRef.set(user);
                });
        return userCreationTask;
    }

    @Override
    public Task<User> getUserByEmail(String email) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("users")
                .whereEqualTo("email", email)
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
                            return documentSnapshot.toObject(User.class);
                        }
                    }
                    return null;
                });
    }

    @Override
    public void deactivateStaffAccount(String email) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("users")
                .whereEqualTo("email", email)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        DocumentSnapshot documentSnapshot = queryDocumentSnapshots.getDocuments().get(0);
                        String userId = documentSnapshot.getId();
                        DocumentReference userRef = db.collection("users").document(userId);

                        userRef.update("active", false)
                                .addOnSuccessListener(aVoid -> {
                                    Log.d(TAG, "Account successfully deactivated");
                                })
                                .addOnFailureListener(e -> {
                                    Log.e(TAG, "Error during account deactivation", e);
                                });
                    } else {
                        Log.d(TAG, "No user found with email: " + email);
                    }
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error finding user", e);
                });
    }

    @Override
    public void activateStaffAccount(String email) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("users")
                .whereEqualTo("email", email)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        DocumentSnapshot documentSnapshot = queryDocumentSnapshots.getDocuments().get(0);
                        String userId = documentSnapshot.getId();
                        DocumentReference userRef = db.collection("users").document(userId);

                        userRef.update("active", true)
                                .addOnSuccessListener(aVoid -> {
                                    Log.d(TAG, "Account successfully activated");
                                })
                                .addOnFailureListener(e -> {
                                    Log.e(TAG, "Error during account activation", e);
                                });
                    } else {
                        Log.d(TAG, "No user found with email: " + email);
                    }
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error finding user", e);
                });
    }

    @Override
    public Task<List<User>> getAllUsers() {
        return db.collection("users")
                .get()
                .continueWith(task -> {
                    List<User> users = new ArrayList<>();
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                User user = documentSnapshot.toObject(User.class);
                                users.add(user);
                            }
                        }
                    }
                    return users;
                });
    }

    @Override
    public Task<List<User>> getPendingVendorUsers() {
        return db.collection("users")
                .whereEqualTo("role", "VENDOR")
                .whereEqualTo("active", false)
                .get()
                .continueWith(task -> {
                    List<User> users = new ArrayList<>();
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                User user = documentSnapshot.toObject(User.class);
                                users.add(user);
                            }
                        }
                    }
                    return users;
                });
    }

    @Override
    public Task<Void> updateUser(User loggedInUser) {
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        return db.collection("users").document(userId)
                .set(loggedInUser)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "User data updated successfully");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "Error updating user data", e);
                    }
                });
    }

    @Override
    public Task<Void> deactivateUserByEmail(String userEmail) {
        // Query for the user document with the specified email
        return db.collection("users")
                .whereEqualTo("email", userEmail)
                .get()
                .continueWithTask(task -> {
                    // Iterate over the query results (should be only one user)
                    for (DocumentSnapshot document : task.getResult()) {
                        // Get the document ID
                        String userId = document.getId();

                        // Update the status field to false
                        return db.collection("users")
                                .document(userId)
                                .update("active", false);
                    }
                    return null; // No user found with the specified email
                })
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "User status updated successfully");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error updating user status", e);
                });
    }

    @Override
    public void addUserReport(UserReport userReport) {

        if (userReport.getVendorEmail() == null || userReport.getVendorEmail().isEmpty() ||
                userReport.getUserEmail() == null || userReport.getUserEmail().isEmpty() ||
                userReport.getDescription() == null || userReport.getDescription().isEmpty()) {
            throw new IllegalArgumentException("All fields of UserReport must be filled");
        }

        db.collection("userReport")
                .add(userReport)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "User successfully reported.");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error reporting user.", e);
                });
    }

    @Override
    public void addCompanyReport(CompanyReport companyReport) {

        if (companyReport.getCompanyName() == null || companyReport.getCompanyName().isEmpty() ||
                companyReport.getOrganizerEmail() == null || companyReport.getOrganizerEmail().isEmpty() ||
                companyReport.getDescription() == null || companyReport.getDescription().isEmpty()) {
            throw new IllegalArgumentException("All fields of UserReport must be filled");
        }

        db.collection("companyReport")
                .add(companyReport)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Company successfully reported.");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error reporting company.", e);
                });
    }

    @Override
    public Task<String> getPasswordByEmail(String email) {
        return db.collection("users")
                .whereEqualTo("email", email)
                .limit(1)
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult().getDocuments().get(0);
                        String password = document.getString("password");
                        return password;
                    } else {
                        return null;
                    }
                });
    }


}

