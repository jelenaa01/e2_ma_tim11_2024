package com.example.eventplanner.model;

public class Activity {
    private int eventId;
    private String name;
    private String description;
    private TimeInterval interval;
    private String location;

    public Activity(int eventId, String name, String description, TimeInterval interval, String location) {
        this.eventId = eventId;
        this.name = name;
        this.description = description;
        this.interval = interval;
        this.location = location;
    }

    public Activity() {
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TimeInterval getInterval() {
        return interval;
    }

    public void setInterval(TimeInterval interval) {
        this.interval = interval;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "eventId=" + eventId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", interval=" + interval +
                ", location='" + location + '\'' +
                '}';
    }
}
