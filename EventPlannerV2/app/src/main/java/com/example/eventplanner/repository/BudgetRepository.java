package com.example.eventplanner.repository;

import android.util.Log;

import com.example.eventplanner.model.Budget;
import com.example.eventplanner.repository.interfaces.BudgetRepositoryInterface;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

public class BudgetRepository implements BudgetRepositoryInterface {

    private static final String TAG = "BudgetRepository";
    private FirebaseFirestore db;
    public BudgetRepository(FirebaseFirestore firestore) {
        this.db = firestore;
    }

    @Override
    public void add(Budget budget) {
        db.collection("budgets")
                .add(budget)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Budget successfully added.");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Budget adding Event.", e);
                });
    }
    @Override
    public Task<Budget> getBudgetById(String budgetId) {
        DocumentReference requestRef = db.collection("budgets").document(budgetId);
        Log.d(TAG, "Fetching budget with ID: " + budgetId);
        return requestRef.get().continueWith(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot documentSnapshot = task.getResult();
                if (documentSnapshot != null && documentSnapshot.exists()) {
                    Log.d(TAG, "budget found: " + documentSnapshot.getData());
                    return documentSnapshot.toObject(Budget.class);
                } else {
                    Log.d(TAG, "No budget found with ID: " + budgetId);
                    return null;
                }
            } else {
                Log.e(TAG, "Error fetching budget with ID: " + budgetId, task.getException());
                throw task.getException();
            }
        });
    }
    @Override
    public Task<Void> addProductToBudget(String budgetId, String productID, double addedPrice) {
        TaskCompletionSource<Void> taskCompletionSource = new TaskCompletionSource<>();

        Query query = db.collection("budgets").whereEqualTo("id", budgetId);

        query.get().continueWithTask(task -> {
            if (task.isSuccessful()) {
                QuerySnapshot querySnapshot = task.getResult();
                if (querySnapshot != null && !querySnapshot.isEmpty()) {
                    DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
                    Budget budget = documentSnapshot.toObject(Budget.class);
                    if (budget != null) {
                        // Update both the product list and the total price in Firestore
                        return documentSnapshot.getReference().update(
                                "productIds", FieldValue.arrayUnion(productID),
                                "totalPrice", FieldValue.increment(addedPrice)
                        ).addOnSuccessListener(aVoid -> {
                            Log.d(TAG, "Product and price successfully added to budget.");
                            taskCompletionSource.setResult(null);
                        }).addOnFailureListener(e -> {
                            Log.e(TAG, "Error adding product and price to budget.", e);
                            taskCompletionSource.setException(e);
                        });
                    } else {
                        throw new Exception("Budget conversion failed.");
                    }
                } else {
                    throw new Exception("No budget found with ID: " + budgetId);
                }
            } else {
                throw task.getException();
            }
        }).addOnFailureListener(e -> {
            Log.e(TAG, "Error fetching budget.", e);
            taskCompletionSource.setException(e);
        });

        return taskCompletionSource.getTask();
    }

}
