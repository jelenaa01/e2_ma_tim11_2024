package com.example.eventplanner.repository;

import android.util.Log;

import com.example.eventplanner.model.Product;
import com.example.eventplanner.repository.interfaces.ProductRepositoryInterface;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProductRepository implements ProductRepositoryInterface {
    private static final String TAG = "ProductRepository";
    private FirebaseFirestore db;

    public ProductRepository(FirebaseFirestore firestore) {
        this.db = firestore;
    }

    @Override
    public void addProduct(Product product) {
        if (product == null) {
            Log.e(TAG, "Product is null.");
            return;
        }
        db.collection("products")
                .add(product)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Product successfully added.");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error adding product.", e);
                });
    }

    @Override
    public Task<List<Product>> getAll() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("products")
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        List<Product> productList = new ArrayList<>();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                Product product = documentSnapshot.toObject(Product.class);
                                if (product != null) {
                                    productList.add(product);
                                }
                            }
                        }
                        return productList;
                    } else {
                        // Handle failure
                        Exception e = task.getException();
                        if (e != null) {
                            Log.e("Firestore", "Error getting documents: " + e.getMessage());
                        }
                        return Collections.emptyList(); // Return an empty list in case of failure
                    }
                });
    }

    @Override
    public Task<List<Product>> getAllVisible() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("products")
                .whereEqualTo("visible", true)
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        List<Product> productList = new ArrayList<>();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                Product product = documentSnapshot.toObject(Product.class);
                                if (product != null) {
                                    productList.add(product);
                                }
                            }
                        }
                        return productList;
                    } else {
                        // Handle failure
                        Exception e = task.getException();
                        if (e != null) {
                            Log.e("Firestore", "Error getting documents: " + e.getMessage());
                        }
                        return Collections.emptyList(); // Return an empty list in case of failure
                    }
                });
    }

    @Override
    public Task<Product> getByName(String itemName) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("products")
                .whereEqualTo("name", itemName)
                .get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
                            return documentSnapshot.toObject(Product.class);
                        }
                    }
                    return null;
                });
    }

    @Override
    public Task<Void> updateProduct(Product product) {
        if (product == null || product.getId() == null) {
            Log.e(TAG, "Product or product ID is null.");
            return Tasks.forException(new IllegalArgumentException("Product or product ID is null."));
        }

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("products")
                .whereEqualTo("id", product.getId())
                .get()
                .continueWithTask(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
                            String documentId = documentSnapshot.getId();

                            // Update the document with the new product data
                            return db.collection("products")
                                    .document(documentId)
                                    .set(product);
                        } else {
                            return Tasks.forException(new IllegalStateException("Product with ID " + product.getId() + " not found."));
                        }
                    } else {
                        // Handle failure
                        Exception e = task.getException();
                        if (e != null) {
                            Log.e(TAG, "Error updating product: " + e.getMessage());
                        }
                        return Tasks.forException(e);
                    }
                });
    }

}
