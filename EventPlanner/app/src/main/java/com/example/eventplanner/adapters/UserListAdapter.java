package com.example.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.eventplanner.R;
import com.example.eventplanner.model.User;

import java.util.ArrayList;

public class UserListAdapter extends ArrayAdapter<User> {
    private ArrayList<User> aUsers;
    private Context context;

    public UserListAdapter(Context context, ArrayList<User> users){
        super(context, R.layout.fragment_employee, users);
        aUsers = users;
        this.context = context;
    }

    @Override
    public int getCount() {
        return aUsers.size();
    }

    @Nullable
    @Override
    public User getItem(int position) {
        return aUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        User user = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.employee_list, parent, false);
        }

        TextView firstNameTextView = convertView.findViewById(R.id.firstNameTextView);
        TextView lastNameTextView = convertView.findViewById(R.id.lastNameTextView);
        TextView addressTextView = convertView.findViewById(R.id.addressTextView);
        TextView phoneNumberTextView = convertView.findViewById(R.id.phoneNumberTextView);
        TextView emailTextView = convertView.findViewById(R.id.emailTextView);

        if (user != null) {
            firstNameTextView.setText(user.getFirstName());
            lastNameTextView.setText(user.getLastName());
            addressTextView.setText(user.getAddress());
            phoneNumberTextView.setText(user.getPhoneNumber());
            emailTextView.setText(user.getEmail());
        }

        return convertView;
    }
}
