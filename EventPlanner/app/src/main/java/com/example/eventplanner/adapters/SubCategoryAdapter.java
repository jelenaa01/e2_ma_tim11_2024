package com.example.eventplanner.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.EditCategoryActivity;
import com.example.eventplanner.activities.EditSubCategoryActivity;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.SubCategory;

import java.util.List;

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.ViewHolder> {
    private List<SubCategory> subCategories;
    private Context context;

    public SubCategoryAdapter(List<SubCategory> subCategories, Context context) {
        this.subCategories = subCategories;
        this.context = context;
    }

    @NonNull
    @Override
    public SubCategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_sub_category_card, parent, false);
        return new SubCategoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubCategoryAdapter.ViewHolder holder, int position) {
        prepareTextViews(holder, position);

        holder.editSubCategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubCategory selectedSubCategory = (SubCategory) subCategories.get(holder.getAdapterPosition());
                Intent intent = new Intent(context, EditSubCategoryActivity.class);
                intent.putExtra("subCategory", (SubCategory)selectedSubCategory);
                context.startActivity(intent);
            }
        });
    }

    private void prepareTextViews(SubCategoryAdapter.ViewHolder holder, int position){
        SubCategory subCategory = subCategories.get(position);
        holder.subCategoryNameTextView.setText("Title: " + subCategory.getName());
        holder.subCategoryDescriptionTextView.setText("Description: " + subCategory.getDescription());
        holder.subCategoryCategoryIdTextView.setText("CategoryId: " + subCategory.getCategory().getId());
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView subCategoryNameTextView;
        TextView subCategoryDescriptionTextView;
        TextView subCategoryCategoryIdTextView;
        Button editSubCategoryButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            subCategoryNameTextView = itemView.findViewById(R.id.subCategoryNameTextView);
            subCategoryDescriptionTextView = itemView.findViewById(R.id.subCategoryDescriptionTextView);
            subCategoryCategoryIdTextView = itemView.findViewById(R.id.subCategoryCategoryIdTextView);
            editSubCategoryButton = itemView.findViewById(R.id.editSubCategoryButton);
        }

        public void bind(SubCategory subCategory) {
            subCategoryNameTextView.setText(subCategory.getName());
            subCategoryDescriptionTextView.setText(subCategory.getDescription());
            subCategoryCategoryIdTextView.setText(subCategory.getCategory().getId());
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return subCategories.size();
    }
}
