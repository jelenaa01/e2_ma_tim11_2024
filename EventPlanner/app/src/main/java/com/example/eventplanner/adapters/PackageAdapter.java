package com.example.eventplanner.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.EditPackageActivity;
import com.example.eventplanner.model.Package;

import java.util.List;

public class PackageAdapter extends RecyclerView.Adapter<PackageAdapter.ViewHolder>{
    private List<Package> packages;
    private Context context;

    public PackageAdapter(List<Package> packages, Context context) {
        this.packages = packages;
        this.context = context;
    }

    @NonNull
    @Override
    public PackageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_package_card, parent, false);
        return new PackageAdapter.ViewHolder(view);
    }

    public void onBindViewHolder(@NonNull PackageAdapter.ViewHolder holder, int position) {
        prepareTextViews(holder, position);

        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Package selectedPackage = packages.get(holder.getAdapterPosition());
                Intent intent = new Intent(context, EditPackageActivity.class);
                Log.i("packageAdapter", selectedPackage.toString());
                intent.putExtra("package", selectedPackage);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return packages.size();
    }

    private void prepareTextViews(PackageAdapter.ViewHolder holder, int position) {
        Package packageItem = packages.get(position);
        holder.packageTitleTextView.setText("Title: " + packageItem.getTitle());
        holder.descriptionTextView.setText("Description: " + packageItem.getDescription());
        holder.discountTextView.setText("Discount: " + packageItem.getDiscount());

        String temp_available = "available";
        if (!packageItem.isAvailable()) temp_available = "unavailable";
        holder.availableTextView.setText("Availability: " + temp_available);

        String temp_visible = "visible";
        if (!packageItem.isVisible()) temp_visible = "not visible"; // corrected visibility text
        holder.visibleTextView.setText("Visibility: " + temp_visible);

        holder.eventTypesTextView.setText("Event types: " + packageItem.getEventTypes());
        holder.priceTextView.setText("Price: " + packageItem.getPrice());
        holder.reservationDeadlineTextView.setText("Reservation Deadline: " + packageItem.getReservationDeadline());
        holder.cancellationDeadlineTextView.setText("Cancellation Deadline: " + packageItem.getCancellationDeadline());
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView packageTitleTextView;
        TextView descriptionTextView;
        TextView discountTextView;
        TextView availableTextView;
        TextView visibleTextView;
        TextView eventTypesTextView;
        TextView priceTextView;
        TextView reservationDeadlineTextView;
        TextView cancellationDeadlineTextView;
        Button editButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            packageTitleTextView = itemView.findViewById(R.id.packageTitleTextView);
            descriptionTextView = itemView.findViewById(R.id.packageDescriptionTextView);
            discountTextView = itemView.findViewById(R.id.packageDiscountTextView);
            availableTextView = itemView.findViewById(R.id.packageAvailableTextView);
            visibleTextView = itemView.findViewById(R.id.packageVisibleTextView);
            eventTypesTextView = itemView.findViewById(R.id.packageEventTypesTextView);
            priceTextView = itemView.findViewById(R.id.packagePriceTextView);
            reservationDeadlineTextView = itemView.findViewById(R.id.packageReservationDeadlineTextView);
            cancellationDeadlineTextView = itemView.findViewById(R.id.packageCancellationDeadlineTextView);
            editButton = itemView.findViewById(R.id.packageEditButton);
        }
    }

}
