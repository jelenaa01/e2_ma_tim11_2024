package com.example.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Event;

import java.util.ArrayList;


public class EventListAdapter extends ArrayAdapter<Event> {
        private ArrayList<Event> aEvents;
        private Context context;

        public EventListAdapter(Context context, ArrayList<Event> events){
            super(context, R.layout.event, events);
            this.aEvents = events;
            this.context = context;
        }

        @Override
        public int getCount() {
            return aEvents.size();
        }

        @Nullable
        @Override
        public Event getItem(int position) {
            return aEvents.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Event event = getItem(position);
            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.event, parent, false);
            }

            TextView eventNameTextView = convertView.findViewById(R.id.eventNameTextView);
            TextView eventDayTextView = convertView.findViewById(R.id.eventDayTextView);
            TextView eventStartTimeTextView = convertView.findViewById(R.id.eventStartTimeTextView);
            TextView eventEndTimeTextView = convertView.findViewById(R.id.eventEndTimeTextView);
            TextView eventTypeTextView = convertView.findViewById(R.id.eventTypeTextView);
            TextView eventEmployeeTextView = convertView.findViewById(R.id.eventEmployeeTextView);

            if (event != null) {
                eventNameTextView.setText(event.getName());
                eventDayTextView.setText(String.valueOf(event.getDate()));
                eventStartTimeTextView.setText(String.valueOf(event.getStartTime()));
                eventEndTimeTextView.setText(String.valueOf(event.getEndTime()));
                eventTypeTextView.setText(String.valueOf(event.getType()));
                eventEmployeeTextView.setText(String.valueOf(event.getEmployee().getFirstName()));
            }

            return convertView;
        }

}
