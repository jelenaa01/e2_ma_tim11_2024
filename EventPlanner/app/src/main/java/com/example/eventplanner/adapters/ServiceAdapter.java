package com.example.eventplanner.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.activities.EditProductActivity;
import com.example.eventplanner.R;
import com.example.eventplanner.activities.EditServiceActivity;
import com.example.eventplanner.model.Service;

import java.util.List;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder>{
    private List<Service> services;
    private Context context;

    public ServiceAdapter(List<Service> services, Context context) {
        this.services = services;
        this.context = context;
    }

    @NonNull
    @Override
    public ServiceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_service_card, parent, false);
        return new ServiceAdapter.ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return services.size();
    }


    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        prepareTextViews(holder, position);

        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Service selectedService = services.get(holder.getAdapterPosition());
                Intent intent = new Intent(context, EditServiceActivity.class);
                intent.putExtra("service", selectedService);
                context.startActivity(intent);
            }
        });
    }

    private void prepareTextViews(ServiceAdapter.ViewHolder holder, int position) {
        Service service = services.get(position);
        holder.titleTextView.setText("Title: " + service.getName());
        holder.descriptionTextView.setText("Description: " + service.getDescription());
        holder.locationTextView.setText("Location: " + service.getLocation());
        holder.pricePerHourTextView.setText("Price Per Hour: " + service.getPricePerHour());
        holder.totalPriceTextView.setText("Total Price: " + service.getTotalPrice());
        holder.discountTextView.setText("Discount: " + service.getDiscount());
        holder.reservationDeadlineTextView.setText("Reservation Deadline: " + service.getReservationDeadline());
        holder.cancellationDeadlineTextView.setText("Cancellation Deadline: " + service.getCancellationDeadline());
        holder.acceptanceTextView.setText("Acceptance: " + service.getAcceptance().toString());

        if (service.getGallery() != null && !service.getGallery().isEmpty()) {
            String imageUrl = service.getGallery().get(0);
            int imageResourceId = context.getResources().getIdentifier(imageUrl, "drawable", context.getPackageName());
            holder.galleryImageView.setImageResource(imageResourceId);
        } else {
            holder.galleryImageView.setImageResource(R.drawable.cat);
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleTextView;
        TextView descriptionTextView;
        TextView locationTextView;
        TextView pricePerHourTextView;
        TextView totalPriceTextView;
        TextView discountTextView;
        TextView reservationDeadlineTextView;
        TextView cancellationDeadlineTextView;
        TextView acceptanceTextView;
        ImageView galleryImageView;
        Button editButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.titleTextView);
            descriptionTextView = itemView.findViewById(R.id.descriptionTextView);
            locationTextView = itemView.findViewById(R.id.locationTextView);
            pricePerHourTextView = itemView.findViewById(R.id.pricePerHourTextView);
            totalPriceTextView = itemView.findViewById(R.id.totalPriceTextView);
            discountTextView = itemView.findViewById(R.id.discountTextView);
            reservationDeadlineTextView = itemView.findViewById(R.id.reservationDeadlineTextView);
            cancellationDeadlineTextView = itemView.findViewById(R.id.cancellationDeadlineTextView);
            acceptanceTextView = itemView.findViewById(R.id.acceptanceTextView);
            galleryImageView = itemView.findViewById(R.id.galleryImageView);
            editButton = itemView.findViewById(R.id.editButton);
        }
    }

}
