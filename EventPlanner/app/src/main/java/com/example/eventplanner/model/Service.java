package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Service extends Asset implements Parcelable {
    private String Specifics;
    private double PricePerHour;
    private double Duration;
    private double TotalPrice;
    private double Discount;
    private String Location;
    private ArrayList<String> People;
    private String ReservationDeadline;
    private String CancellationDeadline;
    private AcceptanceType Acceptance;

    public enum AcceptanceType {
        MANUAL,
        AUTOMATIC
    }

    public Service(int id, int categoryId, int subCategoryId, String name, String description, ArrayList<String> gallery, ArrayList<String> eventTypes, boolean isAvailable, boolean isVisible, String specifics, double pricePerHour, double duration, double totalPrice, double discount, String location, ArrayList<String> people, String reservationDeadline, String cancellationDeadline, AcceptanceType acceptance) {
        super(id, categoryId, subCategoryId, name, description, gallery, eventTypes, isAvailable, isVisible);
        Specifics = specifics;
        PricePerHour = pricePerHour;
        Duration = duration;
        TotalPrice = totalPrice;
        Discount = discount;
        Location = location;
        People = people;
        ReservationDeadline = reservationDeadline;
        CancellationDeadline = cancellationDeadline;
        Acceptance = acceptance;
    }

    public Service(Parcel in) {
        super(in);
        Specifics = in.readString();
        PricePerHour = in.readDouble();
        Duration = in.readDouble();
        TotalPrice = in.readDouble();
        Discount = in.readDouble();
        Location = in.readString();
        People = in.createStringArrayList();
        ReservationDeadline = in.readString();
        CancellationDeadline = in.readString();
        Acceptance = AcceptanceType.valueOf(in.readString());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(Specifics);
        dest.writeDouble(PricePerHour);
        dest.writeDouble(Duration);
        dest.writeDouble(TotalPrice);
        dest.writeDouble(Discount);
        dest.writeString(Location);
        dest.writeStringList(People);
        dest.writeString(ReservationDeadline);
        dest.writeString(CancellationDeadline);
        dest.writeString(Acceptance.name());
    }

    public static final Parcelable.Creator<Service> CREATOR = new Parcelable.Creator<Service>() {
        @Override
        public Service createFromParcel(Parcel in) {
            return new Service(in);
        }

        @Override
        public Service[] newArray(int size) {
            return new Service[size];
        }
    };

    public String getSpecifics() {
        return Specifics;
    }

    public void setSpecifics(String specifics) {
        Specifics = specifics;
    }

    public double getPricePerHour() {
        return PricePerHour;
    }

    public void setPricePerHour(double pricePerHour) {
        PricePerHour = pricePerHour;
    }

    public double getDuration() {
        return Duration;
    }

    public void setDuration(double duration) {
        Duration = duration;
    }

    public double getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        TotalPrice = totalPrice;
    }

    public double getDiscount() {
        return Discount;
    }

    public void setDiscount(double discount) {
        Discount = discount;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public ArrayList<String> getPeople() {
        return People;
    }

    public void setPeople(ArrayList<String> people) {
        People = people;
    }

    public String getReservationDeadline() {
        return ReservationDeadline;
    }

    public void setReservationDeadline(String reservationDeadline) {
        ReservationDeadline = reservationDeadline;
    }

    public String getCancellationDeadline() {
        return CancellationDeadline;
    }

    public void setCancellationDeadline(String cancellationDeadline) {
        CancellationDeadline = cancellationDeadline;
    }

    public AcceptanceType getAcceptance() {
        return Acceptance;
    }

    public void setAcceptance(AcceptanceType acceptance) {
        Acceptance = acceptance;
    }
}
