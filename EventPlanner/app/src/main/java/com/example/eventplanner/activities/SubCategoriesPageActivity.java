package com.example.eventplanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.CategoryAdapter;
import com.example.eventplanner.adapters.SubCategoryAdapter;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.SubCategory;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class SubCategoriesPageActivity extends AppCompatActivity {

    private List<SubCategory> subCategories = new ArrayList<>();
    private SubCategoryAdapter adapter;
    private FloatingActionButton fab;

    public SubCategoriesPageActivity() {}

    public SubCategoriesPageActivity(List<SubCategory> subCategories, SubCategoryAdapter adapter) {
        this.subCategories = subCategories;
        this.adapter = adapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_sub_categories_page);

        prepareSubCategories();

        RecyclerView recyclerView = findViewById(R.id.subCategoriesRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new SubCategoryAdapter(subCategories, this);
        recyclerView.setAdapter(adapter);

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SubCategoriesPageActivity.this, CreateSubCategoryActivity.class);
                startActivity(intent);
            }
        });
    }

    private void prepareSubCategories() {
        Category category1 = new Category(1, "test", "test");
        SubCategory subcategory1 = new SubCategory(1, "Albums", "you get picture albums", category1);
        subCategories.add(subcategory1);
    }
}