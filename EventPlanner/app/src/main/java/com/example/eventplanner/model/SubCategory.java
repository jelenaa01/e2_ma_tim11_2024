package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class SubCategory implements Parcelable {
    private int Id;
    private String Name;
    private String Description;
    private Category Category;

    public SubCategory(int id, String name, String description, com.example.eventplanner.model.Category category) {
        Id = id;
        Name = name;
        Description = description;
        Category = category;
    }

    public static final Creator<SubCategory> CREATOR = new Creator<SubCategory>() {
        @Override
        public SubCategory createFromParcel(Parcel in) {
            return new SubCategory(in);
        }

        @Override
        public SubCategory[] newArray(int size) {
            return new SubCategory[size];
        }
    };

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public com.example.eventplanner.model.Category getCategory() {
        return Category;
    }

    public void setCategory(com.example.eventplanner.model.Category category) {
        Category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubCategory that = (SubCategory) o;
        return Id == that.Id && Objects.equals(Name, that.Name) && Objects.equals(Description, that.Description) && Objects.equals(Category, that.Category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id, Name, Description, Category);
    }

    @Override
    public String toString() {
        return "SubCategory{" +
                "Id=" + Id +
                ", Name='" + Name + '\'' +
                ", Description='" + Description + '\'' +
                ", Category=" + Category +
                '}';
    }

    public SubCategory(Parcel in) {
        Id = in.readInt();
        Name = in.readString();
        Description = in.readString();
        Category = in.readParcelable(Category.class.getClassLoader());
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(Name);
        dest.writeString(Description);
        dest.writeParcelable(Category, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
