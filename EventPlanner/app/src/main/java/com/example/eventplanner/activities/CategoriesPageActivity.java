package com.example.eventplanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.CategoryAdapter;
import com.example.eventplanner.adapters.ProductAdapter;
import com.example.eventplanner.model.Category;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class CategoriesPageActivity extends AppCompatActivity {

    private List<Category> categories = new ArrayList<>();
    private CategoryAdapter adapter;
    private FloatingActionButton fab;

    public CategoriesPageActivity() {}

    public CategoriesPageActivity(List<Category> categories, CategoryAdapter adapter) {
        this.categories = categories;
        this.adapter = adapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_categories_page);

        prepareCategories();

        RecyclerView recyclerView = findViewById(R.id.categoriesRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new CategoryAdapter(categories, this);
        recyclerView.setAdapter(adapter);

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CategoriesPageActivity.this, CreateCategoryActivity.class);
                startActivity(intent);
            }
        });
    }

    private void prepareCategories() {
        Category category1 = new Category(1, "Accommodation", "Hotels, villas, apartments, and other accommodation options for event guests");
        Category category2 = new Category(2, "Photo and video", "Professional event photography and videography");
        categories.add(category1);
        categories.add(category2);
    }
}