package com.example.eventplanner.activities;

import static com.example.eventplanner.R.*;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Asset;
import com.example.eventplanner.model.Product;
import android.widget.ArrayAdapter;



public class EditProductActivity extends AppCompatActivity {

    private EditText editTextName;
    private EditText editTextDescription;
    private EditText editTextPrice;
    private EditText editTextDiscount;
    private EditText editTextEventTypes;
    private CheckBox checkBoxAvailability;
    private CheckBox checkBoxVisibility;
    //private MultiAutoCompleteTextView multiAutoCompleteTextViewEventTypes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_edit_product);

        editTextName = findViewById(id.editTextName);
        editTextDescription = findViewById(id.editTextDescription);
        editTextPrice = findViewById(id.editTextPrice);
        editTextDiscount = findViewById(id.editTextDiscount);
        editTextEventTypes = findViewById(id.editTextEventTypes);
        checkBoxAvailability = findViewById(R.id.checkBoxAvailability);
        checkBoxVisibility = findViewById(R.id.checkBoxVisibility);
        //multiAutoCompleteTextViewEventTypes = findViewById(R.id.multiAutoCompleteTextViewEventTypes);

        Product product = getIntent().getParcelableExtra("product");
        if (product == null){
            return;
        }

        editTextName.setText(product.getName());
        editTextDescription.setText(product.getDescription());
        editTextPrice.setText(String.valueOf(product.getPrice()));
        editTextDiscount.setText(String.valueOf(product.getDiscount()));
        editTextEventTypes.setText("SVADBA, KRSTENJE");
        checkBoxAvailability.setChecked(product.isAvailable());
        checkBoxVisibility.setChecked(product.isVisible());

        /*String[] eventTypes = product.getEventTypes().toArray(new String[0]);
        if (eventTypes != null && eventTypes.length > 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, eventTypes);
            multiAutoCompleteTextViewEventTypes.setAdapter(adapter);
            multiAutoCompleteTextViewEventTypes.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        }*/

        Button saveButton = findViewById(R.id.buttonSave);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(); // Close the current activity and go back to the previous one
            }
        });

        Button cancelButton = findViewById(R.id.buttonCancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}