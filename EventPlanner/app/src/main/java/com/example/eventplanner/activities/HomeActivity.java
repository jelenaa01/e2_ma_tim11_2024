package com.example.eventplanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.ActivityHomeBinding;
import com.google.android.material.navigation.NavigationView;

public class HomeActivity extends AppCompatActivity {
    private ActivityHomeBinding binding;
    private NavigationView navigationView;
    private NavController navController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_home);

        handleProductsButtonClick();
        handleServicesButtonClick();
        handlePackagesButtonClick();
        handleCategoriesButtonClick();
        handleCreateProductButtonClick();
        handleCreateServiceButtonClick();
        handleCreatePackageButtonClick();
        handleSubCategoriesButtonClick();
        handleOwnerHomeButtonClick();

    }

    private void handleProductsButtonClick(){
        Button buttonGoToNewActivity = findViewById(R.id.button_go_to_products_page);
        buttonGoToNewActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ProductsPageActivity.class);
                startActivity(intent);
            }
        });
    }

    private void handleServicesButtonClick(){
        Button buttonGoToNewActivity = findViewById(R.id.button_go_to_services_page);
        buttonGoToNewActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ServicesPageActivity.class);
                startActivity(intent);
            }
        });
    }

    private void handlePackagesButtonClick(){
        Button buttonGoToNewActivity = findViewById(R.id.button_go_to_packages_page);
        buttonGoToNewActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, PackagesPageActivity.class);
                startActivity(intent);
            }
        });
    }

    private void handleCategoriesButtonClick(){
        Button buttonGoToNewActivity = findViewById(R.id.button_go_to_categories_page);
        buttonGoToNewActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, CategoriesPageActivity.class);
                startActivity(intent);
            }
        });
    }

    private void handleCreateProductButtonClick(){
        Button buttonGoToNewActivity = findViewById(R.id.button_go_to_create_product);
        buttonGoToNewActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, CreateProductActivity.class);
                startActivity(intent);
            }
        });
    }

    private void handleCreateServiceButtonClick(){
        Button buttonGoToNewActivity = findViewById(R.id.button_go_to_create_service);
        buttonGoToNewActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, CreateServiceActivity.class);
                startActivity(intent);
            }
        });
    }

    private void handleCreatePackageButtonClick() {
        Button buttonGoToNewActivity = findViewById(R.id.button_go_to_create_package);
        buttonGoToNewActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, CreatePackageActivity.class);
                startActivity(intent);
            }
        });
    }

    private void handleSubCategoriesButtonClick(){
        Button buttonGoToNewActivity = findViewById(R.id.button_go_to_sub_categories_page);
        buttonGoToNewActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, SubCategoriesPageActivity.class);
                startActivity(intent);
            }
        });
    }

    private void handleOwnerHomeButtonClick(){
        Button buttonGoToNewActivity = findViewById(R.id.button_go_to_owner_home);
        buttonGoToNewActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, OwnerHomeActivity.class);
                startActivity(intent);
            }
        });
    }
}