package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentEmployeeRegistrationBinding;


public class EmployeeRegistrationFragment extends Fragment {
    private FragmentEmployeeRegistrationBinding binding;

    public static EmployeeRegistrationFragment newInstance() {
        return new EmployeeRegistrationFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentEmployeeRegistrationBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        binding.pickWorkTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              PickWorkTimeFragment fragment = new PickWorkTimeFragment();
              FragmentTransaction transaction = getFragmentManager().beginTransaction();
              transaction.replace(R.id.registration_container, fragment);
              transaction.addToBackStack(null);
              transaction.commit();
            }
        });
        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}