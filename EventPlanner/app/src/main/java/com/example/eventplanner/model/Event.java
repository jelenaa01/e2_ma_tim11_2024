package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.sql.Date;
import java.sql.Time;

public class Event implements Parcelable {
    private Long id;
    private String name;
    private Date date;
    private Time startTime;
    private Time endTime;
    private Type type;

    private User employee;

    public Event() {
    }

    public Event(Long id, String name, Date date, Time startTime, Time endTime, Event.Type type, User employee) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.type = type;
        this.employee = employee;
    }

    protected Event(Parcel in) {
        id = in.readLong();
        name = in.readString();
        date = Date.valueOf(in.readString());
        startTime = Time.valueOf(in.readString());
        endTime = Time.valueOf(in.readString());
        type =  Event.Type.valueOf(in.readString());
        employee = in.readParcelable(User.class.getClassLoader());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    public Time getEndTime() {
        return endTime;
    }

    public void setEndTime(Time endTime) {
        this.endTime = endTime;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public User getEmployee() {
        return employee;
    }

    public void setEmployee(User employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", date=" + date +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", type=" + type +
                ", employee=" + employee +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(date.toString());
        dest.writeString(startTime.toString());
        dest.writeString(endTime.toString());
        dest.writeString(type.name());
        dest.writeParcelable(employee, flags);
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public enum Type implements Parcelable{
        reserved,
        taken;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(name());
        }

        public static final Creator<Event.Type> CREATOR = new Creator<Event.Type>() {
            @Override
            public Event.Type createFromParcel(Parcel in) {
                return Event.Type.valueOf(in.readString());
            }

            @Override
            public Event.Type[] newArray(int size) {
                return new  Event.Type[size];
            }
        };
    }
}
