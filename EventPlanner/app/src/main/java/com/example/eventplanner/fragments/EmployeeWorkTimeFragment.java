package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentEmployeeWorkTimeBinding;
import com.example.eventplanner.databinding.FragmentLoginBinding;

public class EmployeeWorkTimeFragment extends Fragment {

   private FragmentEmployeeWorkTimeBinding binding;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentEmployeeWorkTimeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        TextView employee1ScheduleTextView = root.findViewById(R.id.employee1ScheduleTextView);
        employee1ScheduleTextView.setText("Nikola: 7-20 Mon, 7-20 Tue, 7-20 Wed, 7-20 Thu, 7-20 Fir, 7-20 Sat, / Sun");

        TextView employee2ScheduleTextView = root.findViewById(R.id.employee2ScheduleTextView);
        employee2ScheduleTextView.setText("Mia: 8-17 Mon, 8-17 Tue, 8-17 Wed, 8-17 Thu, 8-17 Fri, / Sat, / Sun");

        TextView employee3ScheduleTextView = root.findViewById(R.id.employee3ScheduleTextView);
        employee3ScheduleTextView.setText("Marko: 7-16 Mon, 7-16 Tue, 7-16 Wed, 7-16 Thu, 7-16 Fri, 7-16 Sat, / Sun");

        TextView employee4ScheduleTextView = root.findViewById(R.id.employee4ScheduleTextView);
        employee4ScheduleTextView.setText("Ana: 9-19 Mon, 9-19 Tue, 9-19 Wed,9-19 Thu, 9-19 Fri, / Sat, 9-19 Sun");

        TextView employee5ScheduleTextView = root.findViewById(R.id.employee5ScheduleTextView);
        employee5ScheduleTextView.setText("Petar: 10-21 Mon, 10-21 Tue, / Wed, 10-21 Thu, / Fri, 10-21 Sat,10-21 Sun");

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}