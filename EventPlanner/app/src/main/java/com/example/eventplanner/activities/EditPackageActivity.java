package com.example.eventplanner.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Package;

public class EditPackageActivity extends AppCompatActivity {

    private EditText editTextPackageTitle;
    private EditText editTextPackageDescription;
    private EditText editTextPackageEventTypes;
    private EditText editTextPackagePrice;
    private EditText editTextPackageDiscount;
    private EditText editTextPackageAssetList;
    private EditText editTextPackageReservationDeadline;
    private EditText editTextPackageCancellationDeadline;
    private CheckBox checkBoxPackageAvailability;
    private CheckBox checkBoxPackageVisibility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_package);
        Log.i("PRE", "OVDES MO");

        // Initialize UI elements
        editTextPackageTitle = findViewById(R.id.editTextPackageTitle);
        editTextPackageDescription = findViewById(R.id.editTextPackageDescription);
        editTextPackagePrice = findViewById(R.id.editTextPackagePrice);
        editTextPackageDiscount = findViewById(R.id.editTextPackageDiscount);
        editTextPackageEventTypes = findViewById(R.id.editTextPackageEventTypes);
        editTextPackageAssetList = findViewById(R.id.editTextPackageAssetList);
        editTextPackageReservationDeadline = findViewById(R.id.editTextPackageReservationDeadline);
        editTextPackageCancellationDeadline = findViewById(R.id.editTextPackageCancellationDeadline);
        checkBoxPackageAvailability = findViewById(R.id.checkBoxPackageAvailability);
        checkBoxPackageVisibility = findViewById(R.id.checkBoxPackageVisibility);

        // Retrieve package information from intent
        Package packageItem = getIntent().getParcelableExtra("package");
        if (packageItem == null) {
            return;
        }

        // Set the retrieved package information to the corresponding UI elements
        editTextPackageTitle.setText(packageItem.getTitle());
        editTextPackageDescription.setText(packageItem.getDescription());
        editTextPackagePrice.setText(String.valueOf(packageItem.getPrice()));
        editTextPackageDiscount.setText(String.valueOf(packageItem.getDiscount()));
        editTextPackageEventTypes.setText("SVADBA, KRSTENJE"); // Update with actual event types
        editTextPackageAssetList.setText("Asset1, Asset2"); // Update with actual asset list
        editTextPackageReservationDeadline.setText("12 meseci pre termina");
        editTextPackageCancellationDeadline.setText("4 meseca pre termina");
        checkBoxPackageAvailability.setChecked(packageItem.isAvailable());
        checkBoxPackageVisibility.setChecked(packageItem.isVisible());

        // Save button click listener
        Button saveButton = findViewById(R.id.buttonPackageSave);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(); // Close the current activity and go back to the previous one
            }
        });

        // Cancel button click listener
        Button cancelButton = findViewById(R.id.buttonPackageCancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(); // Close the current activity and go back to the previous one
            }
        });
    }

}
