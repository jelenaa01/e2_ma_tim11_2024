package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Package implements Parcelable {
    protected int Id;
    protected String Title;
    protected String Description;
    protected double Discount;
    protected boolean isAvailable;
    protected boolean isVisible;
    protected ArrayList<Integer> AssetIdList;
    protected ArrayList<String> EventTypes;
    protected double Price;
    protected String ReservationDeadline;
    protected String CancellationDeadline;

    @Override
    public String toString() {
        return "Package{" +
                "Id=" + Id +
                ", Title='" + Title + '\'' +
                ", Description='" + Description + '\'' +
                ", Discount=" + Discount +
                ", isAvailable=" + isAvailable +
                ", isVisible=" + isVisible +
                ", AssetIdList=" + AssetIdList +
                ", EventTypes=" + EventTypes +
                ", Price=" + Price +
                ", ReservationDeadline='" + ReservationDeadline + '\'' +
                ", CancellationDeadline='" + CancellationDeadline + '\'' +
                '}';
    }

    public enum PackageAcceptanceType {
        MANUAL,
        AUTOMATIC
    }

    public Package(int id, String title, String description, double discount, boolean isAvailable, boolean isVisible, ArrayList<Integer> assetIdList, ArrayList<String> eventTypes, double price, String reservationDeadline, String cancellationDeadline) {
        Id = id;
        Title = title;
        Description = description;
        Discount = discount;
        this.isAvailable = isAvailable;
        this.isVisible = isVisible;
        AssetIdList = assetIdList;
        EventTypes = eventTypes;
        Price = price;
        ReservationDeadline = reservationDeadline;
        CancellationDeadline = cancellationDeadline;
    }

    protected Package(Parcel in) {
        Id = in.readInt();
        Title = in.readString();
        Description = in.readString();
        Discount = in.readDouble();
        isAvailable = in.readByte() != 0;
        isVisible = in.readByte() != 0;
        int assetListSize = in.readInt();
        AssetIdList = new ArrayList<>();
        for (int i = 0; i < assetListSize; i++) {
            AssetIdList.add(in.readInt());
        }

        // Read EventTypes manually
        int eventTypesSize = in.readInt();
        EventTypes = new ArrayList<>(eventTypesSize);
        for (int i = 0; i < eventTypesSize; i++) {
            EventTypes.add(in.readString());
        }

        Price = in.readDouble();
        ReservationDeadline = in.readString();
        CancellationDeadline = in.readString();
    }

    public static final Creator<Package> CREATOR = new Creator<Package>() {
        @Override
        public Package createFromParcel(Parcel in) {
            return new Package(in);
        }

        @Override
        public Package[] newArray(int size) {
            return new Package[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(Title);
        dest.writeString(Description);
        dest.writeDouble(Discount);
        dest.writeInt(isAvailable ? 1 : 0);
        dest.writeInt(isVisible ? 1 : 0);
        dest.writeList(AssetIdList);

        // Write EventTypes manually
        dest.writeInt(EventTypes.size());
        for (String eventType : EventTypes) {
            dest.writeString(eventType);
        }

        dest.writeDouble(Price);
        dest.writeString(ReservationDeadline);
        dest.writeString(CancellationDeadline);
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public double getDiscount() {
        return Discount;
    }

    public void setDiscount(double discount) {
        Discount = discount;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public ArrayList<Integer> getAssetIdList() {
        return AssetIdList;
    }

    public void setAssetIdList(ArrayList<Integer> assetIdList) {
        AssetIdList = assetIdList;
    }

    public ArrayList<String> getEventTypes() {
        return EventTypes;
    }

    public void setEventTypes(ArrayList<String> eventTypes) {
        EventTypes = eventTypes;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public String getReservationDeadline() {
        return ReservationDeadline;
    }

    public void setReservationDeadline(String reservationDeadline) {
        ReservationDeadline = reservationDeadline;
    }

    public String getCancellationDeadline() {
        return CancellationDeadline;
    }

    public void setCancellationDeadline(String cancellationDeadline) {
        CancellationDeadline = cancellationDeadline;
    }
}
