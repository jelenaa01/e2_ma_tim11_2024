package com.example.eventplanner.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.EditCategoryActivity;
import com.example.eventplanner.activities.EditProductActivity;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Product;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    private List<Category> categories;
    private Context context;

    public CategoryAdapter(List<Category> categories, Context context) {
        this.categories = categories;
        this.context = context;
    }

    @NonNull
    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_category_card, parent, false);
        return new CategoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.ViewHolder holder, int position) {
        prepareTextViews(holder, position);

        holder.editCategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Category selectedCategory = (Category) categories.get(holder.getAdapterPosition());
                Intent intent = new Intent(context, EditCategoryActivity.class);
                intent.putExtra("category", (Category)selectedCategory);
                context.startActivity(intent);
            }
        });
    }

    private void prepareTextViews(CategoryAdapter.ViewHolder holder, int position){
        Category category = categories.get(position);
        holder.categoryNameTextView.setText("Title: " + category.getName());
        holder.categoryDescriptionTextView.setText("Description: " + category.getDescription());
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView categoryNameTextView;
        TextView categoryDescriptionTextView;
        Button editCategoryButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryNameTextView = itemView.findViewById(R.id.categoryNameTextView);
            categoryDescriptionTextView = itemView.findViewById(R.id.categoryDescriptionTextView);
            editCategoryButton = itemView.findViewById(R.id.editCategoryButton);
        }

        public void bind(Category category) {
            categoryNameTextView.setText(category.getName());
            categoryDescriptionTextView.setText(category.getDescription());
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }
}
