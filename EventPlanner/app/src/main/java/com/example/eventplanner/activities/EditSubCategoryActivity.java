package com.example.eventplanner.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.SubCategory;

public class EditSubCategoryActivity extends AppCompatActivity {

    private EditText editSubCategoryName;
    private EditText editSubCategoryDescription;
    private EditText editSubCategoryCategoryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_edit_sub_category);

        editSubCategoryName = findViewById(R.id.editSubCategoryName);
        editSubCategoryDescription = findViewById(R.id.editSubCategoryDescription);
        editSubCategoryCategoryId = findViewById(R.id.editSubCategoryCategoryId);

        SubCategory subCategory = getIntent().getParcelableExtra("subCategory");
        if (subCategory == null){
            return;
        }

        editSubCategoryName.setText(subCategory.getName());
        editSubCategoryDescription.setText(subCategory.getDescription());
        editSubCategoryCategoryId.setText("1");

        Button saveButton = findViewById(R.id.buttonSave);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(); // Close the current activity and go back to the previous one
            }
        });

        Button cancelButton = findViewById(R.id.buttonCancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}