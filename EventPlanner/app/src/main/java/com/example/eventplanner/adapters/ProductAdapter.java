package com.example.eventplanner.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.activities.EditProductActivity;
import com.example.eventplanner.R;
import com.example.eventplanner.model.Product;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    private List<Product> products;
    private Context context;

    public ProductAdapter(List<Product> products, Context context) {
        this.products = products;
        this.context = context;
    }

    @NonNull
    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_product_card, parent, false);
        return new ProductAdapter.ViewHolder(view);
    }

    public void onBindViewHolder(@NonNull ProductAdapter.ViewHolder holder, int position) {
        prepareTextViews(holder, position);

        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Product selectedProduct = (Product) products.get(holder.getAdapterPosition());
                Intent intent = new Intent(context, EditProductActivity.class);
                intent.putExtra("product", (Product)selectedProduct);
                context.startActivity(intent);
            }
        });
    }

    private void prepareTextViews(ProductAdapter.ViewHolder holder, int position){
        Product product = products.get(position);
        holder.titleTextView.setText("Title: " + product.getName());
        holder.descriptionTextView.setText("Description: " + product.getDescription());
        holder.categoryTextView.setText("CategoryId: " + product.getCategoryId());
        holder.subCategoryTextView.setText("SubCategoryId: " + product.getSubCategoryId());

        String temp_available = "available";
        if(!product.isAvailable()) temp_available = "unavailable";
        holder.availableTextView.setText("Availablity: " + temp_available);

        String temp_visible = "visible";
        if(!product.isVisible()) temp_visible = "visible";
        holder.visibleTextView.setText("Visibility: " + temp_visible);

        holder.eventTypesTextView.setText("Event types: " + product.getEventTypes());
        holder.priceTextView.setText("Price: " + product.getPrice());
        holder.discountTextView.setText("Discount: " + product.getDiscount());
        holder.priceWithDiscountTextView.setText("Price with discount: " + product.getPriceWithDiscount());

        if (product.getGallery() != null && !product.getGallery().isEmpty()) {
            String imageUrl = product.getGallery().get(0);
            int imageResourceId = context.getResources().getIdentifier(imageUrl, "drawable", context.getPackageName());
            holder.galleryImageView.setImageResource(imageResourceId);
        } else {
            holder.galleryImageView.setImageResource(R.drawable.photographer1);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleTextView;
        TextView descriptionTextView;
        TextView categoryTextView;
        TextView subCategoryTextView;
        TextView availableTextView;
        TextView visibleTextView;
        TextView eventTypesTextView;
        TextView priceTextView;
        TextView discountTextView;
        TextView priceWithDiscountTextView;
        ImageView galleryImageView;
        Button editButton;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.titleTextView);
            descriptionTextView = itemView.findViewById(R.id.descriptionTextView);
            categoryTextView = itemView.findViewById(R.id.categoryTextView);
            subCategoryTextView = itemView.findViewById(R.id.subCategoryTextView);
            availableTextView = itemView.findViewById(R.id.availableTextView);
            visibleTextView = itemView.findViewById(R.id.visibleTextView);
            eventTypesTextView = itemView.findViewById(R.id.eventTypesTextView);
            priceTextView = itemView.findViewById(R.id.priceTextView);
            discountTextView = itemView.findViewById(R.id.discountTextView);
            priceWithDiscountTextView = itemView.findViewById(R.id.priceWithDiscountTextView);
            galleryImageView = itemView.findViewById(R.id.galleryImageView);
            editButton = itemView.findViewById(R.id.editButton);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return products.size();
    }
}
