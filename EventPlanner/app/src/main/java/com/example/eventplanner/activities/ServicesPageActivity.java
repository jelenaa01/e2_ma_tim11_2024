package com.example.eventplanner.activities;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.ProductAdapter;
import com.example.eventplanner.adapters.ServiceAdapter;
import com.example.eventplanner.model.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ServicesPageActivity extends AppCompatActivity {
    private List<Service> services = new ArrayList<>();
    private ServiceAdapter adapter;

    public ServicesPageActivity() {}

    public ServicesPageActivity(List<Service> services, ServiceAdapter adapter) {
        this.services = services;
        this.adapter = adapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_services_page);

        prepareServices();

        populateFiltrationSpinner();

        RecyclerView recyclerView = findViewById(R.id.serviceRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new ServiceAdapter(services, this);
        recyclerView.setAdapter(adapter);
    }

    private void populateFiltrationSpinner(){
        Spinner spinner = findViewById(R.id.spinner_categories);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.filtration_categories_array, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
    }

    private void prepareServices(){
        services.add(getService1());
        services.add(getService2());
    }

    private static Service getService1() {
        // Creating an example Service object
        return new Service(
                1,
                1,
                1,
                "Photography Service",
                "Professional photography service for events.",
                new ArrayList<>(Arrays.asList("photographer2", "cat")), // Gallery
                new ArrayList<>(Arrays.asList("Svadba", "Krstenje")), // Event types
                true, // Available
                true, // Visible
                "Wedding photography with high-resolution digital images.",
                100, // Price per hour
                3, // Duration in hours
                300, // Total price
                0, // Discount
                "City Hall, New York", // Location
                new ArrayList<>(), // People
                "2024-05-01", // Reservation deadline
                "2024-04-15", // Cancellation deadline
                Service.AcceptanceType.AUTOMATIC // Acceptance type
        );
    }

    private static Service getService2() {
        // Creating another example Service object
        return new Service(
                2,
                2,
                1,
                "Catering Service",
                "Professional catering service for events.",
                new ArrayList<>(Arrays.asList("cat", "photographer1")), // Gallery
                new ArrayList<>(Arrays.asList("Svadba", "Krstenje")), // Event types
                true, // Available
                true, // Visible
                "Buffet-style catering with a variety of dishes.",
                150, // Price per hour
                4, // Duration in hours
                600, // Total price
                50, // Discount
                "Downtown Banquet Hall, Chicago", // Location
                new ArrayList<>(), // People
                "2024-06-01", // Reservation deadline
                "2024-05-15", // Cancellation deadline
                Service.AcceptanceType.MANUAL // Acceptance type
        );
    }
}

