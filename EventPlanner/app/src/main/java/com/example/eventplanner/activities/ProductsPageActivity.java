package com.example.eventplanner.activities;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.ProductAdapter;
import com.example.eventplanner.model.Product;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class ProductsPageActivity extends AppCompatActivity {
    private List<Product> products = new ArrayList<Product>();
    private ProductAdapter adapter;

    public ProductsPageActivity() {}

    public ProductsPageActivity(List<Product> products, ProductAdapter adapter) {
        this.products = products;
        this.adapter = adapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_products_page);

        prepareProducts();

        populateFiltrationSpinner();

        RecyclerView recyclerView = findViewById(R.id.productRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new ProductAdapter(products, this);
        recyclerView.setAdapter(adapter);
    }

    private void populateFiltrationSpinner(){
        Spinner spinner = findViewById(R.id.spinner_categories);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.filtration_categories_array, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
    }

    private void prepareProducts(){
        Product product1 = new Product(1, 2, 3, "Product1", "Product Description",
                new ArrayList<>(Arrays.asList("photographer2", "cat")),
                new ArrayList<>(Arrays.asList("Svadba", "Krstenje")),
                true, true, 100.0, 10.0, 100.0 - (100.0 * 10.0 / 100.0));

        Product product2 = new Product(2, 2, 4, "Product2", "Product Description",
                new ArrayList<>(Arrays.asList("photographer1", "photographer2")),
                new ArrayList<>(Arrays.asList("Event Type 1", "Event Type 2")),
                true, true, 100.0, 10.0, 100.0 - (100.0 * 10.0 / 100.0));

        products.add(product1);
        products.add(product2);
    }
}