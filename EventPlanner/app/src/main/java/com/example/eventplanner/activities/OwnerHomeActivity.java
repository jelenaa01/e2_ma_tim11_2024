package com.example.eventplanner.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;


import com.example.eventplanner.R;
import com.example.eventplanner.databinding.ActivityOwnerHomeBinding;
import com.google.android.material.navigation.NavigationView;

import java.util.HashSet;
import java.util.Set;

public class OwnerHomeActivity extends AppCompatActivity {
    private ActivityOwnerHomeBinding binding;
    private AppBarConfiguration mAppBarConfiguration;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private NavController navController;
    private Toolbar toolbar;
    private ActionBar actionBar;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private Set<Integer> topLevelDestinations = new HashSet<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityOwnerHomeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        drawer = binding.drawerLayout;
        navigationView = binding.navView;
        toolbar = binding.activityOwnerHomeBase.toolbar;
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_nav_content_main);
        if (navHostFragment != null) {
            navController = navHostFragment.getNavController();
        }

        navController.addOnDestinationChangedListener((navController, navDestination, bundle) -> {
            Log.i("ShopApp", "Destination Changed");
            int id = navDestination.getId();
            boolean isTopLevelDestination = topLevelDestinations.contains(id);
            if (!isTopLevelDestination) {
                switch (id) {
                    case R.id.nav_profile:
                        Toast.makeText(OwnerHomeActivity.this, "Profile", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nav_employee:
                        Toast.makeText(OwnerHomeActivity.this, "Employees", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nav_reg_employee:
                        Toast.makeText(OwnerHomeActivity.this, "Employee registration", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nav_work_time:
                        Toast.makeText(OwnerHomeActivity.this, "Employee work time", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nav_calendar:
                        Toast.makeText(OwnerHomeActivity.this, "Employee availability calendar", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nav_account:
                        Toast.makeText(OwnerHomeActivity.this, "Employee account management", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nav_logout:
                        Toast.makeText(OwnerHomeActivity.this, "Logout", Toast.LENGTH_SHORT).show();
                        break;
                }
                drawer.closeDrawers();
            }
        });

        mAppBarConfiguration = new AppBarConfiguration
                .Builder(R.id.nav_profile, R.id.nav_employee, R.id.nav_reg_employee, R.id.nav_work_time, R.id.nav_calendar, R.id.nav_account, R.id.nav_logout)
                .setOpenableLayout(drawer)
                .build();

        NavigationUI.setupWithNavController(navigationView, navController);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return NavigationUI.onNavDestinationSelected(item, navController) || super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(navController, mAppBarConfiguration) || super.onSupportNavigateUp();
    }
}