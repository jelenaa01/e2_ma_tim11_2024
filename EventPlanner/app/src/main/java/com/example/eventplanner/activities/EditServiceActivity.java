package com.example.eventplanner.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Service;

public class EditServiceActivity extends AppCompatActivity {
    private EditText editTextName;
    private EditText editTextDescription;
    private EditText editTextLocation;
    private EditText editTextPricePerHour;
    private EditText editTextTotalPrice;
    private EditText editTextDiscount;
    private EditText editTextReservationDeadline;
    private EditText editTextCancellationDeadline;
    private EditText editTextEventTypes;
    private EditText editTextSpecifics;
    private EditText editTextDuration;
    private EditText editTextPeople;
    private Spinner spinnerAcceptance;
    private CheckBox checkBoxAvailability;
    private CheckBox checkBoxVisibility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_edit_service);

        editTextName = findViewById(R.id.editTextName);
        editTextDescription = findViewById(R.id.editTextDescription);
        editTextLocation = findViewById(R.id.editTextLocation);
        editTextPricePerHour = findViewById(R.id.editTextPricePerHour);
        editTextTotalPrice = findViewById(R.id.editTextTotalPrice);
        editTextDiscount = findViewById(R.id.editTextDiscount);
        editTextReservationDeadline = findViewById(R.id.editTextReservationDeadline);
        editTextCancellationDeadline = findViewById(R.id.editTextCancellationDeadline);
        editTextEventTypes = findViewById(R.id.editTextEventTypes);
        editTextSpecifics = findViewById(R.id.editTextSpecifics);
        editTextDuration = findViewById(R.id.editTextDuration);
        editTextPeople = findViewById(R.id.editTextPeople);
        spinnerAcceptance = findViewById(R.id.spinnerAcceptance);
        checkBoxAvailability = findViewById(R.id.checkBoxAvailability);
        checkBoxVisibility = findViewById(R.id.checkBoxVisibility);

        Service service = getIntent().getParcelableExtra("service");
        if (service == null) {
            return;
        }

        editTextName.setText(service.getName());
        editTextDescription.setText(service.getDescription());
        editTextLocation.setText(service.getLocation());
        editTextPricePerHour.setText(String.valueOf(service.getPricePerHour()));
        editTextTotalPrice.setText(String.valueOf(service.getTotalPrice()));
        editTextDiscount.setText(String.valueOf(service.getDiscount()));
        editTextReservationDeadline.setText(service.getReservationDeadline());
        editTextCancellationDeadline.setText(service.getCancellationDeadline());
        editTextEventTypes.setText("KRSTENJE, SVADBA");
        editTextSpecifics.setText(service.getSpecifics());
        editTextDuration.setText(String.valueOf(service.getDuration()));
        editTextPeople.setText("Milos, Dejan");
        spinnerAcceptance.setSelection(service.getAcceptance().ordinal());
        checkBoxAvailability.setChecked(service.isAvailable());
        checkBoxVisibility.setChecked(service.isVisible());

        Button saveButton = findViewById(R.id.buttonSave);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(); // Close the current activity and go back to the previous one
            }
        });

        Button cancelButton = findViewById(R.id.buttonCancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}