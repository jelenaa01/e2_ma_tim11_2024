package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.Objects;

public class Category implements Parcelable {
    private int Id;
    private String Name;
    private String Description;

    // categories: HOSPITALITY, ACCOMMODATION, PHOTOGRAPHY, ENTERTAINMENT, DECORATION,
    // WARDROBE, BEAUTY, PLANNING, STATIONERY, LOGISTICS

    public Category(int id, String name, String description) {
        Id = id;
        Name = name;
        Description = description;
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public String getDescription() {
        return Description;
    }

    public void setId(int id) {
        Id = id;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setDescription(String description) {
        Description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return Id == category.Id && Objects.equals(Name, category.Name) && Objects.equals(Description, category.Description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id, Name, Description);
    }

    @Override
    public String toString() {
        return "Category{" +
                "Id=" + Id +
                ", Name='" + Name + '\'' +
                ", Description='" + Description + '\'' +
                '}';
    }

    public Category(Parcel in) {
        Name = in.readString();
        Description = in.readString();
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Name);
        dest.writeString(Description);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
