package com.example.eventplanner.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Product;

public class EditCategoryActivity extends AppCompatActivity {

    private EditText editCategoryName;
    private EditText editCategoryDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_edit_category);

        editCategoryName = findViewById(R.id.editCategoryName);
        editCategoryDescription = findViewById(R.id.editCategoryDescription);

        Category category = getIntent().getParcelableExtra("category");
        if (category == null){
            return;
        }

        editCategoryName.setText(category.getName());
        editCategoryDescription.setText(category.getDescription());

        Button saveButton = findViewById(R.id.buttonSave);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(); // Close the current activity and go back to the previous one
            }
        });

        Button cancelButton = findViewById(R.id.buttonCancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}