package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.UserListAdapter;
import com.example.eventplanner.databinding.FragmentAccountManagementBinding;
import com.example.eventplanner.databinding.FragmentEmployeeBinding;
import com.example.eventplanner.model.User;

import java.util.ArrayList;

public class AccountManagementFragment extends Fragment {

    private FragmentAccountManagementBinding binding;
    public static ArrayList<User> users = new ArrayList<User>();

    public static EmployeeFragment newInstance() {
        return new EmployeeFragment();
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentAccountManagementBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        prepareUsersList(users);
        ListView listView = root.findViewById(R.id.listView);
        UserListAdapter adapter = new UserListAdapter(getContext(), users);
        listView.setAdapter(adapter);

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void prepareUsersList(ArrayList<User> users){
        users.add(new User(1L, "Nikola", "Nikolic", "Brace Ribnikar 15, Novi Sad", "0621144623", "nikola@gmail.com", "nikola", User.Role.employee));
        users.add(new User(2L, "Mia", "Markovic", "Dusana Danilovica 8, Novi Sad", "0647894561", "markovic@gmail.com", "mia", User.Role.employee));
        users.add(new User(3L, "Marko", "Ilic", "Janka Cmelika 55, Novi Sad", "0613456789", "marko.ilic@yahoo.com", "mare", User.Role.employee));
        users.add(new User(4L, "Ana", "Petric", "Bulevar Jovana Ducica 27, Novi Sad", "0601234567", "petric@gmail.com", "ana", User.Role.employee));
        users.add(new User(5L, "Petar", "Aleksic", "Sekspirova 10, Novi Sad", "0639876543", "aleksic.pera@gmail.com", "pera", User.Role.employee));
    }
}