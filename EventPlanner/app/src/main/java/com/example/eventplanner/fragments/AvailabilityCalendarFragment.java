package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.EventListAdapter;
import com.example.eventplanner.databinding.FragmentAvailabilityCalendarBinding;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.User;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;

public class AvailabilityCalendarFragment extends Fragment {

   private FragmentAvailabilityCalendarBinding binding;
    public static ArrayList<Event> events = new ArrayList<Event>();
    public static ArrayList<User> users = new ArrayList<User>();

    public static AvailabilityCalendarFragment newInstance() {
        return new AvailabilityCalendarFragment();
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentAvailabilityCalendarBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        prepareUsersList(users);
        prepareEventsList(events, users);
        ListView listView = root.findViewById(R.id.eventsListView);
        EventListAdapter adapter = new EventListAdapter(getContext(), events);
        listView.setAdapter(adapter);

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void prepareUsersList(ArrayList<User> users) {
        users.add(new User(3L, "Marko", "Ilic", "Janka Cmelika 55, Novi Sad", "0613456789", "marko.ilic@yahoo.com", "mare", User.Role.employee));
    }
    private void prepareEventsList(ArrayList<Event> events, ArrayList<User> users) {
        User employee = users.get(0);

        Date date1 = Date.valueOf("2024-04-05");
        Time startTime1 = Time.valueOf("09:00:00");
        Time endTime1 = Time.valueOf("11:00:00");

        Date date2 = Date.valueOf("2024-04-06");
        Time startTime2 = Time.valueOf("14:00:00");
        Time endTime2 = Time.valueOf("16:00:00");

        Date date3 = Date.valueOf("2024-04-07");
        Time startTime3 = Time.valueOf("10:00:00");
        Time endTime3 = Time.valueOf("13:00:00");

        Date date4 = Date.valueOf("2024-04-08");
        Time startTime4 = Time.valueOf("17:00:00");
        Time endTime4 = Time.valueOf("20:00:00");

        events.add(new Event(1L, "Team Meeting", date1, startTime1, endTime1, Event.Type.reserved, employee));
        events.add(new Event(2L, "Client Presentation",  date2, startTime2, endTime2, Event.Type.reserved, employee));
        events.add(new Event(3L, "Training Session",  date3, startTime3, endTime3, Event.Type.reserved, employee));
        events.add(new Event(4L, "Team Lunch",  date4, startTime4, endTime4, Event.Type.reserved, employee));
    }

}