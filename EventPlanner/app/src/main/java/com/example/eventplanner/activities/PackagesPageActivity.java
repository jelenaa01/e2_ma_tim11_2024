package com.example.eventplanner.activities;

import android.os.Bundle;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.PackageAdapter;
import com.example.eventplanner.model.Package;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PackagesPageActivity extends AppCompatActivity {

    private List<Package> packages = new ArrayList<>();
    private PackageAdapter adapter;

    public PackagesPageActivity() {}

    public PackagesPageActivity(List<Package> packages, PackageAdapter adapter) {
        this.packages = packages;
        this.adapter = adapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_packages_page);

        preparePackages();

        RecyclerView recyclerView = findViewById(R.id.packageRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new PackageAdapter(packages, this);
        recyclerView.setAdapter(adapter);
    }

    private void preparePackages() {
        Package package1 = new Package(1, "Package1", "Package Description", 10.0,
                true, true, new ArrayList<>(Arrays.asList(1, 2)),
                new ArrayList<>(Arrays.asList("Svadba", "Krstenje")),
                100.0, "Reservation Deadline 1", "Cancellation Deadline 1");

        Package package2 = new Package(2, "Package2", "Package Description", 10.0,
                true, true, new ArrayList<>(Arrays.asList(2, 3)),
                new ArrayList<>(Arrays.asList("Event Type 1", "Event Type 2")),
                100.0, "Reservation Deadline 2", "Cancellation Deadline 2");

        packages.add(package1);
        packages.add(package2);
    }

}