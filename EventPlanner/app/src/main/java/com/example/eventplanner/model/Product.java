package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Product extends Asset implements Parcelable {

    private double Price;
    private double Discount;
    private double PriceWithDiscount;

    public Product(int id, int categoryId, int subCategoryId, String name, String description, ArrayList<String> gallery, ArrayList<String> eventTypes, boolean isAvailable, boolean isVisible, double price, double discount, double priceWithDiscount) {
        super(id, categoryId, subCategoryId, name, description, gallery, eventTypes, isAvailable, isVisible);
        Price = price;
        Discount = discount;
        PriceWithDiscount = priceWithDiscount;
    }

    public Product(Parcel in) {
        super(in);
        Price = in.readDouble();
        Discount = in.readDouble();
        PriceWithDiscount = in.readDouble();
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags); // Write fields from parent class first
        dest.writeDouble(Price);
        dest.writeDouble(Discount);
        dest.writeDouble(PriceWithDiscount);
    }

    public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        public Product[] newArray(int size) {
            return new Product[size];
        }
    };


    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public double getDiscount() {
        return Discount;
    }

    public void setDiscount(double discount) {
        Discount = discount;
    }

    public double getPriceWithDiscount() {
        return PriceWithDiscount;
    }

    public void setPriceWithDiscount(double priceWithDiscount) {
        PriceWithDiscount = priceWithDiscount;
    }
}
