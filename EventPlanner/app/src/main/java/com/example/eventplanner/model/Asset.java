package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class Asset implements Parcelable {
    protected int Id;
    protected int CategoryId;
    protected int SubCategoryId;
    protected String Name;
    protected String Description;
    protected ArrayList<String> Gallery;
    protected ArrayList<String> EventTypes;
    protected boolean isAvailable;
    protected boolean isVisible;

    public Asset(int id, int categoryId, int subCategoryId, String name, String description, ArrayList<String> gallery, ArrayList<String> eventTypes, boolean isAvailable, boolean isVisible) {
        Id = id;
        CategoryId = categoryId;
        SubCategoryId = subCategoryId;
        Name = name;
        Description = description;
        Gallery = gallery;
        EventTypes = eventTypes;
        this.isAvailable = isAvailable;
        this.isVisible = isVisible;
    }

    protected Asset(Parcel in) {
        Id = in.readInt();
        CategoryId = in.readInt();
        SubCategoryId = in.readInt();
        Name = in.readString();
        Description = in.readString();
        Gallery = in.createStringArrayList();
        EventTypes = in.createStringArrayList();
        isAvailable = in.readByte() != 0;
        isVisible = in.readByte() != 0;
    }

    public static final Creator<Asset> CREATOR = new Creator<Asset>() {
        @Override
        public Asset createFromParcel(Parcel in) {
            return new Asset(in);
        }

        @Override
        public Asset[] newArray(int size) {
            return new Asset[size];
        }
    };

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeInt(CategoryId);
        dest.writeInt(SubCategoryId);
        dest.writeString(Name);
        dest.writeString(Description);
        dest.writeStringList(Gallery);
        dest.writeStringList(EventTypes);
        dest.writeByte((byte) (isAvailable ? 1 : 0));
        dest.writeByte((byte) (isVisible ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    // Getter and setter methods
    // (omitted for brevity)

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int categoryId) {
        CategoryId = categoryId;
    }

    public int getSubCategoryId() {
        return SubCategoryId;
    }

    public void setSubCategoryId(int subCategoryId) {
        SubCategoryId = subCategoryId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public ArrayList<String> getGallery() {
        return Gallery;
    }

    public void setGallery(ArrayList<String> gallery) {
        Gallery = gallery;
    }

    public ArrayList<String> getEventTypes() {
        return EventTypes;
    }

    public void setEventTypes(ArrayList<String> eventTypes) {
        EventTypes = eventTypes;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }
}
